package rza.BPFco.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QTag is a Querydsl query type for Tag
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QTag extends EntityPathBase<Tag> {

    private static final long serialVersionUID = 1238478102L;

    public static final QTag tag = new QTag("tag");

    public final BooleanPath active = createBoolean("active");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final SetPath<Portfoy, QPortfoy> portfoys = this.<Portfoy, QPortfoy>createSet("portfoys", Portfoy.class, QPortfoy.class, PathInits.DIRECT2);

    public final StringPath title = createString("title");

    public final NumberPath<Integer> type = createNumber("type", Integer.class);

    public QTag(String variable) {
        super(Tag.class, forVariable(variable));
    }

    public QTag(Path<? extends Tag> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTag(PathMetadata<?> metadata) {
        super(Tag.class, metadata);
    }

}

