package rza.BPFco.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QAccount is a Querydsl query type for Account
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QAccount extends EntityPathBase<Account> {

    private static final long serialVersionUID = -1766303319L;

    public static final QAccount account = new QAccount("account");

    public final BooleanPath active = createBoolean("active");

    public final SetPath<Ajanda, QAjanda> ajandas = this.<Ajanda, QAjanda>createSet("ajandas", Ajanda.class, QAjanda.class, PathInits.DIRECT2);

    public final ListPath<rza.BPFco.model.portfoys.Chase, rza.BPFco.model.portfoys.QChase> chase = this.<rza.BPFco.model.portfoys.Chase, rza.BPFco.model.portfoys.QChase>createList("chase", rza.BPFco.model.portfoys.Chase.class, rza.BPFco.model.portfoys.QChase.class, PathInits.DIRECT2);

    public final SetPath<Contract, QContract> contract = this.<Contract, QContract>createSet("contract", Contract.class, QContract.class, PathInits.DIRECT2);

    public final DateTimePath<java.util.Date> createDate = createDateTime("createDate", java.util.Date.class);

    public final SetPath<rza.BPFco.model.portfoys.Customer, rza.BPFco.model.portfoys.QCustomer> customer = this.<rza.BPFco.model.portfoys.Customer, rza.BPFco.model.portfoys.QCustomer>createSet("customer", rza.BPFco.model.portfoys.Customer.class, rza.BPFco.model.portfoys.QCustomer.class, PathInits.DIRECT2);

    public final SetPath<rza.BPFco.model.portfoys.Document, rza.BPFco.model.portfoys.QDocument> document = this.<rza.BPFco.model.portfoys.Document, rza.BPFco.model.portfoys.QDocument>createSet("document", rza.BPFco.model.portfoys.Document.class, rza.BPFco.model.portfoys.QDocument.class, PathInits.DIRECT2);

    public final StringPath email = createString("email");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final BooleanPath isAdmin = createBoolean("isAdmin");

    public final BooleanPath isCallAr = createBoolean("isCallAr");

    public final BooleanPath isCallFa = createBoolean("isCallFa");

    public final BooleanPath isWatcher = createBoolean("isWatcher");

    public final BooleanPath language = createBoolean("language");

    public final DateTimePath<java.util.Date> lastsign = createDateTime("lastsign", java.util.Date.class);

    public final StringPath name = createString("name");

    public final StringPath password = createString("password");

    public final StringPath role = createString("role");

    public final SetPath<rza.BPFco.model.portfoys.SosyalMedya, rza.BPFco.model.portfoys.QSosyalMedya> sosyalMedya = this.<rza.BPFco.model.portfoys.SosyalMedya, rza.BPFco.model.portfoys.QSosyalMedya>createSet("sosyalMedya", rza.BPFco.model.portfoys.SosyalMedya.class, rza.BPFco.model.portfoys.QSosyalMedya.class, PathInits.DIRECT2);

    public final SetPath<UserRole, QUserRole> userroles = this.<UserRole, QUserRole>createSet("userroles", UserRole.class, QUserRole.class, PathInits.DIRECT2);

    public QAccount(String variable) {
        super(Account.class, forVariable(variable));
    }

    public QAccount(Path<? extends Account> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAccount(PathMetadata<?> metadata) {
        super(Account.class, metadata);
    }

}

