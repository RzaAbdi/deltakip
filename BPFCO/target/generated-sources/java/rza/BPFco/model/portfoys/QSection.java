package rza.BPFco.model.portfoys;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QSection is a Querydsl query type for Section
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QSection extends EntityPathBase<Section> {

    private static final long serialVersionUID = -830646177L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QSection section = new QSection("section");

    public final DateTimePath<java.util.Date> create_date = createDateTime("create_date", java.util.Date.class);

    public final SetPath<Document, QDocument> document = this.<Document, QDocument>createSet("document", Document.class, QDocument.class, PathInits.DIRECT2);

    public final DateTimePath<java.util.Date> edit_date = createDateTime("edit_date", java.util.Date.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final QKat kat;

    public final rza.BPFco.model.QPortfoy portfoy;

    public final StringPath title = createString("title");

    public QSection(String variable) {
        this(Section.class, forVariable(variable), INITS);
    }

    public QSection(Path<? extends Section> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QSection(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QSection(PathMetadata<?> metadata, PathInits inits) {
        this(Section.class, metadata, inits);
    }

    public QSection(Class<? extends Section> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.kat = inits.isInitialized("kat") ? new QKat(forProperty("kat"), inits.get("kat")) : null;
        this.portfoy = inits.isInitialized("portfoy") ? new rza.BPFco.model.QPortfoy(forProperty("portfoy"), inits.get("portfoy")) : null;
    }

}

