package rza.BPFco.model.portfoys;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QInstallment is a Querydsl query type for Installment
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QInstallment extends EntityPathBase<Installment> {

    private static final long serialVersionUID = -2097184269L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QInstallment installment = new QInstallment("installment");

    public final BooleanPath active = createBoolean("active");

    public final StringPath amount = createString("amount");

    public final rza.BPFco.model.QContract contract;

    public final NumberPath<Integer> currency = createNumber("currency", Integer.class);

    public final StringPath description = createString("description");

    public final SetPath<Document, QDocument> documents = this.<Document, QDocument>createSet("documents", Document.class, QDocument.class, PathInits.DIRECT2);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final DateTimePath<java.util.Date> issueDate = createDateTime("issueDate", java.util.Date.class);

    public final SetPath<Mes, QMes> mes = this.<Mes, QMes>createSet("mes", Mes.class, QMes.class, PathInits.DIRECT2);

    public final BooleanPath paid = createBoolean("paid");

    public final BooleanPath partial = createBoolean("partial");

    public final StringPath partial_amount = createString("partial_amount");

    public final DateTimePath<java.util.Date> paymantDate = createDateTime("paymantDate", java.util.Date.class);

    public final BooleanPath reminding = createBoolean("reminding");

    public final BooleanPath thanks = createBoolean("thanks");

    public final NumberPath<Integer> type = createNumber("type", Integer.class);

    public final BooleanPath warning = createBoolean("warning");

    public QInstallment(String variable) {
        this(Installment.class, forVariable(variable), INITS);
    }

    public QInstallment(Path<? extends Installment> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QInstallment(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QInstallment(PathMetadata<?> metadata, PathInits inits) {
        this(Installment.class, metadata, inits);
    }

    public QInstallment(Class<? extends Installment> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.contract = inits.isInitialized("contract") ? new rza.BPFco.model.QContract(forProperty("contract")) : null;
    }

}

