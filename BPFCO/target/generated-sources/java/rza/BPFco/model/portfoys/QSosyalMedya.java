package rza.BPFco.model.portfoys;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QSosyalMedya is a Querydsl query type for SosyalMedya
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QSosyalMedya extends EntityPathBase<SosyalMedya> {

    private static final long serialVersionUID = 1306874081L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QSosyalMedya sosyalMedya = new QSosyalMedya("sosyalMedya");

    public final SetPath<rza.BPFco.model.Account, rza.BPFco.model.QAccount> account = this.<rza.BPFco.model.Account, rza.BPFco.model.QAccount>createSet("account", rza.BPFco.model.Account.class, rza.BPFco.model.QAccount.class, PathInits.DIRECT2);

    public final SetPath<rza.BPFco.model.Ajanda, rza.BPFco.model.QAjanda> ajandas = this.<rza.BPFco.model.Ajanda, rza.BPFco.model.QAjanda>createSet("ajandas", rza.BPFco.model.Ajanda.class, rza.BPFco.model.QAjanda.class, PathInits.DIRECT2);

    public final QChase chase;

    public final DateTimePath<java.util.Date> createDate = createDateTime("createDate", java.util.Date.class);

    public final StringPath currentPlatform = createString("currentPlatform");

    public final StringPath dsc = createString("dsc");

    public final StringPath email = createString("email");

    public final StringPath firstInfo = createString("firstInfo");

    public final StringPath firstPlatform = createString("firstPlatform");

    public final BooleanPath follow = createBoolean("follow");

    public final StringPath gsm = createString("gsm");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Integer> language = createNumber("language", Integer.class);

    public final DateTimePath<java.util.Date> lastDate = createDateTime("lastDate", java.util.Date.class);

    public final StringPath lastUpdate = createString("lastUpdate");

    public final StringPath name = createString("name");

    public final StringPath nation = createString("nation");

    public final NumberPath<Integer> priority = createNumber("priority", Integer.class);

    public final SetPath<Reply, QReply> reply = this.<Reply, QReply>createSet("reply", Reply.class, QReply.class, PathInits.DIRECT2);

    public final NumberPath<Integer> type = createNumber("type", Integer.class);

    public QSosyalMedya(String variable) {
        this(SosyalMedya.class, forVariable(variable), INITS);
    }

    public QSosyalMedya(Path<? extends SosyalMedya> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QSosyalMedya(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QSosyalMedya(PathMetadata<?> metadata, PathInits inits) {
        this(SosyalMedya.class, metadata, inits);
    }

    public QSosyalMedya(Class<? extends SosyalMedya> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.chase = inits.isInitialized("chase") ? new QChase(forProperty("chase"), inits.get("chase")) : null;
    }

}

