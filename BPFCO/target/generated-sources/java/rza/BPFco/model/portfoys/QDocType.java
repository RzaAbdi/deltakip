package rza.BPFco.model.portfoys;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QDocType is a Querydsl query type for DocType
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QDocType extends EntityPathBase<DocType> {

    private static final long serialVersionUID = -972945908L;

    public static final QDocType docType = new QDocType("docType");

    public final BooleanPath controlled = createBoolean("controlled");

    public final SetPath<Document, QDocument> document = this.<Document, QDocument>createSet("document", Document.class, QDocument.class, PathInits.DIRECT2);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath title = createString("title");

    public QDocType(String variable) {
        super(DocType.class, forVariable(variable));
    }

    public QDocType(Path<? extends DocType> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDocType(PathMetadata<?> metadata) {
        super(DocType.class, metadata);
    }

}

