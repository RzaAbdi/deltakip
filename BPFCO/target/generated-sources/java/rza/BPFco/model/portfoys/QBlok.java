package rza.BPFco.model.portfoys;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QBlok is a Querydsl query type for Blok
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QBlok extends EntityPathBase<Blok> {

    private static final long serialVersionUID = -808888212L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QBlok blok = new QBlok("blok");

    public final DateTimePath<java.util.Date> create_date = createDateTime("create_date", java.util.Date.class);

    public final DateTimePath<java.util.Date> edit_date = createDateTime("edit_date", java.util.Date.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final ListPath<Kat, QKat> kats = this.<Kat, QKat>createList("kats", Kat.class, QKat.class, PathInits.DIRECT2);

    public final QProje proje;

    public final StringPath title = createString("title");

    public QBlok(String variable) {
        this(Blok.class, forVariable(variable), INITS);
    }

    public QBlok(Path<? extends Blok> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QBlok(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QBlok(PathMetadata<?> metadata, PathInits inits) {
        this(Blok.class, metadata, inits);
    }

    public QBlok(Class<? extends Blok> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.proje = inits.isInitialized("proje") ? new QProje(forProperty("proje")) : null;
    }

}

