package rza.BPFco.model.portfoys;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QMes is a Querydsl query type for Mes
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QMes extends EntityPathBase<Mes> {

    private static final long serialVersionUID = -1550103467L;

    public static final QMes mes = new QMes("mes");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final SetPath<Installment, QInstallment> installment = this.<Installment, QInstallment>createSet("installment", Installment.class, QInstallment.class, PathInits.DIRECT2);

    public final SetPath<Reply, QReply> reply = this.<Reply, QReply>createSet("reply", Reply.class, QReply.class, PathInits.DIRECT2);

    public final DateTimePath<java.util.Date> sendDate = createDateTime("sendDate", java.util.Date.class);

    public final StringPath text = createString("text");

    public final NumberPath<Integer> type = createNumber("type", Integer.class);

    public QMes(String variable) {
        super(Mes.class, forVariable(variable));
    }

    public QMes(Path<? extends Mes> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMes(PathMetadata<?> metadata) {
        super(Mes.class, metadata);
    }

}

