package rza.BPFco.model.portfoys;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QProje is a Querydsl query type for Proje
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QProje extends EntityPathBase<Proje> {

    private static final long serialVersionUID = 707377314L;

    public static final QProje proje = new QProje("proje");

    public final StringPath ada = createString("ada");

    public final StringPath address = createString("address");

    public final ListPath<Blok, QBlok> bloks = this.<Blok, QBlok>createList("bloks", Blok.class, QBlok.class, PathInits.DIRECT2);

    public final DateTimePath<java.util.Date> create_date = createDateTime("create_date", java.util.Date.class);

    public final DateTimePath<java.util.Date> edit_date = createDateTime("edit_date", java.util.Date.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath title = createString("title");

    public QProje(String variable) {
        super(Proje.class, forVariable(variable));
    }

    public QProje(Path<? extends Proje> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProje(PathMetadata<?> metadata) {
        super(Proje.class, metadata);
    }

}

