package rza.BPFco.model.portfoys;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QDocument is a Querydsl query type for Document
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QDocument extends EntityPathBase<Document> {

    private static final long serialVersionUID = -66443551L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QDocument document = new QDocument("document");

    public final rza.BPFco.model.QAccount account;

    public final SetPath<Agent, QAgent> agent = this.<Agent, QAgent>createSet("agent", Agent.class, QAgent.class, PathInits.DIRECT2);

    public final SetPath<rza.BPFco.model.Contract, rza.BPFco.model.QContract> contract = this.<rza.BPFco.model.Contract, rza.BPFco.model.QContract>createSet("contract", rza.BPFco.model.Contract.class, rza.BPFco.model.QContract.class, PathInits.DIRECT2);

    public final DateTimePath<java.util.Date> create_date = createDateTime("create_date", java.util.Date.class);

    public final SetPath<Customer, QCustomer> customer = this.<Customer, QCustomer>createSet("customer", Customer.class, QCustomer.class, PathInits.DIRECT2);

    public final SetPath<DocType, QDocType> doctype = this.<DocType, QDocType>createSet("doctype", DocType.class, QDocType.class, PathInits.DIRECT2);

    public final DateTimePath<java.util.Date> editDate = createDateTime("editDate", java.util.Date.class);

    public final StringPath file_path = createString("file_path");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final SetPath<Installment, QInstallment> installment = this.<Installment, QInstallment>createSet("installment", Installment.class, QInstallment.class, PathInits.DIRECT2);

    public final QSection section;

    public final StringPath title = createString("title");

    public final NumberPath<Integer> type = createNumber("type", Integer.class);

    public QDocument(String variable) {
        this(Document.class, forVariable(variable), INITS);
    }

    public QDocument(Path<? extends Document> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QDocument(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QDocument(PathMetadata<?> metadata, PathInits inits) {
        this(Document.class, metadata, inits);
    }

    public QDocument(Class<? extends Document> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.account = inits.isInitialized("account") ? new rza.BPFco.model.QAccount(forProperty("account")) : null;
        this.section = inits.isInitialized("section") ? new QSection(forProperty("section"), inits.get("section")) : null;
    }

}

