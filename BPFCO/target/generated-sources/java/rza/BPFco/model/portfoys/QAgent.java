package rza.BPFco.model.portfoys;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QAgent is a Querydsl query type for Agent
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QAgent extends EntityPathBase<Agent> {

    private static final long serialVersionUID = 693187327L;

    public static final QAgent agent = new QAgent("agent");

    public final StringPath address = createString("address");

    public final StringPath company = createString("company");

    public final SetPath<rza.BPFco.model.Contract, rza.BPFco.model.QContract> contract = this.<rza.BPFco.model.Contract, rza.BPFco.model.QContract>createSet("contract", rza.BPFco.model.Contract.class, rza.BPFco.model.QContract.class, PathInits.DIRECT2);

    public final DateTimePath<java.util.Date> create_date = createDateTime("create_date", java.util.Date.class);

    public final SetPath<Document, QDocument> documents = this.<Document, QDocument>createSet("documents", Document.class, QDocument.class, PathInits.DIRECT2);

    public final DateTimePath<java.util.Date> edit_date = createDateTime("edit_date", java.util.Date.class);

    public final StringPath gsm1 = createString("gsm1");

    public final StringPath gsm2 = createString("gsm2");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath name = createString("name");

    public final StringPath nationality = createString("nationality");

    public final StringPath passport = createString("passport");

    public final NumberPath<Integer> source = createNumber("source", Integer.class);

    public final NumberPath<Long> tax = createNumber("tax", Long.class);

    public final NumberPath<Long> tc = createNumber("tc", Long.class);

    public QAgent(String variable) {
        super(Agent.class, forVariable(variable));
    }

    public QAgent(Path<? extends Agent> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAgent(PathMetadata<?> metadata) {
        super(Agent.class, metadata);
    }

}

