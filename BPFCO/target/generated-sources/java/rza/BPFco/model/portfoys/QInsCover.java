package rza.BPFco.model.portfoys;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QInsCover is a Querydsl query type for InsCover
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QInsCover extends EntityPathBase<InsCover> {

    private static final long serialVersionUID = -417864145L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QInsCover insCover = new QInsCover("insCover");

    public final StringPath amount = createString("amount");

    public final QCover cover;

    public final NumberPath<Integer> currency = createNumber("currency", Integer.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final DateTimePath<java.util.Date> issueDate = createDateTime("issueDate", java.util.Date.class);

    public QInsCover(String variable) {
        this(InsCover.class, forVariable(variable), INITS);
    }

    public QInsCover(Path<? extends InsCover> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QInsCover(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QInsCover(PathMetadata<?> metadata, PathInits inits) {
        this(InsCover.class, metadata, inits);
    }

    public QInsCover(Class<? extends InsCover> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.cover = inits.isInitialized("cover") ? new QCover(forProperty("cover")) : null;
    }

}

