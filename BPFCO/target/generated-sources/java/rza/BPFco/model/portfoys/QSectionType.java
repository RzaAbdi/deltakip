package rza.BPFco.model.portfoys;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QSectionType is a Querydsl query type for SectionType
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QSectionType extends EntityPathBase<SectionType> {

    private static final long serialVersionUID = 628364345L;

    public static final QSectionType sectionType = new QSectionType("sectionType");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath title = createString("title");

    public QSectionType(String variable) {
        super(SectionType.class, forVariable(variable));
    }

    public QSectionType(Path<? extends SectionType> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSectionType(PathMetadata<?> metadata) {
        super(SectionType.class, metadata);
    }

}

