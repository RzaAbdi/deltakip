package rza.BPFco.model.portfoys;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QReply is a Querydsl query type for Reply
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QReply extends EntityPathBase<Reply> {

    private static final long serialVersionUID = 708838116L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QReply reply = new QReply("reply");

    public final SetPath<rza.BPFco.model.Account, rza.BPFco.model.QAccount> account = this.<rza.BPFco.model.Account, rza.BPFco.model.QAccount>createSet("account", rza.BPFco.model.Account.class, rza.BPFco.model.QAccount.class, PathInits.DIRECT2);

    public final rza.BPFco.model.QAjanda ajanda;

    public final DateTimePath<java.util.Date> createDate = createDateTime("createDate", java.util.Date.class);

    public final StringPath dsc = createString("dsc");

    public final BooleanPath hasDoc = createBoolean("hasDoc");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final QMes mes;

    public final StringPath note = createString("note");

    public final QSosyalMedya sosyalmedya;

    public QReply(String variable) {
        this(Reply.class, forVariable(variable), INITS);
    }

    public QReply(Path<? extends Reply> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QReply(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QReply(PathMetadata<?> metadata, PathInits inits) {
        this(Reply.class, metadata, inits);
    }

    public QReply(Class<? extends Reply> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.ajanda = inits.isInitialized("ajanda") ? new rza.BPFco.model.QAjanda(forProperty("ajanda"), inits.get("ajanda")) : null;
        this.mes = inits.isInitialized("mes") ? new QMes(forProperty("mes")) : null;
        this.sosyalmedya = inits.isInitialized("sosyalmedya") ? new QSosyalMedya(forProperty("sosyalmedya"), inits.get("sosyalmedya")) : null;
    }

}

