package rza.BPFco.model.portfoys;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QCustomer is a Querydsl query type for Customer
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QCustomer extends EntityPathBase<Customer> {

    private static final long serialVersionUID = -321989212L;

    public static final QCustomer customer = new QCustomer("customer");

    public final SetPath<rza.BPFco.model.Account, rza.BPFco.model.QAccount> account = this.<rza.BPFco.model.Account, rza.BPFco.model.QAccount>createSet("account", rza.BPFco.model.Account.class, rza.BPFco.model.QAccount.class, PathInits.DIRECT2);

    public final BooleanPath active = createBoolean("active");

    public final StringPath address = createString("address");

    public final DateTimePath<java.util.Date> contarctDate = createDateTime("contarctDate", java.util.Date.class);

    public final SetPath<rza.BPFco.model.Contract, rza.BPFco.model.QContract> contract = this.<rza.BPFco.model.Contract, rza.BPFco.model.QContract>createSet("contract", rza.BPFco.model.Contract.class, rza.BPFco.model.QContract.class, PathInits.DIRECT2);

    public final SetPath<Cover, QCover> cover = this.<Cover, QCover>createSet("cover", Cover.class, QCover.class, PathInits.DIRECT2);

    public final DateTimePath<java.util.Date> create_date = createDateTime("create_date", java.util.Date.class);

    public final SetPath<Document, QDocument> documents = this.<Document, QDocument>createSet("documents", Document.class, QDocument.class, PathInits.DIRECT2);

    public final DateTimePath<java.util.Date> edit_date = createDateTime("edit_date", java.util.Date.class);

    public final StringPath fname = createString("fname");

    public final StringPath gsm1 = createString("gsm1");

    public final StringPath gsm2 = createString("gsm2");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath lname = createString("lname");

    public final StringPath mail = createString("mail");

    public final StringPath nationality = createString("nationality");

    public final StringPath passport = createString("passport");

    public final StringPath portfoy = createString("portfoy");

    public final NumberPath<Integer> source = createNumber("source", Integer.class);

    public final NumberPath<Long> tax = createNumber("tax", Long.class);

    public final StringPath taxOffice = createString("taxOffice");

    public final NumberPath<Long> tc = createNumber("tc", Long.class);

    public QCustomer(String variable) {
        super(Customer.class, forVariable(variable));
    }

    public QCustomer(Path<? extends Customer> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCustomer(PathMetadata<?> metadata) {
        super(Customer.class, metadata);
    }

}

