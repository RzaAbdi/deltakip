package rza.BPFco.model.portfoys;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QCover is a Querydsl query type for Cover
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QCover extends EntityPathBase<Cover> {

    private static final long serialVersionUID = 695288753L;

    public static final QCover cover = new QCover("cover");

    public final SetPath<rza.BPFco.model.Account, rza.BPFco.model.QAccount> account = this.<rza.BPFco.model.Account, rza.BPFco.model.QAccount>createSet("account", rza.BPFco.model.Account.class, rza.BPFco.model.QAccount.class, PathInits.DIRECT2);

    public final StringPath cash = createString("cash");

    public final DateTimePath<java.util.Date> cashDate = createDateTime("cashDate", java.util.Date.class);

    public final DateTimePath<java.util.Date> contractDate = createDateTime("contractDate", java.util.Date.class);

    public final StringPath court = createString("court");

    public final NumberPath<Integer> currency = createNumber("currency", Integer.class);

    public final SetPath<Customer, QCustomer> customer = this.<Customer, QCustomer>createSet("customer", Customer.class, QCustomer.class, PathInits.DIRECT2);

    public final StringPath des = createString("des");

    public final StringPath dsc = createString("dsc");

    public final NumberPath<Integer> fee = createNumber("fee", Integer.class);

    public final StringPath guarantor = createString("guarantor");

    public final StringPath harc = createString("harc");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final SetPath<InsCover, QInsCover> insCover = this.<InsCover, QInsCover>createSet("insCover", InsCover.class, QInsCover.class, PathInits.DIRECT2);

    public final StringPath place = createString("place");

    public final SetPath<rza.BPFco.model.Portfoy, rza.BPFco.model.QPortfoy> portfoy = this.<rza.BPFco.model.Portfoy, rza.BPFco.model.QPortfoy>createSet("portfoy", rza.BPFco.model.Portfoy.class, rza.BPFco.model.QPortfoy.class, PathInits.DIRECT2);

    public final StringPath price = createString("price");

    public final BooleanPath reminde = createBoolean("reminde");

    public final StringPath reminding = createString("reminding");

    public final DateTimePath<java.util.Date> senetDate = createDateTime("senetDate", java.util.Date.class);

    public final NumberPath<Integer> senettype = createNumber("senettype", Integer.class);

    public QCover(String variable) {
        super(Cover.class, forVariable(variable));
    }

    public QCover(Path<? extends Cover> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCover(PathMetadata<?> metadata) {
        super(Cover.class, metadata);
    }

}

