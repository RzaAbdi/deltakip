package rza.BPFco.model.portfoys;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QChase is a Querydsl query type for Chase
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QChase extends EntityPathBase<Chase> {

    private static final long serialVersionUID = 695060456L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QChase chase = new QChase("chase");

    public final rza.BPFco.model.QAccount account;

    public final StringPath color = createString("color");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Integer> language = createNumber("language", Integer.class);

    public final SetPath<SosyalMedya, QSosyalMedya> sosyalmedya = this.<SosyalMedya, QSosyalMedya>createSet("sosyalmedya", SosyalMedya.class, QSosyalMedya.class, PathInits.DIRECT2);

    public final StringPath title = createString("title");

    public QChase(String variable) {
        this(Chase.class, forVariable(variable), INITS);
    }

    public QChase(Path<? extends Chase> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QChase(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QChase(PathMetadata<?> metadata, PathInits inits) {
        this(Chase.class, metadata, inits);
    }

    public QChase(Class<? extends Chase> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.account = inits.isInitialized("account") ? new rza.BPFco.model.QAccount(forProperty("account")) : null;
    }

}

