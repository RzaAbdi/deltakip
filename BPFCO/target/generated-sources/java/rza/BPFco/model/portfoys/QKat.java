package rza.BPFco.model.portfoys;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QKat is a Querydsl query type for Kat
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QKat extends EntityPathBase<Kat> {

    private static final long serialVersionUID = -1550105512L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QKat kat = new QKat("kat");

    public final QBlok blok;

    public final DateTimePath<java.util.Date> create_date = createDateTime("create_date", java.util.Date.class);

    public final DateTimePath<java.util.Date> edit_date = createDateTime("edit_date", java.util.Date.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final ListPath<Section, QSection> sections = this.<Section, QSection>createList("sections", Section.class, QSection.class, PathInits.DIRECT2);

    public final StringPath title = createString("title");

    public QKat(String variable) {
        this(Kat.class, forVariable(variable), INITS);
    }

    public QKat(Path<? extends Kat> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QKat(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QKat(PathMetadata<?> metadata, PathInits inits) {
        this(Kat.class, metadata, inits);
    }

    public QKat(Class<? extends Kat> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.blok = inits.isInitialized("blok") ? new QBlok(forProperty("blok"), inits.get("blok")) : null;
    }

}

