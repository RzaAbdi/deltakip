package rza.BPFco.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QContract is a Querydsl query type for Contract
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QContract extends EntityPathBase<Contract> {

    private static final long serialVersionUID = -1645596394L;

    public static final QContract contract = new QContract("contract");

    public final SetPath<Account, QAccount> account = this.<Account, QAccount>createSet("account", Account.class, QAccount.class, PathInits.DIRECT2);

    public final BooleanPath active = createBoolean("active");

    public final SetPath<rza.BPFco.model.portfoys.Agent, rza.BPFco.model.portfoys.QAgent> agent = this.<rza.BPFco.model.portfoys.Agent, rza.BPFco.model.portfoys.QAgent>createSet("agent", rza.BPFco.model.portfoys.Agent.class, rza.BPFco.model.portfoys.QAgent.class, PathInits.DIRECT2);

    public final StringPath cash = createString("cash");

    public final StringPath contract_no = createString("contract_no");

    public final DateTimePath<java.util.Date> createDate = createDateTime("createDate", java.util.Date.class);

    public final NumberPath<Integer> currency = createNumber("currency", Integer.class);

    public final SetPath<rza.BPFco.model.portfoys.Customer, rza.BPFco.model.portfoys.QCustomer> customer = this.<rza.BPFco.model.portfoys.Customer, rza.BPFco.model.portfoys.QCustomer>createSet("customer", rza.BPFco.model.portfoys.Customer.class, rza.BPFco.model.portfoys.QCustomer.class, PathInits.DIRECT2);

    public final BooleanPath deed = createBoolean("deed");

    public final StringPath description = createString("description");

    public final StringPath description2 = createString("description2");

    public final SetPath<rza.BPFco.model.portfoys.Document, rza.BPFco.model.portfoys.QDocument> documents = this.<rza.BPFco.model.portfoys.Document, rza.BPFco.model.portfoys.QDocument>createSet("documents", rza.BPFco.model.portfoys.Document.class, rza.BPFco.model.portfoys.QDocument.class, PathInits.DIRECT2);

    public final DateTimePath<java.util.Date> downDate = createDateTime("downDate", java.util.Date.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final BooleanPath insEnd = createBoolean("insEnd");

    public final SetPath<rza.BPFco.model.portfoys.Installment, rza.BPFco.model.portfoys.QInstallment> installment = this.<rza.BPFco.model.portfoys.Installment, rza.BPFco.model.portfoys.QInstallment>createSet("installment", rza.BPFco.model.portfoys.Installment.class, rza.BPFco.model.portfoys.QInstallment.class, PathInits.DIRECT2);

    public final BooleanPath installmented = createBoolean("installmented");

    public final StringPath last_portfoy = createString("last_portfoy");

    public final SetPath<Portfoy, QPortfoy> portfoys = this.<Portfoy, QPortfoy>createSet("portfoys", Portfoy.class, QPortfoy.class, PathInits.DIRECT2);

    public final StringPath price = createString("price");

    public final NumberPath<Long> remaining = createNumber("remaining", Long.class);

    public final StringPath section = createString("section");

    public QContract(String variable) {
        super(Contract.class, forVariable(variable));
    }

    public QContract(Path<? extends Contract> path) {
        super(path.getType(), path.getMetadata());
    }

    public QContract(PathMetadata<?> metadata) {
        super(Contract.class, metadata);
    }

}

