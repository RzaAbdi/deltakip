package rza.BPFco.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QPortfoy is a Querydsl query type for Portfoy
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QPortfoy extends EntityPathBase<Portfoy> {

    private static final long serialVersionUID = -981112789L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPortfoy portfoy = new QPortfoy("portfoy");

    public final QContract contract;

    public final DateTimePath<java.util.Date> createDate = createDateTime("createDate", java.util.Date.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final BooleanPath reserved = createBoolean("reserved");

    public final BooleanPath saled = createBoolean("saled");

    public final SetPath<rza.BPFco.model.portfoys.Section, rza.BPFco.model.portfoys.QSection> sections = this.<rza.BPFco.model.portfoys.Section, rza.BPFco.model.portfoys.QSection>createSet("sections", rza.BPFco.model.portfoys.Section.class, rza.BPFco.model.portfoys.QSection.class, PathInits.DIRECT2);

    public final SetPath<Tag, QTag> tag = this.<Tag, QTag>createSet("tag", Tag.class, QTag.class, PathInits.DIRECT2);

    public final StringPath title = createString("title");

    public QPortfoy(String variable) {
        this(Portfoy.class, forVariable(variable), INITS);
    }

    public QPortfoy(Path<? extends Portfoy> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPortfoy(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPortfoy(PathMetadata<?> metadata, PathInits inits) {
        this(Portfoy.class, metadata, inits);
    }

    public QPortfoy(Class<? extends Portfoy> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.contract = inits.isInitialized("contract") ? new QContract(forProperty("contract")) : null;
    }

}

