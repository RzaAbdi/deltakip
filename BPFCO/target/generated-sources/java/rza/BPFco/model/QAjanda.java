package rza.BPFco.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QAjanda is a Querydsl query type for Ajanda
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QAjanda extends EntityPathBase<Ajanda> {

    private static final long serialVersionUID = 1196352023L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QAjanda ajanda = new QAjanda("ajanda");

    public final SetPath<Account, QAccount> account = this.<Account, QAccount>createSet("account", Account.class, QAccount.class, PathInits.DIRECT2);

    public final DateTimePath<java.util.Date> createDate = createDateTime("createDate", java.util.Date.class);

    public final BooleanPath done = createBoolean("done");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final DateTimePath<java.util.Date> issueDate = createDateTime("issueDate", java.util.Date.class);

    public final DateTimePath<java.util.Date> lastUpdate = createDateTime("lastUpdate", java.util.Date.class);

    public final SetPath<rza.BPFco.model.portfoys.Reply, rza.BPFco.model.portfoys.QReply> reply = this.<rza.BPFco.model.portfoys.Reply, rza.BPFco.model.portfoys.QReply>createSet("reply", rza.BPFco.model.portfoys.Reply.class, rza.BPFco.model.portfoys.QReply.class, PathInits.DIRECT2);

    public final rza.BPFco.model.portfoys.QSosyalMedya sosyalmedya;

    public final NumberPath<Integer> status = createNumber("status", Integer.class);

    public final StringPath title = createString("title");

    public final NumberPath<Integer> type = createNumber("type", Integer.class);

    public QAjanda(String variable) {
        this(Ajanda.class, forVariable(variable), INITS);
    }

    public QAjanda(Path<? extends Ajanda> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QAjanda(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QAjanda(PathMetadata<?> metadata, PathInits inits) {
        this(Ajanda.class, metadata, inits);
    }

    public QAjanda(Class<? extends Ajanda> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.sosyalmedya = inits.isInitialized("sosyalmedya") ? new rza.BPFco.model.portfoys.QSosyalMedya(forProperty("sosyalmedya"), inits.get("sosyalmedya")) : null;
    }

}

