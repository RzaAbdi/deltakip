$(document).ready(function() {
	Spinner();
	Spinner.hide();
	 
	var cid = 0;
	var insid = 0;
	
	$('input[type="file"]').ezdz();
	
	var name=null;
	var issueDate=null;
	var amount=null;
	var contractDate=null;
	var contractNo=null;
	var customer=null;
	
	$(document.body).on("change","#type",function(){
		  
		 
		 switch(this.value) {
		  case "1"://hatirlatam
		    var text = Tdear + name + " "+contractDate+ Tdate + amount + Tamount + issueDate + Tremaining ;
		   $('#text').val(text);
			  
		    break;
		  case "2"://tesekkur
			  
			  var text = Tdear+ name + " "+contractDate+ Tdate + amount + Tamount + issueDate+ Tthanks ;
			   $('#text').val(text);
		    
			  break;
		  case "3"://uyari
			  
			  var text = Tdear + name + " "+contractDate+ Tdate + amount +Tamount + issueDate+Twarning ;
			   $('#text').val(text);
			  
			  
		    break;
		  default:
		    // code block
		}
		});
	
	Spinner.show();
	 $.ajax({
	        type: "POST",
	        url: '/json/installment/loadCalender',
	        
	        dataType: "html",
	        success: function(data) {
		 var array = JSON.parse(data);
	            var calendarEl = document.getElementById('calendar');
		        var calendar = new FullCalendar.Calendar(calendarEl, {
		        	 
		        	events: array,
		        	
		          initialView: 'dayGridMonth',
		          
		          eventClick: function(info) {
		        	Spinner();
		        	Spinner.show();
		    			var modal = $('#calender_modal');
		    			modal.modal('show');
		    			modal.meshop({

		    				url : "/json/installment/loadForCalender",
		    				params : {
		    					id : info.event.id,
		    				},
		    			 loop : loop,
		    			 modifyJson : modifyJson
		    			})
		    			
		    			
		    			
		    			cid =   info.event.groupId;
		    			insid = info.event.id;
		    			
		    			  name=info.event.title;
		    			  issueDate=info.event.extendedProps.issueDate;
		    			  contractNo=info.event.extendedProps.contractNo;
		    			  customer=info.event.extendedProps.customerid;
		    			  amount=info.event.extendedProps.amount
;
		    			  contractDate=info.event.extendedProps.contractDate
;
		        	  }
		        });
		        calendar.setOption('locale', 'tr');
		        calendar.render();
		        Spinner.hide();
		     
	        },
	        error: function(e) {
	           
	            Spinner.hide();
	        }
	    });
	 
	 
	 
	 function loopMsg(obj, json, mainobj) {
		 obj.click(function(e) {
				e.stopPropagation();
						
						window.open("/adminpanel/outbox"+"/?listId="+json.mid, "_blank");
					});
		 
	 }
	 function loop(obj, json, mainobj) {
		 Spinner.hide();
		 obj.find('#viewIns').click(function(e) {
			 e.stopPropagation();
			 
			 window.open("/installment_manage"+"/?contracts="+cid, "_blank");
		 });
		 

		 obj.find('#insModal').click(function(e) {
				e.stopPropagation();
				var modal = $('#contract_modal');
				modal.modal('show');
				modal.meshop({

					url : "/json/contract/load",
					params : {
						id : cid,
					},
					
					 
				})

				
			}); 
		 
		 obj.find('#editIns').click(function(e) {
			 e.stopPropagation();
				var modal = $('#edit_installment_modal');
				modal.modal('show');
				modal.meshop({

					url : "/adminpanel/json/installment/load",
					params : {
						id : insid,
					},
					
					
//				 loop : loopModals,
				 modifyJson : modifyJsonModal
				})

			 
			 
		 }); 
//		 
	
		 $('#message').click(function(e) {
			 e.stopPropagation();
			 var modal = $('#new_message_modal');
			 modal.modal('show');
			 modal.meshop({
					json : {
						insid : insid,
					},
			 });
			 
			 $('#MessageTable').meshop({
				 url : '/adminpanel/json/messages/loadbyId?insid='+insid,
					page : $('[data-id=TableMessage_page]'),
				 
					
					
					loop : loopMsg
					
				 
			 });
			 
			 
			 
		 }); 
		        
	 }  
	 
	 
	 
	
	 
	 
	 
	 function modifyJsonModal(json) {

			json.part = json.partial ? 'enable' : 'disable';
				

				return json;
			}
			
			  
	 function modifyJson(json) {

		 
		 switch(json.status) {
		  case "1":
			  json.text = remaining;
		    break;
		  case "2":
			  json.text = thanks;
		    break;
		  case "3":
			  json.text = warning;
			  break;
		  default:
		}

			return json;
		}
			
		
			 $('#print').click(function(e) {
				
				$("#modalDiv").printThis({ 
				    debug: false,              
				    importCSS: true,             
				    importStyle: true,         
				    printContainer: true,       
				    loadCSS: "../css/style.css", 
				    pageTitle: "My Modal",             
				    removeInline: false,        
				    printDelay: 333,            
				    header: null,             
				    formValues: true          
				}); 
				});
				   
			 $('#save').click(function() {
					$('#save,#resetModal').addClass("disabled");
					$(this).addClass('loading');
					var form = $('#floor_form');
					$.ajax({
						url : "/adminpanel/json/installment/save",
						type : "POST",
						data : form.serialize(),
					}).done(function(respond) {
						// window.location =
						// "/";

						$('#new_floor_modal').modal('hide');
						meshop.refresh($("#Table"));
						meshop.log("ok", tok);

					}).fail(function(e) {
						meshop.log("error", terror);

					}).always(function() {
						$('#save').removeClass('loading').removeClass('disabled');
						// meshop.refresh($("#Table"));
					});
//					 meshop.refresh($("#Table"));

				});     
			 $('#save_message').click(function() {
				 $('#save_message,#resetModal').addClass("disabled");
				 $(this).addClass('loading');
				 var form = $('#message_form');
				 $.ajax({
					 url : "/adminpanel/json/message/save",
					 type : "POST",
					 data : form.serialize(),
				 }).done(function(respond) {
					 // window.location =
					 // "/";
					 
					 $('#new_message_modal').modal('hide');
					 meshop.refresh($("#Table"));
					 meshop.refresh($('#edit_installment_modal'));
					 meshop.log("ok", tok);
					 
				 }).fail(function(e) {
					 meshop.log("error", terror);
					 
				 }).always(function() {
					 $('#save_message').removeClass('loading').removeClass('disabled');
					 // meshop.refresh($("#Table"));
				 });
//					 meshop.refresh($("#Table"));
				 
			 });     
		   
	 
			 $('#newInsDoc').click(function(e) {
				 e.stopPropagation();
				 var modal = $('#new_file_modal');
				 modal.modal('show');
				 modal.meshop({
						json : {
					 installments : {id: insid, title:insid},
					 contracts : {id: cid, title:contractNo},
					 customers : {id: customer, title:name},
						},
				 });
				 
			}); 
			 $('#docs').click(function(e) {
				 e.stopPropagation();
				 
				 
				 window.open("/docs"+"?customerid="+customer, "_blank");
			 }); 

			$('#saveDOC').click(function() {
				$('#saveDOC').addClass('loading');
				$('#saveDOC').addClass('disable');
				 var modal = $('#new_file_modal');
				var formData = new FormData();
				 var form = $('form#doc_form');
			//formData = form.serializeArray();
				 $('input[type="file"]').each(function(i, input) {
						var oo = input.files[0];
						if (oo) {
							formData.append("myNewFileName", oo);
							 
						}
				 });
				 
				 var other_data = form.serializeArray();
				 $.each(other_data,function(key,input){
					 formData.append(input.name,input.value);
				 });
				 
					$.ajax({

						success : function(response) {
							
							meshop.log("ok", tok);
							meshop.refresh($("#Table"));
							$('#saveDOC').removeClass('loading');
							$('#saveDOC').removeClass('disable');
							modal.modal('hide');
						},

						error : function(err) {
							
							$('#saveDOC').removeClass('loading');
							$('#saveDOC').removeClass('disable');
							
							if (err.responseText == '0') {
								meshop.log("error", " Dosya Adı Benzerliği Var! ");
							}else{
								meshop.log("error", "");
								
							}

						},
					

						data : formData,
						processData : false,
						contentType : false,
						url : "/adminpanel/json/doc/save",
						type : "POST",

					});

			});
			

});