$(document).ready(function() {
	
	var last;
	
	Spinner();
	Spinner.hide();
	Spinner.show();
	var del = $('#delete')
	
	$('input[type="file"]').ezdz();
	
	var pathname = window.location.href;
	path = pathname.split('?')[1];
	if(path!= null) {
	url =	"/json/customers/load"+"?"+path;
	}else {
		url =	"/json/customers/load";
	}
	
	
	
	
	var name=null;
	var cid=null;
	
	$("#Table").meshop({
		url : url,
		page : $('[data-id=Table_page]'),
		modifyJson : modifyJson,
		loop : loop,
		filterForm:$('#filter-form')

	}).on("empty",function(){
		
		$("#Table").addClass('hidden');
		$('[data-id=Table_page]').addClass('hidden');
		$("#empty").removeClass('hidden');
	});
	
	
	function modifyJson(json) {

		json.trclass = json.active ? '' : 'danger';
//		

		return json;
	}
	

	
	function loop(obj, json, mainObj) {
		Spinner.hide();

		 
		obj.find('.drop').dropdown()	
		 

		obj.data('id', json.cid);
		obj.find('.docs').click(function(e) {
			e.stopPropagation();
			
		});
		obj.find('#teslim').click(function(e) {
			e.stopPropagation();
			obj.find('.drop').dropdown('toggle')
			var modal = $('#konut_teslim_modal');
			modal.modal('show');
			modal.meshop({
				json : {
				customer : json.fname,
				createDate : json.createDate,
				portfoys : json.portfoys,
				},
		 });
			
			
		});
		obj.find('#teknik').click(function(e) {
			e.stopPropagation();
			obj.find('.drop').dropdown('toggle')
			var modal = $('#teknik_modal');
			modal.modal('show');
			modal.meshop({
				json : {
				customer : json.fname,
				createDate : json.createDate,
				portfoys : json.portfoys,
			},
			});
			
			
		});
		obj.find('#dekor').click(function(e) {
			e.stopPropagation();
			obj.find('.drop').dropdown('toggle')
			var modal = $('#dekor_modal');
			modal.modal('show');
			modal.meshop({
				json : {
				customer : json.fname,
				createDate : json.createDate,
				portfoys : json.portfoys,
				
			},
				loop : loopDekor,
			});
			
			
		});
		obj.find('#radisson').click(function(e) {
			e.stopPropagation();
			obj.find('.drop').dropdown('toggle')
			var modal = $('#radisson_modal');
			modal.modal('show');
			modal.meshop({
				json : {
				 
				createDate : json.createDate,
				id : json.cid,
				
			},
			});
			
			
		});
		obj.find('#dekont').click(function(e) {
			e.stopPropagation();
			obj.find('.drop').dropdown('toggle')
			var modal = $('#dekont_modal');
			modal.modal('show');
			modal.meshop({
				json : {
				portfoys : json.portfoys,
				createDate : json.createDate,
				id : json.cid,
				customer : json.fname,
				
			},
				loop : loopDekor,
			});
			
			
		});
		obj.find('#aydinlatma').click(function(e) {
			e.stopPropagation();
			obj.find('.drop').dropdown('toggle')
			var modal = $('#aydinlatma_modal');
			modal.modal('show');
			modal.meshop({
				json : {
				
				customer : json.fname,
				id : json.cid,
				
			},
			});
			
			
		});
		obj.find('#alimsatim').click(function(e) {
			e.stopPropagation();
			obj.find('.drop').dropdown('toggle')
			var modal = $('#alim_modal');
			modal.modal('show');
			modal.meshop({
				json : {
				customer : json.fname,
				mail : json.mail,
				address : json.address,
				gsm1 : json.gsm1,
				createDate : json.createDate,
				portfoys : json.portfoys,
				id : json.cid,
				
			},
			});
			
			
		});

		obj.find('.scheck').click(function(e) {

			e.stopPropagation();
			var checkbox = obj.find(".selected");

			if (checkbox.hasClass('glyphicon-unchecked')) {
				checkbox.addClass(" glyphicon-check").removeClass(" glyphicon-unchecked ");
			} else {
				checkbox.addClass(" glyphicon-unchecked").removeClass(" glyphicon-check ");
				del.removeClass("disabled");
			}
			var count = $('span.glyphicon-check').map(function check_del(index, obj) {

				return $(obj).closest('tr').data('id');
			})
			if (count.length > 0) {

				del.removeClass("disabled");
			} else {
				del.addClass("disabled");
				$('#selectall').removeClass(" glyphicon-check").addClass(" glyphicon-unchecked ");
			}

		});


		obj.click(function(e) {
			e.stopPropagation();
			var modal = $('#new_customer_modal');
			modal.modal('show');
			modal.meshop({

				url : "/json/customer/load",
				params : {
					id : json.cid,
				},
				
				
			// loop : loopModal,
			// modifyJson : modifyJsonModal
			})
			 name=json.fname;
			  cid=json.cid;

		});

		obj.find('td.docs #newContract').click(function(e) {
			e.stopPropagation();
			
			
			var modal = $('#dubai_contract_modal');
			modal.modal('show');
			modal.meshop({
				json : {
				customer : json.fname,
				email : json.mail,
				address : json.address,
				createDate : json.createDate,
				portfoys : json.portfoys,
				gsm : json.gsm1,
				},
		 });
			
		}); 
		
		
		obj.find('td.docs #newCover').click(function(e) {
			e.stopPropagation();
			Spinner.show();
			
			var modal = $('#new_cover_modal');
			
			modal.meshop({

				url : "/json/customer/cover/load",
				params : {
					id : json.cid,
				}, 
				
				loop : loopCover
			// loop : loopModal,
			// modifyJson : modifyJsonModal
			}).on("render", function(obj, json) {
				Spinner.hide();
				modal.modal('show');
			});
				
				
				
			
		}); 
		
	
		
		obj.find('td.docs #contracts').click(function(e) {
			e.stopPropagation();
			
			window.open("/docs"+"/?customerid="+json.cid+"&"+"docType=8", "_blank");
		});
		
		
		
		obj.find('td.docs #CusDocs').click(function(e) {
			e.stopPropagation();
			
			window.open("/docs"+"/?customerid="+json.cid, "_blank");
		});
	}
	
	function loopDekor(obj, json, mainObj) {
		
		
obj.find("input[name='price']").blur(function() {
			
			var val =  parseFloat(this.value.replace(/,/g, ""));
			
			var fix =  val.toFixed(2);
			
			var price = fix.toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			
			$(this).val(price );
			
			setAmount();
			
    
	});
obj.find("input[name='limit']").blur(function() {
	
	
	
	
	setAmount();
	
	
});
		
function setAmount() {
	var price = obj.find("input[name='price']");
	var limit = obj.find("input[name='limit']");
	var amount = obj.find("input[name='amount']");
	var a;
	
	if(price.val()!="") {
		if(limit.val()!="") {
		var p = 	price.val().replace(/,/g, "") ;
		var l = 	limit.val().replace(/,/g, "") ;
		a = p/l;
		var val =  parseFloat(a.toString().replace(/,/g, ""));
		
		var fix =  val.toFixed(2);
		
		var am = fix.toString()
				.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		amount.val(am);
		
		}
	}
	
}
	}
	
	
	function loopCover(obj, json, mainObj) {
		
		
		$(document.body).on("change","#currency",function(){
			
			
			var cur = obj.find("span#cur");
			switch(this.value) {
			  case "1"://tl
				  cur.html(" TL")
			    break;
			  case "2"://usd
				  cur.html(" USD")
			     
			    break;
			  case "3"://euro
				  cur.html(" EURO")
				 
				  break;
			  default:
			    
			}
			});
		
		
		obj.find("input[name='price']").blur(function() {
			
			var val =  parseFloat(this.value.replace(/,/g, ""));
			
			var fix =  val.toFixed(2);
			
			var price = fix.toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			
			$(this).val(price );
			
//			setRemining();
			
    
	});
		obj.find("input[name='cash']").blur(function() {
			
			var val =  parseFloat(this.value.replace(/,/g, ""));
			
			var fix =  val.toFixed(2);
			
			var price = fix.toString()
					.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			
			$(this).val(price.toString() );
			
//			setRemining();
			
			
			
			
			
		});
		obj.find("input[name='amount']").blur(function() {
			
			var val =  parseFloat(this.value.replace(/,/g, ""));
			
			var fix =  val.toFixed(2);
			
			var price = fix.toString()
					.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			
			$(this).val(price.toString() );
			
//			setRemining();
			
			
			
			
			
		});
		
		obj.find("input[name='limit']").blur(function() {
			
			setRemining();
			
		});
		
		function setRemining() {
			
		var p =	obj.find("input[name='price']").val().replace(/,/g, "") ;
		var c =	obj.find("input[name='cash']").val().replace(/,/g, "") ;
		var a =	obj.find("input[name='amount']").val().replace(/,/g, "") ;
		if(p=="") {
			p=0
		}
		if(c=="") {
			c=0
		}
		
		var l =  parseFloat(obj.find("input[name='limit']").val().replace(/,/g, ""));
		
		 
		if(isNaN(last)) {
			last=0;
		}
		
		var last_total= (p-c);
		var limit;
		if(!isNaN(l)) {
			
			if(a=="") {
				
				limit = last_total/l; 
				last_total = (last_total-(limit*l));
				var val =  parseFloat(limit.toString().replace(/,/g, ""));
				var fix =  val.toFixed(2);
				var remain = fix.toString()
						.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				obj.find("input.am").val(remain);
			}else {
				last_total = (last_total-(a*l));
			}
			
				
				
				
				
		}
		
		
		
			
var val =  parseFloat(last_total.toString().replace(/,/g, ""));

last = val;
			
			var fix =  val.toFixed(0);
			
			 last_total = fix.toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			 
			
			 
			obj.find("span.rem").html(last_total);
		 
			
			
			
		}
	};
	
	
	$('#save_cover').click(function(e) {
		$('#save_cover,#resetModal').addClass("disabled");
		$(this).addClass('loading');
		var form = $('#cover_form');
		$.ajax({
			url : "/json/cover/save",
			type : "POST",
			data : form.serialize(),
		}).done(function(respond) {
			$('#new_cover_modal').modal('hide');
			e.preventDefault();
			window.location = "/cover_manage/?id="+respond;

			 

		}).fail(function(e) {
			meshop.log("error", terror);

		}).always(function() {
			$('#save_cover').removeClass('loading').removeClass('disabled');
			Spinner.hide();
			// meshop.refresh($("#Table"));
		});
//		 meshop.refresh($("#Table"));

	});
	
	
	$('#saveDOC').click(function() {
		var formData = new FormData();
		 var form = $('form#doc_form');
	//formData = form.serializeArray();
		 $('input[type="file"]').each(function(i, input) {
				var oo = input.files[0];
				if (oo) {
					formData.append("myNewFileName", oo);
					 
				}
		 });
		 
		 
		 
		 
		 
		 
		 var other_data = form.serializeArray();
		 $.each(other_data,function(key,input){
			 formData.append(input.name,input.value);
		 });
		 
			$.ajax({

				success : function(response) {
					
					meshop.log("ok", tok);
					 var modal = $('#new_file_modal');
					 modal.modal("hide");
				},

				error : function(err) {
					
					if (err.responseText == '0') {
						meshop.log("error", trepeat);
					}else{
						meshop.log("error", "");
						
					}

				},
			

				data : formData,
				processData : false,
				contentType : false,
				url : "/adminpanel/json/doc/save",
				type : "POST",

			});

	});
	
$('#save_teslim').click(function(e) {
		
		var saveToDocs = false;
		var modal = meshop.alert("ask", "Dosyalara Eklensin mi?");
		modal.size("sm");
		modal.yes = function() {
			saveToDocs=true;
			 getTeslim(saveToDocs);
			
		}
		modal.no = function() {
			saveToDocs=false;
			 getTeslim(saveToDocs);
		}
		
		
		
	});
$('#save_teknik').click(function(e) {
	
	var saveToDocs = false;
	var modal = meshop.alert("ask", "Dosyalara Eklensin mi?");
	modal.size("sm");
	modal.yes = function() {
		saveToDocs=true;
		getTeknik(saveToDocs);
		
	}
	modal.no = function() {
		saveToDocs=false;
		getTeknik(saveToDocs);
	}
	
	
	
});
$('#save_dekor').click(function(e) {
	
	var saveToDocs = false;
	var modal = meshop.alert("ask", "Dosyalara Eklensin mi?");
	modal.size("sm");
	modal.yes = function() {
		saveToDocs=true;
		getDekor(saveToDocs);
		
	}
	modal.no = function() {
		saveToDocs=false;
		getDekor(saveToDocs);
	}
	
	
	
});
$('#save_alim').click(function(e) {
	
	var saveToDocs = false;
	var modal = meshop.alert("ask", "Dosyalara Eklensin mi?");
	modal.size("sm");
	modal.yes = function() {
		saveToDocs=true;
		getAlim(saveToDocs);
		
	}
	modal.no = function() {
		saveToDocs=false;
		getAlim(saveToDocs);
	}
	
	
	
});


$('#save_radisson').click(function(e) {
	
	var data = $('#radisson_form').serializeArray();
	$.ajax({
		url : "/json/contract/radisson",
		type : "POST",
		data : data,
	}).done(function(respond) {
		var win = window.open("/files"+respond.path);
		if (win) {
			win.focus()
		} else {
			alert('Please allow popups for this website');
		}
		$('#radisson_modal').modal('hide');
		meshop.log("ok", tok);
	}).fail(function(e) {
		meshop.log("error", terror);
	}).always(function() {
		$('#save_radisson').removeClass('loading').removeClass('disabled');
	});
});
$('#save_aydinlatma').click(function(e) {
	
	var data = $('#aydinlatma_form').serializeArray();
	$.ajax({
		url : "/json/contract/aydinlatma",
		type : "POST",
		data : data,
	}).done(function(respond) {
		var win = window.open("/files"+respond.path);
		if (win) {
			win.focus()
		} else {
			alert('Please allow popups for this website');
		}
//		$('#aydinlatma_modal').modal('hide');
		meshop.log("ok", tok);
	}).fail(function(e) {
		meshop.log("error", terror);
	}).always(function() {
		$('#save_aydinlatma').removeClass('loading').removeClass('disabled');
	});
});



$('#save_dekont').click(function(e) {
	
	var data = $('#dekont_form').serializeArray();
	$.ajax({
		url : "/json/contract/dekont",
		type : "POST",
		data : data,
	}).done(function(respond) {
		var win = window.open("/files"+respond.path);
		if (win) {
			win.focus()
		} else {
			alert('Please allow popups for this website');
		}
		$('#dekont_modal').modal('hide');
		meshop.log("ok", tok);
	}).fail(function(e) {
		meshop.log("error", terror);
	}).always(function() {
		$('#save_dekont').removeClass('loading').removeClass('disabled');
	});
});


function getTeslim(saveToDocs){
	$('#save_teslim').addClass("disabled");
	$('#save_teslim').addClass('loading');
	var data = $('#teslim_form').serializeArray();
	data.push({ name: "saveToDocs", value: saveToDocs });

	
	$.ajax({
		url : "/json/contract/teslim",
		type : "POST",
		data : data,
	}).done(function(respond) {
//		e.preventDefault();  //stop the browser from following
		
		var win = window.open("/files"+respond.path);
		
		
		if (win) {
		    //Browser has allowed it to be opened
			win.focus()
		} else {
		    //Browser has blocked it
		    alert('Please allow popups for this website');
		}
//		
//	    window.location.assign("/files/"+respond.path);
		$('#konut_teslim_modal').modal('hide');
		meshop.log("ok", tok);

	}).fail(function(e) {
		meshop.log("error", terror);

	}).always(function() {
		$('#save_teslim').removeClass('loading').removeClass('disabled');
		// meshop.refresh($("#Table"));
	});
}
function getAlim(saveToDocs){
	$('#save_alim').addClass("disabled");
	$('#save_alim').addClass('loading');
	var data = $('#alim_form').serializeArray();
	data.push({ name: "saveToDocs", value: saveToDocs });
	
	
	$.ajax({
		url : "/json/contract/alim",
		type : "POST",
		data : data,
	}).done(function(respond) {
//		e.preventDefault();  //stop the browser from following
		
		var win = window.open("/files"+respond.path);
		
		
		if (win) {
			//Browser has allowed it to be opened
			win.focus()
		} else {
			//Browser has blocked it
			alert('Please allow popups for this website');
		}
//		
//	    window.location.assign("/files/"+respond.path);
		$('#alim_modal').modal('hide');
		meshop.log("ok", tok);
		
	}).fail(function(e) {
		meshop.log("error", terror);
		
	}).always(function() {
		$('#save_alim').removeClass('loading').removeClass('disabled');
		// meshop.refresh($("#Table"));
	});
}
function getDekor(saveToDocs){
	$('#save_dekor').addClass("disabled");
	$('#save_dekor').addClass('loading');
	var data = $('#dekor_form').serializeArray();
	data.push({ name: "saveToDocs", value: saveToDocs });
	
	
	$.ajax({
		url : "/json/contract/dekor",
		type : "POST",
		data : data,
	}).done(function(respond) {
//		e.preventDefault();  //stop the browser from following
		
		var win = window.open("/files"+respond.path);
		
		
		if (win) {
			//Browser has allowed it to be opened
			win.focus()
		} else {
			//Browser has blocked it
			alert('Please allow popups for this website');
		}
//		
//	    window.location.assign("/files/"+respond.path);
//		$('#dekor_modal').modal('hide');
		meshop.log("ok", tok);
		
	}).fail(function(e) {
		meshop.log("error", terror);
		
	}).always(function() {
		$('#save_dekor').removeClass('loading').removeClass('disabled');
		// meshop.refresh($("#Table"));
	});
}
	
function getTeknik(saveToDocs){
	$('#save_teknik').addClass("disabled");
	$('#save_teknik').addClass('loading');
	var data = $('#teknik_form').serializeArray();
	data.push({ name: "saveToDocs", value: saveToDocs });
	
	
	$.ajax({
		url : "/json/contract/teknik",
		type : "POST",
		data : data,
	}).done(function(respond) {
//		e.preventDefault();  //stop the browser from following
		
		var win = window.open("/files"+respond.path);
		
		
		if (win) {
			//Browser has allowed it to be opened
			win.focus()
		} else {
			//Browser has blocked it
			alert('Please allow popups for this website');
		}
		$('#teknik_modal').modal('hide');
		meshop.log("ok", tok);
		
	}).fail(function(e) {
		meshop.log("error", terror);
		
	}).always(function() {
		$('#save_teknik').removeClass('loading').removeClass('disabled');
		// meshop.refresh($("#Table"));
	});
}

	
			var selectall = $('#selectall');
			selectall.parent().click(function(e) {
				if (selectall.hasClass('glyphicon-unchecked')) {
					check(selectall);
					check($(".selected"));
					del.removeClass("disabled");
				} else {
					uncheck(selectall);
					uncheck($(".selected"));
					del.addClass("disabled");
				}

				function check(obj) {
					obj.addClass(" glyphicon-check").removeClass(" glyphicon-unchecked ");
				}
				function uncheck(obj) {
					obj.removeClass(" glyphicon-check ").addClass(" glyphicon-unchecked");
				}

			});

			del.click(function(e) {
				var modal = meshop.alert("ask", customerDel, "Uyarı");
				modal.size("sm");
				modal.yes = function() {
					e.preventDefault();
					del.addClass('loading');
					var selecteditems = "";
					var count = $('span.glyphicon-check.selected').map(function(index, obj) {
						return $(obj).closest('tr').data('id');
					})
					var selecteditems = count.get().join();

					if (selecteditems) {
						$.post("/adminpanel/json/customer/cancel", {
							ids : selecteditems
						}, function(data) {
							meshop.refresh($("#Table"));
							meshop.log("ok", tok);
						}).fail(function(e) {
							del.removeClass('loading');
							meshop.log("error", terror);
						}).always(function() {
							del.removeClass('loading');
							del.addClass("disabled");
						});
					}
				};
			});
	

});