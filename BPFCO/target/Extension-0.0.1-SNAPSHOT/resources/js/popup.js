$(document).ready(function() {
	$.ajax({
		url : "/event-count",
		type : "GET",
		async: true
	}).done(function(fragment) {
		 $("#Replace").replaceWith(fragment);
	})
	
	 

		$('#createContract').click(function(e) {
			e.stopPropagation();
			
			var modal = $('#new_customer_modal');
			modal.modal('show');

			modal.meshop({
				json : {
					active : false,
				},
			});

			
		});
	
window.onscroll = function() {
		
		if((this.oldScroll  > this.scrollY)) {//up
			hideButtons()
			 
			 
			 if(document.body.scrollTop > 200 || document.documentElement.scrollTop < 200) {
				 hideMenu()
			 }else {
				 showMenu()
			 }
			 this.oldScroll = this.scrollY;

		}else {//bottom
			scrollFunction()
			  this.oldScroll = this.scrollY;

		}
		
		
	
	};

	function scrollFunction() {
	  if (document.body.scrollTop > 200 || document.documentElement.scrollTop > 200) {
		  hideMenu()
		  showButtons()
		  
	  } else {
		  hideMenu()
		  hideButtons()
	  }
	}
	
	
	
	function showMenu() {
		 document.getElementById("MenuSec").style.top = "0";
		  document.getElementById("MenuSec").style.opacity = "1";
		  var parentwidth = $("div.main").width();
		  $("#MenuSec").width(parentwidth);
	}
	function showButtons() {
		document.getElementById("Buttons").style.top = "0";
		document.getElementById("Buttons").style.opacity = "1";
		var parentwidth = $("div.main").width();
		$("#Buttons").width(parentwidth);
	}
	function hideButtons() {
		document.getElementById("Buttons").style.top = "-50px";
		document.getElementById("Buttons").style.opacity = "0";
		var parentwidth = $("div.main").width();
		$("#Buttons").width(parentwidth);
	}
	
	
	function hideMenu() {
		 document.getElementById("MenuSec").style.top = "-50px";
		  document.getElementById("MenuSec").style.opacity = "0";
		  var parentwidth = $("div.main").width();
		  $("#MenuSec").width(parentwidth);
	}

	
	

 
 



	
	$('#saveCustomer').click(function() {
		var modal = $('#new_customer_modal');
		
		$('#save,#resetModal').addClass("disabled");
		$(this).addClass('loading');
		var form = $('#cus_form');
		$.ajax({
			url : "/json/customer/save",
			type : "POST",
			data : form.serialize(),
		}).done(function(respond) {
			// window.location =
			// "/";

			
		}).fail(function(e) {
if(e.responseText.includes('name')) {
				
	$('#new_portfoy_modal').modal('hide');
	meshop.refresh($("#Table"));
	meshop.log("ok", tok);
	modal.modal('hide');

				
			}else {
				meshop.log("error", terror);
				
			}

		}).always(function() {
			$('#save').removeClass('loading').removeClass('disabled');
			// meshop.refresh($("#Table"));
		});
//		 meshop.refresh($("#Table"));

	});
	
	
	$('#saveAndNext').click(function() {
		var modal = $('#new_customer_modal');
		
		$('#saveAndNext,#resetModal').addClass("disabled");
		$(this).addClass('loading');
		var form = $('#cus_form');
		$.ajax({
			url : "/json/customer/save",
			type : "POST",
			data : form.serialize(),
		}).done(function(respond) {
			
		}).fail(function(e) {
			
			$('#saveAndNext').removeClass('loading').removeClass('disabled');
			
			if(e.responseText.includes('name')) {
				
				var res = e.responseText.replace("name : ", "");
				window.location = "/customer_manage/?name="+res;
				 
				
			}else if(e.responseText.includes('1')){
				
				
				
				meshop.log("error",trepeat);
				
				
			}else {
				meshop.log("error",terror);
				
			}
			
			
		}).always(function() {
			$('#saveAndNext').removeClass('loading').removeClass('disabled');
			// meshop.refresh($("#Table"));
		});
//		 meshop.refresh($("#Table"));
		
	});
	
	
	$('#save_word').click(function(e) {
		
		var saveToDocs = false;
		var modal = meshop.alert("ask", "Dosyalara Eklensin mi?");
		modal.size("sm");
		modal.yes = function() {
			saveToDocs=true;
			 getPdf(saveToDocs);
			
		}
		modal.no = function() {
			saveToDocs=false;
			 getPdf(saveToDocs);
		}
		
		
		
	});
	
	
	function getPdf(saveToDocs){
		$('#save_word').addClass("disabled");
		$('#save_word').addClass('loading');
		var data = $('#word_form').serializeArray();
		data.push({ name: "saveToDocs", value: saveToDocs });
		 

		
		$.ajax({
			url : "/json/contract/word",
			type : "POST",
			data : data,
		}).done(function(respond) {
//			e.preventDefault();  //stop the browser from following
			
			var win = window.open("/files"+respond.path);
			
			
			if (win) {
			    //Browser has allowed it to be opened
				win.focus()
			    
			} else {
			    //Browser has blocked it
			    alert('Please allow popups for this website');
			}
//			
//		    window.location.assign("/files/"+respond.path);

			$('#dubai_contract_modal').modal('hide');
			meshop.log("ok", tok);

		}).fail(function(e) {
			meshop.log("error", terror);

		}).always(function() {
			$('#save_word').removeClass('loading').removeClass('disabled');
			// meshop.refresh($("#Table"));
		});
	}
	
	$('#sign').click(function(e) {
		var modal = meshop.alert("signin");
		modal.size("sm");
		modal.yes = function() {
			var u_txt = $('input#user').val();
			var p_txt = $('input#pass').val();
			e.preventDefault();
			$.post("signin", {

				u : u_txt,
				p : p_txt
			}, function(data) {
				location.reload();

			}).fail(function(e) {

				meshop.log("error", "!");
			}).always(function() {

			});
		};
		var sign = $('#signup');
		sign.click(function(e) {
			modal.modal('hide');

			var modal2 = meshop.alert("signup");
			modal2.size("sm");
			modal2.yes = function() {
				var u_txt = $('input#user').val();
				var p_txt = $('input#pass').val();
				var p_txt2 = $('input#pass2').val();
				var name= $('input#name').val();

				if (meshop.check.email($('input#user'))) {
					if (meshop.check.length($('input#pass'), 5)) {
						if (meshop.check.match($('input#pass2'), p_txt)) {
							e.preventDefault();
							$.post("signup", {
								name : name,
								email : u_txt,
								password : p_txt
							}, function(data) {
								location.reload();

							}).fail(function(e) {

								meshop.log("error", "!");
							}).always(function() {
//								modal2.modal('hide');

							});
						}
					}

				}

			};
		});

	});
	
	$('#changePass').click(function(e) {
		var modal = meshop.alert("change_pass");
		if(email=="admin") {
			email="a@a.com"
		}
		modal.size("sm");
		modal.yes = function() {
			
			//check
			if (meshop.check.length($('input#newpass'), 5)) {
				if (meshop.check.match($('input#newpass'), $('input#repass').val())) {
					
					$.post("/changepass", {
						password : $('input#pass').val(),
						newpass :$('input#newpass').val(),
						email:email
						
					}, function(data) {
						location.reload();

					}).fail(function(e) {

						meshop.log("error", "!");
					}).always(function() {
//						modal2.modal('hide');

					});
					
				}
				
			}
		 
		};
	});
	$('#signout').click(function(e) {
		var modal = meshop.alert("ask", exit_t);
		modal.size("sm");
		modal.yes = function() {
			
			e.preventDefault();
			$.post("logout", {
				
			}, function(data) {
				location.reload();
				
			}).fail(function(e) {
				
				meshop.log("error", "");
			}).always(function() {
				
				// $('#delete-modal').modal('hide');
			});
		};
	});

	$('li#adminpanel').click(function(e) {

		window.location = "/adminpanel";
	});

	$('img#home').click(function(e) {

		window.location = "/";
	});
	
	 

});