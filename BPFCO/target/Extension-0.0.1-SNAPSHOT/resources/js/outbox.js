$(document).ready(function() {

	var replyId;
	
	var insid;
	var institle;
	
	Spinner();
	Spinner.show();
	$('input[type="file"]').ezdz();
	
	var pathname = window.location.href;
	path = pathname.split('?')[1];
	if(path!= null) {
	url =	"/adminpanel/json/messages/loadbyId"+"?"+path;
	}else {
		url =	"/adminpanel/json/messages/loadbyId";
	}
	
	
	$("#Table").meshop({
		url : url,
		page : $('[data-id=Table_page]'),
		modifyJson : modifyJson,
		loop : loop,
		filterForm:$('#filter-form')

	}).on("render", function(obj, json) {
		Spinner.hide();
	}).on("empty", function() {
		Spinner.hide();
	});
	
	var click = false;
	
$("input.daterange").click(function(e) {
		
		if(click==false) {
			
			$('input[name="daterange"]').daterangepicker({
				opens: 'left',
				
				
			}, function(start, end, label) {
				console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
			});
		}
		click=true;
		
	});
	
	
	function modifyJson(json) {

	 
		

		return json;
	}
	
		$("#new_message").click(function(e) {
		var modal = $('#new_message_modal');
		modal.modal('show');

		modal.meshop({
			json : {
				active : false,
			},
			loop : loopModal
		});

	});

	function loopModal(obj, json, mainobj) {

	  

	}
	
	function loop(obj, json, mainObj) {

		obj.data('id', json.mid);

		obj.find('.scheck').click(function(e) {

			e.stopPropagation();
			var checkbox = obj.find(".selected");

			if (checkbox.hasClass('glyphicon-unchecked')) {
				checkbox.addClass(" glyphicon-check").removeClass(" glyphicon-unchecked ");
			} else {
				checkbox.addClass(" glyphicon-unchecked").removeClass(" glyphicon-check ");
				del.removeClass("disabled");
			}
			var count = $('span.glyphicon-check').map(function check_del(index, obj) {

				return $(obj).closest('tr').data('id');
			})
			if (count.length > 0) {

				del.removeClass("disabled");
			} else {
				del.addClass("disabled");
				$('#selectall').removeClass(" glyphicon-check").addClass(" glyphicon-unchecked ");
			}

		});


		obj.click(function(e) {
			Spinner.hide();
			Spinner.show();
			
			e.stopPropagation();
			var modal = $('#message_print_modal');
			modal.meshop({

				url : "/adminpanel/json/message/printable/load",
				params : {
					id : json.mid,
				},
				
			 modifyJson : modifyJsonModal,
			 loop:loopReply
			 
	 
			 
			}).on("render", function(obj, json) {
				modal.modal('show');
				Spinner.hide();
				
			});
			 
			
		});
		
		
		
obj.find("#doc").click(function(e) {
	
	var ob = obj.find("#doc");
	ob.addClass('loading');
	e.stopPropagation();
	
	$.ajax({
		url : "/adminpanel/json/message/getinsid",
		type : "POST",
		data : { mid: json.mid}, 
	}).done(function(respond) {
		 
		
		window.open("/docs"+"/?insid="+respond, "_blank");
		
	}).fail(function(e) {
		meshop.log("error",terror);
		
	}).always(function() {
		ob.removeClass('loading');
	});
			
			
		});



		
obj.find("#reply").click(function(e) {
	e.stopPropagation();
	var modal = $('#new_reply_modal');
	modal.modal('show');
	modal.meshop({

		 
		json : {
			mid : json.mid,
		},
//	 loop : loopReply,
	// modifyJson : modifyJsonModal
	})
	
});

		

	}
	;
	
	
	function loopReply(obj, json, mainObj) {
		
		obj.find('i.edit-reply').click(function(e) {
			e.stopPropagation();
			
			var id = $(this).attr('data-id');
			
				
				var modal = $('#new_reply_modal');
				
				modal.meshop({

					url : "/json/reply/loadbyId",
					params : {
						id : id,
					},
				}).on("render", function(obj, json) {
					modal.modal('show');
					
				});
			
			
		});
		obj.find('i.del-reply').click(function(e) {
			e.stopPropagation();
			
			var id = $(this).attr('data-id');
			
			
			var modal = meshop.alert("ask", " Takip silinecek. Emin misiniz?", "Uyarı");
			modal.size("sm");
			modal.yes = function() {
				e.preventDefault();

					$.post("/adminpanel/json/reply/remove", {
						id : id
					}, function(data) {
						meshop.log("ok",tdelete);
						meshop.refresh($("#message_print_modal"));
					}).fail(function(e) {
						del.removeClass('loading');
						meshop.log("error", terror);
					}).always(function() {
						del.removeClass('loading');
						del.addClass("disabled");
					});
			};
			
			
		});
		
	}
	
	
	
function modifyJsonModal(json) {

	  
	 

		return json;
	}
	

	$('#saveReply').click(function() {
		$('#saveReply,#resetModal').addClass("disabled");
		$(this).addClass('loading');
		var form = $('#reply_form');
		$.ajax({
			url : "/adminpanel/json/message/reply/save",
			type : "POST",
			data : form.serialize(),
		}).done(function(respond) {
			// window.location =
			// "/";
			
			$('#new_reply_modal').modal('hide');
			meshop.refresh($("#message_print_modal"));
			meshop.log("ok", tok);
			
		}).fail(function(e) {
			meshop.log("error",terror);
			
		}).always(function() {
			$('#saveReply').removeClass('loading').removeClass('disabled');
			// meshop.refresh($("#Table"));
		});
//		 meshop.refresh($("#Table"));
		
	});

var del = $('#delete-floor')
		var selectall = $('#selectall');
		selectall.parent().click(function(e) {
			if (selectall.hasClass('glyphicon-unchecked')) {
				check(selectall);
				check($(".selected"));
				del.removeClass("disabled");
			} else {
				uncheck(selectall);
				uncheck($(".selected"));
				del.addClass("disabled");
			}

			function check(obj) {
				obj.addClass(" glyphicon-check").removeClass(" glyphicon-unchecked ");
			}
			function uncheck(obj) {
				obj.removeClass(" glyphicon-check ").addClass(" glyphicon-unchecked");
			}

		});

		del.click(function(e) {
			var modal = meshop.alert("ask", " Mesaj(lar) silinecek. Emin misiniz?", "Uyarı");
			modal.size("sm");
			modal.yes = function() {
				e.preventDefault();
				del.addClass('loading');
				var selecteditems = "";
				var count = $('span.glyphicon-check.selected').map(function(index, obj) {
					return $(obj).closest('tr').data('id');
				})
				var selecteditems = count.get().join();

				if (selecteditems) {
					$.post("/adminpanel/json/message/remove", {
						ids : selecteditems
					}, function(data) {
						meshop.refresh($("#Table"));
						meshop.log("ok",tdelete);
					}).fail(function(e) {
						del.removeClass('loading');
						meshop.log("error", terror);
					}).always(function() {
						del.removeClass('loading');
						del.addClass("disabled");
					});
				}
			};
		});
		
		$('#newMesDoc').click(function(e) {
			 e.stopPropagation();
			 var modal = $('#new_file_modal');
			 
			 var insid = $('#message_print_modal input[name="insid"]').val();
			 if(insid) {
				 
				 modal.modal('show');
				 modal.meshop({
					 json : {
					 installments : {id:insid, title:insid},
				 },
				 });
			 }else {
				 modal.modal('show');
				 modal.meshop({
					 json : {
					 installments : {id:insid, title:insid},
				 },
				 });
			 }
			 
		}); 
		
		$('#saveDOC').click(function() {
			var formData = new FormData();
			 var form = $('form#doc_form');
			 
			 var modal = $('#new_file_modal');
		//formData = form.serializeArray();
			 $('input[type="file"]').each(function(i, input) {
					var oo = input.files[0];
					if (oo) {
						formData.append("myNewFileName", oo);
						 
					}
			 });
			 
			 var other_data = form.serializeArray();
			 $.each(other_data,function(key,input){
				 formData.append(input.name,input.value);
			 });
			 
				$.ajax({

					success : function(response) {
						
						meshop.log("ok", tok);
						 modal.modal('hide');
					},

					error : function(err) {
						
						if (err.responseText == '0') {
							meshop.log("error", trepeat);
						}else{
							meshop.log("error", "");
							
						}

					},
				

					data : formData,
					processData : false,
					contentType : false,
					url : "/adminpanel/json/doc/save",
					type : "POST",

				});

		});


});