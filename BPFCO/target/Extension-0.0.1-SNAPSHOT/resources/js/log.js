$(document).ready(function() {

	 
	
	 
	
	$("#Table").meshop({
		url : "/adminpanel/json/logs/load",

	}).on("render", function(obj, json) {
		
		 
	});
	
	
	 $('#searchDate').click(function(e) {
		 e.stopPropagation();
		 var modal = $('#serachDateModal');
		 modal.modal('show');
		 
	 }); 
	 $('#prev').click(function(e) {
		 var date = $('#yesterday').text();
		 
			$.ajax({
				url : "/changedate",
				type : "GET",
				async: true,
				 data: {date:date,day:true}
			}).done(function(fragment) {
				 $("#Buttons").replaceWith(fragment);
				 
				 
				 $("#Table").meshop({
						url : "/adminpanel/json/log/loadBydate?date="+date,
						 
//						data : form.serialize(),
					}).on("render", function(obj, json) {
						$("#DATE").text(' ['+json[0].today+']');
						$("#Table").removeClass('hidden');
						$("#empty").addClass('hidden');
					}).on("empty",function(){
						
						$('#searchLog').removeClass("disabled");
						$('#searchLog').removeClass("loading");
						$("#Table").addClass('hidden');
						$("#empty").removeClass('hidden');
					});
				 
			})
		 
	 }); 
	 $('#next').click(function(e) {
		 var date = $('#tommorow').text();
		 
		 $.ajax({
			 url : "/changedate",
			 type : "GET",
			 async: true,
			 data: {date:date,day:false}
		 }).done(function(fragment) {
			 
			 $("#Buttons").replaceWith(fragment);
			 $("#Table").meshop({
				 url : "/adminpanel/json/log/loadBydate?date="+date,
				 
//						data : form.serialize(),
			 }).on("render", function(obj, json) {
				 $("#Table").removeClass('hidden');
				 $("#empty").addClass('hidden');
			 }).on("empty",function(){
				 
				 $('#searchLog').removeClass("disabled");
				 $('#searchLog').removeClass("loading");
				 $("#Table").addClass('hidden');
				 $("#empty").removeClass('hidden');
			 });
			 
		 })
		 
	 }); 
	
	$('#searchLog').click(function() {
		 var modal = $('#serachDateModal');
		$('#searchLog,#resetModal').addClass("disabled");
		$(this).addClass('loading');
		var date = $('input[name="date"]').val();
		
		 $.ajax({
			 url : "/changeBydate",
			 type : "GET",
			 async: true,
			 data: {date:date}
		 }).done(function(fragment) {
			 $("#Buttons").replaceWith(fragment);
		
		$("#Table").meshop({
			url : "/adminpanel/json/log/loadBydate?date="+date,
			 
//			data : form.serialize(),
		}).on("render", function(obj, json) {
			$("#Table").removeClass('hidden');
			$("#empty").addClass('hidden');
			$('#searchLog').removeClass("disabled");
			$('#searchLog').removeClass("loading");
			 modal.modal('hide');
			 
			 
			 
			 
		}).on("empty",function(){
			
			$('#searchLog').removeClass("disabled");
			$('#searchLog').removeClass("loading");
			$("#Table").addClass('hidden');
			$("#empty").removeClass('hidden');
			modal.modal('hide');
//		 meshop.refresh($("#Table"));
		});
		

		 })
	});
	$('#today').click(function() {
		$(this).addClass('loading');
		var date = $(this).val();
		
		$.ajax({
			url : "/changedateToday",
			type : "GET",
			async: true,
			 data: {date:date}
		}).done(function(fragment) {
			 $("#Buttons").replaceWith(fragment);
		});	
		
		
		$("#Table").meshop({
			url : "/adminpanel/json/log/loadBydate?date="+date,
			
//			data : form.serialize(),
		}).on("render", function(obj, json) {
			$("#Table").removeClass('hidden');
			$("#empty").addClass('hidden');
			$('#today').removeClass("loading");
		}).on("empty",function(){
			
			$('#today').removeClass("loading");
			$("#Table").addClass('hidden');
			$("#empty").removeClass('hidden');
//		 meshop.refresh($("#Table"));
		});
	});
	 
});