$(document).ready(function() {

	$('input[type="file"]').ezdz();
	
	
	var name=null;
	var id=null;
	
	$("#Table").meshop({
		url : "/adminpanel/json/agents/load",
		page : $('[data-id=Table_page]'),
		modifyJson : modifyJson,
		loop : loop,
		filterForm:$('#filter-form')

	});
	
	
	function modifyJson(json) {

//		json.trclass = json.active ? '' : 'danger';
//		

		return json;
	}
	
		$("#new_agent").click(function(e) {
		var modal = $('#new_agent_modal');
		modal.modal('show');

		modal.meshop({
			json : {
			active : false,
			},
			loop : loopModal
		});

	});

	function loopModal(obj, json, mainobj) {

	  

	}
	
	function loop(obj, json, mainObj) {

		obj.data('id', json.id);
		obj.find('.docs').click(function(e) {
			e.stopPropagation();
			window.open("/adminpanel/docs"+"/?agentid="+json.id, "_blank");
		});

		obj.find('.scheck').click(function(e) {

			e.stopPropagation();
			var checkbox = obj.find(".selected");

			if (checkbox.hasClass('glyphicon-unchecked')) {
				checkbox.addClass(" glyphicon-check").removeClass(" glyphicon-unchecked ");
			} else {
				checkbox.addClass(" glyphicon-unchecked").removeClass(" glyphicon-check ");
				del.removeClass("disabled");
			}
			var count = $('span.glyphicon-check').map(function check_del(index, obj) {

				return $(obj).closest('tr').data('id');
			})
			if (count.length > 0) {

				del.removeClass("disabled");
			} else {
				del.addClass("disabled");
				$('#selectall').removeClass(" glyphicon-check").addClass(" glyphicon-unchecked ");
			}

		});


		obj.click(function(e) {
			e.stopPropagation();
			var modal = $('#new_agent_modal');
			modal.modal('show');
			modal.meshop({

				url : "/json/agent/load",
				params : {
					id : json.id,
				},
			// loop : loopModal,
			// modifyJson : modifyJsonModal
			})
			
			name=json.name;
			id=json.id;

		});

	}
	;
	$('#save').click(function() {
		var modal = $('#new_agent_modal');
		
		$('#save,#resetModal').addClass("disabled");
		$(this).addClass('loading');
		var form = $('#portfoy_form');
		$.ajax({
			url : "/adminpanel/json/agent/save",
			type : "POST",
			data : form.serialize(),
		}).done(function(respond) {
			// window.location =
			// "/";

			$('#new_agent_modal').modal('hide');
			meshop.refresh($("#Table"));
			meshop.log("ok", tok);
			modal.modal('hide');

		}).fail(function(e) {
			meshop.log("error", terror);

		}).always(function() {
			$('#save').removeClass('loading').removeClass('disabled');
			// meshop.refresh($("#Table"));
		});
//		 meshop.refresh($("#Table"));

	});
	
	$('#newDoc').click(function(e) {
		 e.stopPropagation();
		 var modal = $('#new_file_modal');
		 modal.modal('show');
		 modal.meshop({
				json : {
			 agents : {id:id, title:name},
				},
		 });
		 
	}); 
	
	$('#docs').click(function(e) {
		e.stopPropagation();
		 
		window.open("/adminpanel/docs"+"/?agentid="+id, "_blank");
	});
	
	$('#saveDOC').click(function() {
		var formData = new FormData();
		 var form = $('form#doc_form');
	//formData = form.serializeArray();
		 $('input[type="file"]').each(function(i, input) {
				var oo = input.files[0];
				if (oo) {
					formData.append("myNewFileName", oo);
					 
				}
		 });
		 
		 
		 
		 var other_data = form.serializeArray();
		 $.each(other_data,function(key,input){
			 formData.append(input.name,input.value);
		 });
		 
			$.ajax({

				success : function(response) {
					
					meshop.log("ok",tok);
					 var modal = $('#new_file_modal');
					 modal.modal("hide");
				},

				error : function(err) {
					
					if (err.responseText == '0') {
						meshop.log("error", trepeat);
					}else{
						meshop.log("error", "");
						
					}

				},
			

				data : formData,
				processData : false,
				contentType : false,
				url : "/adminpanel/json/doc/save",
				type : "POST",

			});

	});

});