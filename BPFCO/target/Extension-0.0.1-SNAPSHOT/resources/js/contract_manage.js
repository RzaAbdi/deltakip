$(document).ready(function() {
	
	Spinner();
	Spinner.show();
	
	var object;
	
	$('input[type="file"]').ezdz();
	var pathname = window.location.href;
	path = pathname.split('?')[1];
	if(path!= null) {
	url =	"/json/contracts/load"+"?"+path;
	}else {
		url =	"/json/contracts/load";
	}
	
	
	var contarcno=null;
	var cid=null;

	var order = 0;
	$("#Table").meshop({
		url : url,
		page : $('[data-id=Table_page]'),
		modifyJson : modifyJson,
		loop : loop,
		filterForm:$('#filter-form')

	});
	
$('#print').click(function(e) {
		
	$("#modalDiv").printThis({ 
	    debug: false,              
	    importCSS: true,             
	    importStyle: true,         
	    printContainer: true,       
	    loadCSS: "../css/style.css", 
	    pageTitle: "My Modal",             
	    removeInline: false,        
	    printDelay: 333,            
	    header: null,             
	    formValues: true          
	}); 
	});


$('.printthis').click(function(e) {
	
	window.frames["obj"].focus();
	window.frames["obj"].print();

	}); 
var click = false;

$("input.daterange").click(function(e) {
	
	if(click==false) {
		
		$('input[name="daterange"]').daterangepicker({
			opens: 'left',
			
			
		}, function(start, end, label) {
			console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
		});
	}
	click=true;
});
	
	function modifyJson(json) {

		json.trclass = json.active ? '' : 'danger';
		json.installment = json.installment ? 'taksit' : 'viewtaksit';
		json.hasDoc = json.lastDocScan ? 'glyphicon-check' : 'glyphicon-unchecked';
		
		
		return json;
	}
 
	
		$("#new_contract").click(function(e) {
		var modal = $('#new_contract_modal');
		modal.modal('show');

		modal.meshop({
			json : {
				active : false,
			},
			loop : loopModal
		});

	});

	function loopModal(obj, json, mainobj) {
		
		
		obj.find('img.icon_pic2').parent().click(function(e) {//click obj on check docs modal
			e.stopPropagation();
			
			if(json.exist) {
				var modal = $('#pdf_modal');
				var cssLink = document.createElement("link");
				cssLink.href = "/resources/css/siniq.css"; 
				cssLink.rel = "stylesheet"; 
				cssLink.type = "text/css"; 
				
				modal.meshop({
					json : {
						path : json.path,
						clas : "top3",
					},
					
				// loop : loopModal
				}).on("render", function(obj, json2) {
					modal.modal('show');
					setTimeout(function() { 
						frames['obj'].document.head.appendChild(cssLink);
				    }, 500);
					
					
//					Spinner.hide();
				});
				
				
			}else {
				//add file
				
				
				
				
			}

		});
		obj.find('.addDoc').parent().click(function(e) {//click obj on check docs modal
			e.stopPropagation();
			
			 
			 e.stopPropagation();
			 var modal = $('#new_file_modal');
			 
			 var customers = {};
			 object.customers.forEach(function(item) {
				 customers["id"] = item.id;
				 customers["title"] = item.name;
				});
			 
			 
			 modal.modal('show');
			 modal.meshop({
					json : {
				 contracts : {id: object.id, title:object.contract_no},
				 doctype: {id: json.typeId, title:json.type},
				 customers: customers,
					},
			 });
				
		 
			
		});
		obj.click(function(e) {//click obj on check docs modal
			e.stopPropagation();
			
			  
			
			if(json.id) {
				
				window.open("/docs"+"/?docType="+json.typeId+"&cid="+json.cid, "_blank");
			}
			
			
		});
		
		
		

obj.find("input[name='cash']" ).blur(function() {
	
			
			var val =  parseFloat(this.value.replace(/,/g, ""));
			
			var fix =  val.toFixed(2);
			
			var price = fix.toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			
			$(this).val(price );
			
//			setRemining();
			
    
	});



obj.find("input[name='price']" ).blur(function() {
	
	var val =  parseFloat(this.value.replace(/,/g, ""));
	
	var fix =  val.toFixed(2);
	
	var price = fix.toString()
			.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	
	$(this).val(price );
	
//			setRemining();
	
	
});
obj.find("input[name='amount']" ).blur(function() {
	
	var val =  parseFloat(this.value.replace(/,/g, ""));
	
	var fix =  val.toFixed(2);
	
	var price = fix.toString()
			.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	
	$(this).val(price );
	
//			setRemining();
	
	
});

	}
	
	var cancel = $('#cancel');
	
	var active = $('#active');
	
	function loop(obj, json, mainObj) {
		
		if(json.lastDocScan) {
			
			obj.find('[data-toggle="tooltip"]').tooltip();  
		}

		
		obj.find('#add_ins').click(function(e) {
			e.stopPropagation();
			var modal = $('#new_installment_modal');
			modal.modal('show');

			modal.meshop({
				json : {
					active : false,
					contract_no: json.id
					
				},
				loop : loopModal
				
			});
			
			$("#InsTable").meshop({
				url : '/adminpanel/json/installments/load?id='+json.id,
				page : $('[data-id=Table_page]'),
				

			});

			
		});
		obj.find('#view_ins').click(function(e) {
			e.stopPropagation();
			window.open("/installment_manage"+"/?contracts="+json.id, "_blank");
			
			
		});
		obj.find('#view_ins').click(function(e) {
			e.stopPropagation();
			Spinner.show();
		});
		obj.find('.pervent').click(function(e) {
			e.stopPropagation();
		});
		obj.find('#docs').click(function(e) {
			e.stopPropagation();
			 
			window.open("/docs"+"/?cid="+json.id, "_blank");
		});
		obj.find('#check').click(function(e) {
			e.stopPropagation();
			Spinner();
			Spinner.show();
			var modal = $('#check_modal');
			modal.meshop({

				url : "/adminpanel/json/document/check",
				params : {
					id : json.id,
				},
			 loop : loopModal,
			 modifyJson : modifyJsonModal
			}).on("render", function(obj, json2) {
				modal.modal('show');
				
				
				object = json;
//				Spinner.hide();
			});
			
			
	 
			
		});
		obj.find('#newDoc').click(function(e) {
			 e.stopPropagation();
			 var modal = $('#new_file_modal');
			 
			 
			 var customers = {};
			 json.customers.forEach(function(item) {
				 customers["id"] = item.id;
				 customers["title"] = item.name;
				});
			 
			 
			 
			 
			 modal.modal('show');
			 modal.meshop({
					json : {
				 contracts : {id: json.id, title:json.contract_no},
				 customers: customers
					},
			 });
			 
		}); 
		
		obj.find('#edit').click(function(e) {
			e.stopPropagation();
			var modal = $('#new_contract_modal');
			modal.modal('show');
			modal.meshop({

				url : "/json/contract/load",
				params : {
					id : json.id,
				},
			 loop : loopModal,
			// modifyJson : modifyJsonModal
			})
			
			
		});
		obj.find('.customer').click(function(e) {
			e.stopPropagation();
			 
			
			
		});
		obj.click(function(e) {
			
			e.stopPropagation();
			
			
			if(e.altKey) {
				 
				 
	 e.stopPropagation();
	 
	 var win = window.open("/contracts"+"/?listId="+json.id, '_blank');
	  win.focus();
	 
				 
			    } else{
			    	

					var btn = obj;
					// printable 
					btn.addClass('loading')
					var modal = $('#contract_modal');
					
					modal.meshop({

						url : "/json/contract/load",
						params : {
							id : json.id,
						},
					// loop : loopModal,
					  
					}).on("render", function(obj, json) {
						
						
						contarcno = json.contract_no;
								cid = json.id;
						
						
						btn.removeClass('loading')
						modal.modal('show');
						
					})
					

			    }
			
			  
			 
			
		});

		obj.data('id', json.id);
		 

		obj.find('.scheck').click(function(e) {

			e.stopPropagation();
			var checkbox = obj.find(".selected");

			if (checkbox.hasClass('glyphicon-unchecked')) {
				checkbox.addClass(" glyphicon-check").removeClass(" glyphicon-unchecked ");
			} else {
				checkbox.addClass(" glyphicon-unchecked").removeClass(" glyphicon-check ");
				
				if(json.active) {
					cancel.removeClass("disabled");
				}else {
					active.removeClass("disabled");
				}
				
			}
			var count = $('span.glyphicon-check').map(function check_del(index, obj) {

				return $(obj).closest('tr').data('id');
			})
			if (count.length > 0) {
				
				if(json.active) {
					cancel.removeClass("disabled");
				}else {
					active.removeClass("disabled");
				}

			} else {
				if(json.active) {
					cancel.addClass("disabled");
				}else {
					active.addClass("disabled");
				}
				
				$('#selectall').removeClass(" glyphicon-check").addClass(" glyphicon-unchecked ");
			}

		});


		

	}
	;
	
	function modifyJsonModal(json) {

		json.trclass = json.exist ? 'success' : 'danger';
		
		
		if(json.saved) {
			
			json.trclass =  'warning';
		}
		 
		
		
		return json;
	}
	$('#save').click(function() {
		$('#save,#resetModal').addClass("disabled");
		$(this).addClass('loading');
		var form = $('#portfoy_form');
		$.ajax({
			url : "/adminpanel/json/contract/save",
			type : "POST",
			data : form.serialize(),
		}).done(function(respond) {
			// window.location =
			// "/";

			$('#new_portfoy_modal').modal('hide');
			meshop.refresh($("#Table"));
			meshop.log("ok", tok);

		}).fail(function(e) {
			
			if(e.responseText ="999") {
				meshop.log("error", "Bu Portfoy halen baska bir solzesmede mevcuttur");
			}else {
				
				meshop.log("error", terror);
			}
			

		}).always(function() {
			$('#save').removeClass('loading').removeClass('disabled');
			// meshop.refresh($("#Table"));
		});
//		 meshop.refresh($("#Table"));

	});
	
	
	
	cancel.click(function(e) {
		var modal = meshop.alert("ask", "Iptal Edilecek! Emin misiniz?", "UYARI");
		modal.size("sm");
		modal.yes = function() {
			e.preventDefault();
			cancel.addClass('loading');
			var selecteditems = "";
			var count = $('span.glyphicon-check.selected').map(function(index, obj) {
				return $(obj).closest('tr').data('id');
			})
			var selecteditems = count.get().join();

			if (selecteditems) {
				$.post("/adminpanel/json/contract/cancel", {
					ids : selecteditems
				}, function(data) {
					meshop.refresh($("#Table"));
					meshop.log("ok", tok);
					cancel.removeClass('loading');
					cancel.addClass("disabled");
				}).fail(function(e) {
					cancel.removeClass('loading');
					meshop.log("error", terror);
				}).always(function() {
					cancel.removeClass('loading');
					cancel.addClass("disabled");
				});
			}
		};
	});
	active.click(function(e) {
		var modal = meshop.alert("ask", " Yeniden Aktive Edilecek! Emin misiniz?", "UYARI");
		modal.size("sm");
		modal.yes = function() {
			e.preventDefault();
			active.addClass('loading');
			var selecteditems = "";
			var count = $('span.glyphicon-check.selected').map(function(index, obj) {
				return $(obj).closest('tr').data('id');
			})
			var selecteditems = count.get().join();

			if (selecteditems) {
				$.post("/adminpanel/json/contract/active", {
					ids : selecteditems
				}, function(data) {
					meshop.refresh($("#Table"));
					meshop.log("ok", tok);
					active.removeClass('loading');
					active.addClass("disabled");
				}).fail(function(e) {
					active.removeClass('loading');
					meshop.log("error", terror);
				}).always(function() {
					active.removeClass('loading');
					active.addClass("disabled");
				});
			}
		};
	});
	$('#save_ins').click(function() {
		var modal = $('#new_installment_modal');
		
		
		$('#save_ins,#resetModal').addClass("disabled");
		$(this).addClass('loading');
		var form = $('#installment_form');
		var id = $('#installment_form input#contract_no').val()
		$.ajax({
			url : "/adminpanel/json/installment/save",
			type : "POST",
			data : form.serialize(),
		}).done(function(respond) {
			// window.location =
			// "/";

//			$('#new_installment_modal').modal('hide');
			meshop.refresh($("#Table"));
			meshop.log("ok", tok);
			$("#InsTable").meshop({
				url : 'json/installments/load?id='+id,
				page : $('[data-id=Table_page]'),

			});
			
			 
			

		}).fail(function(e) {
			meshop.log("error", terror);
			meshop.refresh($("#InsTable"));

		}).always(function() {
			$('#save_ins').removeClass('loading').removeClass('disabled');
			// meshop.refresh($("#Table"));
		});
//		 meshop.refresh($("#Table"));

	});
	
	
	
	
	
	$('#docs').click(function(e) {
		e.stopPropagation();
		 
		window.open("/adminpanel/docs"+"/?cid="+cid, "_blank");
	});
	
	$('#saveDOC').click(function() {
		var formData = new FormData();
		 var form = $('form#doc_form');
	//formData = form.serializeArray();
		 $('input[type="file"]').each(function(i, input) {
				var oo = input.files[0];
				if (oo) {
					formData.append("myNewFileName", oo);
					 
				}
		 });
		 
		 
		 
		 var other_data = form.serializeArray();
		 $.each(other_data,function(key,input){
			 formData.append(input.name,input.value);
		 });
		 
			$.ajax({

				success : function(response) {
					
					meshop.log("ok", tok);
					 var modal = $('#new_file_modal');
					 modal.modal("hide");
					 meshop.refresh($('#check_modal'));
				},

				error : function(err) {
					
					if (err.responseText == '0') {
						meshop.log("error", trepeat);
					}else{
						meshop.log("error", terror);
						
					}

				},
			

				data : formData,
				processData : false,
				contentType : false,
				url : "/adminpanel/json/doc/save",
				type : "POST",

			});

	});
	
	

});