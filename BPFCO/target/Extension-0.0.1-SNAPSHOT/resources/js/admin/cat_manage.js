$(document).ready(function() {

	var url;
	var selected;
	
	var MODAL = {
	         CATEGORY: 0,
	         S_CATEGORY: 1,
	         SS_CATEGORY : 2
	}

	$("#Table").meshop({
		url : "/adminpanel/json/categories/load",
		page : $('[data-id=Table_page]'),
		modifyJson : modifyJson,
		loop : loop,
		filterForm : $('#filter-form')
	});

	function modifyJson(json) {


		json.status = json.active ? "cat_active" : "cat_deactive ";

		return json;
	}

	function loop(obj, json, mainObj) { // 2.loop

		obj.data('id', json.id);

		// $.each(json.subCats,function(i,v){
		//			
		// // $.each(v.picpath,function(i,l){
		//				
		// var src = '/images/'+v.picpath+'.jpg';
		// mainObj.find("img.example-image.sub_cat").attr("src",src+'?lastmod='+Math.random());
		//			
		// // });
		// });

		// var src = '/images/'+item.picpath+'.jpg';

		obj.click(function() {
			var modal = $('#subcat-modal');
			modal.modal('show');
			GetCatModal();
			modal.meshop({

				url : "/adminpanel/json/category/load",
				params : {
					id : json.id,
				},

				modifyJson : modifyJson_cat,
			})

		});

		var sub = obj.find($("div [data-render='subCats']"));
		var subsub = obj.find(("div [data-render='subCats.subsubCats']"));
		sub.click(function xxx(e) {

			e.stopPropagation();

			$("#setAspectRatio").trigger("click");

			var subIs = $(this).find("span.id").text();
			$(".pic_body").append('<img src="/images/' + $(this).find("span.path").text() + '.jpg" class="img-responsive img_full" />');
			$("div.sub").removeClass("hidden");
			GetSubModal();
			var modal = $('#subcat-modal');
			modal.modal('show');
			modal.meshop({

				url : "/adminpanel/json/sub-category/load",
				params : {
					id : subIs,
				},

				modifyJson : modifyJson_sub,
			})
		});

		sub.mouseleave(function() { 
			clearTimeout($(this).data('timeoutId'));
		}).mouseenter(function() {

			var elm = $(this).find(("div [data-render='subCats.subsubCats']"));
			var someElement = $(this), timeoutId = setTimeout(function() {

				if (elm.hasClass('opacity-false')) {
					
					elm.removeClass("opacity-false").addClass("opacity-true").animate({height: '100%'}, 1700);
					

//					elm.removeClass('opacity-false');
//					elm.addClass('opacity-true');

//				} else {
//					subsub.addClass('opacity-false');
//					subsub.removeClass('opacity-true');
//				}
				}
			}, 800);
			someElement.data('timeoutId', timeoutId);
		});
		
		subsub.click(function(e){
			
			e.stopPropagation();

			$("#setAspectRatio").trigger("click");

			var subsubId = $(this).find("span.ssid").text();
			$(".pic_body").append('<img src="/images/' + $(this).find("span.path").text() + '.jpg" class="img-responsive img_full" />');
			GetSubSubModal();
			
			var modal = $('#subcat-modal');
			modal.modal('show');
			modal.meshop({

				url : "/adminpanel/json/subsub-category/load",
				params : {
					id : subsubId,
				},

				modifyJson : modifyJson_subsub,
			})
			
		});

	};
	
	$('body').dblclick(function() {
		$("div[data-render='subCats.subsubCats']").addClass('opacity-false').removeClass('opacity-true');
	
		});
	 

	function modifyJson_subsub(json) {
		
		 
			
			json.visi = "hidden";
			json.acc = "hidden";
		 
			json.sisi = ""
		 
		return json;
	}
	function modifyJson_cat(json) {

		if (json.category) {

			json.visi = "";
			json.acc = "";
		} else {

			json.sisi = "hidden"
			json.visi = "hidden";
		}
		return json;
	}
	function modifyJson_sub(json) {
		if (json.category) {

			json.visi = "";
			json.sisi = "hidden";
			json.acc = "";
		} else {

			json.visi = "hidden";
		}
		return json;
	}

	$('#addnew-cat').click(function(e) { //

		e.preventDefault();

		$("#setAspectRatio").trigger("click");
		var modal = $('#subcat-modal');
		modal.modal('show');
		GetCatModal();
		modal.meshop({
			json : {
				active : false,
				visi : "hidden",
				sisi : "hidden",
				acc : ""
			}

		});
	});
	$('#addnew-subcat').click(function(e) { //
		e.preventDefault();

		$("#setAspectRatio").trigger("click");
		var modal = $('#subcat-modal');
		modal.modal('show');
		GetSubModal();
		modal.meshop({
			json : {
				active : true,
				visi : "",
				sisi : "hidden",
				acc : ""
			}
		});
	});
	$('#addnew-subsubcat').click(function(e) { //
		e.preventDefault();

		$("#setAspectRatio").trigger("click");
		var modal = $('#subcat-modal');
		modal.modal('show');
		GetSubSubModal();
		modal.meshop({
			json : {
				active : true,
				visi : "hidden",
				sisi : "",
				acc : "hidden"
			}
		});
	});

	$(".btn_upload").click(function(e) {

		$("#upload").trigger("click");

	});

	$('#save_cat').click(function() {
		
		
		switch (selected) {
		case MODAL.CATEGORY:

			
		 
			
			break;
		case MODAL.S_CATEGORY:
			
			if (meshop.check.length($("input[name='category']"), 1)

			){
				
			}else{
				
				meshop.log("warn", "لوطفن بیر اوست بؤلوم سئچینیز");
				return;
			}
			
			 
			
			break;
		case MODAL.SS_CATEGORY:
			if (meshop.check.length($("input[name='subcategories']"), 1)
					
			){
				
			}else{
				
				meshop.log("warn", "لوطفن بیر اوست بؤلوم سئچینیز");
				return;
			}
		 
			
			break;

		default:
		
			
			 
		}
 
		
 

		$('#save_cat,#resetModal').addClass("disabled");
		$(this).addClass('loading');
		var form = $('#cat_form');

		var formdata = new FormData();
		 
		var serilized = form.serializeArray();
		$.each(serilized, function(key, input) {
			formdata.append(input.name, input.value);
		});

		$.ajax({
			url : url,
			type : "POST",
			data : formdata,
			// data: formdata + form.serialize(),
			processData : false,
			contentType : false,
		}).done(function(respond) {
			// window.location = "/";

			$('#subcat-modal').modal('hide');
			meshop.refresh($("#Table"));

		}).fail(function(e) {
			alert("Hata");

		}).always(function() {
			$('#save_cat').removeClass('loading').removeClass('disabled');
			// meshop.refresh($("#Table"));
		});
		// meshop.refresh($("#Table"));

	});

	function getProfilePic() {

		var canvas = $("#my_profile")
		var domImg = new Image();
		domImg.src = canvas.get(0).toDataURL();

		return domImg.src;

	}

	function GetSubSubModal() {
		url = "/adminpanel/json/subsubcategory/save";
		selected = MODAL.SS_CATEGORY;
		
	}
	function GetSubModal() {
		url = "/adminpanel/json/subcategory/save";
		selected = MODAL.S_CATEGORY;
	}
	function GetCatModal() {
		url = "/adminpanel/json/category/save";
		selected = MODAL.CATEGORY;
	}

	$('#subcat-modal').on('hidden.bs.modal', function() {

		$(".pic_body img").remove();
		$(".pic_body canvas").remove();
		$('#subcat-modal').off('keyup.dismiss.modal');
		$("div#cat").css("display", "block")
		selected = -1;

	})

});
