$(document).ready(function() {

	$("#Table").meshop({
		url : "/adminpanel/json/doctypes/load",
		page : $('[data-id=Table_page]'),
//		modifyJson : modifyJson,
		loop : loop,
	// filterForm:$('#filter-form')
	});
	
	 

	function loop(obj, json, mainObj) {

		obj.data('id', json.id);

		obj.find('.scheck').click(function(e) {

			e.stopPropagation();
			var checkbox = obj.find(".selected");

			if (checkbox.hasClass('glyphicon-unchecked')) {
				checkbox.addClass(" glyphicon-check").removeClass(" glyphicon-unchecked ");
			} else {
				checkbox.addClass(" glyphicon-unchecked").removeClass(" glyphicon-check ");
				del.removeClass("disabled");
			}
			var count = $('span.glyphicon-check').map(function check_del(index, obj) {

				return $(obj).closest('tr').data('id');
			})
			if (count.length > 0) {

				del.removeClass("disabled");
			} else {
				del.addClass("disabled");
				$('#selectall').removeClass(" glyphicon-check").addClass(" glyphicon-unchecked ");
			}

		});


		obj.click(function(e) {
			e.stopPropagation();
			var modal = $('#new_belgeturu_modal');
			modal.modal('show');
			modal.meshop({

				url : "/adminpanel/json/doctype/load",
				params : {
					id : json.id,
				},
			// loop : loopModal,
//			 modifyJson : modifyJsonModal
			})

		});

	}
	;
	
	 

	$("#new_doctype").click(function(e) {

		var modal = $('#new_belgeturu_modal');
		modal.modal('show');
 
		modal.meshop({
			json : {
				id : null,
			},
		// loop : loopModal
		});

	});
	$("#docControl").click(function(e) {
		
		var modal = $('#checkDocModal');
		
		modal.modal('show');
		
		modal.meshop({
			url : "/adminpanel/json/doctypes/loadChecked",
		 
		}).on("render", function(obj, json) {
			modal.modal('show');
			
		});
	}).on("empty", function() {
		
	 
		
	});

	$('#saveCheckedDoc').click(function() {
		$('#saveCheckedDoc,#resetModal').addClass("disabled");
		$(this).addClass('loading');
		var form = $('#checkDoc_form');
		$.ajax({
			url : "/adminpanel/json/doctype/saveChecked",
			type : "POST",
			data : form.serialize(),
		}).done(function(respond) {
			// window.location =
			// "/";
			
			$('#checkDocModal').modal('hide');
			meshop.log("ok", tok);
			
		}).fail(function(e) {
			meshop.log("error",terror);
			
		}).always(function() {
			$('#saveCheckedDoc').removeClass('loading').removeClass('disabled');
			// meshop.refresh($("#Table"));
		});
		// meshop.refresh($("#Table"));
		
	});
	$('#save').click(function() {
		$('#save,#resetModal').addClass("disabled");
		$(this).addClass('loading');
		var form = $('#type_form');
		$.ajax({
			url : "/adminpanel/json/doctype/save",
			type : "POST",
			data : form.serialize(),
		}).done(function(respond) {
			// window.location =
			// "/";

			$('#new_belgeturu_modal').modal('hide');
			meshop.refresh($("#Table"));
			meshop.log("ok", tok);

		}).fail(function(e) {
			meshop.log("error", terror);

		}).always(function() {
			$('#save').removeClass('loading').removeClass('disabled');
			// meshop.refresh($("#Table"));
		});
		// meshop.refresh($("#Table"));

	});

	// // SELECT OR DESELECT ALL OF INSTALLMENT AND CHANGE
	// DELETE BTN//
	var del = $('#delete_tag')
	var selectall = $('#selectall');
	selectall.parent().click(function(e) {
		if (selectall.hasClass('glyphicon-unchecked')) {
			check(selectall);
			check($(".selected"));
			del.removeClass("disabled");
		} else {
			uncheck(selectall);
			uncheck($(".selected"));
			del.addClass("disabled");
		}

		function check(obj) {
			obj.addClass(" glyphicon-check").removeClass(" glyphicon-unchecked ");
		}
		function uncheck(obj) {
			obj.removeClass(" glyphicon-check ").addClass(" glyphicon-unchecked");
		}

	});

	del.click(function(e) {
		var modal = meshop.alert("ask", "سئچدیینیز دامغا/لار سیلینه جک . امین می سینیز؟", "اویاری");
		modal.size("sm");
		modal.yes = function() {
			e.preventDefault();
			del.addClass('loading');
			var selecteditems = "";
			var count = $('span.glyphicon-check.selected').map(function(index, obj) {
				return $(obj).closest('tr').data('id');
			})
			var selecteditems = count.get().join();

			if (selecteditems) {
				$.post("/adminpanel/json/tag/remove", {
					ids : selecteditems
				}, function(data) {
					meshop.refresh($("#Table"));
					meshop.log("ok", "دامغا باشارییلا سیلیندی");
				}).fail(function(e) {
					del.removeClass('loading');
					meshop.log("error", "سیلینیرکن بیر سورون اولوشدو. لوطفن یئنه دنه‌یین");
				}).always(function() {
					del.removeClass('loading');
					del.addClass("disabled");
				});
			}
		};
	});

});