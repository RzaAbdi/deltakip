$(document).ready(function() {
	
	
	var pathname = window.location.href;
	path = pathname.split('?')[1];
	if(path!= null) {
	url =	"/json/docs/load"+"?"+path;
	}else {
		url =	"/json/docs/load";
	}
	 
			

	$("#Table").meshop({
		url : url,
		page : $('[data-id=Table_page]'),
		modifyJson : modifyJson,
		loop : loop,
		filterForm:$('#filter-form')
	}).on("empty",function(){
		
		$("#Table").addClass('hidden');
		$('[data-id=Table_page]').addClass('hidden');
		$("#empty").removeClass('hidden');
	});
	 
var click = false;
	
	$("input.daterange").click(function(e) {
		
		if(click==false) {
			
			$('input[name="daterange"]').daterangepicker({
				opens: 'left',
				
				
			}, function(start, end, label) {
				console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
			});
		}
		click=true;
		
	});
	
	$('input[type="file"]').ezdz();

	function modifyJson(json) {
		 return json;
	}
	
		$('#print').click(function(e) {
			
			window.frames["obj"].focus();
			window.frames["obj"].print();
	  
			});

	function loop(obj, json, mainObj) {

		obj.data('id', json.id);

		obj.find('td img.icon_pic').click(function(e) {
			e.stopPropagation();
			var modal = $('#pdf_modal');
			
			var cssLink = document.createElement("link");
			cssLink.href = "/resources/css/siniq.css"; 
			cssLink.rel = "stylesheet"; 
			cssLink.type = "text/css"; 
			
			
			modal.meshop({
				json : {
					path : json.path,
				},
			// loop : loopModal
			}).on("render", function(obj, json2) {
				modal.modal('show');
				setTimeout(function() { 
					frames['obj'].document.head.appendChild(cssLink);
			    }, 500);
				
				
//				Spinner.hide();
			});
			
			
			
			
			
					
		});
		obj.find('.scheck').click(function(e) {

			e.stopPropagation();
			var checkbox = obj.find(".selected");

			if (checkbox.hasClass('glyphicon-unchecked')) {
				checkbox.addClass(" glyphicon-check").removeClass(" glyphicon-unchecked ");
			} else {
				checkbox.addClass(" glyphicon-unchecked").removeClass(" glyphicon-check ");
				del.removeClass("disabled");
			}
			var count = $('span.glyphicon-check').map(function check_del(index, obj) {

				return $(obj).closest('tr').data('id');
			})
			if (count.length > 0) {

				del.removeClass("disabled");
			} else {
				del.addClass("disabled");
				$('#selectall').removeClass(" glyphicon-check").addClass(" glyphicon-unchecked ");
			}

		});


		obj.click(function(e) {
			e.stopPropagation();
			var modal = $('#new_file_modal');
			modal.modal('show');
			modal.meshop({

				url : "/json/doc/load",
				params : {
					id : json.id,
				},
			// loop : loopModal,
//			 modifyJson : modifyJsonModal
			})

		});

	}
	;
	
	 

	$(".new_file").click(function(e) {

		var modal = $('#new_file_modal');
		modal.modal('show');
		$("#setAspectRatio").trigger("click");
		setTimeout(function() {
			$('#saveCanvas').click();
		}, 200);
		modal.meshop({
			json : {
				id : null,
			},
		// loop : loopModal
		});

	});

	$('#saveDOC').click(function() {
		var formData = new FormData();
		 var form = $('form#doc_form');
//	formData = form.serializeArray();
		 $('input[type="file"]').each(function(i, input) {
				var oo = input.files[0];
				if (oo) {
					formData.append("myNewFileName", oo);
					 
				}
		 });
		 
		 var other_data = form.serializeArray();
		 $.each(other_data,function(key,input){
			 formData.append(input.name,input.value);
		 });
		 
			$.ajax({

				success : function(response) {
					
					meshop.log("ok", tok);
					meshop.refresh($("#Table"));
				},

				error : function(err) {
					
					if (err.responseText == '2') {
						meshop.log("error", trepeat);
					}else{
						meshop.log("error", "");
						
					}

				},
			

				data : formData,
				processData : false,
				contentType : false,
				url : "/adminpanel/json/doc/save",
				type : "POST",

			});

	});

	// // SELECT OR DESELECT ALL OF INSTALLMENT AND CHANGE
	// DELETE BTN//
	var del = $('.delete-doc')
	var selectall = $('#selectall');
	selectall.parent().click(function(e) {
		if (selectall.hasClass('glyphicon-unchecked')) {
			check(selectall);
			check($(".selected"));
			del.removeClass("disabled");
		} else {
			uncheck(selectall);
			uncheck($(".selected"));
			del.addClass("disabled");
		}

		function check(obj) {
			obj.addClass(" glyphicon-check").removeClass(" glyphicon-unchecked ");
		}
		function uncheck(obj) {
			obj.removeClass(" glyphicon-check ").addClass(" glyphicon-unchecked");
		}

	});

	del.click(function(e) {
		var modal = meshop.alert("ask", "Belge(ler) Silinecek. Emin misiniz?", "Uyarı");
		modal.size("sm");
		modal.yes = function() {
			e.preventDefault();
			del.addClass('loading');
			var selecteditems = "";
			var count = $('span.glyphicon-check.selected').map(function(index, obj) {
				return $(obj).closest('tr').data('id');
			})
			var selecteditems = count.get().join();

			if (selecteditems) {
				$.post("/adminpanel/json/doc/remove", {
					ids : selecteditems
				}, function(data) {
					meshop.refresh($("#Table"));
					meshop.log("ok", tdelete);
				}).fail(function(e) {
					del.removeClass('loading');
					meshop.log("error", terror);
				}).always(function() {
					del.removeClass('loading');
					del.addClass("disabled");
				});
			}
		};
	});
	
	
 

});