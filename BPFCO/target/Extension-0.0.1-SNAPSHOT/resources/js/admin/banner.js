$(document).ready(function() {

	$('tbody').sortable();
	var del = $('#delete')

	$("#Table").meshop({
		url : "/adminpanel/json/banners/load",
		page : $('[data-id=Table_page]'),
		modifyJson : modifyJson,
		loop : loop,
	// filterForm:$('#filter-form')
	});

	function modifyJson(json) {

		json.trclass = json.active ? '' : 'danger';

		return json;
	}

	function loop(obj, json, mainObj) {

		obj.data('id', json.id);

		obj.click(function(e) {
			e.stopPropagation();
			var modal = $('#new_banner_modal');
			modal.modal('show');
			modal.meshop({

				url : "/adminpanel/json/banner/load",
				params : {
					id : json.id,
				},
			 loop : loopModal,
			// modifyJson : modifyJsonModal
			})

		});
		obj.find('.preview').click(function(e) {
			
			e.stopPropagation();
		});
		obj.find('.scheck').click(function(e) {
			
			e.stopPropagation();
			var del = $('#delete')
			var checkbox = obj.find(".selected");
			
			if (checkbox.hasClass('glyphicon-unchecked')) {
				checkbox.addClass(" glyphicon-check").removeClass(" glyphicon-unchecked ");
			} else {
				checkbox.addClass(" glyphicon-unchecked").removeClass(" glyphicon-check ");
				del.removeClass("disabled");
			}
			var count = $('span.glyphicon-check').map(function check_del(index, obj) {

				return $(obj).closest('tr').data('id');
			})
			if (count.length > 0) {

				del.removeClass("disabled");
			} else {
				del.addClass("disabled");
				$('#selectall').removeClass(" glyphicon-check").addClass(" glyphicon-unchecked ");
			}

		});
				
		

	}
	;

	$("#new_banner").click(function(e) {

		var modal = $('#new_banner_modal');
		modal.modal('show');
		$("#setAspectRatio").trigger("click");
		setTimeout(function() {
			$('#saveCanvas').click();
		}, 200);
		modal.meshop({
			json : {
				id : null,
			},
			loop : loopModal
		});

	});
	
	del.click(function(e) {
		var modal = meshop.alert("ask", "Banner Silinsin mi?", "UYARI");
		modal.size("sm");
		modal.yes = function() {
			e.preventDefault();
			del.addClass('loading');
			var selecteditems = "";
			var count = $('span.glyphicon-check.selected').map(function(index, obj) {
				return $(obj).closest('tr').data('id');
			})
			var selecteditems = count.get().join();

			if (selecteditems) {
				$.post("/adminpanel/json/banner/remove", {
					ids : selecteditems
				}, function(data) {
					meshop.refresh($("#Table"));
					meshop.log("ok", tdelete);
					del.removeClass('loading');
					del.addClass("disabled");
				}).fail(function(e) {
					del.removeClass('loading');
					meshop.log("error", terror);
				}).always(function() {
					del.removeClass('loading');
					del.addClass("disabled");
				});
			}
		};
	});


	function loopModal(obj, json, mainobj) {

		$('.glyphicon-open-file.banner-tr').click(function(e) {
			$(this).find('input[type="file"]').click();
		});

		$('.glyphicon-open-file.banner-en').click(function(e) {
			$(this).find('input[type="file"]').click();
		});

		$('.glyphicon-open-file.banner-ru').click(function(e) {
			$(this).find('input[type="file"]').click();
		});

		$('.glyphicon-open-file.banner-ar').click(function(e) {
			$(this).find('input[type="file"]').click();
		});

		$('.glyphicon-open-file.banner-tr input').click(function(e) {
			e.stopPropagation();
		});

		$('.glyphicon-open-file.banner-en input').click(function(e) {
			e.stopPropagation();
		});

		$('.glyphicon-open-file.banner-ru input').click(function(e) {
			e.stopPropagation();
		});

		$('.glyphicon-open-file.banner-ar input').click(function(e) {
			e.stopPropagation();
		});

		$('#banner_tr_file').change(function() { // select banner
			var ext = $(this).val().split('.').pop().toLowerCase();
			if ($.inArray(ext, [ 'jpg', 'jpeg' ]) == -1) {
				meshop.log("error", "Sadece .jpg yüklenebilir");
			} else {
				$("input[name='banner_path_tr']").val(this.value || 'Nothing selected').removeAttr('disabled');
			}
		});
		$('#banner_en_file').change(function() { // select banner
			var ext = $(this).val().split('.').pop().toLowerCase();
			if ($.inArray(ext, [ 'jpg', 'jpeg' ]) == -1) {
				meshop.log("error", "Sadece .jpg yüklenebilir");
			} else {
				$("input[name='banner_path_en']").val(this.value || 'Nothing selected').removeAttr('disabled');
			}
		});
		$('#banner_ru_file').change(function() { // select banner
			var ext = $(this).val().split('.').pop().toLowerCase();
			if ($.inArray(ext, [ 'jpg', 'jpeg' ]) == -1) {
				meshop.log("error", "Sadece .jpg yüklenebilir");
			} else {
				$("input[name='banner_path_ru']").val(this.value || 'Nothing selected').removeAttr('disabled');
			}
		});
		$('#banner_ar_file').change(function() { // select banner
			var ext = $(this).val().split('.').pop().toLowerCase();
			if ($.inArray(ext, [ 'jpg', 'jpeg' ]) == -1) {
				meshop.log("error", "Sadece .jpg yüklenebilir");
			} else {
				$("input[name='banner_path_ar']").val(this.value || 'Nothing selected').removeAttr('disabled');
			}
		});

	}

	$('#save').click(function(e) {
		e.stopPropagation();
		save();

	});

	function save() {
		var formData = new FormData();
		var form = $('#banner_form');
		var modal = $('#new_banner_modal');
		var isanyfile = false;
		$("#banner_tr_file").each(function(i, input) {
			var oo = input.files[0];
			if (oo) {
				formData.append("trBanner", oo);
				isanyfile = true;
			}
		});
		$("#banner_en_file").each(function(i, input) {
			var oo = input.files[0];
			if (oo) {
				formData.append("enBanner", oo);
				isanyfile = true;
			}
		});
		$("#banner_ru_file").each(function(i, input) {
			var oo = input.files[0];
			if (oo) {
				formData.append("ruBanner", oo);
				isanyfile = true;
			}
		});
		$("#banner_ar_file").each(function(i, input) {
			var oo = input.files[0];
			if (oo) {
				formData.append("arBanner", oo);
				isanyfile = true;
			}
		});
		var serilized = form.serializeArray();
		$.each(serilized, function(key, input) {
			formData.append(input.name, input.value);
		});
		if (isanyfile) {

			$("#progress_banner_tr").removeClass("hidden");
			$('#progress_banner_tr .progress-bar').show().fadeIn(500, function() {
			});

			$("#progress_banner_en").removeClass("hidden");
			$('#progress_banner_en .progress-bar').show().fadeIn(500, function() {
			});

			$("#progress_banner_ru").removeClass("hidden");
			$('#progress_banner_ru .progress-bar').show().fadeIn(500, function() {
			});

			$("#progress_banner_ar").removeClass("hidden");
			$('#progress_banner_ar .progress-bar').show().fadeIn(500, function() {
			});

		}
		$.ajax({

			success : function(response) {

				modal.modal('hide');
				meshop.refresh($("#Table"));

			},
			error : function(xhr, status, error) {
				meshop.log("error", xhr.responseText);
				$('#progress_banner_tr').addClass("progress-bar-danger");

			},
			complete : function(e) {

				if (e.responseText == 'a') {

				}

			},

			xhr : function() {
				var myXhr = $.ajaxSettings.xhr();
				if (myXhr.upload) { // Check if upload property exists
					progressBar = $(".progress");
					myXhr.upload.addEventListener('progress', progressHandlingFunction, false);

				}
				return myXhr;
			},

			data : formData,
			processData : false,
			contentType : false,
			url : "/adminpanel/json/banner/save",
			type : "POST",

		});

	}

	// // SELECT OR DESELECT ALL OF INSTALLMENT AND CHANGE
	// DELETE BTN//
	var del = $('#delete_tag')
	var selectall = $('#selectall');
	selectall.parent().click(function(e) {
		if (selectall.hasClass('glyphicon-unchecked')) {
			check(selectall);
			check($(".selected"));
			del.removeClass("disabled");
		} else {
			uncheck(selectall);
			uncheck($(".selected"));
			del.addClass("disabled");
		}

		function check(obj) {
			obj.addClass(" glyphicon-check").removeClass(" glyphicon-unchecked ");
		}
		function uncheck(obj) {
			obj.removeClass(" glyphicon-check ").addClass(" glyphicon-unchecked");
		}

	});

	del.click(function(e) {
		var modal = meshop.alert("ask", "Silinecek! Emin misiniz?", "Uyarı");
		modal.size("sm");
		modal.yes = function() {
			e.preventDefault();
			del.addClass('loading');
			var selecteditems = "";
			var count = $('span.glyphicon-check.selected').map(function(index, obj) {
				return $(obj).closest('tr').data('id');
			})
			var selecteditems = count.get().join();

			if (selecteditems) {
				$.post("/adminpanel/json/banner/remove", {
					ids : selecteditems
				}, function(data) {
					meshop.refresh($("#Table"));
					meshop.log("ok", tdelete);
				}).fail(function(e) {
					del.removeClass('loading');
					meshop.log("error", terror);
				}).always(function() {
					del.removeClass('loading');
					del.addClass("disabled");
				});
			}
		};
	});

	function progressHandlingFunction(e) {
		if (e.lengthComputable) {
			console.log("e.loaded" + e.loaded + ":" + e.total);

			var prog = progressBar.find(".progress-bar")

			var percent = Math.round(e.loaded * 100 / e.total) + "%";
			prog.css({
				width : percent
			});// 45% Tamamlandı
			if (percent == "100%") {

				setTimeout(function() {

					prog.fadeOut(500, function() {
						prog.css({
							width : '1%'
						});
						progressBar.addClass("hidden");
						$("#uploadXml").prop("disabled", false);
						prog.removeClass("progress-bar-danger");
					});

				}, 2000);

			}
		}
	}

	$("#sortable").sortable({

		stop : function(event, ui) {
			var count = $('#Table tr').map(function(index, obj) {
				return $(obj).closest('tr').data('id');
			})
		
			var selecteditems = count.get().join();
			
		 
			$.post("/adminpanel/json/sira/save", {
				list : selecteditems
			}, function(data) {
				meshop.refresh($("#Table"));
			}).fail(function(e) {
				meshop.log("error", "");
			}).always(function() {
				del.removeClass('loading');
				del.addClass("disabled");
			});
		 
		 
			 
		}
	});

});