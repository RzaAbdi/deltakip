$(document).ready(function() {

	$("#Table").meshop({
		url : "json/tags/load",
		page : $('[data-id=Table_page]'),
		modifyJson : modifyJson,
		loop : loop,
	// filterForm:$('#filter-form')
	});

	function modifyJson(json) {

		return json;
	}

	function loop(obj, json, mainObj) {

		obj.data('id', json.id);

		obj.find('.scheck').click(function(e) {

			e.stopPropagation();
			var checkbox = obj.find(".selected");

			if (checkbox.hasClass('glyphicon-unchecked')) {
				checkbox.addClass(" glyphicon-check").removeClass(" glyphicon-unchecked ");
			} else {
				checkbox.addClass(" glyphicon-unchecked").removeClass(" glyphicon-check ");
				del.removeClass("disabled");
			}
			var count = $('span.glyphicon-check').map(function check_del(index, obj) {

				return $(obj).closest('tr').data('id');
			})
			if (count.length > 0) {

				del.removeClass("disabled");
			} else {
				del.addClass("disabled");
				$('#selectall').removeClass(" glyphicon-check").addClass(" glyphicon-unchecked ");
			}

		});


		obj.click(function(e) {
			e.stopPropagation();
			var modal = $('#new_tag_modal');
			modal.modal('show');
			modal.meshop({

				url : "/adminpanel/json/tag/load",
				params : {
					id : json.id,
				},
			// loop : loopModal,
//			 modifyJson : modifyJsonModal
			})

		});

	}
	;
	
	 

	$("#new_tag").click(function(e) {

		var modal = $('#new_tag_modal');
		modal.modal('show');
		$("#setAspectRatio").trigger("click");
		setTimeout(function() {
			$('#saveCanvas').click();
		}, 200);
		modal.meshop({
			json : {
				id : null,
			},
		// loop : loopModal
		});

	});

	$('#save').click(function() {
		$('#save,#resetModal').addClass("disabled");
		$(this).addClass('loading');
		var form = $('#tag_form');
		$.ajax({
			url : "/adminpanel/json/tag/save",
			type : "POST",
			data : form.serialize(),
		}).done(function(respond) {
			// window.location =
			// "/";

			$('#new_tag_modal').modal('hide');
			meshop.refresh($("#Table"));
			meshop.log("ok", tok);

		}).fail(function(e) {
			meshop.log("error", terror);

		}).always(function() {
			$('#save').removeClass('loading').removeClass('disabled');
			// meshop.refresh($("#Table"));
		});
		// meshop.refresh($("#Table"));

	});

	// // SELECT OR DESELECT ALL OF INSTALLMENT AND CHANGE
	// DELETE BTN//
	var del = $('#delete_tag')
	var selectall = $('#selectall');
	selectall.parent().click(function(e) {
		if (selectall.hasClass('glyphicon-unchecked')) {
			check(selectall);
			check($(".selected"));
			del.removeClass("disabled");
		} else {
			uncheck(selectall);
			uncheck($(".selected"));
			del.addClass("disabled");
		}

		function check(obj) {
			obj.addClass(" glyphicon-check").removeClass(" glyphicon-unchecked ");
		}
		function uncheck(obj) {
			obj.removeClass(" glyphicon-check ").addClass(" glyphicon-unchecked");
		}

	});

	del.click(function(e) {
		var modal = meshop.alert("ask", "سئچدیینیز دامغا/لار سیلینه جک . امین می سینیز؟", "اویاری");
		modal.size("sm");
		modal.yes = function() {
			e.preventDefault();
			del.addClass('loading');
			var selecteditems = "";
			var count = $('span.glyphicon-check.selected').map(function(index, obj) {
				return $(obj).closest('tr').data('id');
			})
			var selecteditems = count.get().join();

			if (selecteditems) {
				$.post("/adminpanel/json/tag/remove", {
					ids : selecteditems
				}, function(data) {
					meshop.refresh($("#Table"));
					meshop.log("ok", "دامغا باشارییلا سیلیندی");
				}).fail(function(e) {
					del.removeClass('loading');
					meshop.log("error", "سیلینیرکن بیر سورون اولوشدو. لوطفن یئنه دنه‌یین");
				}).always(function() {
					del.removeClass('loading');
					del.addClass("disabled");
				});
			}
		};
	});

});