 package rza.BPFco.utilities;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang.time.DateUtils;

public class Time {

	public static long now() {
		return System.currentTimeMillis();
	}

	public static long future(int after) {
		return System.currentTimeMillis() + after;
	}

	public static Date today() {
		Calendar today = Calendar.getInstance();
		today.set(Calendar.HOUR_OF_DAY, 0);
		today.set(Calendar.MINUTE, 0);
		today.set(Calendar.SECOND, 0);
		today.set(Calendar.MILLISECOND, 0);
		return today.getTime();
	}
	
	public static Date todayDateOnly() {
		Calendar today = Calendar.getInstance();
		 
		return today.getTime();
	}
	 
	

	public static Date todayDateTime() throws ParseException {
		Calendar today = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		String ds = dateFormat.format(today.getTime());

		return ConvertToDate(ds);
	}

	public static Date morning() {
		Calendar today = Calendar.getInstance();
		today.set(Calendar.HOUR_OF_DAY, 6);
		today.set(Calendar.MINUTE, 0);
		today.set(Calendar.SECOND, 0);
		today.set(Calendar.MILLISECOND, 0);
		return today.getTime();
	}

	public static Date ConvertToDate(String dateString) throws ParseException {
		String month = dateString.trim().substring(0, 2);
		String day = dateString.trim().substring(3, 5);
		String year = dateString.trim().substring(6, 10);
		
//		String hourse = dateString.trim().substring(11, 13);
//		String min = dateString.trim().substring(14, 16);
//		String second = dateString.trim().substring(17, 19);
		// 20120106
		dateString = year + month + day ;
		Date tradeDate;

		tradeDate = new SimpleDateFormat("yyyyMMdd").parse(dateString);

		// String krwtrDate = new SimpleDateFormat("yyyy-MM-dd",
		// Locale.ENGLISH).format(tradeDate);

		return tradeDate;
	}
	public static Date ConvertToDateTime(String dateString) throws ParseException {
		String month = dateString.trim().substring(0, 2);
		String day = dateString.trim().substring(3, 5);
		String year = dateString.trim().substring(6, 10);
		Calendar today = Calendar.getInstance();
		
		
		int hourse = Calendar.HOUR_OF_DAY;
		int min = Calendar.MINUTE;
		 
		// 20120106
		dateString = year + month + day + hourse + min + 00;
		Date tradeDate;
		
		
		tradeDate = new SimpleDateFormat("yyyyMMddHHmmss").parse(dateString);
		
		
		
		return tradeDate;
	}
	public static Date ConvertExcelToDate(String dateString) throws ParseException {
		String day = dateString.trim().substring(0, 2);
		String year = dateString.trim().substring(7, 11);
		String month = dateString.trim().substring(3, 6);
		
		String m = null;
		//29-Jun-2020
		
		
		switch (month) {
		case "Jan":
			 m= "01";
			break;
		case "Feb":
			m= "02";
			
			break;
		case "Mar":
			m= "03";
			
			break;
		case "Apr":
			m= "04";
			
			break;
		case "May":
			m= "05";
			
			break;
		case "Jun":
			m= "06";
			
			break;
		case "Jul":
			m= "07";
			
			break;
		case "Aug":
			m= "08";
			
			break;
		case "Sep":
			m= "09";
			
			break;
		case "Oct":
			m= "10";
			
			break;
		case "Nov":
			m= "11";
			
			break;
		case "Dec":
			m= "12";
			
			break;

		default:
			break;
		}
//		 
		dateString = year + m + day ;
		Date tradeDate;
		
		tradeDate = new SimpleDateFormat("yyyyMMdd").parse(dateString);
		
		// String krwtrDate = new SimpleDateFormat("yyyy-MM-dd",
		// Locale.ENGLISH).format(tradeDate);
		
		return tradeDate;
	}
	public static long ConvertToDateLong(String dateString) throws ParseException {
		String month = dateString.trim().substring(0, 2);
		String day = dateString.trim().substring(3, 5);
		String year = dateString.trim().substring(6, 10);
		
		String hourse = dateString.trim().substring(11, 13);
		String min = dateString.trim().substring(14, 16);
		String second = dateString.trim().substring(17, 19);
		// 20120106
		dateString = year + month + day + hourse + min + second;
		Date tradeDate;
		
		tradeDate = new SimpleDateFormat("yyyyMMddHHmmss").parse(dateString);
		
		// String krwtrDate = new SimpleDateFormat("yyyy-MM-dd",
		// Locale.ENGLISH).format(tradeDate);
		
		return tradeDate.getTime();
	}
	
	public static Date ConvertToDatenew(String dateString) throws ParseException {
		Calendar calendar = Calendar.getInstance();
		String month = dateString.trim().substring(0, 2);
		String day = dateString.trim().substring(3, 5);
		String year = dateString.trim().substring(6, 10);
		// 20120106
		dateString = year + month + day + "23" + "00" + "00";
		Date tradeDate;
		

		tradeDate = new SimpleDateFormat("yyyyMMddHHmmss").parse(dateString);
		calendar.setTime(tradeDate);

		// String krwtrDate = new SimpleDateFormat("yyyy-MM-dd",
		// Locale.ENGLISH).format(tradeDate);

		return calendar.getTime();
	}
	public static Date ConvertToDatemorning(String dateString) throws ParseException {
		Calendar calendar = Calendar.getInstance();
		String month = dateString.trim().substring(0, 2);
		String day = dateString.trim().substring(3, 5);
		String year = dateString.trim().substring(6, 10);
		// 20120106
		dateString = year + month + day ;
		Date tradeDate;
		
		
		tradeDate = new SimpleDateFormat("yyyyMMdd").parse(dateString);
		calendar.setTime(tradeDate);
		// String krwtrDate = new SimpleDateFormat("yyyy-MM-dd",
		// Locale.ENGLISH).format(tradeDate);
		
		return calendar.getTime();
	}

	public static Date ConvertToDate(int year, int month, int day) throws ParseException {
		String sday = String.valueOf(day);
		String smonth = String.valueOf(month);
		if (day < 10)
			sday = "0" + day;
		if (month < 10)
			smonth = "0" + month;
		// 20120106
		String dateString = String.valueOf(year) + smonth + sday;
		Date tradeDate;

		tradeDate = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH).parse(dateString);

		// String krwtrDate = new SimpleDateFormat("yyyy-MM-dd",
		// Locale.ENGLISH).format(tradeDate);

		return tradeDate;
	}

	public static Date tomorrow() {
		Calendar today = Calendar.getInstance();
		today.set(Calendar.MINUTE, 0);
		today.set(Calendar.SECOND, 0);
		today.set(Calendar.MILLISECOND, 0);
		today.add(Calendar.HOUR_OF_DAY, 24);
		return today.getTime();
	}

	public static Date addSecondsAfterNow(long seconds) {
		seconds = seconds * 1000; // convert to millisecond
		Long currenttime = System.currentTimeMillis() + seconds; // current time
		// + my
		// seconds
		// on
		// Millisecond
		Calendar today = Calendar.getInstance();
		today.setTimeInMillis(currenttime);
		return today.getTime();
	}

	public static Date addDaysAfterNow(int days) {
		int hours = days * 24; // convert to millisecond
		Calendar mydate = Calendar.getInstance();
		mydate.set(Calendar.MINUTE, 0);
		mydate.set(Calendar.SECOND, 0);
		mydate.set(Calendar.MILLISECOND, 0);
		mydate.set(Calendar.HOUR_OF_DAY, hours);
		return mydate.getTime();
	}
	public static Date addDaysAfterDates(int days,Date date) {
		int hours = days * 24; // convert to millisecond
		Calendar mydate = Calendar.getInstance();
		mydate.setTime(date);
		mydate.set(Calendar.MINUTE, 0);
		mydate.set(Calendar.SECOND, 0);
		mydate.set(Calendar.MILLISECOND, 0);
		mydate.set(Calendar.HOUR_OF_DAY, hours);
		return mydate.getTime();
	}

//	public static Date addDaysAfterGivenDate(long days,Date currentdate) {
//		long millisecond = days * 24 * 60 * 60 * 1000; // convert to millisecond44
//		
//		Long currenttime = currentdate.getTime() + millisecond; // current time
//																// + my
//																	// seconds
//																	// on
//																	// Millisecond
//		Calendar today = Calendar.getInstance();
//		today.setTimeInMillis(currenttime);
//		 
//		return today.getTime();
//	}
	public static Date ToDate(Long millis) {
		// Date date = new Date(millis);
		Calendar dte = Calendar.getInstance();
		dte.setTimeInMillis(millis * 1000);
		// DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		return dte.getTime();
	}

	public static Date ToDateOnly(Date prmdate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(prmdate);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	public static String ToSplitedDate(Date prmdate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(prmdate);
		DecimalFormat mFormat = new DecimalFormat("00");

		int y = cal.get(Calendar.YEAR);
		int m = cal.get(Calendar.MONTH) + 1;
		int d = cal.get(Calendar.DAY_OF_MONTH);

		return mFormat.format(Double.valueOf(m)) + "/" + mFormat.format(Double.valueOf(d)) + "/" + y;
	}
	
	public static String ToSplitedDateCalneder(Date prmdate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(prmdate);
		DecimalFormat mFormat = new DecimalFormat("00");

		int y = cal.get(Calendar.YEAR);
		int m = cal.get(Calendar.MONTH) + 1;
		int d = cal.get(Calendar.DAY_OF_MONTH);
		return mFormat.format(Double.valueOf(d)) + "-" + mFormat.format(Double.valueOf(m)) + "-" +  mFormat.format(Double.valueOf(y));
	}
	
	public static String ToSplitedDateJustCalneder(Date prmdate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(prmdate);
		DecimalFormat mFormat = new DecimalFormat("00");

		int y = cal.get(Calendar.YEAR);
		int m = cal.get(Calendar.MONTH) + 1;
		int d = cal.get(Calendar.DAY_OF_MONTH);

		return mFormat.format(Double.valueOf(y)) + "-" + mFormat.format(Double.valueOf(m)) + "-" +  mFormat.format(Double.valueOf(d));
	}
	public static boolean chackHourse(Date prmdate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(prmdate);
		DecimalFormat mFormat = new DecimalFormat("00");
		int H = cal.get(Calendar.HOUR);
		 
		
		if(H==9) {
			return true;
		}else {
			
			return false;
		}
		
	}
	public static Date AddDay(Date prmdate,int day) {
		 
		Calendar cal = Calendar.getInstance();
		cal.setTime(prmdate);
		cal.add(Calendar.DATE, +(day));
		cal.add(Calendar.HOUR, +(day));
		return cal.getTime();
		
	}


	public static String[] SplitedDate(String value) {

		ArrayList<String> retList = new ArrayList<String>();
		String[] splits = value.split("-");
		for (String s : splits) {
			retList.add(s);
		}

		return retList.toArray(new String[retList.size()]);
	}

	public static String ToTurkishDateOnly(Date prmdate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(prmdate);

		int y = cal.get(Calendar.YEAR);
		int h = cal.get(Calendar.HOUR_OF_DAY);
		int min = cal.get(Calendar.MINUTE);
		String m = null;
		switch (cal.get(Calendar.MONTH)) {
		case 0:
			m = "Ocak";
			break;
		case 1:
			m = "Şubat";
			break;
		case 2:
			m = "Mart";
			break;
		case 3:
			m = "Nisan";
			break;
		case 4:
			m = "Mayıs";
			break;
		case 5:
			m = "Haziran";
			break;
		case 6:
			m = "Temmuz";
			break;
		case 7:
			m = "Ağustos";
			break;
		case 8:
			m = "Eylül";
			break;
		case 9:
			m = "Ekim";
			break;
		case 10:
			m = "Kasım";
			break;
		case 11:
			m = "Aralık";
			break;

		default:
			break;
		}
		int d = cal.get(Calendar.DAY_OF_MONTH);
		return d + " " + m + " " + y ;
	}

	public static String ToTurkishDateMonthDateOnly(Date prmdate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(prmdate);

		int y = cal.get(Calendar.YEAR);
		String m = null;
		switch (cal.get(Calendar.MONTH)) {
		case 0:
			m = "Ocak";
			break;
		case 1:
			m = "Şubat";
			break;
		case 2:
			m = "Mart";
			break;
		case 3:
			m = "Nisan";
			break;
		case 4:
			m = "Mayıs";
			break;
		case 5:
			m = "Haziran";
			break;
		case 6:
			m = "Temmuz";
			break;
		case 7:
			m = "Ağustos";
			break;
		case 8:
			m = "Eylül";
			break;
		case 9:
			m = "Ekim";
			break;
		case 10:
			m = "Kasım";
			break;
		case 11:
			m = "Aralık";
			break;

		default:
			break;
		}
		int d = cal.get(Calendar.DAY_OF_MONTH);
		return d + " " + m + " " + y + "  ";
	}

	public static int compare(Date issueDate) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(issueDate);
		cal.add(Calendar.DATE, -15);
		
		Date today = today();

		if (DateUtils.isSameDay(issueDate, today)) {

			return 0; //same
		} else if (issueDate.before(today)) {
			return 1; //before
			
			
		} else {
			if(cal.getTime().before(today)) {
				return 3; //15 days
			}else {
				
				return 2; //after
			}
			
		}

	}
	
	 

	public static boolean fifteenDays(Date issueDate) {

		Calendar cal = Calendar.getInstance();
		cal.setTime(issueDate);
		cal.add(Calendar.DATE, -15);

		if (DateUtils.isSameDay(cal.getTime(), today())) {

			return true;
		} else if (cal.getTime().before(today())) {
			return true;
		} else {
			return false;
		}

	}
	

	public static Date fifteenNextDays() {

		Calendar cal = Calendar.getInstance();
		cal.setTime(Time.today());
		cal.add(Calendar.DATE, +15);
		return cal.getTime();

	}
	public static Date fifteenDaysBefore() {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(Time.today());
		cal.add(Calendar.DATE,-45);
		return cal.getTime();
		
	}
	
	public static Date CustomDaysAfter(Date date,int month) {

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, +month);
		return cal.getTime();

	}
	
	public static Date nextMount(Date date) {
		 
		Date newDate = DateUtils.addMonths(date, 1);
		
		return newDate;
		
	}
	public static Date nextTwoMount(Date date) {
		 
		Date newDate = DateUtils.addMonths(date, 2);
		
		return newDate;
		
	}
	public static Date nextThreeMount(Date date) {
		 
		Date newDate = DateUtils.addMonths(date, 3);
		
		return newDate;
		
	}
}
