package rza.BPFco.utilities;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.multipart.MultipartFile;

import rza.BPFco.model.Account;
import rza.BPFco.model.portfoys.Reply;
import rza.BPFco.model.portfoys.SosyalMedya;
import rza.BPFco.service.SosyalMedyaService;
import rza.BPFco.service.UserService;

/**
 * TODO Put here a description of what this class does.
 *
 * @author rza. Created Oct 12, 2020.
 */

public class ReadExcel {
	
	UserService userService;
	SosyalMedyaService sosyalMedyaService;

	public ReadExcel(UserService userService, SosyalMedyaService sosyalMedyaService) {
		this.userService = userService;
		this.sosyalMedyaService = sosyalMedyaService;
	}

	public List<SosyalMedya> ReadSosyalMedyaExcelFromFile(MultipartFile file) throws IOException {

		List<SosyalMedya> sosyalMedyas = new ArrayList<SosyalMedya>();
		String err = null;
		int order = 0;
		try {
			
			
			 Workbook wb;
			
			 String lowerCaseFileName = file.getOriginalFilename().toLowerCase();
		        if (lowerCaseFileName.endsWith(".xlsx")) {
		        	wb = new XSSFWorkbook(file.getInputStream());
		        } else {
		        	return null;
		        }


//			HSSFWorkbook wb = new HSSFWorkbook(file.getInputStream());
			Sheet sheet =  wb.getSheetAt(0);
			Row row;
			Cell cell;

			int rows; // No of rows
			rows = sheet.getPhysicalNumberOfRows();

			int cols = 0; // No of columns
			int tmp = 0;
			
			

			// This trick ensures that we get the data properly even if it doesn't start
			// from first few rows
			for (int i = 0;    i < rows; i++) {
				row = sheet.getRow(i);
				if (row != null) {
					tmp = sheet.getRow(i).getPhysicalNumberOfCells();
					if (tmp > cols)
						cols = tmp;
				}
			}
			
			

			

			for (int r = 0; r < rows; r++) {
				row = sheet.getRow(r);
				if (row != null) {
					SosyalMedya medya = new SosyalMedya();
					for (int c = 0; c < cols; c++) {
						order = sosyalMedyas.size()+1;
						cell = row.getCell(c);
						if (cell != null) {
						//
							switch (c) {
							
							case 0://onem
								err="onem";
								
								Float tt = Float.valueOf(cell.toString());
								medya.setPriority(Math.round(tt));
								
								break;
							case 1://name
								err="name";
								if(cell.toString()=="") {
									medya.setName("BELİRSİZ");
									
								}else {
									
									medya.setName(cell.toString());
								}
								
								
								break;
								
							case 2://uyruk
								err="uytuk";
								medya.setNation(cell.toString());
								
								break;
							case 3://email
								err="email";
								medya.setEmail(cell.toString()); 
								
								break;
							case 4://gsm
								err="gsm";

								medya.setGsm(cell.toString()); 
								
								break;
							case 5://temsilci
								err="temsilci";
								Set<Account> account = new HashSet<Account>();
								Account acc =	userService.loadAccountByEmail(cell.toString());
								account.add(acc);
								medya.setAccount(account);
								
								break;
							case 6://firstplatform
								err="firstplatform";
								medya.setFirstInfo(cell.toString());
								
								break;
							
							case 7://currentplatform
								err="currentplatform";
								medya.setCurrentPlatform(cell.toString());
								
								break;
							case 8://createdate
								err="createdate";
								String date = cell.toString();
								medya.setCreateDate(Time.ConvertExcelToDate(date));
								
								break;
							case 9://lastdate
								err="lastdate";
								 
							String gg = 	cell.toString();
							if(gg!="") {
								
								medya.setLastDate(Time.ConvertExcelToDate(cell.toString()));
							}
								 
								//set reply date here
								break;
							case 10://firstinfo
								err="firstinfo";
								medya.setFirstInfo(cell.toString());
								
								break;
							case 11://lastinfo
								err="lastinfo";
								medya.setLastUpdate(cell.toString());
								if(cell.toString()!="") {
									Reply reply = new Reply();
									reply.setNote(cell.toString());
									
									Set<Account> accounts = new HashSet<Account>();
									Account acco =	userService.loadAccountByEmail(row.getCell(5).toString());
									accounts.add(acco);
									reply.setAccount(accounts);
									
									reply.setCreateDate(Time.ConvertExcelToDate(row.getCell(9).toString()));
									
									Set<Reply> rep = new HashSet<Reply>();
									rep.add(reply);
									medya.setReply(rep);
									 
								}
								// set reply not here
								
								
								break;

							default:
								break;
							}
							
							
							
						 
						}
					}
					sosyalMedyas.add(medya);
				}
			}

		} catch (Exception ioe) {
			System.out.println("SAVE EXCEL HAS AN ERROR ON ROW=" + order + "AND CELL=" + err);
		ioe.printStackTrace();
			return null;
		}

		 
		return sosyalMedyas;

	}
	public List<Reply> ReadReplyExcelFromFile(MultipartFile file) throws IOException {
		
		List<Reply> sosyalMedyas = new ArrayList<Reply>();
		String err = null;
		int order = 0;
		try {
			
			
			Workbook wb;
			
			String lowerCaseFileName = file.getOriginalFilename().toLowerCase();
			if (lowerCaseFileName.endsWith(".xlsx")) {
				wb = new XSSFWorkbook(file.getInputStream());
			} else {
				return null;
			}
			
			
//			HSSFWorkbook wb = new HSSFWorkbook(file.getInputStream());
			Sheet sheet =  wb.getSheetAt(0);
			Row row;
			Cell cell;
			
			int rows; // No of rows
			rows = sheet.getPhysicalNumberOfRows();
			
			int cols = 0; // No of columns
			int tmp = 0;
			
			
			
			// This trick ensures that we get the data properly even if it doesn't start
			// from first few rows
			for (int i = 0;    i < rows; i++) {
				row = sheet.getRow(i);
				if (row != null) {
					tmp = sheet.getRow(i).getPhysicalNumberOfCells();
					if (tmp > cols)
						cols = tmp;
				}
			}
			
			
			
			
			
			for (int r = 0; r < rows; r++) {
				row = sheet.getRow(r);
				if (row != null) {
					Reply reply = new Reply();
					SosyalMedya medya = null;
					for (int c = 0; c < cols; c++) {
						
						
						order = sosyalMedyas.size()+1;
						cell = row.getCell(c);
						if (cell != null) {
							//
							switch (c) {
							
							 
							 
							case 5://gsm
								err="gsm";
								
								medya = sosyalMedyaService.loadSosyalMedyaByGSM(cell.toString());
								reply.setSosyalmedya(medya);
								if(row.getCell(1).toString()!="") {
									medya.setName(row.getCell(1).toString());
									
								}else {
									
									medya.setName("BELİRSİZ");
								}
								
								break;

							case 6://temsilci
								err="temsilci";
								
								Account acc =	userService.loadAccountByEmail(cell.toString());
								Set<Account> exsitAccounts = new HashSet<Account>();
									
									exsitAccounts.add(acc);
								Set<Account> account = new HashSet<Account>(exsitAccounts);
								reply.setAccount(account);
								
							 
								 
								break;
							case 7://lastdate
								err="lastdate";
								
								String gg = 	cell.toString();
								if(gg!="") {
									
									reply.setCreateDate(Time.ConvertExcelToDate(cell.toString()));
								}
								
								break;
							case 10://firstinfo
								err="note";
								reply.setNote(cell.toString());
								
								break;
								
							default:
								break;
							}
							
							
							
							
						}
					}
					sosyalMedyas.add(reply);
				}
			}
			
		} catch (Exception ioe) {
			System.out.println("SAVE EXCEL HAS AN ERROR ON ROW=" + order + "AND CELL=" + err);
			ioe.printStackTrace();
			return null;
		}
		
		
		return sosyalMedyas;
		
	}

}
