package rza.BPFco.utilities;

import org.springframework.core.io.ClassPathResource;

import com.google.common.io.Files;

import rza.BPFco.config.initProperties;

public class CustomViewResolver {

	public static String getHtml(String path) throws Exception {

		ClassPathResource file = new ClassPathResource("../../WEB-INF/views/"  + path + ".html");
		return Files.toString(file.getFile(), java.nio.charset.Charset.forName("UTF-8"));

	}
	
	public static long getLastModified(String path) throws Exception {
		
		ClassPathResource file = new ClassPathResource("../../WEB-INF/views/"  + path + ".html");
		return file.lastModified();
		
	}

}