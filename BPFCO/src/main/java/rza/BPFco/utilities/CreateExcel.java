package rza.BPFco.utilities;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.springframework.data.domain.Page;

import rza.BPFco.config.initProperties;
import rza.BPFco.form.filter.DocumentFilterForm;
import rza.BPFco.model.Account;
import rza.BPFco.model.Contract;
import rza.BPFco.model.Portfoy;
import rza.BPFco.model.portfoys.DocType;
import rza.BPFco.model.portfoys.Document;
import rza.BPFco.model.portfoys.Installment;
import rza.BPFco.model.portfoys.Section;
import rza.BPFco.model.portfoys.SosyalMedya;
import rza.BPFco.service.DocumentService;

/**
 * TODO Put here a description of what this class does.
 *
 * @author rza. Created Oct 5, 2020.
 */
public class CreateExcel {

	public CreateExcel() {

	}

	public String CrateTestFromInstallmens() throws FileNotFoundException, IOException {
		XSSFWorkbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("student Details");
		CellStyle headerCellStyle = sheet.getWorkbook().createCellStyle();
		headerCellStyle.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.index);
//		headerCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);

		Map<String, Object[]> data = new TreeMap<String, Object[]>();
		data.put("1", new Object[] { "ID", "NAME", "LASTNAME" });
		data.put("2", new Object[] { 1, "Pankaj", "Kumar" });
		data.put("3", new Object[] { 2, "Prakashni", "Yadav" });
		data.put("4", new Object[] { 3, "Ayan", "Mondal" });
		data.put("5", new Object[] { 4, "Virat", "kohli" });

		// Iterate over data and write to sheet
		Set<String> keyset = data.keySet();
		int rownum = 0;
		for (String key : keyset) {
			// this creates a new row in the sheet
			Row row = sheet.createRow(rownum++);
			Object[] objArr = data.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				// this line creates a cell in the next column of that row
				Cell cell = row.createCell(cellnum++);
				// if rownum is 1 (first row was created before) then set header CellStyle
				if (rownum == 1)
					cell.setCellStyle(headerCellStyle);
				if (obj instanceof String)
					cell.setCellValue((String) obj);
				else if (obj instanceof Integer)
					cell.setCellValue((Integer) obj);
			}
		}

		for (int c = 0; c < 3; c++) {
			sheet.autoSizeColumn(c);
		}

		String path = "TAKSIT_" + Randomize.TwoDigit() + ".xlsx";
		try (FileOutputStream outputStream = new FileOutputStream(initProperties.FILEFOLDERNAME + path)) {
			workbook.write(outputStream);

		}
		return path;

	}

	public String CrateExcelFromInstallmens(Page<Installment> pageAuthors) throws FileNotFoundException, IOException {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("Installment Books");
		sheet.setDefaultRowHeightInPoints(80);

		DecimalFormatSymbols dfs = new DecimalFormatSymbols();
		dfs.setDecimalSeparator(',');
		dfs.setGroupingSeparator('.');
		DecimalFormat formatter = new DecimalFormat("###,###,###.##");
		formatter.setDecimalFormatSymbols(dfs);

		sheet.setColumnWidth(0, 30 * 256);
		sheet.setColumnWidth(1, 20 * 256);
		sheet.setColumnWidth(2, 20 * 256);
		sheet.setColumnWidth(3, 20 * 256);
		sheet.setColumnWidth(4, 20 * 256);
		sheet.setColumnWidth(5, 20 * 256);
		sheet.setColumnWidth(6, 50 * 256);
		sheet.setColumnWidth(7, 20 * 256);

		CellStyle style = workbook.createCellStyle();
		CellStyle cellStyle = workbook.createCellStyle();
		CellStyle styleWrap = workbook.createCellStyle();

//		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());

		style.setWrapText(true);
		styleWrap.setWrapText(true);

		int rowCount = 0;

		int titleCount = -1;
		Row titleRow = sheet.createRow(0);

		Cell Tname = titleRow.createCell(++titleCount);
		Tname.setCellValue("ADI / SOYADI");
		Tname.setCellStyle(style);

		Cell Tamont = titleRow.createCell(++titleCount);
		Tamont.setCellValue("ÖDENECEK TUTAR");
		Tamont.setCellStyle(style);

		Cell Tissuedate = titleRow.createCell(++titleCount);
		Tissuedate.setCellValue("VADE TARİHİ");
		Tissuedate.setCellStyle(style);

		Cell Tpartial = titleRow.createCell(++titleCount);
		Tpartial.setCellValue("ÖDENEN TUTAR");
		Tpartial.setCellStyle(style);

		Cell Tpaymantdate = titleRow.createCell(++titleCount);
		Tpaymantdate.setCellValue("ÖDEME TARİHİ");
		Tpaymantdate.setCellStyle(style);

		Cell Tstatus = titleRow.createCell(++titleCount);
		Tstatus.setCellValue("DURUM");
		Tstatus.setCellStyle(style);

		Cell Tportfoy = titleRow.createCell(++titleCount);
		Tportfoy.setCellValue("PORTFÖY");
		Tportfoy.setCellStyle(style);

		Cell Tkalan = titleRow.createCell(++titleCount);
		Tkalan.setCellValue("KALAN BORÇ");
		Tkalan.setCellStyle(style);

		for (Installment installment : pageAuthors.getContent()) {
			System.out.println(installment.getId());
			ArrayList<Float> paids = new ArrayList<Float>();
			Contract contracts = installment.getContract();
			for (Installment ins : contracts.getInstallment()) {
				System.out.println(ins.getId());

				float amount = 0;
				if (ins.isPaid()) {

					if (ins.isPartial()) {
						// partial
						amount = Float.parseFloat(ins.getPartial_amount().replaceAll("[^\\d.]", ""));

					} else {
						// amount
						amount = Float.parseFloat(ins.getAmount().replaceAll("[^\\d.]", ""));
					}

				} else if (ins.isPartial()) {
					// partial
					if (ins.getPartial_amount() != null) {

						amount = Float.parseFloat(ins.getPartial_amount().replaceAll("[^\\d.]", ""));
					}

				}

				paids.add(amount);
			}

			float sum = 0;

			for (int i = 0; i < paids.size(); i++) {

				sum += paids.get(i);

			}
			String chash = "0";

			if (!installment.getContract().getCash().isEmpty()) {
				chash = installment.getContract().getCash();
			}

			float pesin = Float.parseFloat(chash.replaceAll("[^\\d.]", ""));

			String remain = formatter.format(
					Float.parseFloat(installment.getContract().getPrice().replaceAll("[^\\d.]", "")) - pesin - sum);

			//
			Row row = sheet.createRow(++rowCount);

			cellStyle = workbook.createCellStyle();
			cellStyle.setWrapText(true);

			if (installment.isPaid()) {
				if (installment.isPartial()) {

//					cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);

					cellStyle.setFillForegroundColor(IndexedColors.SKY_BLUE.getIndex());

				} else {

//					cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
					cellStyle.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
				}

			} else {
				if (installment.isPartial()) {

//					cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
					cellStyle.setFillForegroundColor(IndexedColors.SKY_BLUE.getIndex());
				} else {

					if (Time.compare(installment.getIssueDate()) == 1) {
						cellStyle.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
//						cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);

					} else {

						cellStyle.setFillForegroundColor(IndexedColors.PALE_BLUE.getIndex());
//						cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
					}

				}
			}

			int columnCount = -1;

			Cell name = row.createCell(++columnCount);
			name.setCellValue(installment.getContract().getCustomer().iterator().next().getFname());
			name.setCellStyle(cellStyle);

			Cell amount = row.createCell(++columnCount);
			amount.setCellValue(
					formatter.format(Float.parseFloat(installment.getAmount().replaceAll("[^\\d.]", ""))) + " " + Source.CurrencySource().get(installment.getCurrency() - 1));
			amount.setCellStyle(cellStyle);

			Cell issuedate = row.createCell(++columnCount);
			issuedate.setCellValue(Time.ToTurkishDateMonthDateOnly(installment.getIssueDate()));
			issuedate.setCellStyle(cellStyle);

			Cell paymant = row.createCell(++columnCount);
			paymant.setCellStyle(cellStyle);

			if (installment.isPaid()) {
				if (installment.isPartial()) {
					paymant.setCellValue(formatter.format(Float.parseFloat(installment.getPartial_amount().replaceAll("[^\\d.]", ""))) + " "
							+ Source.CurrencySource().get(installment.getCurrency() - 1));
				} else {

					paymant.setCellValue(
							formatter.format(Float.parseFloat(installment.getAmount().replaceAll("[^\\d.]", ""))) + " " + Source.CurrencySource().get(installment.getCurrency() - 1));
				}

			} else {
				if (installment.isPartial()) {

					paymant.setCellValue(formatter.format(Float.parseFloat(installment.getPartial_amount().replaceAll("[^\\d.]", ""))) + " "
							+ Source.CurrencySource().get(installment.getCurrency() - 1));
				} else {

					paymant.setCellValue("0");
				}
			}

			Cell paymantdate = row.createCell(++columnCount);
			if (installment.getPaymantDate() != null) {
				paymantdate.setCellValue(Time.ToTurkishDateMonthDateOnly(installment.getPaymantDate()));
				paymantdate.setCellStyle(cellStyle);

			} else {
				paymantdate.setCellStyle(cellStyle);
			}
			

			Cell paid = row.createCell(++columnCount);
			paid.setCellStyle(cellStyle);
			if (installment.isPaid()) {
				if (installment.isPartial()) {
					paid.setCellValue("KISMİ OLARAK ÖDENDİ");

				} else {

					paid.setCellValue("ÖDENDİ");
				}

			} else {
				if (installment.isPartial()) {

					paid.setCellValue("KISMİ OLARAK ÖDENDİ");
				} else {

					if (Time.compare(installment.getIssueDate()) == 1) {

						paid.setCellValue("ÖDENMEDİ");
					} else {

						paid.setCellValue("BEKLENİYOR");
					}

				}
			}
			int docCount = 0;
			StringBuilder portf = new StringBuilder();
			Set<Portfoy> portfoys = installment.getContract().getPortfoys();
			for (Portfoy portfoy : portfoys) {
				docCount++;

				portf.append(portfoy.getTitle());
				if(docCount<portfoys.size()) {
					portf.append(" || ");
					
				}
			}

			Cell portfoy = row.createCell(++columnCount);
			portfoy.setCellValue(portf.toString());
			portfoy.setCellStyle(cellStyle);

			Cell kalan = row.createCell(++columnCount);
			kalan.setCellValue(remain + "  " + Source.CurrencySource().get(installment.getCurrency() - 1));
			kalan.setCellStyle(cellStyle);

		}
		String path = "TAKSIT_" + Randomize.TwoDigit() + ".xlsx";
		try (FileOutputStream outputStream = new FileOutputStream(initProperties.FILEFOLDERNAME + path)) {
			workbook.write(outputStream);

		}
		return path;

	}

	public String CrateExcelFromSosyalMedya(Page<SosyalMedya> pageAuthors) throws IOException {

		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("Installment Books");
		sheet.setDefaultRowHeightInPoints(80);

		sheet.setColumnWidth(0, 10 * 256);
		sheet.setColumnWidth(1, 20 * 256);
		sheet.setColumnWidth(2, 10 * 256);
		sheet.setColumnWidth(3, 50 * 256);
		sheet.setColumnWidth(4, 20 * 256);
		sheet.setColumnWidth(5, 30 * 256);
		sheet.setColumnWidth(6, 20 * 256);
		sheet.setColumnWidth(7, 30 * 256);
		sheet.setColumnWidth(8, 30 * 256);
		sheet.setColumnWidth(9, 30 * 256);
		sheet.setColumnWidth(10, 30 * 256);
		sheet.setColumnWidth(11, 30 * 256);
		sheet.setColumnWidth(12, 30 * 256);
		sheet.setColumnWidth(13, 30 * 256);

		CellStyle style = workbook.createCellStyle();
		CellStyle cellStyle = workbook.createCellStyle();
		CellStyle styleWrap = workbook.createCellStyle();

//		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());

		style.setWrapText(true);
		styleWrap.setWrapText(true);

		int rowCount = 0;

		int titleCount = -1;
		Row titleRow = sheet.createRow(0);

		Cell Tname = titleRow.createCell(++titleCount);
		Tname.setCellValue("SIRA");
		Tname.setCellStyle(style);

		Cell dereceT = titleRow.createCell(++titleCount);
		dereceT.setCellValue("ÖNEM");
		dereceT.setCellStyle(style);

		Cell idc = titleRow.createCell(++titleCount);
		idc.setCellValue("id");
		idc.setCellStyle(style);

		Cell Tamont = titleRow.createCell(++titleCount);
		Tamont.setCellValue("ADI SOYADI");
		Tamont.setCellStyle(style);

		Cell Tissuedate = titleRow.createCell(++titleCount);
		Tissuedate.setCellValue("UYRUK");
		Tissuedate.setCellStyle(style);

		Cell mail = titleRow.createCell(++titleCount);
		mail.setCellValue("EMAIL");
		mail.setCellStyle(style);

		Cell Tpartial = titleRow.createCell(++titleCount);
		Tpartial.setCellValue("GSM");
		Tpartial.setCellStyle(style);

		Cell Tpaymantdate = titleRow.createCell(++titleCount);
		Tpaymantdate.setCellValue("TEMSİLCİ");
		Tpaymantdate.setCellStyle(style);

		Cell yp = titleRow.createCell(++titleCount);
		yp.setCellValue("YÖNLENDİRİLEN PLATFORM");
		yp.setCellStyle(style);

		Cell mp = titleRow.createCell(++titleCount);
		mp.setCellValue("İLK MESAJ PLATFORMU");
		mp.setCellStyle(style);

		Cell Tstatus = titleRow.createCell(++titleCount);
		Tstatus.setCellValue("TARİH");
		Tstatus.setCellStyle(style);

		Cell Tportfoy = titleRow.createCell(++titleCount);
		Tportfoy.setCellValue("GÜNCELLEME TARİHİ");
		Tportfoy.setCellStyle(style);

		Cell finfo = titleRow.createCell(++titleCount);
		finfo.setCellValue("İLK BİLGİ");
		finfo.setCellStyle(style);
		Cell Tkalan = titleRow.createCell(++titleCount);
		Tkalan.setCellValue("SON DURUM");
		Tkalan.setCellStyle(style);

		for (SosyalMedya SM : pageAuthors.getContent()) {
			ArrayList<Long> paids = new ArrayList<Long>();

			//
			Row row = sheet.createRow(++rowCount);

			cellStyle = workbook.createCellStyle();
			cellStyle.setWrapText(true);

			String derece = null;

			switch (SM.getPriority()) {
			case 1:// ciddi

//				cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cellStyle.setFillForegroundColor(IndexedColors.SKY_BLUE.getIndex());
				derece = "CİDDİ";

				break;
			case 2:

//				cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cellStyle.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
				derece = "TEKİPTE";

				break;
			case 3:

				cellStyle.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
//				cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
				derece = "2.DERECE";
				break;
			case 4:

				cellStyle.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
//				cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
				derece = "ACENTE";

				break;

			default:
				break;
			}

			int columnCount = -1;

			Cell name = row.createCell(++columnCount);
			name.setCellValue(rowCount);
			name.setCellStyle(cellStyle);

			Cell derecec = row.createCell(++columnCount);
			derecec.setCellValue(derece);
			derecec.setCellStyle(cellStyle);

			Cell id = row.createCell(++columnCount);
			id.setCellValue(SM.getId());
			id.setCellStyle(cellStyle);

			Cell amount = row.createCell(++columnCount);
			amount.setCellValue(SM.getName());
			amount.setCellStyle(cellStyle);

			Cell nation = row.createCell(++columnCount);
			nation.setCellValue(SM.getNation());
			nation.setCellStyle(cellStyle);

			Cell email = row.createCell(++columnCount);
			email.setCellValue(SM.getEmail());
			email.setCellStyle(cellStyle);

			Cell gsm = row.createCell(++columnCount);
			gsm.setCellValue(SM.getGsm());
			gsm.setCellStyle(cellStyle);

			StringBuilder portf = new StringBuilder();
			Set<Account> acc = SM.getAccount();

			for (Account account : acc) {
				portf.append(account.getName());

				if (acc.size() > 1)
					portf.append(" / ");

			}

			Cell account = row.createCell(++columnCount);
			account.setCellValue(portf.toString());
			account.setCellStyle(cellStyle);

			Cell yon = row.createCell(++columnCount);
			yon.setCellValue(SM.getFirstPlatform());
			yon.setCellStyle(cellStyle);

			Cell cur = row.createCell(++columnCount);
			cur.setCellValue(SM.getCurrentPlatform());
			cur.setCellStyle(cellStyle);

			Cell issuedate = row.createCell(++columnCount);
			if (SM.getCreateDate() != null) {

				issuedate.setCellValue(Time.ToTurkishDateMonthDateOnly(SM.getCreateDate()));
			}
			issuedate.setCellStyle(cellStyle);

			Cell update = row.createCell(++columnCount);
			if (SM.getLastDate() != null) {

				update.setCellValue(Time.ToTurkishDateMonthDateOnly(SM.getLastDate()));
			}
			update.setCellStyle(cellStyle);

			Cell info = row.createCell(++columnCount);
			info.setCellValue(SM.getFirstInfo());
			info.setCellStyle(cellStyle);

			Cell lupdate = row.createCell(++columnCount);
			lupdate.setCellValue(SM.getLastUpdate());
			lupdate.setCellStyle(cellStyle);

		}
		String path = "SosyalMedya_" + Randomize.TwoDigit() + ".xlsx";
		try (FileOutputStream outputStream = new FileOutputStream(initProperties.FILEFOLDERNAME + path)) {
			workbook.write(outputStream);

		}
		return path;

	}

	public String CrateExcelFromContracts(Page<Contract> pageAuthors, DocumentFilterForm dform,
			DocumentService documentService) throws FileNotFoundException, IOException {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("Installment Books");
		sheet.setDefaultRowHeightInPoints(80);

		DecimalFormatSymbols dfs = new DecimalFormatSymbols();
		dfs.setDecimalSeparator(',');
		dfs.setGroupingSeparator('.');
		DecimalFormat formatter = new DecimalFormat("###,###,###.##");
		formatter.setDecimalFormatSymbols(dfs);

		sheet.setColumnWidth(0, 30 * 256);
		sheet.setColumnWidth(1, 20 * 256);
		sheet.setColumnWidth(2, 20 * 256);
		sheet.setColumnWidth(3, 20 * 256);
		sheet.setColumnWidth(4, 20 * 256);
		sheet.setColumnWidth(5, 20 * 256);
		sheet.setColumnWidth(6, 50 * 256);
		sheet.setColumnWidth(7, 20 * 256);

		CellStyle style = workbook.createCellStyle();
		CellStyle cellStyle = workbook.createCellStyle();
		CellStyle styleWrap = workbook.createCellStyle();

		style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());

		style.setWrapText(true);
		styleWrap.setWrapText(true);

		int rowCount = 0;

		int titleCount = -1;
		Row titleRow = sheet.createRow(0);

		Cell Tsira = titleRow.createCell(++titleCount);
		Tsira.setCellValue("SIRA");
		Tsira.setCellStyle(style);

		Cell Tno = titleRow.createCell(++titleCount);
		Tno.setCellValue("SÖZLEŞME NUMARASI");
		Tno.setCellStyle(style);

		Cell Tname = titleRow.createCell(++titleCount);
		Tname.setCellValue("MÜŞTERİ ADI");
		Tname.setCellStyle(style);

		Cell Tportfoy = titleRow.createCell(++titleCount);
		Tportfoy.setCellValue("PORTFÖY");
		Tportfoy.setCellStyle(style);

		Cell Tdate = titleRow.createCell(++titleCount);
		Tdate.setCellValue("SÖZLEŞME TARİHİ");
		Tdate.setCellStyle(style);

		Cell Tprice = titleRow.createCell(++titleCount);
		Tprice.setCellValue("SATIŞ TUTARI");
		Tprice.setCellStyle(style);

		Cell Tremaın = titleRow.createCell(++titleCount);
		Tremaın.setCellValue("KALAN TUTAR");
		Tremaın.setCellStyle(style);

		Cell Tdoc = titleRow.createCell(++titleCount);
		Tdoc.setCellValue("EKSİK EVRAKLAR");
		Tdoc.setCellStyle(style);

		int count = 0;
		for (Contract contract : pageAuthors) {

			Row row = sheet.createRow(++rowCount);

			cellStyle = workbook.createCellStyle();
			cellStyle.setWrapText(true);

			int columnCount = -1;

			Cell sira = row.createCell(++columnCount);
			sira.setCellValue(++count);
			sira.setCellStyle(cellStyle);

			Cell no = row.createCell(++columnCount);
			no.setCellValue(contract.getContract_no());
			no.setCellStyle(cellStyle);

			Cell name = row.createCell(++columnCount);
			name.setCellValue(contract.getCustomer().iterator().next().getFname());
			name.setCellStyle(cellStyle);

			Cell portfoy = row.createCell(++columnCount);
			StringBuilder builder = new StringBuilder();
			int portfoySize = contract.getPortfoys().size();
			int portfoyCounter = 0;

			for (Portfoy portfo : contract.getPortfoys()) {

				portfoyCounter++;

				int secSize = portfo.getSections().size();
				int secCounter = 0;

				for (Section section : portfo.getSections()) {
					secCounter++;

					builder.append(section.getKat().getBlok().getTitle() + "-");
					builder.append(section.getTitle());
					if (secCounter < secSize) {
						builder.append(" / ");
					}
				}

				if (portfoyCounter < portfoySize) {
					builder.append(" || ");
				}

			}
			portfoy.setCellValue(builder.toString());
			portfoy.setCellStyle(cellStyle);

			Cell date = row.createCell(++columnCount);
			date.setCellValue(Time.ToTurkishDateMonthDateOnly(contract.getCreateDate()));
			date.setCellStyle(cellStyle);

			Cell price = row.createCell(++columnCount);
			String source = Source.CurrencySource().get(contract.getCurrency() - 1);
			price.setCellValue(
					formatter.format(Float.parseFloat(contract.getPrice().replaceAll("[^\\d.]", ""))) + " " + source);
			price.setCellStyle(cellStyle);

			Cell reamount = row.createCell(++columnCount);

			Set<Installment> Installments = contract.getInstallment();

			float sum = 0l;
			for (Installment installment : Installments) {
				float amount = 0l;
				if (installment.isPaid()) {

					if (installment.isPartial()) {
						// partial
						if (installment.getPartial_amount() != null) {

							amount = Float.parseFloat(installment.getPartial_amount().replaceAll("[^\\d.]", ""));
						}

					} else {
						// amount

						amount = Float.parseFloat(installment.getAmount().replaceAll("[^\\d.]", ""));
					}

				} else if (installment.isPartial()) {
					// partial
					if (installment.getPartial_amount() != null) {

						amount = Float.parseFloat(installment.getPartial_amount().replaceAll("[^\\d.]", ""));
					}

				}

				sum += amount;

			}

			if (!contract.getCash().isEmpty()) {

				reamount.setCellValue(
						formatter
								.format(Float.parseFloat(contract.getPrice().replaceAll("[^\\d.]", ""))
										- Float.parseFloat(contract.getCash().replaceAll("[^\\d.]", "")) - sum)
								+ " " + source);
				reamount.setCellStyle(cellStyle);

			} else {

				reamount.setCellValue(
						formatter.format(Float.parseFloat(contract.getPrice().replaceAll("[^\\d.]", "")) - sum) + " "
								+ source);
				reamount.setCellStyle(cellStyle);

			}

			Page<DocType> checkDoc = documentService.loadCheckedDocsTypes(dform);
			
			int docCount = 0;
			
			StringBuilder docs = new StringBuilder();

			for (DocType docType : checkDoc.getContent()) {
				//
				docCount++;
				dform.setCid(contract.getId());
				dform.setDocType(docType.getId());
				dform.setPsize(50);
				Page<Document> DOC = documentService.loadAll(dform);
				if (DOC.getContent().size() > 0) {
					Document document = DOC.iterator().next();
					String path = document.getFile_path();
					boolean ex = FileExist.check(initProperties.FILEFOLDERNAME + path);
					if (!ex) {
						docs.append(docType.getTitle());
						if(docCount<checkDoc.getContent().size() ) {
							docs.append(" || ");
						}
					}

				}else {
					docs.append(docType.getTitle());
					if(docCount<checkDoc.getContent().size() ) {
						docs.append(" || ");
					}
				}

			}
			
			Cell doc = row.createCell(++columnCount);
			doc.setCellValue(docs.toString());
			doc.setCellStyle(cellStyle);

		}

		String path = "CONTRACT_" + Randomize.TwoDigit() + ".xlsx";
		try (FileOutputStream outputStream = new FileOutputStream(initProperties.FILEFOLDERNAME + path)) {
			workbook.write(outputStream);

		}
		return path;
	}

}
