package rza.BPFco.utilities;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import rza.BPFco.model.portfoys.Installment;

public class Source {

	public static ArrayList<String> CustomerSource() {
		ArrayList<String> cur = new ArrayList<String>();
		cur.add("Direkt");
		cur.add("Kapıdan");
		cur.add("Sosyal Medya");
		cur.add("Tavsiye");
		cur.add("Acente");
		cur.add("Sosyal Medya Direkt");
		return cur;
	}

	public static ArrayList<String> CurrencySource() {
		ArrayList<String> cur = new ArrayList<String>();
		cur.add("TL");
		cur.add("USD");
		cur.add("EURO");

		return cur;
	}
	public static ArrayList<String> SenetType() {
		ArrayList<String> cur = new ArrayList<String>();
		cur.add("Malen");
		cur.add("Nakden");
		
		return cur;
	}
	public static ArrayList<String> RangeSource() {
		ArrayList<String> cur = new ArrayList<String>();
		cur.add("30");
		cur.add("60");
		cur.add("90");
		cur.add("120");
		
		return cur;
	}
	
//	public static ArrayList<String> MessageType() {
//		ArrayList<String> msg = new ArrayList<String>();
//		msg.add(1,"Hatırlatma");
//		msg.add(2,"Teşekkür");
//		msg.add(3,"Uyarı");
//		
//		return msg;
//	}
	public static ArrayList<String> MessageType2() {
		ArrayList<String> msg = new ArrayList<String>();
		msg.add("Hatırlatma");
		msg.add("Teşekkür");
		msg.add("Uyarı");
		
		return msg;
	}
	public static String convertToCurrency(long amount) {
		
		DecimalFormatSymbols dfs = new DecimalFormatSymbols();
		dfs.setDecimalSeparator(',');
		dfs.setGroupingSeparator('.');
		DecimalFormat formatter = new DecimalFormat("###,###,###");
		formatter.setDecimalFormatSymbols(dfs);
		String formattedNumber = formatter.format(amount);
		 

		return formattedNumber;
	}

	public static int getSection(String str) {
		Pattern lastIntPattern = Pattern.compile("[^0-9]+([0-9]+)$");
		int lastNumberInt = 0;
		Matcher matcher = lastIntPattern.matcher(str.replaceAll("\\s",""));
		if (matcher.find()) {
			String someNumberStr = matcher.group(1);
			lastNumberInt = Integer.parseInt(someNumberStr);
		}

		return lastNumberInt;
	}
	
	
	public static List<Date> autoInstallmentDate(int mounth,int loop,Date first){
		
		List<Date> issue_date = new ArrayList<Date>();
		issue_date.add(first);
		
		Date lastDay;
		lastDay = first;
		
		for (int i = 0; i < loop-1; i++) {
			
			Date newDate = Time.CustomDaysAfter(lastDay,mounth);
			lastDay = newDate;
			issue_date.add(newDate);
			
		}
		return issue_date;
		
	}
	
	public static List<String> getAutoStringNumber(String a,String b){
		
		List<String> titles= new ArrayList<String>();
		
		
		for (int i = Integer.parseInt(a); i <Integer.parseInt (b)+1; i++) {
			
			titles.add(String.valueOf(i));
			
		}
		
		
		return titles;
		
	}
	
	@SuppressWarnings("static-access")
	public static ArrayList<String> getMounthBetwwenDates (String a,String b) {
	      
	        String date1 = a;
	        String date2 = b;

	        DateFormat formater = new SimpleDateFormat("MM/DD/yyyy");

	        Calendar beginCalendar = Calendar.getInstance();
	        Calendar finishCalendar = Calendar.getInstance();

	        try {
	        	Date A = Time.ConvertToDate(date1);
	            beginCalendar.setTime(Time.ConvertToDate(date1));
	            finishCalendar.setTime(Time.ConvertToDate(date2));
	            
	        } catch (ParseException e) {
	            e.printStackTrace();
	        }

	        DateFormat formaterYd = new SimpleDateFormat("M");
	        DateFormat formaterYY = new SimpleDateFormat("YYYY");
	        
	        ArrayList<String> dates = new ArrayList<String>();

	        while (beginCalendar.before(finishCalendar)) {

	            String date =     formaterYd.format(beginCalendar.getTime()).toUpperCase();
		        String year =     formaterYY.format(beginCalendar.getTime()).toUpperCase();

		        dates.add(date+"-"+year);
//	            System.out.println(date);
	            // Add One Month to get next Month
	            beginCalendar.add(Calendar.MONTH, 1);
	        }
	        
	        int s = beginCalendar.getTime().getDate();
	        int f = finishCalendar.getTime().getDate();
	        
	        
	        if(s>=f) {
	        String date =     formaterYd.format(finishCalendar.getTime()).toUpperCase();
	        String year =     formaterYY.format(finishCalendar.getTime()).toUpperCase();
	        dates.add(date+"-"+year);
	        	
	        }
	        
	        
			return dates;
	    
	}
	public static String getMounth (Date date) {
		DateFormat formaterYd = new SimpleDateFormat("M");
		String D = formaterYd.format(date.getTime()).toUpperCase();
		return D;
		
	}
	public static String getYear (Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int year = cal.get(Calendar.YEAR);
		DateFormat formaterYd = new SimpleDateFormat("YYYY");
		String D = formaterYd.format(date.getTime()).toUpperCase();
		return String.valueOf(year);
		
	}
	
	public static ArrayList<String>  getMounthTitle(List<String> dates) {
		
		ArrayList<String> mStrings = new ArrayList<String>();
		for (String string : dates) {
			
			switch (string) {
			case "01":
				mStrings.add("OCAK");
				break;
			case "02":
				mStrings.add("ŞUBAT");
				
				break;
			case "03":
				mStrings.add("MART");
				
				break;
			case "04":
				mStrings.add("NİSAN");
				
				break;
			case "05":
				mStrings.add("MAYIS");
				
				break;
			case "06":
				mStrings.add("HAZİRAN");
				
				break;
			case "07":
				mStrings.add("TEMMUZ");
				
				break;
			case "08":
				mStrings.add("AĞUSTOS");
				
				break;
			case "09":
				mStrings.add("EYLÜL");
				
				break;
			case "10":
				mStrings.add("EKİM");
				
				break;
			case "11":
				mStrings.add("KASIM");
				
				break;
			case "12":
				mStrings.add("ARALIK");
				
				break;
			
			
			}
		}
		
		
		return mStrings;
		
	}
	
	

}
