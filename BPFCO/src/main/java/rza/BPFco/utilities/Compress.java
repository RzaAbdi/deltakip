package rza.BPFco.utilities;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

public class Compress {

    public static void compressImg(String inPath, String outPath) {
	try {
	    File imageFile = new File(inPath);
	    File compressedImageFile = new File(outPath);
	    InputStream is = new FileInputStream(imageFile);

	    Image img = ImageIO.read(is);
	    int w = img.getWidth(null);
	    int h = img.getHeight(null);
	    Image image = img.getScaledInstance(w , h , Image.SCALE_DEFAULT);
	    BufferedImage bi = createResizedCopy(image, w / 8, h / 8, true);
	    ImageIO.write(bi, "jpg", compressedImageFile);
	} catch (IOException e) {
	    System.out.println("Error");
	}

    }

    static BufferedImage createResizedCopy(Image originalImage, int scaledWidth, int scaledHeight,
	    boolean preserveAlpha) {
	int imageType = preserveAlpha ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
	BufferedImage scaledBI = new BufferedImage(scaledWidth, scaledHeight, imageType);
	Graphics2D g = scaledBI.createGraphics();
	if (preserveAlpha) {
	    g.setComposite(AlphaComposite.Src);
	}
	g.drawImage(originalImage, 0, 0, scaledWidth, scaledHeight, null);
	g.dispose();
	return scaledBI;
    }

}
