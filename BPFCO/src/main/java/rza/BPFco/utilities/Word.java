package rza.BPFco.utilities;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.apache.xmlbeans.impl.store.Locale;

import com.lowagie.text.Font;
import com.lowagie.text.pdf.BaseFont;

import fr.opensagres.poi.xwpf.converter.pdf.PdfConverter;
import fr.opensagres.poi.xwpf.converter.pdf.PdfOptions;
import fr.opensagres.xdocreport.itext.extension.font.IFontProvider;
import fr.opensagres.xdocreport.itext.extension.font.ITextFontRegistry;
import rza.BPFco.config.initProperties;
import rza.BPFco.form.AdminContractForm;
import rza.BPFco.model.portfoys.Customer;

/**
 * TODO Put here a description of what this class does.
 *
 * @author rza. Created Oct 18, 2020.
 */

public class Word {

	public Word() {

	}

	public String ManipulateWord(InputStream inputStream, String customer, String date, String address, String mail,
			String gsm, String portfoys, String ekmadde) {

		try {

			XWPFDocument document = new XWPFDocument(inputStream);
			List<XWPFParagraph> data = document.getParagraphs();

			for (XWPFTable tbl : document.getTables()) {
				for (XWPFTableRow row : tbl.getRows()) {
					for (XWPFTableCell cell : row.getTableCells()) {
						for (XWPFParagraph p : cell.getParagraphs()) {
							for (XWPFRun r : p.getRuns()) {
								String text = r.getText(0);
								if (text != null && text.contains("##date##")) {
									text = text.replace("##date##", date);
									r.setText(text, 0);
								}
								if (text != null && text.contains("##customer##")) {
									text = text.replace("##customer##", customer);
									r.setText(text, 0);
								}

							}
						}
					}
				}
			}

			for (XWPFParagraph p : data) {
				for (XWPFRun r : p.getRuns()) {
					String text = r.getText(0);

					if (text != null && text.contains("##date##")) {
						text = text.replace("##date##", date);
						r.setText(text, 0);
					}
					if (text != null && text.contains("##customer##")) {
						text = text.replace("##customer##", customer);
						r.setText(text, 0);
					}
					if (text != null && text.contains("##address##")) {
						text = text.replace("##address##", address);
						r.setText(text, 0);
					}
					if (text != null && text.contains("##mail##")) {
						text = text.replace("##mail##", mail);
						r.setText(text, 0);
					}
					if (text != null && text.contains("##gsm##")) {
						text = text.replace("##gsm##", gsm);
						r.setText(text, 0);
					}
					if (text != null && text.contains("##portfoy##")) {
						text = text.replace("##portfoy##", portfoys);
						r.setText(text, 0);
					}
					if (text != null && text.contains("parçası niteliğindedir")) {
						
						 
						r.setText(text, 0);
					}
					if (text != null && text.contains("##order##")) {
						
						if (ekmadde != null && !ekmadde.isEmpty()) {
							text = text.replace("##order##", "14.");
						} else {
							
							text = text.replace("##order##", "");
						}
						
						r.setBold(true);
						r.setText(text, 0);
					}
					if (text != null && text.contains("##ekmadde##")) {

						if (ekmadde != null && !ekmadde.isEmpty()) {
							 
							text = text.replace("##ekmadde##", String.valueOf("  " + ekmadde));
						} else {

							text = text.replace("##ekmadde##", "");
						}

						r.setText(text, 0);
					}

					if (text != null && text.contains("##count##")) {

						if (ekmadde != null) {

							if (!ekmadde.isEmpty()) {
								text = text.replace("##count##", ("14"));
								r.setText(text, 0);

							} else {
								text = text.replace("##count##", ("13"));
								r.setText(text, 0);
							}
						}
					}

				}
			}

			try {
				String path = customer + "-" + Randomize.TwoDigit() + ".pdf";
				PdfOptions options = PdfOptions.create();
				options.fontProvider(new IFontProvider() {

					@Override
					public Font getFont(String familyName, String encoding, float size, int style, Color color) {

						try {
							BaseFont bfChinese = BaseFont.createFont("../../resources/fonts/CALIBRI.ttf",
									BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
							Font fontChinese = new Font(bfChinese, size, style, color);
							if (familyName != null)
								fontChinese.setFamily(familyName);
							return fontChinese;
						} catch (Throwable e) {
							e.printStackTrace();
							// An error occurs, use the default font provider.
							return ITextFontRegistry.getRegistry().getFont(familyName, encoding, size, style, color);
						}

					}

				});

				FileOutputStream out = new FileOutputStream((initProperties.FILEFOLDERNAME + "/contracts/" + path));
				PdfConverter.getInstance().convert(document, out, options);
				return path;

			} catch (Exception exception) {
				exception.printStackTrace();
			}

//        	 String path = "SosyalMedya_" + Randomize.TwoDigit() + ".docx";
//     		try (FileOutputStream outputStream = new FileOutputStream(initProperties.FILEFOLDERNAME + path)) {
//     			document.write(outputStream);
//
//     		}
//     		return path;

		} catch (Exception exep) {
			exep.printStackTrace();
		}
		return null;

	}

	public String ManipulateTeslimWord(InputStream inputStream, String customer, String date, String project,
			String projectAddress, String portfoys, String ada) {

		try {
			
			
			if(customer==null) {
				customer = "null";
			}
			if(date==null) {
				date = "null";
			}
			if(project==null) {
				project = "null";
			}
			if(projectAddress==null) {
				projectAddress = "null";
			}
			if(portfoys==null) {
				portfoys = "null";
			}
			if(ada==null) {
				ada = "null";
			}

			XWPFDocument document = new XWPFDocument(inputStream);
			List<XWPFParagraph> data = document.getParagraphs();

			for (XWPFTable tbl : document.getTables()) {
				for (XWPFTableRow row : tbl.getRows()) {
					for (XWPFTableCell cell : row.getTableCells()) {
						for (XWPFParagraph p : cell.getParagraphs()) {
							for (XWPFRun r : p.getRuns()) {
								String text = r.getText(0);
								if (text != null && text.contains("##customer##")) {
									text = text.replace("##customer##", customer);
									r.setText(text, 0);
								}
								if (text != null && text.contains("##ada##")) {
									text = text.replace("##ada##", ada);
									r.setText(text, 0);
								}
								if (text != null && text.contains("##portfoys##")) {
									text = text.replace("##portfoys##", portfoys);
									r.setText(text, 0);
								}
								if (text != null && text.contains("##projectAddress##")) {
									text = text.replace("##projectAddress##", projectAddress);
									r.setText(text, 0);
								}
								if (text != null && text.contains("##project##")) {
									text = text.replace("##project##", project);
									r.setText(text, 0);
								}

							}
						}
					}
				}
			}

			for (XWPFParagraph p : data) {
				for (XWPFRun r : p.getRuns()) {
					String text = r.getText(0);

					if (text != null && text.contains("##contractDate##")) {
						text = text.replace("##contractDate##", date);
						r.setText(text, 0);
					}
					if (text != null && text.contains("##customer##")) {
						text = text.replace("##customer##", customer);
						r.setText(text, 0);
					}
					if (text != null && text.contains("##projectAddress##")) {
						text = text.replace("##projectAddress##", projectAddress);
						r.setText(text, 0);
					}
					if (text != null && text.contains("##ada##")) {
						text = text.replace("##ada##", ada);
						r.setText(text, 0);
					}

					if (text != null && text.contains("##portfoys##")) {
						text = text.replace("##portfoys##", portfoys);
						r.setText(text, 0);
					}
					if (text != null && text.contains("##project##")) {
						text = text.replace("##project##", project);
						r.setText(text, 0);
					}

				}
			}

			try {
				String path = customer + "-" + Randomize.TwoDigit() + ".pdf";
				PdfOptions options = PdfOptions.create();
				options.fontProvider(new IFontProvider() {

					@Override
					public Font getFont(String familyName, String encoding, float size, int style, Color color) {

						try {
							BaseFont bfChinese = BaseFont.createFont("../../resources/fonts/CALIBRI.ttf",
									BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
							Font fontChinese = new Font(bfChinese, size, style, color);
							if (familyName != null)
								fontChinese.setFamily(familyName);
							return fontChinese;
						} catch (Throwable e) {
							e.printStackTrace();
							// An error occurs, use the default font provider.
							return ITextFontRegistry.getRegistry().getFont(familyName, encoding, size, style, color);
						}

					}

				});

				FileOutputStream out = new FileOutputStream((initProperties.FILEFOLDERNAME + "/contracts/" + path));
				PdfConverter.getInstance().convert(document, out, options);
				return path;

			} catch (Exception exception) {
				exception.printStackTrace();
			}


		} catch (Exception exep) {
			exep.printStackTrace();
		}
		return null;

	}

	public String ManipulateTeknikWord(InputStream inputStream, String customer, String date, String project,
			String portfoys, String ek) {

		try {

			XWPFDocument document = new XWPFDocument(inputStream);
			List<XWPFParagraph> data = document.getParagraphs();

			for (XWPFTable tbl : document.getTables()) {
				for (XWPFTableRow row : tbl.getRows()) {
					for (XWPFTableCell cell : row.getTableCells()) {
						for (XWPFParagraph p : cell.getParagraphs()) {
							for (XWPFRun r : p.getRuns()) {
								String text = r.getText(0);

								if (text != null && text.contains("##portfoys##")) {
									text = text.replace("##portfoys##", portfoys);
									r.setText(text, 0);
								}

								if (text != null && text.contains("##project##")) {
									text = text.replace("##project##", project);
									r.setText(text, 0);
								}

								if (text != null && text.contains("##contractDate##")) {
									text = text.replace("##contractDate##", date);
									r.setText(text, 0);
								}

								if (text != null && text.contains("##customer##")) {
									text = text.replace("##customer##", customer);
									r.setText(text, 0);
								}

								if (text != null && text.contains("12-)")) {

									if (ek != null) {
										if (!ek.isEmpty()) {

											r.setText(" " + ek);

										} else {
											r.setText("", 0);
										}
									} else {
										r.setText("", 0);

									}

								}

							}
						}
					}
				}
			}

			for (XWPFParagraph p : data) {
				for (XWPFRun r : p.getRuns()) {
					String text = r.getText(0);

				}
			}

			try {
				String path = customer + "-" + Randomize.TwoDigit() + ".pdf";
				PdfOptions options = PdfOptions.create();
				options.fontProvider(new IFontProvider() {

					@Override
					public Font getFont(String familyName, String encoding, float size, int style, Color color) {

						try {
							BaseFont bfChinese = BaseFont.createFont("../../resources/fonts/CALIBRI.ttf",
									BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
							Font fontChinese = new Font(bfChinese, size, style, color);
							if (familyName != null)
								fontChinese.setFamily(familyName);
							return fontChinese;
						} catch (Throwable e) {
							e.printStackTrace();
							// An error occurs, use the default font provider.
							return ITextFontRegistry.getRegistry().getFont(familyName, encoding, size, style, color);
						}

					}

				});

				FileOutputStream out = new FileOutputStream((initProperties.FILEFOLDERNAME + "/contracts/" + path));
				PdfConverter.getInstance().convert(document, out, options);
				return path;

			} catch (Exception exception) {
				exception.printStackTrace();
			}


		} catch (Exception exep) {
			exep.printStackTrace();
		}
		return null;

	}

	public String ManipulateDekorWord(InputStream inputStream, AdminContractForm form) {

		try {

			XWPFDocument document = new XWPFDocument(inputStream);
			List<XWPFParagraph> data = document.getParagraphs();

			for (XWPFTable tbl : document.getTables()) {
				for (XWPFTableRow row : tbl.getRows()) {
					for (XWPFTableCell cell : row.getTableCells()) {
						for (XWPFParagraph p : cell.getParagraphs()) {
							for (XWPFRun r : p.getRuns()) {
								String text = r.getText(0);

								if (text != null && text.contains("##contractDate##")) {
									text = text.replace("##contractDate##", Time.ToSplitedDate(form.getCreateDate()));
									r.setText(text, 0);
								}
								
								if (text != null && text.contains("##customer##")) {
									text = text.replace("##customer##", (form.getCustomer()));
									r.setText(text, 0);
								}

							}
						}
					}
				}
			}

			for (XWPFParagraph p : data) {
				for (XWPFRun r : p.getRuns()) {
					String text = r.getText(0);

					if (text != null && text.contains("##date##")) {
						text = text.replace("##date##", Time.ToSplitedDateCalneder(form.getCreateDate()));
						r.setText(text, 0);
					}
					if (text != null && text.contains("##customer##")) {
						text = text.replace("##customer##", (form.getCustomer()));
						r.setText(text, 0);
					}
					if (text != null && text.contains("##cur##")) {
						text = text.replace("##cur##", (Source.CurrencySource().get(form.getCurrency() - 1)));
						r.setText(text, 0);
					}
					if (text != null && text.contains("##amount##")) {
						
						DecimalFormatSymbols dfs = new DecimalFormatSymbols();
						dfs.setDecimalSeparator(',');
						dfs.setGroupingSeparator('.');
						DecimalFormat formatter = new DecimalFormat("###,###,###.00");
						formatter.setDecimalFormatSymbols(dfs);
						String dollar = formatter.format(Float.parseFloat(form.getAmount().replaceAll("[^\\d.]", "")));
						
						text = text.replace("##amount##", dollar);
						r.setText(text, 0);
					}
					if (text != null && text.contains("##amountText##")) {
						String am = form.getAmount();

						String strNew = am.replace(",", "").replace(".", "");
						strNew = strNew.substring(0, strNew.length() - 2);

						long c = Long.valueOf(strNew);

						String dollar = NumToWords.convert(c).replaceAll("\\s+", "");
						text = text.replace("##amountText##", dollar);
						r.setText(text, 0);
					}

					if (text != null && text.contains("##mount##")) {
						
						
						
						text = text.replace("##mount##", String.valueOf(Source.RangeSource().get(form.getRange() - 1)));
						r.setText(text, 0);
					}
					if (text != null && text.contains("##price##")) {
						
						DecimalFormatSymbols dfs = new DecimalFormatSymbols();
						dfs.setDecimalSeparator(',');
						dfs.setGroupingSeparator('.');
						DecimalFormat formatter = new DecimalFormat("###,###,###.00");
						formatter.setDecimalFormatSymbols(dfs);
						String dollar = formatter.format(Float.parseFloat(form.getPrice().replaceAll("[^\\d.]", "")));
						
						text = text.replace("##price##", dollar);
						r.setText(text, 0);
					}
					if (text != null && text.contains("##dayofmount##")) {
						text = text.replace("##dayofmount##", String.valueOf(form.getDayOfMount()));
						r.setText(text, 0);
					}
					if (text != null && text.contains("##installmentCount##")) {
						text = text.replace("##installmentCount##", String.valueOf(form.getLimit()));
						r.setText(text, 0);
					}
					if (text != null && text.contains("##ekmadde##")) {

						if (form.getDescription() != null) {

							text = text.replace("##ekmadde##", String.valueOf(form.getDescription()));
						} else {

							text = text.replace("##ekmadde##", "");
						}

						r.setText(text, 0);
					}
					if (text != null && text.contains("##priceText##")) {

						String am = form.getPrice();
						
						

						String strNew = am.replace(",", "").replace(".", "");
						strNew = strNew.substring(0, strNew.length() - 2);

						long c = Long.valueOf(strNew);

						String dollar = NumToWords.convert(c).replaceAll("\\s+", "");

						text = text.replace("##priceText##",
								dollar + " " + Source.CurrencySource().get(form.getCurrency() - 1));
						r.setText(text, 0);
					}

				}
			}

			try {
				String path = form.getCustomer() + "-" + Randomize.TwoDigit() + ".pdf";
				PdfOptions options = PdfOptions.create();
				options.fontProvider(new IFontProvider() {

					@Override
					public Font getFont(String familyName, String encoding, float size, int style, Color color) {

						try {
							BaseFont bfChinese = BaseFont.createFont("../../resources/fonts/CALIBRI.ttf",
									BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
							Font fontChinese = new Font(bfChinese, size, style, color);
							if (familyName != null)
								fontChinese.setFamily(familyName);
							return fontChinese;
						} catch (Throwable e) {
							e.printStackTrace();
							// An error occurs, use the default font provider.
							return ITextFontRegistry.getRegistry().getFont(familyName, encoding, size, style, color);
						}

					}

				});

				FileOutputStream out = new FileOutputStream((initProperties.FILEFOLDERNAME + "/contracts/" + path));
				PdfConverter.getInstance().convert(document, out, options);
				return path;

			} catch (Exception exception) {
				exception.printStackTrace();
			}

//        	 String path = "SosyalMedya_" + Randomize.TwoDigit() + ".docx";
//     		try (FileOutputStream outputStream = new FileOutputStream(initProperties.FILEFOLDERNAME + path)) {
//     			document.write(outputStream);
//
//     		}
//     		return path;

		} catch (Exception exep) {
			exep.printStackTrace();
		}
		return null;
	}

	public String ManipulateDekorWord(InputStream inputStream, Date createDate, Customer customer, String ek) {
		try {

			XWPFDocument document = new XWPFDocument(inputStream);
			List<XWPFParagraph> data = document.getParagraphs();

			for (XWPFTable tbl : document.getTables()) {
				for (XWPFTableRow row : tbl.getRows()) {
					for (XWPFTableCell cell : row.getTableCells()) {
						for (XWPFParagraph p : cell.getParagraphs()) {
							for (XWPFRun r : p.getRuns()) {
								String text = r.getText(0);

								if (text != null && text.contains("##customer##")) {
									text = text.replace("##customer##", customer.getFname());
									r.setText(text, 0);
								}

							}
						}
					}
				}
			}

			for (XWPFParagraph p : data) {
				for (XWPFRun r : p.getRuns()) {
					String text = r.getText(0);

					if (text != null && text.contains("##date##")) {
						text = text.replace("##date##", Time.ToSplitedDateCalneder(createDate));
						r.setText(text, 0);
					}
					if (text != null && text.contains("##customer##")) {
						text = text.replace("##customer##", (customer.getFname()));
						r.setText(text, 0);
					}
					if (text != null && text.contains("##gsm##")) {
						text = text.replace("##gsm##", (customer.getGsm1()));
						r.setText(text, 0);
					}
					if (text != null && text.contains("##address##")) {
						text = text.replace("##address##", (customer.getAddress()));
						r.setText(text, 0);
					}
					if (text != null && text.contains("##mail##")) {
						text = text.replace("##mail##", (customer.getMail()));
						r.setText(text, 0);
					}
					if (text != null && text.contains("taşımaktadır.")) {

						if (ek != null) {
							if (!ek.isEmpty()) {
								r.addBreak();
								r.addBreak();
								r.setBold(true);

								r.setText("7-");
								r.setText("     ");

								r.setText(ek);

							}
						}

					}
					if (text != null && text.contains("##6##")) {

						if (ek != null) {

							if (!ek.isEmpty()) {
								text = text.replace("##6##", ("7"));
								r.setText(text, 0);

							} else {
								text = text.replace("##6##", ("6"));
								r.setText(text, 0);
							}
						}
					}

				}
			}

			try {
				String path = customer.getFname() + "-" + Randomize.TwoDigit() + ".pdf";
				PdfOptions options = PdfOptions.create();
				options.fontProvider(new IFontProvider() {

					@Override
					public Font getFont(String familyName, String encoding, float size, int style, Color color) {

						try {
							BaseFont bfChinese = BaseFont.createFont("../../resources/fonts/CALIBRI.ttf",
									BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
							Font fontChinese = new Font(bfChinese, size, style, color);
							if (familyName != null)
								fontChinese.setFamily(familyName);
							return fontChinese;
						} catch (Throwable e) {
							e.printStackTrace();
							// An error occurs, use the default font provider.
							return ITextFontRegistry.getRegistry().getFont(familyName, encoding, size, style, color);
						}

					}

				});

				FileOutputStream out = new FileOutputStream((initProperties.FILEFOLDERNAME + "/contracts/" + path));
				PdfConverter.getInstance().convert(document, out, options);
				return path;

			} catch (Exception exception) {
				exception.printStackTrace();
			}

		} catch (Exception exep) {
			exep.printStackTrace();
		}
		return null;
	}

	public String ManipulateRadissonWord(InputStream inputStream, AdminContractForm form) {
		try {

			XWPFDocument document = new XWPFDocument(inputStream);
			List<XWPFParagraph> data = document.getParagraphs();

			for (XWPFParagraph p : data) {
				for (XWPFRun r : p.getRuns()) {
					String text = r.getText(0);

					if (text != null && text.contains("##date##")) {
						text = text.replace("##date##", Time.ToSplitedDateCalneder(form.getCreateDate()));
						r.setText(text, 0);
					}

					if (text != null && text.contains("##ekmadde##")) {

						if (form.getDescription() != null) {

							text = text.replace("##ekmadde##", String.valueOf(form.getDescription()));
						} else {

							text = text.replace("##ekmadde##", "");
						}

						r.setText(text, 0);
					}

				}
			}
			
			for (XWPFTable tbl : document.getTables()) {
				for (XWPFTableRow row : tbl.getRows()) {
					for (XWPFTableCell cell : row.getTableCells()) {
						for (XWPFParagraph p : cell.getParagraphs()) {
							for (XWPFRun r : p.getRuns()) {
								String text = r.getText(0);

								if (text != null && text.contains("##date##")) {
									text = text.replace("##date##", Time.ToSplitedDateCalneder(form.getCreateDate()));
									r.setText(text, 0);
								}

								if (text != null && text.contains("##ekmadde##")) {

									if (form.getDescription() != null) {

										text = text.replace("##ekmadde##", String.valueOf(form.getDescription()));
									} else {

										text = text.replace("##ekmadde##", "");
									}

									r.setText(text, 0);
								}

							}
						}
					}
				}
			}

			try {
				String path = "radisson_" + Randomize.TwoDigit() + Randomize.TwoDigit() + ".pdf";
				PdfOptions options = PdfOptions.create();
				options.fontProvider(new IFontProvider() {

					@Override
					public Font getFont(String familyName, String encoding, float size, int style, Color color) {

						try {
							BaseFont bfChinese = BaseFont.createFont("../../resources/fonts/Courgette-Regular.ttf",
									BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
							Font fontChinese = new Font(bfChinese, size, style, color);
							if (familyName != null)
								fontChinese.setFamily(familyName);
							return fontChinese;
						} catch (Throwable e) {
							e.printStackTrace();
							// An error occurs, use the default font provider.
							return ITextFontRegistry.getRegistry().getFont(familyName, encoding, size, style, color);
						}
						

					}

				});

				FileOutputStream out = new FileOutputStream((initProperties.FILEFOLDERNAME + "/contracts/" + path));
				PdfConverter.getInstance().convert(document, out, options);
				return path;

			} catch (Exception exception) {
				exception.printStackTrace();
			}

		} catch (Exception exep) {
			exep.printStackTrace();
		}
		return null;
	}

	public String ManipulateAydinlatmaWord(InputStream inputStream, AdminContractForm form) {
		try {

			XWPFDocument document = new XWPFDocument(inputStream);
			List<XWPFParagraph> data = document.getParagraphs();

			for (XWPFParagraph p : data) {
				for (XWPFRun r : p.getRuns()) {
					String text = r.getText(0);

					if (text != null && text.contains("##customer##")) {
						text = text.replace("##customer##", form.getCustomer());
						r.setText(text, 0);
					}

					if (text != null && text.contains("##ekmadde##")) {

						if (form.getDescription() != null) {

							text = text.replace("##ekmadde##", String.valueOf(form.getDescription()));
						} else {

							text = text.replace("##ekmadde##", "");
						}

						r.setText(text, 0);
					}

				}
			}

			try {
				String path = "aydinlatma_" + Randomize.TwoDigit() + Randomize.TwoDigit() + ".pdf";
				PdfOptions options = PdfOptions.create();
				options.fontProvider(new IFontProvider() {

					@Override
					public Font getFont(String familyName, String encoding, float size, int style, Color color) {

						try {
							BaseFont bfChinese = BaseFont.createFont("../../resources/fonts/CALIBRI.ttf",
									BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
							Font fontChinese = new Font(bfChinese, size, style, color);
							if (familyName != null)
								fontChinese.setFamily(familyName);
							return fontChinese;
						} catch (Throwable e) {
							e.printStackTrace();
							// An error occurs, use the default font provider.
							return ITextFontRegistry.getRegistry().getFont(familyName, encoding, size, style, color);
						}

					}

				});

				FileOutputStream out = new FileOutputStream((initProperties.FILEFOLDERNAME + "/contracts/" + path));
				PdfConverter.getInstance().convert(document, out, options);
				return path;

			} catch (Exception exception) {
				exception.printStackTrace();
			}

		} catch (Exception exep) {
			exep.printStackTrace();
		}
		return null;
	}

	public String ManipulateDekontWord(InputStream inputStream, AdminContractForm form, String account, String proje,
			String portfoys) {
		try {

			XWPFDocument document = new XWPFDocument(inputStream);
			List<XWPFParagraph> data = document.getParagraphs();

			for (XWPFTable tbl : document.getTables()) {
				for (XWPFTableRow row : tbl.getRows()) {
					for (XWPFTableCell cell : row.getTableCells()) {
						for (XWPFParagraph p : cell.getParagraphs()) {
							for (XWPFRun r : p.getRuns()) {
								String text = r.getText(0);

								if (text != null && text.contains("##customer##")) {
									text = text.replace("##customer##", form.getCustomer());
									r.setText(text, 0);
								}
								if (text != null && text.contains("##account##")) {
									
									 
									
									text = text.replace("##account##", account);
									r.setText(text, 0);
								}

							}
						}
					}
				}
			}

			for (XWPFParagraph p : data) {
				for (XWPFRun r : p.getRuns()) {
					String text = r.getText(0);

					if (text != null && text.contains("##portfoy##")) {
						text = text.replace("##portfoy##", portfoys);
						r.setText(text, 0);
					}
					if (text != null && text.contains("##proje##")) {
						text = text.replace("##proje##", proje);
						r.setText(text, 0);
					}
					if (text != null && text.contains("##price##")) {
						
						DecimalFormatSymbols dfs = new DecimalFormatSymbols();
						dfs.setDecimalSeparator(',');
						dfs.setGroupingSeparator('.');
						DecimalFormat formatter = new DecimalFormat("###,###,###");
						formatter.setDecimalFormatSymbols(dfs);
						String dollar = formatter.format(Float.parseFloat(form.getPrice().replaceAll("[^\\d.]", "")));
						text = text.replace("##price##", dollar);
						r.setText(text, 0);
					}

					if (text != null && text.contains("##cur##")) {
						text = text.replace("##cur##", (Source.CurrencySource().get(form.getCurrency() - 1)));
						r.setText(text, 0);
					}

					if (text != null && text.contains("##date##")) {
						text = text.replace("##date##", Time.ToSplitedDateCalneder(form.getCreateDate()));
						r.setText(text, 0);
					}

					if (text != null && text.contains("##ekmadde##")) {

						if (form.getDescription() != null) {

							text = text.replace("##ekmadde##", String.valueOf(form.getDescription()));
						} else {

							text = text.replace("##ekmadde##", "");
						}

						r.setText(text, 0);
					}

				}
			}

			try {
				String path = "dekont_" + Randomize.TwoDigit() + Randomize.TwoDigit() + ".pdf";
				PdfOptions options = PdfOptions.create();
				options.fontProvider(new IFontProvider() {

					@Override
					public Font getFont(String familyName, String encoding, float size, int style, Color color) {

						try {
							BaseFont bfChinese = BaseFont.createFont("../../resources/fonts/CALIBRI.ttf",
									BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
							Font fontChinese = new Font(bfChinese, size, style, color);
							if (familyName != null)
								fontChinese.setFamily(familyName);
							return fontChinese;
						} catch (Throwable e) {
							e.printStackTrace();
							// An error occurs, use the default font provider.
							return ITextFontRegistry.getRegistry().getFont(familyName, encoding, size, style, color);
						}

					}

				});

				FileOutputStream out = new FileOutputStream((initProperties.FILEFOLDERNAME + "/contracts/" + path));
				PdfConverter.getInstance().convert(document, out, options);
				return path;

			} catch (Exception exception) {
				exception.printStackTrace();
			}

		} catch (Exception exep) {
			exep.printStackTrace();
		}
		return null;
	}

}
