package rza.BPFco.utilities;

import java.text.DecimalFormat;

public class NumToWords {

	 public NumToWords() {
		 
	 }
	 
	 public static String convertLessThanOneThousand(int number) {
		 
		 String soFar;
		 if (number % 100 < 20){
			 
		      soFar = numNames[number % 100].trim();
		      number /= 100;
		    }
		    else {
		      soFar = numNames[number % 10].trim();
		      number /= 10;

		      soFar = tensNames[number % 10].trim() + soFar;
		      number /= 10;
		    }
		 
		 if (number == 0) return soFar;
		 
		 
		 if(number ==1) return "YÜZ"+ soFar;
		 if(number !=1) return numNames[number].trim() + "YÜZ" + soFar;
		 
		 
		  
		 
		    return numNames[number].trim() + "YÜZ" + soFar;
		  }
		 
	 public static String convert(long number) {
		    // 0 to 999 999 999 999
		    if (number == 0) { return "SIFIR"; }

		    String snumber = Long.toString(number);

		    // pad with "0"
		    String mask = "000000000000";
		    DecimalFormat df = new DecimalFormat(mask);
		    snumber = df.format(number);

		    // XXXnnnnnnnnn
		    int billions = Integer.parseInt(snumber.substring(0,3));
		    // nnnXXXnnnnnn
		    int millions  = Integer.parseInt(snumber.substring(3,6));
		    // nnnnnnXXXnnn
		    int hundredThousands = Integer.parseInt(snumber.substring(6,9));
		    // nnnnnnnnnXXX
		    int thousands = Integer.parseInt(snumber.substring(9,12));

		    String tradBillions;
		    switch (billions) {
		    case 0:
		      tradBillions = "";
		      break;
		    case 1 :
		      tradBillions = convertLessThanOneThousand(billions)
		      + "MİLYAR";
		      break;
		    default :
		      tradBillions = convertLessThanOneThousand(billions)
		      + "MİLYAR";
		    }
		    String result =  tradBillions;

		    String tradMillions;
		    switch (millions) {
		    case 0:
		      tradMillions = "";
		      break;
		    case 1 :
		      tradMillions = convertLessThanOneThousand(millions)
		         + "MİLYON";
		      break;
		    default :
		      tradMillions = convertLessThanOneThousand(millions)
		         + "MİLYON";
		    }
		    result =  result + tradMillions;

		    String tradHundredThousands;
		    switch (hundredThousands) {
		    case 0:
		      tradHundredThousands = "";
		      break;
		    case 1 :
		      tradHundredThousands = "BİN";
		      break;
		    default :
		      tradHundredThousands = convertLessThanOneThousand(hundredThousands)
		         + "BİN";
		    }
		    result =  result + tradHundredThousands;

		    String tradThousand;
		    tradThousand = convertLessThanOneThousand(thousands);
		    result =  result + tradThousand;

		    // remove extra spaces!
		    return result.replaceAll("^\\s+", "").replaceAll("\\b\\s{2,}\\b", "");
		  }
		 
	 
	 private static final String[] tensNames = {
			    "",
			    "ON",
			    "YİRMİ",
			    "OTUZ",
			    "KIRK",
			    "ELLİ",
			    "ALTMIŞ",
			    "YETMİŞ",
			    "SEKSEN",
			    "DOKSAN"
			  };

			  private static final String[] numNames = {
			    "",
			    "BİR",
			    "İKİ",
			    "ÜÇ",
			    "DÖRT",
			    "BEŞ",
			    "ALTI",
			    "YEDİ",
			    "SEKİZ",
			    "DOKUZ",
			    "ON",
			    "ONBİR",
			    "ONİKİ",
			    "ONÜÇ",
			    "ONDÖRT",
			    "ONBEŞ",
			    "ONALTI",
			    "ONYEDİ",
			    "ONSEKİZ",
			    "ONDOKUZ"
			  };
 

}
