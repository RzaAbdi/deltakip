package rza.BPFco.utilities;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import rza.BPFco.model.portfoys.InsCover;
import rza.BPFco.model.portfoys.Kat;
import rza.BPFco.model.portfoys.Reply;
import rza.BPFco.model.portfoys.Section;

public class Sorting {
	
	
	
	public Sorting(){
		
	}
	

	 
	public  List<InsCover> SortInsCovers(List<InsCover> ins) {
		
		Collections.sort(ins, new CoverInsComparator());
		
		return ins;
		
	};
	public  List<Section> SortSection(List<Section> sections) {

		Collections.sort(sections, new SectionComparator());

		return sections;

	};
	
	public  List<Kat> SortKat(List<Kat> kats) {

		Collections.sort(kats, new FloorComparator());

		return kats;

	};
	
	public  List<Reply> SortReply(List<Reply> replies) {
		Collections.sort(replies, new LexicographicComparatorReply());

		return replies;

	};

	class LexicographicComparatorReply implements Comparator<Reply> {
		@Override
		public int compare(Reply a, Reply b) {
	        return a.getId() > b.getId() ? -1 : a.getId() ==  b.getId() ? 0 : 1;

		}
	}
	class LexicographicComparator implements Comparator<Section> {
		@Override
		public int compare(Section a, Section b) {
			return a.getTitle().compareToIgnoreCase(b.getTitle());
		}
	}
	
	
	class SectionComparator implements Comparator<Section> {
	    @Override
	    public int compare(Section a, Section b) {
	        return Long.parseLong(a.getTitle()) < Long.parseLong(b.getTitle()) ? -1 : Long.parseLong(a.getTitle()) ==  Long.parseLong(b.getTitle()) ? 0 : 1;
	    }
	}
	
	class FloorComparator implements Comparator<Kat> {
	    @Override
	    public int compare(Kat a, Kat b) {
	        return Long.parseLong(a.getTitle()) < Long.parseLong(b.getTitle()) ? -1 : Long.parseLong(a.getTitle()) ==  Long.parseLong(b.getTitle()) ? 0 : 1;
	    }
	}
	
	class DateComparator implements Comparator<Date> {
	    @Override
	    public int compare(Date a, Date b) {
	        return a.compareTo(b);
	    }
	}
	class CoverInsComparator implements Comparator<InsCover> {
		 

		@Override
		public int compare(InsCover o1, InsCover o2) {
			 if (o1.getIssueDate() == null || o2.getIssueDate() == null)
			        return 0;
			      return o1.getIssueDate().compareTo(o2.getIssueDate());
		}
	}
	
	
}