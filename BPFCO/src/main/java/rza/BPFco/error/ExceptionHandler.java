package rza.BPFco.error;

import java.security.Principal;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.base.Throwables;

/**
 * General error handler for the application.
 */
@ControllerAdvice
class ExceptionHandlers {
	static final Logger logger = LogManager.getLogger();
	/**
	 * Handle exceptions thrown by handlers.
	 */
	@org.springframework.web.bind.annotation.ExceptionHandler(value = Exception.class)
	public ModelAndView exception(Exception exception, WebRequest request,Principal principal) {
		ModelAndView modelAndView = new ModelAndView("error/general");
		Throwable rootCause = Throwables.getRootCause(exception);
		modelAndView.addObject("errorMessage", rootCause);
		modelAndView.addObject("user", "ij");
		System.out.println(rootCause.toString());
		logger.error(rootCause.toString(), exception);
		return modelAndView;
	}
	

}