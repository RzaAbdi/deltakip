package rza.BPFco.model;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "userrole")
public class UserRole {

	public static final Set<UserRole> ADMIN = new HashSet<UserRole>(Arrays.asList(new UserRole(1L)));
	public static final Set<UserRole> GUEST = new HashSet<UserRole>(Arrays.asList(new UserRole(2L)));
	public static final Set<UserRole> USER = new HashSet<UserRole>(Arrays.asList(new UserRole(3L)));
	
	public static final String user = "ROLE_USER";
	public static final String admin = "ROLE_ADMIN";
	

	@Id
	@GeneratedValue
	private Long id;

	@Column
	private String name;
	
	@Column
	private Long bayi = 1L;

	@Column
	private String description;

//	@ManyToMany(mappedBy = "userroles")
//	private List<Category> categories;

	@ManyToMany(mappedBy="userroles")
	private List<Account> accounts;

	 
	
	private boolean active;

	public UserRole() {
	}

	public UserRole(long id) {
		this.id = id;
	}


	public Long getBayi() {
		return bayi;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return the id of tag
	 */
	public Long getId() {
		return id;
	}
	
	/**
	 * @return the name of tag
	 */
	public String getName() {
		return name;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
		
	}

	public void setBayi(Long bayi) {
		this.bayi = bayi;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public List<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<Account> accounts) {
		this.accounts = accounts;
	}

 


	 
	@Override
    public boolean equals(Object anObject) {
        if (anObject instanceof UserRole) {
        	UserRole another = (UserRole) anObject;
            if (another.getId().equals(this.id)) {
                return true;
            }
        }
        return false;
    }
}
