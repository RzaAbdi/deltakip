
package rza.BPFco.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import rza.BPFco.model.portfoys.Agent;
import rza.BPFco.model.portfoys.Customer;
import rza.BPFco.model.portfoys.Document;
import rza.BPFco.model.portfoys.Installment;
import rza.BPFco.model.portfoys.Proje;

@SuppressWarnings("serial")
@Entity
@Table(name = "contract")
public class Contract implements java.io.Serializable {

	@Id
	@GeneratedValue
	private Long id;

	@Column
	private Date createDate;
	@Column
	private Date downDate;

	public Date getDownDate() {
		return this.downDate;
	}

	public void setDownDate(Date downDate) {
		this.downDate = downDate;
	}

	@Column
	private String price;
	
	@Column
	private String section;
	
	public Long getRemaining() {
		return this.remaining;
	}

	public void setRemaining(Long remaining) {
		this.remaining = remaining;
	}

	@Column
	private Long remaining;

	public String getSection() {
		return this.section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	@Column
	private int currency;

	@Lob 
	@Column(name="LAST_PORTFOY", length=512)
	private String last_portfoy;

	public String getLast_portfoy() {
		return this.last_portfoy;
	}

	public void setLast_portfoy(String last_portfoy) {
		this.last_portfoy = last_portfoy;
	}

	public int getCurrency() {
		return this.currency;
	}

	public void setCurrency(int currency) {
		this.currency = currency;
	}

	@Column
	@Lob
	private String description;

	@Column
	@Lob
	private String description2;

	public String getDescription2() {
		return this.description2;
	}

	public void setDescription2(String description2) {
		this.description2 = description2;
	}

	@Column
	private boolean installmented;
	
	@Column
	private boolean deed;
	
	@Column(columnDefinition="boolean default false")
	private boolean insEnd=false;

	public boolean isInsEnd() {
		return this.insEnd;
	}

	public void setInsEnd(boolean insEnd) {
		this.insEnd = insEnd;
	}

	public boolean isDeed() {
		return this.deed;
	}

	public void setDeed(boolean deed) {
		this.deed = deed;
	}

	@Column
	private boolean active;

	public boolean isActive() {
		return this.active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPrice() {
		return this.price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getCash() {
		return this.cash;
	}

	public void setCash(String cash) {
		this.cash = cash;
	}

	public String getContract_no() {
		return this.contract_no;
	}

	public void setContract_no(String contract_no) {
		this.contract_no = contract_no;
	}

	@Column
	private String cash;

	@Column
	private String contract_no;

	public Contract(Long id) {
		this.id = id;
	}

	public Contract() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isInstallmented() {
		return this.installmented;
	}

	public void setInstallmented(boolean installmented) {
		this.installmented = installmented;
	}

	@OneToMany(mappedBy = "contract", fetch = FetchType.EAGER)
	private Set<Portfoy> portfoys;

	@OneToMany(mappedBy = "contract", fetch = FetchType.EAGER)
	private Set<Installment> installment;

	public Set<Installment> getInstallment() {
		return this.installment;
	}

	public void setInstallment(Set<Installment> installment) {
		this.installment = installment;
	}

	public Set<Document> getDocuments() {
		return this.documents;
	}

	public void setDocuments(Set<Document> documents) {
		this.documents = documents;
	}
	
//	@ManyToOne
//	@OnDelete(action = OnDeleteAction.CASCADE)
//	@JoinColumn(name = "proje")
//	private Proje proje;

//	public Proje getProje() {
//		return this.proje;
//	}
//
//	public void setProje(Proje proje) {
//		this.proje = proje;
//	}

	@ManyToMany(mappedBy = "contract" )
	private Set<Document> documents; 
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "contrat_customer", joinColumns = {
			@JoinColumn(name = "contratid", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "customerid", referencedColumnName = "id") })
	private Set<Customer> customer;

	 

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "contrat_agent", joinColumns = {
			@JoinColumn(name = "contratid", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "agentid", referencedColumnName = "id") })
	private Set<Agent> agent;

	public Set<Agent> getAgent() {
		return this.agent;
	}

	public void setAgent(Set<Agent> agent) {
		this.agent = agent;
	}

	public Set<Account> getAccount() {
		return this.account;
	}

	public void setAccount(Set<Account> account) {
		this.account = account;
	}

	@ManyToMany
	@JoinTable(name = "contrat_account", joinColumns = {
			@JoinColumn(name = "contratid", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "accontid", referencedColumnName = "id") })
	private Set<Account> account;

//	@ManyToMany
//	@JoinTable(name = "prtfoy_tags", joinColumns = {@JoinColumn(name = "portfoyid", referencedColumnName = "id")}, inverseJoinColumns = {@JoinColumn(name = "tagid", referencedColumnName = "id")})
//	private Set<Tag> tag;

	public Set<Customer> getCustomer() {
		return this.customer;
	}

	public void setCustomer(Set<Customer> customer) {
		this.customer = customer;
	}

	public Set<Portfoy> getPortfoys() {
		return this.portfoys;
	}

	public void setPortfoys(Set<Portfoy> portfoys) {
		this.portfoys = portfoys;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

}
