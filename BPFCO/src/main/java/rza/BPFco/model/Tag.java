package rza.BPFco.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tag")
public class Tag {
	
	public Tag() {
		super();
	}
	
	
	@Id
	@GeneratedValue
	private Long id;
	
	@ManyToMany(mappedBy = "tag")
	private Set<Portfoy> portfoys;
	
	 
	
	 


	public Set<Portfoy> getPortfoys() {
		return this.portfoys;
	}


	public void setPortfoys(Set<Portfoy> portfoys) {
		this.portfoys = portfoys;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	@Column
	private int type;


	public int getType() {
		return this.type;
	}


	public void setType(int type) {
		this.type = type;
	}


	@Column
	private String title;
	
	
	@Column
	private boolean active;
	
	
	public boolean isActive() {
		return active;
	}


	public void setActive(boolean active) {
		this.active = active;
	}

 
	

}
