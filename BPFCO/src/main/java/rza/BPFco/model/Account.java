
package rza.BPFco.model;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import rza.BPFco.model.portfoys.Chase;
import rza.BPFco.model.portfoys.Customer;
import rza.BPFco.model.portfoys.Document;
import rza.BPFco.model.portfoys.Reply;
import rza.BPFco.model.portfoys.Section;
import rza.BPFco.model.portfoys.SosyalMedya;

@SuppressWarnings("serial")
@Entity
@Table(name = "account")
@NamedQuery(name = Account.FIND_BY_EMAIL, query = "select a from Account a where a.email = :email")
public class Account implements java.io.Serializable {

	public static final String FIND_BY_EMAIL = "Account.findByEmail";

	public static final String ROLE_ADMIN = "ROLE_ADMIN";
	public static final String ROLE_USER = "ROLE_USER";

	public static String getFindByEmail() {
		return FIND_BY_EMAIL;
	}

	public static String getRoleAdmin() {
		return ROLE_ADMIN;
	}

	public static String getRoleUser() {
		return ROLE_USER;
	}

	public static Account guestUser() {
		Account account = new Account(2L);
		if(account.getEmail()==null)
			account.setEmail("");
		account.setUserroles(UserRole.GUEST);
		return account;
	}

	@Column(columnDefinition = "boolean default false")
	private boolean active = false; // if account is active or not
	
	@OneToMany(mappedBy="account",fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<Chase> chase;
	
	@ManyToMany(mappedBy = "account")
	private Set<Contract> contract;
	
	 

	@Column
	private Date createDate;

	@ManyToMany(mappedBy = "account")
	private Set<Customer> customer;

	@OneToMany(mappedBy = "account", fetch = FetchType.EAGER)
	private Set<Document> document;

	@Column(unique = true)
	private String email;

	@Id
	@GeneratedValue
	private Long id;
	
	@Column
	private boolean isAdmin;
	
	@Column(columnDefinition = "boolean default false")
	private boolean isWatcher = false;
	
	@Column(columnDefinition = "boolean default false")
	private boolean isCallFa = false;
	
	@Column(columnDefinition = "boolean default false")
	private boolean isCallAr = false;
	
	
	public boolean isWatcher() {
		return this.isWatcher;
	}

	public boolean isCallFa() {
		return this.isCallFa;
	}

	public void setCallFa(boolean isCallFa) {
		this.isCallFa = isCallFa;
	}

	public boolean isCallAr() {
		return this.isCallAr;
	}

	public void setCallAr(boolean isCallAr) {
		this.isCallAr = isCallAr;
	}

	public void setWatcher(boolean isWatcher) {
		this.isWatcher = isWatcher;
	}

	@Column(columnDefinition = "boolean default true")
	private boolean language = true;
	
	@Column
	private Date lastsign;

	@Column()
	private String name;

	@JsonIgnore
	private String password;

	private String role = ROLE_USER;

	@ManyToMany(mappedBy = "account" )
	private Set<SosyalMedya> sosyalMedya;
	
	@ManyToMany(mappedBy = "account" )
	private Set<Ajanda> ajandas;

	@ManyToMany
	@JoinTable(name = "User_roles", joinColumns = {@JoinColumn(name = "user", referencedColumnName = "id")}, inverseJoinColumns = {@JoinColumn(name = "role", referencedColumnName = "id")})
	private Set<UserRole> userroles;

	public Account() {
		super();
	}

	public Account(Long id) {
		this.id = id;
	}

	//	public Account(String email, String password, String role) {
//		this.email = email;
//		this.password = password;
//		this.role = role;
//	}
//
	public Account(
			String email, String password,  
			String role,Date createDate, String name
		) {
		 
		this.email = email;
		this.password = password;
		this.createDate = createDate;
		 this.name = name;
		 
		this.role = role;
	}

	//	 
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return Collections.singleton(new SimpleGrantedAuthority(getRole()));

	}
//
//	 
//	 

	public List<Chase> getChase() {
		return this.chase;
	}
	
	public Set<Contract> getContract() {
		return this.contract;
	}

	public Date getCreateDate() {
	    return createDate;
	}

	public Set<Customer> getCustomer() {
		return this.customer;
	}
 
	
	public Set<Document> getDocument() {
		return this.document;
	}

	public String getEmail() {
		return email;
	}

	public Long getId() {
		return id;
	}

	public Date getLastsign() {
		return this.lastsign;
	}

	public String getName() {
		return this.name;
	}

	public String getPassword() {
		return password;
	}
	
	
	 
	public String getRole() {
		return role;
	}
	 
	
	public Set<SosyalMedya> getSosyalMedya() {
		return this.sosyalMedya;
	}

	 
	 
	

	public Set<UserRole> getUserroles() {
		return userroles;
	}
	
	public boolean isActive() {
		return active;
	}
	
	

	public boolean isAdmin() {
		return isAdmin;
	}
	

	public boolean isLanguage() {
		return language;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

 

 

	public void setChase(List<Chase> chase) {
		this.chase = chase;
	}

 
	public void setContract(Set<Contract> contract) {
		this.contract = contract;
	}
 
 

	public void setCreateDate(Date createDate) {
	    this.createDate = createDate;
	}
 

	public void setCustomer(Set<Customer> customer) {
		this.customer = customer;
	}

	 
	public void setDocument(Set<Document> document) {
		this.document = document;
	}



	public void setEmail(String email) {
		this.email = email;
	}

	public void setId(Long id) {
		this.id = id;
	}

 

	public void setLanguage(boolean language) {
		this.language = language;
	}

	public void setLastsign(Date lastsign) {
		this.lastsign = lastsign;
	}

 


	 public void setName(String name) {
		this.name = name;
	} 

 
	
 

	 
	
	public void setPassword(String password) {
		this.password = password;
	}

	 
 

	public void setRole(String role) {
		this.role = role;
	}

 

 
	
	
public void setSosyalMedya(Set<SosyalMedya> sosyalMedya) {
	this.sosyalMedya = sosyalMedya;
}
 

	 
//	
//	@Override
//    public boolean equals(Object anObject) {
//        if (anObject instanceof Account) {
//        	Account another = (Account) anObject;
//            if (another.getId().equals(this.id)) {
//                return true;
//            }
//        }
//        return false;
//    }

	 

	 
	public void setUserroles(Set<UserRole> userroles) {
		this.userroles = userroles;
	}
 
	
//	
//	public boolean isGuest(){
//		return this.id.equals(2L);
//	}
}
