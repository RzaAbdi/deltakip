package rza.BPFco.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import rza.BPFco.model.portfoys.Reply;
import rza.BPFco.model.portfoys.SosyalMedya;

@Entity
@Table(name = "ajanda")
public class Ajanda {
	
	public Ajanda() {
		super();
	}
	
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column
	private String title;
	
	@Column
	private int status;
	
	public int getStatus() {
		return this.status;
	}


	public void setStatus(int status) {
		this.status = status;
	}


	@Column
	private Date createDate;
	
	@Column
	private Date lastUpdate;
	
	@Column
	private Date issueDate;
	
	@Column(columnDefinition = "boolean default false")
	private boolean done= false;
	
	
	public boolean isDone() {
		return this.done;
	}


	public void setDone(boolean done) {
		this.done = done;
	}


	public Date getIssueDate() {
		return this.issueDate;
	}


	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}


	public SosyalMedya getSosyalmedya() {
		return this.sosyalmedya;
	}


	public void setSosyalmedya(SosyalMedya sosyalmedya) {
		this.sosyalmedya = sosyalmedya;
	}


	@Column
	private int type;
	
	 
	@ManyToMany()
	@JoinTable(name = "ajanda_account", joinColumns = {
			@JoinColumn(name = "ajid", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "accountid", referencedColumnName = "id") })
	private Set<Account> account;
	
//	@ManyToOne
//	@JoinColumn(name = "sosyalmedya")
//	private SosyalMedya sosyalMedya;
	
	@ManyToOne
	@JoinColumn(name = "sosyalmedya")
	private SosyalMedya sosyalmedya;
	
	@OneToMany(mappedBy = "ajanda")
	private Set<Reply> reply;


	public int getType() {
		return this.type;
	}


	public void setType(int type) {
		this.type = type;
	}


	 
	public Long getId() {
		return this.id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getTitle() {
		return this.title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public Date getCreateDate() {
		return this.createDate;
	}


	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	public Date getLastUpdate() {
		return this.lastUpdate;
	}


	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}


	public Set<Account> getAccount() {
		return this.account;
	}


	public void setAccount(Set<Account> account) {
		this.account = account;
	}


	public Set<Reply> getReply() {
		return this.reply;
	}


	public void setReply(Set<Reply> reply) {
		this.reply = reply;
	}
	

}
