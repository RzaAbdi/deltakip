
package rza.BPFco.model;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import rza.BPFco.model.portfoys.Section;

@SuppressWarnings("serial")
@Entity
@Table(name = "portfoy")
public class Portfoy implements java.io.Serializable {

	@Id
	@GeneratedValue
	private Long id;

	@Column
	private Date createDate;
	
	@Column(columnDefinition="boolean default false")
	private boolean saled=false;
	
	@Column(columnDefinition="boolean default false")
	private boolean reserved=false;
	
	public boolean isReserved() {
		return this.reserved;
	}

	public void setReserved(boolean reserved) {
		this.reserved = reserved;
	}

	public boolean isSaled() {
		return this.saled;
	}

	public void setSaled(boolean saled) {
		this.saled = saled;
	}

	@Column
	private String title;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Portfoy(Long id) {
		this.id = id;
	}

	public Portfoy() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@OneToMany(mappedBy = "portfoy", fetch = FetchType.EAGER,cascade = {CascadeType.PERSIST})
	private Set<Section> sections;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "prtfoy_tags", joinColumns = {@JoinColumn(name = "portfoyid", referencedColumnName = "id")}, inverseJoinColumns = {@JoinColumn(name = "tagid", referencedColumnName = "id")})
	private Set<Tag> tag;
	
	@ManyToOne
	@JoinColumn(name = "contract")
	private Contract contract;

	public Contract getContract() {
		return this.contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public Set<Tag> getTags() {
		return this.tag;
	}

	public void setTags(Set<Tag> tags) {
		this.tag = tags;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Set<Section> getSections() {
		return this.sections;
	}

	public void setSections(Set<Section> sections) {
		this.sections = sections;
	}

	public Set<Tag> getTag() {
		return this.tag;
	}

	public void setTag(Set<Tag> tag) {
		this.tag = tag;
	}

	 

	 
}
