
package rza.BPFco.model.portfoys;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import rza.BPFco.model.Account;
import rza.BPFco.model.Contract;

@SuppressWarnings("serial")
@Entity
@Table(name = "document")
public class Document implements java.io.Serializable {

	@Id
	@GeneratedValue
	private Long id;

	@Column
	private Date create_date;

	public int getType() {
		return this.type;
	}

	public void setType(int type) {
		this.type = type;
	}

	@Column
	private Date editDate;
	
	@Column
	private int type;

	@Column
	private String file_path;
	
	@ManyToOne
	@JoinColumn(name = "account")
	private Account account;
	
	@ManyToOne
	@JoinColumn(name = "section")
	private Section section;

	public Section getSection() {
		return this.section;
	}

	public void setSection(Section section) {
		this.section = section;
	}

	public Account getAccount() {
		return this.account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "docs_types", joinColumns = {
			@JoinColumn(name = "docid", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "typeid", referencedColumnName = "id") })
	private Set<DocType> doctype;
	
	@ManyToMany()
	@JoinTable(name = "docs_contract", joinColumns = {
			@JoinColumn(name = "docid", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "contractid", referencedColumnName = "id") })
	private Set<Contract> contract;
	
	@ManyToMany()
	@JoinTable(name = "docs_installment", joinColumns = {
			@JoinColumn(name = "docid", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "installmentid", referencedColumnName = "id") })
	private Set<Installment> installment;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "docs_customer", joinColumns = {
			@JoinColumn(name = "docid", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "customerid", referencedColumnName = "id") })
	private Set<Customer> customer;
	
	@ManyToMany()
	@JoinTable(name = "docs_agent", joinColumns = {
			@JoinColumn(name = "docid", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "agentid", referencedColumnName = "id") })
	private Set<Agent> agent;
	
	
	
	
	 
	
	
 

	 

	public Set<Contract> getContract() {
		return this.contract;
	}

	public void setContract(Set<Contract> contract) {
		this.contract = contract;
	}

	public Set<Installment> getInstallment() {
		return this.installment;
	}

	public void setInstallment(Set<Installment> installment) {
		this.installment = installment;
	}

	public Set<Customer> getCustomer() {
		return this.customer;
	}

	public void setCustomer(Set<Customer> customer) {
		this.customer = customer;
	}

	public Set<Agent> getAgent() {
		return this.agent;
	}

	public void setAgent(Set<Agent> agent) {
		this.agent = agent;
	}

	public Set<DocType> getDoctype() {
		return this.doctype;
	}

	public void setDoctype(Set<DocType> doctype) {
		this.doctype = doctype;
	}

	public String getFile_path() {
		return this.file_path;
	}

	public void setFile_path(String file_path) {
		this.file_path = file_path;
	}

	public Date getCreate_date() {
		return this.create_date;
	}

	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}


	public Date getEditDate() {
		return this.editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Document(Long id) {
		this.id = id;
	}

	@Column
	private String title;

	public Document() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
