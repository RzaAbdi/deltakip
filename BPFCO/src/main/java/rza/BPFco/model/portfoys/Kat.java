
package rza.BPFco.model.portfoys;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "kat")
public class Kat implements java.io.Serializable {

 

	@Id
	@GeneratedValue
	private Long id;
	
	@Column
	private Date create_date;
	
	@Column
	private Date edit_date;
	
	 

	public Date getCreate_date() {
		return this.create_date;
	}


	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}


	public Date getEdit_date() {
		return this.edit_date;
	}


	public void setEdit_date(Date edit_date) {
		this.edit_date = edit_date;
	}


 


	public String getTitle() {
		return this.title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public Kat(Long id) {
		this.id = id;
	}
	
	@Column
	private String title;
	

	public Kat() {
		super();
	}

	 
	public Long getId() {
		return id;
	}
 
 
	
	public void setId(Long id) {
		this.id = id;
	}

	public Blok getBlok() {
		return blok;
	}


	public void setBlok(Blok blok) {
		this.blok = blok;
	}


	public List<Section> getSections() {
		return sections;
	}


	public void setSections(List<Section> sections) {
		this.sections = sections;
	}

	@ManyToOne
	@JoinColumn(name = "blok")
	private Blok blok;
	
	@OneToMany(mappedBy="kat",fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	private List<Section> sections;

}
