
package rza.BPFco.model.portfoys;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import rza.BPFco.model.Contract;

@SuppressWarnings("serial")
@Entity
@Table(name = "installment")
public class Installment implements java.io.Serializable {

 

	@Id
	@GeneratedValue
	private Long id;
	
	private int type;

	public int getType() {
		return this.type;
	}

	public void setType(int type) {
		this.type = type;
	}

	@Column
	private Date issueDate;
	
	public Set<Mes> getMes() {
		return this.mes;
	}

	public void setMes(Set<Mes> mes) {
		this.mes = mes;
	}

	@ManyToMany()
	@JoinTable(name = "installemt_mes", joinColumns = {
			@JoinColumn(name = "insid", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "msgid", referencedColumnName = "id") })
	private Set<Mes> mes;

	public Date getIssueDate() {
		return this.issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}
	
	

	public Date getPaymantDate() {
		return this.paymantDate;
	}

	public void setPaymantDate(Date paymantDate) {
		this.paymantDate = paymantDate;
	}

	public Set<Document> getDocuments() {
		return this.documents;
	}

	public void setDocuments(Set<Document> documents) {
		this.documents = documents;
	}

	@Column
	private Date paymantDate;
	
	@Column
	private String amount;
	@Column
	private int currency;
	
	@Column(columnDefinition="boolean default false")
	private boolean reminding=false;
	
	public boolean isReminding() {
		return this.reminding;
	}

	public void setReminding(boolean reminding) {
		this.reminding = reminding;
	}

	public boolean isThanks() {
		return this.thanks;
	}

	public void setThanks(boolean thanks) {
		this.thanks = thanks;
	}

	public boolean isWarning() {
		return this.warning;
	}

	public void setWarning(boolean warning) {
		this.warning = warning;
	}

	@Column(columnDefinition="boolean default false")
	private boolean thanks=false;
	
	@Column(columnDefinition="boolean default false")
	private boolean warning=false;
	
	
	
	 
	public int getCurrency() {
		return this.currency;
	}

	public void setCurrency(int currency) {
		this.currency = currency;
	}

	@Column
	private boolean paid;
	
	@Column
	private boolean partial;
	
	@Column
	@Lob
	private String description;
	
	@Column
	private String partial_amount;
	
	@Column(columnDefinition="boolean default true")
	private boolean active=true;
	
	public boolean isActive() {
		return this.active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isPartial() {
		return this.partial;
	}

	public void setPartial(boolean partial) {
		this.partial = partial;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPartial_amount() {
		return this.partial_amount;
	}

	public void setPartial_amount(String partial_amount) {
		this.partial_amount = partial_amount;
	}

	public Installment() {
		super();
	}
	
	@ManyToOne
	@JoinColumn(name = "contract")
	private Contract contract;
	
	 

	
	 
//	public Set<Msg> getMsg() {
//		return this.msg;
//	}
//
//	public void setMsg(Set<Msg> msg) {
//		this.msg = msg;
//	}

//	@ManyToMany(mappedBy = "installment" )
//	private Set<Msg> msg; 
	 
	@ManyToMany(mappedBy = "installment" )
	private Set<Document> documents; 

	public Installment(Long id) {
		 this.id = id;
	}

	public Contract getContract() {
		return this.contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

 
	 

	public String getAmount() {
		return this.amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public boolean isPaid() {
		return this.paid;
	}

	public void setPaid(boolean paid) {
		this.paid = paid;
	}

	 

 
	 
	 

	 

	 
}
