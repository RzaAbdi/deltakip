
package rza.BPFco.model.portfoys;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "doctype")
public class DocType implements java.io.Serializable {

	@Id
	@GeneratedValue
	private Long id;
	
	@Column(columnDefinition = "boolean default false")
	private boolean controlled = false;
	
	public boolean isControlled() {
		return this.controlled;
	}

	public void setControlled(boolean controlled) {
		this.controlled = controlled;
	}

	@ManyToMany(mappedBy = "doctype" )
	private Set<Document> document;

	public String getTitle() {
		return title;
	}

	public Set<Document> getDocument() {
		return this.document;
	}

	public void setDocument(Set<Document> document) {
		this.document = document;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public DocType(Long id) {
		this.id = id;
	}

	@Column
	private String title;

	public DocType() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
