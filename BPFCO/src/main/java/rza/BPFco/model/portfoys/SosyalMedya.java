
package rza.BPFco.model.portfoys;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import rza.BPFco.model.Account;
import rza.BPFco.model.Ajanda;

@SuppressWarnings("serial")
@Entity
@Table(name = "sosyalmedya")
public class SosyalMedya implements java.io.Serializable {

	@Id
	@GeneratedValue
	private Long id;

	@Column
	private Date createDate;

	@Column
	private Date lastDate;
	
	@Column
	private int language;
	
	@Column
	private int type;
	
	@Column
	private int priority;
	
	@Column
	private String firstPlatform;
	
	@Column
	private String currentPlatform;
	
	
	@Column
	private String lastUpdate;
	
	@Column
	private String firstInfo;
	
	@Column
	private String gsm;
	
	

	@Column
	private String nation;
	
	@Column(columnDefinition = "boolean default false")
	private boolean follow = false;
	
//	@OneToMany(mappedBy = "sosyalmedya", fetch = FetchType.EAGER)
//	private Set<Ajanda> ajanda;
	
	@OneToMany(mappedBy = "sosyalmedya")
	private Set<Ajanda> ajandas;


	public boolean isFollow() {
		return this.follow;
	}

	public Set<Ajanda> getAjandas() {
		return this.ajandas;
	}

	public void setAjandas(Set<Ajanda> ajandas) {
		this.ajandas = ajandas;
	}

	public void setFollow(boolean follow) {
		this.follow = follow;
	}

	public Set<Account> getAccount() {
		return this.account;
	}

	public void setAccount(Set<Account> account) {
		this.account = account;
	}

	@Column
	private String email;
	
	@Column
	private String name;
	
	
	@ManyToMany()
	@JoinTable(name = "sosyalmedya_account", joinColumns = {
			@JoinColumn(name = "smid", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "accountid", referencedColumnName = "id") })
	private Set<Account> account;
	
	
	 
	 
	 
	
	@ManyToOne
	@JoinColumn(name = "chase")
	private Chase chase;

	 

	public Chase getChase() {
		return this.chase;
	}

	public void setChase(Chase chase) {
		this.chase = chase;
	}

	@OneToMany(mappedBy = "sosyalmedya")
	private Set<Reply> reply;
	
	 
	public Date getCreateDate() {
		return this.createDate;
	}

	public Set<Reply> getReply() {
		return this.reply;
	}

	public void setReply(Set<Reply> reply) {
		this.reply = reply;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getLastDate() {
		return this.lastDate;
	}

	public void setLastDate(Date lastDate) {
		this.lastDate = lastDate;
	}

	public int getLanguage() {
		return this.language;
	}

	public void setLanguage(int language) {
		this.language = language;
	}

	public int getType() {
		return this.type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getPriority() {
		return this.priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	 

	public String getFirstPlatform() {
		return this.firstPlatform;
	}

	public void setFirstPlatform(String firstPlatform) {
		this.firstPlatform = firstPlatform;
	}

	public String getCurrentPlatform() {
		return this.currentPlatform;
	}

	public void setCurrentPlatform(String currentPlatform) {
		this.currentPlatform = currentPlatform;
	}

	public String getLastUpdate() {
		return this.lastUpdate;
	}

	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public String getFirstInfo() {
		return this.firstInfo;
	}

	public void setFirstInfo(String firstInfo) {
		this.firstInfo = firstInfo;
	}

	public String getGsm() {
		return this.gsm;
	}

	public void setGsm(String gsm) {
		this.gsm = gsm;
	}

	public String getNation() {
		return this.nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDsc() {
		return this.dsc;
	}

	public void setDsc(String dsc) {
		this.dsc = dsc;
	}

	public SosyalMedya(Long id) {
		this.id = id;
	}

	@Column
	private String dsc;

	public SosyalMedya() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
