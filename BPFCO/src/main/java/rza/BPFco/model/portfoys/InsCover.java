
package rza.BPFco.model.portfoys;

import java.util.Currency;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import rza.BPFco.model.Contract;

@SuppressWarnings("serial")
@Entity
@Table(name = "inscover")
public class InsCover implements java.io.Serializable {

 

	@Column
	private String amount;
	 
	@Column
	private int currency;
	
	 
	@Id
	@GeneratedValue
	private Long id;

	@Column
	private Date issueDate;

	
	@ManyToOne
	@JoinColumn(name = "cover")
	private Cover cover;
 
	public Cover getCover() {
		return this.cover;
	}

	public void setCover(Cover cover) {
		this.cover = cover;
	}

	public InsCover() {
		super();
	}
	
	public InsCover(Long id) {
		 this.id = id;
	}
	
	 
	
	 
 
 
	
	
	
	 
	 

	public int getCurrency() {
		return this.currency;
	}

	 
	
	 
 

	public Long getId() {
		return this.id;
	}
	
	 
	 

	
 
	public Date getIssueDate() {
		return this.issueDate;
	}

	 
 

	public String getAmount() {
		return this.amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public void setCurrency(int currency) {
		this.currency = currency;
	}

 
	 

	public void setId(Long id) {
		this.id = id;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	 
	 

 
	 
	 

	 

	 
}
