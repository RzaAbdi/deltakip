
package rza.BPFco.model.portfoys;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import rza.BPFco.model.Portfoy;
import rza.BPFco.model.Tag;

@SuppressWarnings("serial")
@Entity
@Table(name = "section")
public class Section implements java.io.Serializable {

 

	 

	@Id
	@GeneratedValue
	private Long id;
	
	@Column
	private Date create_date;
	
	@Column
	private Date edit_date;
	
	
	@OneToMany(mappedBy = "section", fetch = FetchType.EAGER)
	private Set<Document> document;
	 

	public Set<Document> getDocument() {
		return this.document;
	}


	public void setDocument(Set<Document> document) {
		this.document = document;
	}


	public Date getCreate_date() {
		return this.create_date;
	}


	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}


	public Date getEdit_date() {
		return this.edit_date;
	}


	public void setEdit_date(Date edit_date) {
		this.edit_date = edit_date;
	}


	 


	public String getTitle() {
		return this.title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public Section(Long id) {
		this.id = id;
	}
	
	@Column
	private String title;
	

	public Section() {
		super();
	}

	 
	public Long getId() {
		return id;
	}
 
 
	
	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "kat")
	private Kat kat;

	@ManyToOne
	@JoinColumn(name = "portfoy")
	private Portfoy portfoy;

	public Portfoy getPortfoy() {
		return portfoy;
	}


	public void setPortfoy(Portfoy portfoy) {
		this.portfoy = portfoy;
	}


	public Kat getKat() {
		return kat;
	}


	public void setKat(Kat kat) {
		this.kat = kat;
	}
	
 

}
