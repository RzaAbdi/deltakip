
package rza.BPFco.model.portfoys;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import rza.BPFco.model.Account;
import rza.BPFco.model.Portfoy;

@SuppressWarnings("serial")
@Entity
@Table(name = "cover")
public class Cover implements java.io.Serializable {

 

	@Id
	@GeneratedValue
	private Long id;
	 
	public String getGuarantor() {
		return this.guarantor;
	}

	public void setGuarantor(String guarantor) {
		this.guarantor = guarantor;
	}

	public String getPlace() {
		return this.place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getCourt() {
		return this.court;
	}

	public void setCourt(String court) {
		this.court = court;
	}

	public int getSenettype() {
		return this.senettype;
	}

	public void setSenettype(int senettype) {
		this.senettype = senettype;
	}

	public Date getSenetDate() {
		return this.senetDate;
	}

	public void setSenetDate(Date senetDate) {
		this.senetDate = senetDate;
	}

	@Column
	private Date contractDate;
	
	@Column
	private String price;
	
	@Column
	private String guarantor;

	@Column
	private String place;
	
	@Column
	private String harc;

	public String getHarc() {
		return this.harc;
	}

	public void setHarc(String harc) {
		this.harc = harc;
	}

	@Column
	private String court;
	
	@Column
	private String dsc;
	
	@Column
	private int fee;
	
	 
 

	public int getFee() {
		return this.fee;
	}

	public void setFee(int fee) {
		this.fee = fee;
	}

	public String getDsc() {
		return this.dsc;
	}

	public void setDsc(String dsc) {
		this.dsc = dsc;
	}

	@Column
	private int senettype;

	@Column
	private Date senetDate;
	
	@Column
	private Date cashDate;
	
	public Date getCashDate() {
		return this.cashDate;
	}

	public void setCashDate(Date cashDate) {
		this.cashDate = cashDate;
	}

	public int getCurrency() {
		return this.currency;
	}

	public void setCurrency(int currency) {
		this.currency = currency;
	}

	@Column
	private int currency;
	
	public String getCash() {
		return this.cash;
	}

	public void setCash(String cash) {
		this.cash = cash;
	}

	@Column
	private String cash;
	
	@Column
	private String reminding;
	
	@Column(columnDefinition="boolean default false")
	private boolean reminde=false;
	
	@Column
	@Lob
	private String des;
	
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "cover_customer", joinColumns = {
			@JoinColumn(name = "coverid", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "customerid", referencedColumnName = "id") })
	private Set<Customer> customer;
	
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "cover_account", joinColumns = {
			@JoinColumn(name = "coverid", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "accontid", referencedColumnName = "id") })
	private Set<Account> account;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "cover_portfoy", joinColumns = {
			@JoinColumn(name = "portfoyid", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "accontid", referencedColumnName = "id") })
	private Set<Portfoy> portfoy;

	@OneToMany(mappedBy = "cover", fetch = FetchType.EAGER)
	private Set<InsCover> insCover;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getContractDate() {
		return this.contractDate;
	}

	public void setContractDate(Date contractDate) {
		this.contractDate = contractDate;
	}

	 

	public String getPrice() {
		return this.price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getReminding() {
		return this.reminding;
	}

	public void setReminding(String reminding) {
		this.reminding = reminding;
	}

	public boolean isReminde() {
		return this.reminde;
	}

	public void setReminde(boolean reminde) {
		this.reminde = reminde;
	}

	public String getDes() {
		return this.des;
	}

	public void setDes(String des) {
		this.des = des;
	}

	public Set<Customer> getCustomer() {
		return this.customer;
	}

	public void setCustomer(Set<Customer> customer) {
		this.customer = customer;
	}

	public Set<Account> getAccount() {
		return this.account;
	}

	public void setAccount(Set<Account> account) {
		this.account = account;
	}

	public Set<Portfoy> getPortfoy() {
		return this.portfoy;
	}

	public void setPortfoy(Set<Portfoy> portfoy) {
		this.portfoy = portfoy;
	}

	public Set<InsCover> getInsCover() {
		return this.insCover;
	}

	public void setInsCover(Set<InsCover> insCover) {
		this.insCover = insCover;
	}
	 
}
