
package rza.BPFco.model.portfoys;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "sectiontype")
public class SectionType implements java.io.Serializable {

 

	@Id
	@GeneratedValue
	private Long id;
	
	 
	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public SectionType(Long id) {
		this.id = id;
	}
	
	@Column
	private String title;
	

	public SectionType() {
		super();
	}

	 
	public Long getId() {
		return id;
	}
 
 
	
	public void setId(Long id) {
		this.id = id;
	}

	 
 

}
