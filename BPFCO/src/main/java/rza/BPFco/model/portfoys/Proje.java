
package rza.BPFco.model.portfoys;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import rza.BPFco.model.Contract;

@SuppressWarnings("serial")
@Entity
@Table(name = "proje")
public class Proje implements java.io.Serializable {

 

	public String getAda() {
		return this.ada;
	}


	public void setAda(String ada) {
		this.ada = ada;
	}

	@Id
	@GeneratedValue
	private Long id;
	
	@Column
	private Date create_date;
	
	@Column
	private Date edit_date;
	
	 
	public Date getCreate_date() {
		return this.create_date;
	}


	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}


	public Date getEdit_date() {
		return this.edit_date;
	}


	public void setEdit_date(Date edit_date) {
		this.edit_date = edit_date;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public Proje(Long id) {
		this.id = id;
	}
	
	@Column
	private String title;
	
	@Column
	private String address;
	
	public String getAddress() {
		return this.address;
	}


	public void setAddress(String address) {
		this.address = address;
	}

	@Column
	private String ada;
	

	public Proje() {
		super();
	}

	 
	public Long getId() {
		return id;
	}
 
 
	
	public void setId(Long id) {
		this.id = id;
	}

	@OneToMany(mappedBy="proje",fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	private List<Blok> bloks;
	
//	@OneToMany(mappedBy="proje",fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
//	private List<Contract> contracts;


//	public List<Contract> getContracts() {
//		return this.contracts;
//	}
//
//
//	public void setContracts(List<Contract> contracts) {
//		this.contracts = contracts;
//	}


	public List<Blok> getBloks() {
		return bloks;
	}


	public void setBloks(List<Blok> bloks) {
		this.bloks = bloks;
	}
	 
 

}
