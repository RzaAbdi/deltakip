
package rza.BPFco.model.portfoys;

import java.util.Collection;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import rza.BPFco.model.Account;
import rza.BPFco.model.Contract;
import rza.BPFco.model.Portfoy;

@SuppressWarnings("serial")
@Entity
@Table(name = "customer")
public class Customer implements java.io.Serializable {

 

	@Id
	@GeneratedValue
	private Long id;
	
	@Column
	private Date create_date;
	
	public Date getContarctDate() {
		return this.contarctDate;
	}


	public void setContarctDate(Date contarctDate) {
		this.contarctDate = contarctDate;
	}

	@Column
	private Date contarctDate;
	
	@Column
	private String portfoy;
	
	 
	 


	public String getPortfoy() {
		return this.portfoy;
	}


	public void setPortfoy(String portfoy) {
		this.portfoy = portfoy;
	}

	@Column
	private Date edit_date;
	
	@Column
	private String fname;
	
	@Column
	private String mail;
	
	public String getMail() {
		return this.mail;
	}


	public void setMail(String mail) {
		this.mail = mail;
	}

	@Column
	private String taxOffice;
	
	public String getTaxOffice() {
		return this.taxOffice;
	}


	public void setTaxOffice(String taxOffice) {
		this.taxOffice = taxOffice;
	}

	@Column
	private String lname;
	
	@Column
	private String gsm1;
	
	@Column
	private String gsm2;
	
	@Column
	private String nationality;
	
	@Column(columnDefinition = "boolean default true")
	private boolean active = true;
	
	 
	
	public boolean isActive() {
		return this.active;
	}


	public void setActive(boolean active) {
		this.active = active;
	}

	@Column
	private Long tax;
	
	@Column
	private Long tc;
	
	@Column
	private String passport;
	
	@Column
	private String address;
	
	@Column
	private int source;
	
	public int getSource() {
		return this.source;
	}


	public void setSource(int source) {
		this.source = source;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "customer_account", joinColumns = {
			@JoinColumn(name = "customerid", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "accountid", referencedColumnName = "id") })
	private Set<Account> account;
	
	
	public Set<Account> getAccount() {
		return this.account;
	}


	public void setAccount(Set<Account> account) {
		this.account = account;
	}

	@ManyToMany(mappedBy = "customer" )
	private Set<Document> documents;
	
	
	public Set<Cover> getCover() {
		return this.cover;
	}


	public void setCover(Set<Cover> cover) {
		this.cover = cover;
	}

	@ManyToMany(mappedBy = "customer" )
	private Set<Cover> cover;


	public Set<Document> getDocuments() {
		return this.documents;
	}


	public void setDocuments(Set<Document> documents) {
		this.documents = documents;
	}


	@ManyToMany(mappedBy = "customer" )
	private Set<Contract> contract;
	 

	public String getFname() {
		return this.fname;
	}


	public void setFname(String fname) {
		this.fname = fname;
	}


	public String getLname() {
		return this.lname;
	}


	public void setLname(String lname) {
		this.lname = lname;
	}


	public String getGsm1() {
		return this.gsm1;
	}


	public void setGsm1(String gsm1) {
		this.gsm1 = gsm1;
	}


	public String getGsm2() {
		return this.gsm2;
	}


	public void setGsm2(String gsm2) {
		this.gsm2 = gsm2;
	}


	public String getNationality() {
		return this.nationality;
	}


	public void setNationality(String nationality) {
		this.nationality = nationality;
	}


	public Long getTax() {
		return this.tax;
	}


	public void setTax(Long tax) {
		this.tax = tax;
	}


	public Long getTc() {
		return this.tc;
	}


	public void setTc(Long tc) {
		this.tc = tc;
	}


	public String getPassport() {
		return this.passport;
	}


	public void setPassport(String passport) {
		this.passport = passport;
	}


	public String getAddress() {
		return this.address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public Set<Contract> getContract() {
		return this.contract;
	}


	public void setContract(Set<Contract> contract) {
		this.contract = contract;
	}


	public Date getCreate_date() {
		return this.create_date;
	}


	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}


	public Date getEdit_date() {
		return this.edit_date;
	}


	public void setEdit_date(Date edit_date) {
		this.edit_date = edit_date;
	}



	public Customer(Long id) {
		this.id = id;
	}
	
	
	

	public Customer() {
		super();
	}

	 
	public Long getId() {
		return id;
	}
 
 
	
	public void setId(Long id) {
		this.id = id;
	}

	 

	 
}
