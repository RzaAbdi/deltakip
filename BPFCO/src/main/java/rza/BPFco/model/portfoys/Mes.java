
package rza.BPFco.model.portfoys;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "mes")
public class Mes implements java.io.Serializable {

 

	@Id
	@GeneratedValue
	private Long id;
	
	@Column
	private Date sendDate;
	
	@Column
	private String text;
	
	
	@OneToMany(mappedBy = "mes", fetch = FetchType.EAGER)
	private Set<Reply> reply;
	
	
	 
	public Set<Reply> getReply() {
		return this.reply;
	}


	public void setReply(Set<Reply> reply) {
		this.reply = reply;
	}


	public Mes(Long id2) {
		this.id = id2;
	}


	public Mes() {
		super();
	}


	public Long getId() {
		return this.id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Date getSendDate() {
		return this.sendDate;
	}


	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}


	public String getText() {
		return this.text;
	}


	public void setText(String text) {
		this.text = text;
	}


	public int getType() {
		return this.type;
	}


	public void setType(int type) {
		this.type = type;
	}


	public Set<Installment> getInstallment() {
		return this.installment;
	}


	public void setInstallment(Set<Installment> installment) {
		this.installment = installment;
	}


	@Column
	private int type;
	
	
	@ManyToMany(mappedBy = "mes" )
	private Set<Installment> installment;

	 

}
