
package rza.BPFco.model.portfoys;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import rza.BPFco.model.Account;

@SuppressWarnings("serial")
@Entity
@Table(name = "chase")
public class Chase implements java.io.Serializable {

	@Id
	@GeneratedValue
	private Long id;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	 
	
	@Column
	private String color;

	public String getColor() {
		return this.color;
	}

	public void setColor(String color) {
		this.color = color;
	}



	@Column
	private String title;
	
	
	@Column
	private int language;
	
	
	public int getLanguage() {
		return this.language;
	}

	public void setLanguage(int language) {
		this.language = language;
	}



	@ManyToOne
	@JoinColumn(name = "account")
	private Account account;
	
	
	@OneToMany(mappedBy = "chase", fetch = FetchType.EAGER)
	private Set<SosyalMedya> sosyalmedya ;

	public Set<SosyalMedya> getSosyalmedya() {
		return this.sosyalmedya;
	}

	public void setSosyalmedya(Set<SosyalMedya> sosyalmedya) {
		this.sosyalmedya = sosyalmedya;
	}

	public Account getAccount() {
		return this.account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}
	
	
	
}
