
package rza.BPFco.model.portfoys;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import rza.BPFco.model.Account;
import rza.BPFco.model.Ajanda;

@SuppressWarnings("serial")
@Entity
@Table(name = "reply")
public class Reply implements java.io.Serializable {

	@Id
	@GeneratedValue
	private Long id;

	@Column
	private Date createDate;
	
	@Column(columnDefinition = "boolean default false")
	private boolean hasDoc = false;
	
	public boolean isHasDoc() {
		return this.hasDoc;
	}

	public void setHasDoc(boolean hasDoc) {
		this.hasDoc = hasDoc;
	}



	@Column
	private String note;

	public Set<Account> getAccount() {
		return this.account;
	}

	public void setAccount(Set<Account> account) {
		this.account = account;
	}


	public SosyalMedya getSosyalmedya() {
		return this.sosyalmedya;
	}

	public void setSosyalmedya(SosyalMedya sosyalmedya) {
		this.sosyalmedya = sosyalmedya;
	}



	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "reply_account", joinColumns = {
			@JoinColumn(name = "replyid", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "accountid", referencedColumnName = "id") })
	private Set<Account> account;
	 
	
	@ManyToOne
	@JoinColumn(name = "sosyalmedya")
	private SosyalMedya sosyalmedya;
	
	@ManyToOne
	@JoinColumn(name = "ajanda")
	private Ajanda ajanda;
	
	
	public Ajanda getAjanda() {
		return this.ajanda;
	}

	public void setAjanda(Ajanda ajanda) {
		this.ajanda = ajanda;
	}



	@ManyToOne
	@JoinColumn(name = "mes")
	private Mes mes;

	public Mes getMes() {
		return this.mes;
	}

	public void setMes(Mes mes) {
		this.mes = mes;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getDsc() {
		return this.dsc;
	}

	public void setDsc(String dsc) {
		this.dsc = dsc;
	}

	public Reply(Long id) {
		this.id = id;
	}

	@Column
	private String dsc;

	public Reply() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
