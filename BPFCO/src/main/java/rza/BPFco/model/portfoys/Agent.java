
package rza.BPFco.model.portfoys;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import rza.BPFco.model.Contract;
import rza.BPFco.model.Portfoy;

@SuppressWarnings("serial")
@Entity
@Table(name = "agent")
public class Agent implements java.io.Serializable {

 

	@Id
	@GeneratedValue
	private Long id;
	
	@Column
	private Date create_date;
	
	@Column
	private Date edit_date;
	
	@Column
	private String name;
	
	@Column
	private String company;
	
	
	@Column
	private String gsm1;
	
	@Column
	private String gsm2;
	
	@Column
	private String nationality;
	
	@Column
	private Long tax;
	
	@Column
	private Long tc;
	
	@Column
	private String passport;
	
	@Column
	private String address;
	
	@Column
	private int source;
	
	public int getSource() {
		return this.source;
	}


	 

	public void setSource(int source) {
		this.source = source;
	}

	 

	@ManyToMany(mappedBy = "agent" )
	private Set<Contract> contract;
	 
	@ManyToMany(mappedBy = "agent" )
	private Set<Document> documents;
	

	 


	public Set<Document> getDocuments() {
		return this.documents;
	}




	public void setDocuments(Set<Document> documents) {
		this.documents = documents;
	}




	public Set<Contract> getContract() {
		return this.contract;
	}


	public void setContract(Set<Contract> contract) {
		this.contract = contract;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getCompany() {
		return this.company;
	}


	public void setCompany(String company) {
		this.company = company;
	}


	public String getGsm1() {
		return this.gsm1;
	}


	public void setGsm1(String gsm1) {
		this.gsm1 = gsm1;
	}


	public String getGsm2() {
		return this.gsm2;
	}


	public void setGsm2(String gsm2) {
		this.gsm2 = gsm2;
	}


	public String getNationality() {
		return this.nationality;
	}


	public void setNationality(String nationality) {
		this.nationality = nationality;
	}


	public Long getTax() {
		return this.tax;
	}


	public void setTax(Long tax) {
		this.tax = tax;
	}


	public Long getTc() {
		return this.tc;
	}


	public void setTc(Long tc) {
		this.tc = tc;
	}


	public String getPassport() {
		return this.passport;
	}


	public void setPassport(String passport) {
		this.passport = passport;
	}


	public String getAddress() {
		return this.address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	 


	public Date getCreate_date() {
		return this.create_date;
	}


	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}


	public Date getEdit_date() {
		return this.edit_date;
	}


	public void setEdit_date(Date edit_date) {
		this.edit_date = edit_date;
	}



	public Agent(Long id) {
		this.id = id;
	}
	
	
	

	public Agent() {
		super();
	}

	 
	public Long getId() {
		return id;
	}
 
 
	
	public void setId(Long id) {
		this.id = id;
	}

	 

	 
}
