
package rza.BPFco.model.portfoys;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@SuppressWarnings("serial")
@Entity
@Table(name = "blok")
public class Blok implements java.io.Serializable {

 

	@Id
	@GeneratedValue
	private Long id;
	
	@Column
	private Date create_date;
	
	@Column
	private Date edit_date;
	
	 
	public Date getCreate_date() {
		return this.create_date;
	}


	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}


	public Date getEdit_date() {
		return this.edit_date;
	}


	public void setEdit_date(Date edit_date) {
		this.edit_date = edit_date;
	}


	public List<Kat> getKats() {
		return this.kats;
	}


	public void setKats(List<Kat> kats) {
		this.kats = kats;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public Blok(Long id) {
		this.id = id;
	}
	
	@Column
	private String title;
	

	public Blok() {
		super();
	}

	 
	public Long getId() {
		return id;
	}
 
 
	
	public void setId(Long id) {
		this.id = id;
	}

	 
	@ManyToOne
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "proje")
	private Proje proje;
	
	@OneToMany(mappedBy="blok",fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	private List<Kat> kats;


	public Proje getProje() {
		return proje;
	}


	public void setProje(Proje proje) {
		this.proje = proje;
	}

}
