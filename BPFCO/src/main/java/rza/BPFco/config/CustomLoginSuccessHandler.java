package rza.BPFco.config;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import rza.BPFco.model.Account;

    /**
     * Adding custom action after login 
     * @author Alireza> bludream@gmail.com
     *
     *
     */
    public class CustomLoginSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {
  
    	
    	@Override
        public void onAuthenticationSuccess(final HttpServletRequest request,
                final HttpServletResponse response, final Authentication authentication)
                		throws IOException, ServletException {

        	HttpSession session = request.getSession(true);
        	rza.BPFco.service.UserService.User io = (rza.BPFco.service.UserService.User) authentication.getPrincipal();

        		
        	//String redirectUrl = (String) session.getAttribute("url_prior_login");
//        	if (redirectUrl != null) {
//        		// clean this attribute from session
//        		session.removeAttribute("url_prior_login");
//        		// then we redirect
//        		getRedirectStrategy().sendRedirect(request, response, redirectUrl);
//        	} else {
//        		super.onAuthenticationSuccess(request, response, authentication);
//        	}
        		super.onAuthenticationSuccess(request, response, authentication); //better to redirect ourselves

        	//to save required user info in session after login
        	try {
        		Account account = io.getAccount();
        		session.setAttribute("userName",account.getEmail());
        		session.setAttribute("active", account.isActive());
        		if(account.isActive()){
        			session.setAttribute("userId", account.getId());
        			session.setAttribute("userAdmin", account.isAdmin());
        			session.setAttribute("fname", account.getEmail().split("@")[0]);	
        			
        		} 
        		session.setAttribute("fname", account.getEmail().split("@")[0]);	
        		
        	} catch (Exception e) {
        		logger.error("Error in getting User()", e);
        	} 
        }

    }
