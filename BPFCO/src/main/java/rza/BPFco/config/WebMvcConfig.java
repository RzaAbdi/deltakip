package rza.BPFco.config;

import java.util.List;

import nz.net.ultraq.thymeleaf.LayoutDialect;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.mobile.device.DeviceHandlerMethodArgumentResolver;
import org.springframework.mobile.device.DeviceResolverHandlerInterceptor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.thymeleaf.extras.springsecurity3.dialect.SpringSecurityDialect;
import org.thymeleaf.extras.tiles2.dialect.TilesDialect;
import org.thymeleaf.extras.tiles2.spring4.web.view.ThymeleafTilesView;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;
import org.thymeleaf.templateresolver.TemplateResolver;
import org.thymeleaf.templateresolver.UrlTemplateResolver;

@Configuration
@EnableWebMvc
//@ComponentScan(basePackageClasses = Application.class, includeFilters = @Filter(Controller.class), useDefaultFilters = false)
@ComponentScan(basePackages = { "rza.BPFco.controller" })
public class WebMvcConfig extends WebMvcConfigurerAdapter {
	
//	WebMvcConfigurerAdapter
	
	  @Controller
	    static class FaviconController {
	        @RequestMapping("favicon.ico")
	        String favicon() {
	            return "forward:/resources/images/favicon.ico";
	        }
	    }
	
//	@Bean
//	public ViewResolver viewResolver() {
//		//logger.log(Level.DEBUG, "setting up view resolver");
//		//logger.log(Level.DEBUG, "");
//
//		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
//		viewResolver.setPrefix("/WEB-INF/html/");
//		viewResolver.setSuffix(".html");
//		 
//		return viewResolver;
//	}
	
	 
	   
//	      @Bean public ViewResolver viewResolver() {
//	          ServletContextTemplateResolver templateResolver = new ServletContextTemplateResolver();
//	          templateResolver.setTemplateMode("HTML5");
//	          templateResolver.setPrefix("/WEB-INF/views/html/");
//	          templateResolver.setSuffix(".html");
//	          SpringTemplateEngine engine = new SpringTemplateEngine();
//	          engine.setTemplateResolver(templateResolver);
//	   
//	          ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
//	          viewResolver.setTemplateEngine(engine);
//	          return viewResolver;
//	      }
	   

//	  @Override
//	  public RequestMappingHandlerMapping requestMappingHandlerMapping() {
//			RequestMappingHandlerMapping requestMappingHandlerMapping = super
//					.requestMappingHandlerMapping();
//			requestMappingHandlerMapping.setUseSuffixPatternMatch(false);
//			requestMappingHandlerMapping.setUseTrailingSlashMatch(false);
//			return requestMappingHandlerMapping;
//		}
 
	  
	  
	  @Bean
	  public DeviceResolverHandlerInterceptor deviceResolverHandlerInterceptor() {
	      return new DeviceResolverHandlerInterceptor();
	  }

	  @Override
	  public void addInterceptors(InterceptorRegistry registry) {
	      registry.addInterceptor(deviceResolverHandlerInterceptor());
	  }
	  
	  
	  @Bean
	  public DeviceHandlerMethodArgumentResolver deviceHandlerMethodArgumentResolver() {
	      return new DeviceHandlerMethodArgumentResolver();
	  }

	  @Override
	  public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
	      argumentResolvers.add(deviceHandlerMethodArgumentResolver());
	  }
	  
	
	@Override
	public void configureDefaultServletHandling(
			DefaultServletHandlerConfigurer configurer) {
		//logger.debug("configureDefaultServletHandling");
		// if the spring dispatcher is mapped to / then forward non handled
		// requests
		// (e.g. static resource) to the container's "default servlet"
		configurer.enable();
	}
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		
		registry.addResourceHandler("/views/").addResourceLocations(
				"/views/**");
	}
	
	
	////
	
	
	@Bean
	public TemplateResolver templateResolver() {
		TemplateResolver templateResolver = new ServletContextTemplateResolver();
		templateResolver.setPrefix("/WEB-INF/views/");
		templateResolver.setSuffix(".html");
		templateResolver.setTemplateMode("HTML5");
		templateResolver.setCacheable(false);//changed to test performance
		templateResolver.setCharacterEncoding("UTF-8");
		return templateResolver;
	}
	
	
	
	
	@Bean
	public SpringTemplateEngine templateEngine() {
		SpringTemplateEngine templateEngine = new SpringTemplateEngine();
		templateEngine.addTemplateResolver(templateResolver());
		templateEngine.addTemplateResolver(urlTemplateResolver());
		templateEngine.addDialect(new SpringSecurityDialect());
		templateEngine.addDialect(new TilesDialect());
		templateEngine.addDialect(new LayoutDialect());
		return templateEngine;
	}

	@Bean
	public ViewResolver thymeleafViewResolver() {
		ThymeleafViewResolver vr = new ThymeleafViewResolver();
		vr.setTemplateEngine(templateEngine());
		vr.setCharacterEncoding("UTF-8");
		vr.setOrder(Ordered.HIGHEST_PRECEDENCE);
		// all message/* views will not be handled by this resolver as they are
		// Tiles views
		//vr.setExcludedViewNames(new String[]{"*"});
		return vr;
	}
	
	@Bean
	public ViewResolver tilesViewResolver() {
		ThymeleafViewResolver vr = new ThymeleafViewResolver();
		vr.setTemplateEngine(templateEngine());
		vr.setViewClass(ThymeleafTilesView.class);
		vr.setCharacterEncoding("UTF-8");
		vr.setOrder(Ordered.LOWEST_PRECEDENCE);
		return vr;
	}
	
	@Bean
	public UrlTemplateResolver urlTemplateResolver() {
		return new UrlTemplateResolver();
	}
}