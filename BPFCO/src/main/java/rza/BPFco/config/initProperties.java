package rza.BPFco.config;

import java.util.Properties;

import org.springframework.core.io.ClassPathResource;

public class initProperties {
	public static String BASEURL = "baseurl";
	public static String THEME = "default";
	public static String IMAGE_ARCHIVE = "c://meshop-Image-Archive//";
//	public static String IMGFOLDERNAME = "/Users/rza/deltakip/";
//	public static String FILEFOLDERNAME = "/Users/rza/deltakip/files/";
//	public static String LOGFOLDERNAME = "/Users/rza/deltakip/logs/";
	public static String IMGFOLDERNAME = "c://deltakip/";
	public static String FILEFOLDERNAME = "c://deltakip/files/";
	public static String LOGFOLDERNAME = "c:/logs/";
	public static String CMSTEMPLATENAME = "template";
	public static String EMAILTEMPLATENAME = "template";
	public static String DEFUALTREKLAM = "defaultreklam";
	public static boolean active = true;
	public static String DEFUALTREKLAMLINK = "defaultreklamlink";
	public static String EMAILTEMPLATEFOLDERNAME = "emailtemplates";
	
	
//	TR PROPERTIES
	public static String TR_CATEGORIES = "Ürünler";
	public static String TR_ABOUT = "İletişim";
	
//	EN PROPERTIES
	public static String EN_CATEGORIES = "Products";
	public static String EN_ABOUT = "contact Us";
	
//	RU PROPERTIES
	public static String RU_CATEGORIES = "продукты";
	public static String RU_ABOUT = "контакт";
	
	
//	AR PROPERTIES
	public static String AR_CATEGORIES = "Products";
	public static String AR_ABOUT = "اتصل";

	
	static {

		
		// For each property you need.
		try {
			Properties prop = new Properties();
			prop.load(new ClassPathResource("init.properties").getInputStream());
			BASEURL = prop.getProperty("baseurl");
			THEME = prop.getProperty("theme");
			IMAGE_ARCHIVE = prop.getProperty("imageArchive");
			IMGFOLDERNAME = prop.getProperty("imgfoldername");
			FILEFOLDERNAME = prop.getProperty("filefoldername");
			LOGFOLDERNAME = prop.getProperty("logfoldername");
			EMAILTEMPLATEFOLDERNAME = prop.getProperty("emailtemplatefoldername");
			CMSTEMPLATENAME = prop.getProperty("cmstemplatename");
			EMAILTEMPLATENAME = prop.getProperty("emailtemplatename");
			DEFUALTREKLAM = prop.getProperty("defaultreklam");
			DEFUALTREKLAMLINK = prop.getProperty("defaultreklamlink");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
