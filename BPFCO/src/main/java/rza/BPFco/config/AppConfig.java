package rza.BPFco.config;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Controller;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import rza.BPFco.Applications;

//	
//@Configuration
//public class AppConfig
//{
//    //<context:property-placeholder location="classpath:application.properties"></context:property-placeholder>
//    @Bean
//    public PropertyPlaceholderConfigurer getPropertyPlaceholderConfigurer()
//    {
//        PropertyPlaceholderConfigurer ppc = new PropertyPlaceholderConfigurer();
//        ppc.setLocation(new ClassPathResource("application.properties"));
//        ppc.setIgnoreUnresolvablePlaceholders(true);
//        return ppc;
//    }
//}
 
@Configuration
@EnableScheduling
@EnableAsync
@ComponentScan(basePackageClasses = Applications.class, excludeFilters = @Filter({
		Controller.class, Configuration.class}))
class ApplicationConfig {

	@Bean
	public static PropertyPlaceholderConfigurer propertyPlaceholderConfigurer() {
		PropertyPlaceholderConfigurer ppc = new PropertyPlaceholderConfigurer();
		ppc.setLocation(new ClassPathResource("/persistence.properties"));
		return ppc;
	}

	/**
	 * Using this config for file upload
	 * @return
	 * @author Alireza
	 */
	@Bean
	public static CommonsMultipartResolver multipartResolver() {
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		multipartResolver.setMaxUploadSize(10000000); //10 mb
		return multipartResolver;
	}
	
	
}
	
 