package rza.BPFco.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;
import rza.BPFco.config.initProperties;
import rza.BPFco.form.SignupForm;
import rza.BPFco.model.Account;
import rza.BPFco.repository.Account.AccountRepository;
import rza.BPFco.service.UserService;
import rza.BPFco.service.UserService.User;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.mobile.device.Device;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;




@Controller
public class SignupController {
	
	static final Logger logger = LogManager.getLogger();

	
	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private UserService userService;
	
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@RequestMapping(value = "/signup")
	public String userManagementPage(Device device, Model model) {
		
		
		
		return "html/signup";
		
		
		
	}
	
	
	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	@ResponseBody
	public String signup(@Valid @ModelAttribute SignupForm signupForm,Errors errors, HttpServletResponse response, HttpSession session) {
		if (errors.hasErrors()) {
			response.setStatus(400);
			return errors.getFieldError().getDefaultMessage();
		}
		Account account = new Account();
		try {
			 
		 String encoded = passwordEncoder.encode(signupForm.getPassword());
//			account.setPassword(passwordEncoder.encode(signupForm.getPassword()));
			account = accountRepository.save(signupForm.createAccount(encoded));
			
			//Send User WellCome mail
			//TODO Rza please add the token 
			try {
//				emailService.SendUserActivationMail(account.getEmail(), "mytoken");
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
			userService.signin(account);
			logger.fatal(account.getName() + " REGISTER ON DELTAKIP" );
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(400);
			return "E-mail is already used";
		}
		
//		pointSystemService.checkForPointChange(account);
		
		
		return null;
	}
	
	@RequestMapping(value = "/changepass", method = RequestMethod.POST)
	@ResponseBody
	public String changepass(@Valid @ModelAttribute SignupForm signupForm,Errors errors, HttpServletResponse response, Authentication authentication) {
		if (errors.hasErrors()) {
			response.setStatus(400);
			return errors.getFieldError().getDefaultMessage();
		}
		
		
		try {
			 
			User user = (User) authentication.getPrincipal();
			if(user!= null ) {
				
				Account account = user.getAccount();
				if(passwordEncoder.matches(signupForm.getPassword(),account.getPassword())){
					account.setPassword(passwordEncoder.encode(signupForm.getNewpass()));
					accountRepository.saveAndFlush(account);
					logger.fatal(account.getName() + " CHANGE PASSWORD" );
					 	
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(400);
			return "E-mail is already used";
		}
		
//		pointSystemService.checkForPointChange(account);
		
		
		return null;
	}
	
	 

 
	 
	
}
