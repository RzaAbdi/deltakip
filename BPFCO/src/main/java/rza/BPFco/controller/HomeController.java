package rza.BPFco.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rza.BPFco.service.ContractService;
import rza.BPFco.service.UserService.User;

@Controller
public class HomeController {

	@Autowired
	ContractService contractService;

	static final Logger logger = LogManager.getLogger();

	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public String userManagementPage( Authentication authentication, HttpServletRequest request) {

		try {
			if (authentication != null) {

				User user = (User) authentication.getPrincipal();
				if (user != null && user.isAdmin() ) {
					return "redirect:/adminpanel/";
				}
			}


		} catch (Exception e) {
			logger.error("load index page issue " + " with error : " + e.getMessage());
			e.printStackTrace();
		}

		return "html/index";

	}

}
