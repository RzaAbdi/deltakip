package rza.BPFco.controller.admin;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import rza.BPFco.form.AdminBlokForm;
import rza.BPFco.form.AdminFloorForm;
import rza.BPFco.form.AdminPortfoyForm;
import rza.BPFco.form.AdminProjectForm;
import rza.BPFco.form.AdminSectionForm;
import rza.BPFco.form.filter.BlokFilterForm;
import rza.BPFco.form.filter.FloorFilterForm;
import rza.BPFco.form.filter.PortfoyFilterForm;
import rza.BPFco.form.filter.SectionFilterForm;
import rza.BPFco.model.Portfoy;
import rza.BPFco.model.Tag;
import rza.BPFco.model.portfoys.Blok;
import rza.BPFco.model.portfoys.Kat;
import rza.BPFco.model.portfoys.Proje;
import rza.BPFco.model.portfoys.Section;
import rza.BPFco.service.PortfoyService;
import rza.BPFco.service.TagService;
import rza.BPFco.service.TagService.TagType;
import rza.BPFco.service.UserService.User;
import rza.BPFco.utilities.Sorting;
import rza.BPFco.utilities.Source;
import rza.BPFco.utilities.Time;

@Controller
public class AdminPortfoyController {

	@Autowired
	private PortfoyService portfoyService;

	@Autowired
	private TagService tagService;

	protected static final Logger logger = LogManager.getLogger();

	@RequestMapping(value = { "/portfoy_manage" }, method = RequestMethod.GET)
	public String index() {
		return "html/admin/portfoy_manage";
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "json/portfoys/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadAuthors(HttpServletResponse response, @ModelAttribute PortfoyFilterForm form,
			Authentication authentication) {
		try {

			User user = (User) authentication.getPrincipal();
			if (user != null) {
				if (user.getAccount().isActive()) {

					Page<Portfoy> pageAuthors = portfoyService.loadAllPortfoys(form);
					ObjectMapper mapper = new ObjectMapper()
							.configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
					ArrayNode jsonRep = mapper.createArrayNode();

					for (Portfoy portfoy : pageAuthors.getContent()) {
						ObjectNode json = mapper.createObjectNode();
						json.put("id", portfoy.getId());
						json.put("name", portfoy.getTitle());

						jsonRep.add(json);
					}

					ObjectNode page = mapper.createObjectNode();
					page.put("total", pageAuthors.getTotalPages());
					page.put("number", pageAuthors.getNumber() + 1);
					jsonRep.add(page);

					String jsonreport = mapper.writeValueAsString(jsonRep);
					return jsonreport;

				}
			}

		} catch (Exception e) {
			logger.error("Error load portfoys in adminpanel: " + " with error" + e.getMessage());
			e.printStackTrace();
		}

		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/portfoy/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadAuthorById(@RequestParam(value = "id") long id) {
		try {

//			User user = (User) authentication.getPrincipal();
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			// ArrayNode jsonRep = mapper.createArrayNode();
			ObjectNode json = mapper.createObjectNode();

			Portfoy portfoy = portfoyService.loadPortfoyById(id);

			json.put("id", portfoy.getId());
			json.put("title", portfoy.getTitle());
			json.put("reserved", portfoy.isReserved());

			ArrayNode subs = mapper.createArrayNode();

			for (Section section : portfoy.getSections()) {
				ObjectNode auth = mapper.createObjectNode();

				auth.put("id", section.getId());
				auth.put("title", "NO:" + section.getTitle() + "  KAT: " + section.getKat().getTitle() + "   "
						+ section.getKat().getBlok().getTitle());

				subs.add(auth);
			}

			json.put("sections", subs);

			ArrayNode tags = mapper.createArrayNode();

			for (Tag tagg : portfoy.getTag()) {
				ObjectNode tag = mapper.createObjectNode();

				tag.put("title", tagg.getTitle());
				tag.put("id", tagg.getId());
				tags.add(tag);
			}

			json.put("tag", tags);

			String jsonreport = mapper.writeValueAsString(json);
			return jsonreport;

//			 

		} catch (Exception e) {
			logger.error("ERROR load TAG failed from loading this id: " + id + " with error" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/portfoy/save", method = RequestMethod.POST)
	@ResponseBody
	public String saveTag(HttpServletResponse response, @Valid @ModelAttribute AdminPortfoyForm form,
			Authentication authentication) {

		try {

			User user = (User) authentication.getPrincipal();

			if (user.isAdmin()) {

				if (form.getId() != null) {

					if (form.getSections() != null) {

						Portfoy portfoy = portfoyService.loadPortfoyById(form.getId());
						Set<Section> Lastsections = portfoy.getSections();
						if (Lastsections.size() > 0) {
							for (Section section : Lastsections) {
								section.setPortfoy(null);
								portfoyService.saveSection(section);
							}
						}
						portfoy = form.manipulatPortfoy(portfoy);
//					List<Section> sections = portfoyService.loadSectionsByPortfoyId(portfoy.getId());

						Set<Section> SecList = new HashSet<Section>();
						ArrayList<String> ar = new ArrayList<String>();
						String listString = "";
						String kat = "", blok = "", proje = "";
						Section ss;
						for (Long sectionId : form.getSections()) {
							ss = portfoyService.loadSectionById(sectionId);
							ar.add(ss.getTitle());
							kat = ss.getKat().getTitle();
							blok = ss.getKat().getBlok().getTitle();
							proje = ss.getKat().getBlok().getProje().getTitle();
							ss.setPortfoy(portfoy);
							portfoyService.saveSection(ss);
						}
						for (int i = 0; i < ar.size(); i++) {

							listString += ar.get(i) + "\t";
							if (ar.size() > i + 1) {
								listString += "-";
							}

						}

						portfoy.setTags(new HashSet<Tag>(tagService.findTagsByIds(form.getTag())));
						portfoy.setSections(SecList);

//						portfoy.setSections(
//								new HashSet<Section>(portfoyService.findSectionByIds(form.getSections())));

						portfoy.setTitle(proje + " - " + blok + " - KAT: " + kat + " - BAĞIMSIZ BÖLÜM: " + listString);

						logger.fatal(user.getAccount().getName() + " MODIFY PORTFOY BY NAME: " + portfoy.getTitle());
						portfoyService.save(portfoy);
					}

				} else {

//					Portfoy portfoy = portfoyService.findByPortfoyTitle(form.getTitle());

					Portfoy portf = form.createPortfoys();
					portf.setTags(new HashSet<Tag>(tagService.findTagsByIds(form.getTag())));
					portfoyService.save(portf);
					portf = portfoyService.loadPortfoyById(portf.getId());
//						portf.setSections(new HashSet<Section>(portfoyService.findSectionByIds(form.getSections())));
					ArrayList<String> ar = new ArrayList<String>();
					for (Long sectionId : form.getSections()) {

						Portfoy report = portfoyService.findByPortfoyBySectionid(sectionId);

						if (report == null) {

							Section ss = portfoyService.loadSectionById(sectionId);
							ss.setPortfoy(portf);
							portfoyService.saveSection(ss);
						} else {
							response.setStatus(HttpStatus.BAD_REQUEST.value());
							return "1";
						}

					}

					List<Section> sections = portfoyService.loadSectionsByPortfoyId(portf.getId());

					for (Section section : sections) {
						for (Section sec : sections) {

							ar.add(sec.getTitle());
						}
						String listString = "";

						for (int i = 0; i < ar.size(); i++) {

							listString += ar.get(i) + "\t";
							if (ar.size() > i + 1) {
								listString += "-";
							}

						}

						String kat = section.getKat().getTitle();
						String blok = section.getKat().getBlok().getTitle();
						String proje = section.getKat().getBlok().getProje().getTitle();

						portf.setTitle(proje + " - " + blok + " - KAT: " + kat + " - BAĞIMSIZ BÖLÜM: " + listString);
						break;

					}
					logger.fatal(user.getAccount().getName() + " CREATE PORTFOY BY NAME: " + portf.getTitle());
					portfoyService.save(portf);

				}

			}

			return null;
		} catch (Exception e) {
			logger.error("CREATE PORTFOY HAS AN ERROR " + e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());

			return null;
		}

	}

	/**/////////// PROJECT

	@RequestMapping(value = { "/adminpanel/project_manage" }, method = RequestMethod.GET)
	public String project() {
		return "html/admin/project_manage";
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "adminpanel/json/projects/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadProjects(HttpServletResponse response, @ModelAttribute PortfoyFilterForm form
			) {
		try {

//			User user = (User) authentication.getPrincipal();
			Page<Proje> pageAuthors = portfoyService.loadAllProjects(form);
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			ArrayNode jsonRep = mapper.createArrayNode();

			for (Proje proje : pageAuthors.getContent()) {
				ObjectNode json = mapper.createObjectNode();
				json.put("id", proje.getId());
				json.put("name", proje.getTitle());
				json.put("ada", proje.getAda());

				if (proje.getCreate_date() != null) {

					json.put("create_date", Time.ToTurkishDateOnly(proje.getCreate_date()));
				}

				if (proje.getEdit_date() != null) {

					json.put("last_edit", Time.ToTurkishDateOnly(proje.getEdit_date()));

				}

				jsonRep.add(json);
			}

			ObjectNode page = mapper.createObjectNode();
			page.put("total", pageAuthors.getTotalPages());
			page.put("number", pageAuthors.getNumber() + 1);
			jsonRep.add(page);

			String jsonreport = mapper.writeValueAsString(jsonRep);
			return jsonreport;

		} catch (Exception e) {
			logger.error("Error load projects in adminpanel: " + " with error" + e.getMessage());
			e.printStackTrace();
		}

		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/project/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadProjectById(@RequestParam(value = "id") long id) {
		try {

			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			ObjectNode json = mapper.createObjectNode();
			Proje proje = portfoyService.loadProjectById(id);

			json.put("id", proje.getId());
			json.put("title", proje.getTitle());
			json.put("ada", proje.getAda());
			json.put("address", proje.getAddress());

			String jsonreport = mapper.writeValueAsString(json);
			return jsonreport;

		} catch (Exception e) {
			logger.error("ERROR load PROJECT FAILED FROM LOADING THIS ID: " + id + " WITH ERROR" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/project/save", method = RequestMethod.POST)
	@ResponseBody
	public String saveProject(HttpServletResponse response, @Valid @ModelAttribute AdminProjectForm form,
			Authentication authentication) {

		try {

			User user = (User) authentication.getPrincipal();

			if (user.isAdmin()) {

				if (form.getId() != null) {

					Proje proje = portfoyService.loadProjectById(form.getId());
					proje = form.manipulatProject(proje);
					portfoyService.save(proje);
					logger.fatal(user.getAccount().getName() + " MODIFY PROJECT BY NAME: " + proje.getTitle());

				} else {

					Proje proje = portfoyService.findByProjectTitle(form.getTitle());
					if (proje == null) {
						Proje project = form.createProject();
						portfoyService.save(project);
						logger.fatal(user.getAccount().getName() + " CREATE PROJECT BY NAME: " + project.getTitle());
					} else {
						response.setStatus(HttpStatus.BAD_REQUEST.value());
						return "1";
					}

				}

			}

			return null;
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());

			return null;
		}

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/project/remove", method = RequestMethod.POST)
	@ResponseBody
	public String deleteVariantvalue(HttpServletResponse response, @RequestParam(value = "ids") List<Long> ids,
			Authentication authentication) {
		try {
			User user = (User) authentication.getPrincipal();
			List<Proje> projects = new ArrayList<Proje>();
			for (Long Projectid : ids) {

				projects.add(new Proje(Projectid));
			}
			logger.fatal(user.getAccount().getName() + " REMOVE PROJECT BY IDS: "
					+ ids.toString().replace("[", "").replace("]", ""));
			portfoyService.remove(projects);

		} catch (Exception e) {
			logger.error("ERROR deleting projects with ids= " + ids + " with error: " + e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());
		}
		return null;
	}

	/**/////////// blok

	@RequestMapping(value = { "/adminpanel/blok_manage" }, method = RequestMethod.GET)
	public String blok() {
		return "html/admin/blok_manage";
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/bloks/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadBloks(HttpServletResponse response, @ModelAttribute BlokFilterForm form
			 ) {
		try {

//			User user = (User) authentication.getPrincipal();
			Page<Blok> pageAuthors = portfoyService.loadAllBloks(form);
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			ArrayNode jsonRep = mapper.createArrayNode();

			for (Blok blok : pageAuthors.getContent()) {
				ObjectNode json = mapper.createObjectNode();
				json.put("id", blok.getId());
				json.put("title", blok.getTitle());
				json.put("proje", blok.getProje().getTitle());

				if (blok.getCreate_date() != null) {

					json.put("create_date", Time.ToTurkishDateOnly(blok.getCreate_date()));
				}

				if (blok.getEdit_date() != null) {

					json.put("last_edit", Time.ToTurkishDateOnly(blok.getEdit_date()));

				}

				jsonRep.add(json);
			}

			ObjectNode page = mapper.createObjectNode();
			page.put("total", pageAuthors.getTotalPages());
			page.put("number", pageAuthors.getNumber() + 1);
			jsonRep.add(page);

			String jsonreport = mapper.writeValueAsString(jsonRep);

			return jsonreport;

		} catch (Exception e) {
			logger.error("Error load projects in adminpanel: " + " with error" + e.getMessage());
			e.printStackTrace();
		}

		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/blok/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadBlokById(@RequestParam(value = "id") long id) {
		try {

			 
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			// ArrayNode jsonRep = mapper.createArrayNode();
			ObjectNode json = mapper.createObjectNode();
			Blok blok = portfoyService.loadBlokById(id);

			json.put("id", blok.getId());
			json.put("title", blok.getTitle());

			ArrayNode subs = mapper.createArrayNode();

			ObjectNode auth = mapper.createObjectNode();
			auth.put("title", blok.getProje().getTitle());
			auth.put("id", blok.getProje().getId());

			subs.add(auth);

			json.put("projects", auth);


		} catch (Exception e) {
			logger.error("ERROR load BLOK failed from loading this id: " + id + " with error" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/blokplan/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadBlokPlanById(@RequestParam(value = "id", required = false) Long id,
			@RequestParam(value = "name", required = false) String name, Authentication authentication) {
		try {

			User user = (User) authentication.getPrincipal();
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);

			// ArrayNode jsonRep = mapper.createArrayNode();
			ObjectNode json = mapper.createObjectNode();
			Blok blok = null;

			if (id != null) {

				blok = portfoyService.loadBlokPlanById(id);
			} else if (name != null) {

				blok = portfoyService.loadBlokPlanByTitle(name);
			} else {
				return null;
			}

			json.put("id", blok.getId());
			json.put("title", blok.getTitle() + ", " + blok.getProje().getTitle());

			int saleSize = 0;
			int tapuSize = 0;

			List<Kat> floors = blok.getKats();
			Sorting sort = new Sorting();
			List<Kat> floor = sort.SortKat(floors);
			ArrayNode kats = mapper.createArrayNode();
			ArrayNode kats2 = mapper.createArrayNode();
			ArrayNode types = mapper.createArrayNode();
			ArrayNode types2 = mapper.createArrayNode();

			for (int i = 0; i < floor.size(); i++) {

				if (i < 14) {

					ObjectNode kat = mapper.createObjectNode();
					kat.put("title", floor.get(i).getTitle());
					kat.put("id", floor.get(i).getId());

					List<Section> sections = floor.get(i).getSections();
					List<Section> se = sort.SortSection(sections);

					ArrayNode bolumler = mapper.createArrayNode();
					Portfoy A = new Portfoy();
					Portfoy B = new Portfoy();
					for (Section sec : se) {

						ObjectNode section = mapper.createObjectNode();
						section.put("title", sec.getTitle());
						section.put("id", sec.getId());

						id = sec.getId();

//						/TAGS
						ArrayNode TAGS = mapper.createArrayNode();
						if (sec.getPortfoy() != null) {

							if (sec.getPortfoy().isSaled()) {
								section.put("status", "tdclass");
								saleSize++;
							}
							if(!sec.getDocument().isEmpty()) {
								section.put("mode", "text-line");
								tapuSize++;
							}else {
								section.put("mode", "");
							}
							if (sec.getPortfoy().isReserved()) {
								section.put("status", "reserveclass");
							}

							section.put("pid", sec.getPortfoy().getId());

							Set<Tag> tagss = sec.getPortfoy().getTag();
							if (tagss != null) {

								for (Tag tag : tagss) {

									ObjectNode Tag = mapper.createObjectNode();
									switch (TagType.values()[tag.getType()]) {
									case Tip:
										Tag.put("tip", tag.getTitle());
										break;

									case Durum:
//										

									default:
										break;
									}

									TAGS.add(Tag);

									section.put("tags", TAGS);
								}
							}

						}

						A = sec.getPortfoy();

						if (A != null) {

							if (A.equals(B)) {
//ll
							} else {
//							section.put("col", sec.getPortfoy().getSections().size());

								if (i == 0) {//// TODO must be automatically
									ObjectNode type = mapper.createObjectNode();

									type.put("col", sec.getPortfoy().getSections().size());
									if (sec.getPortfoy() != null) {

										section.put("pid", sec.getPortfoy().getId());

										Set<Tag> tags = sec.getPortfoy().getTag();

										for (Tag tag : tags) {

											switch (TagType.values()[tag.getType()]) {
											case Tip:
												type.put("tip", tag.getTitle());
												break;

											case Durum:
											default:
												break;
											}

										}

										types.add(type);
									}
								}

								B = A;

							}
						}

						bolumler.add(section);

					}
					kat.put("sections", bolumler);
					kats.add(kat);

				} else {
					ObjectNode kat2 = mapper.createObjectNode();
					kat2.put("title", floor.get(i).getTitle());
					kat2.put("id", floor.get(i).getId());

					List<Section> sections = floor.get(i).getSections();
					List<Section> se = sort.SortSection(sections);

					ArrayNode bolumler2 = mapper.createArrayNode();
					Portfoy A = new Portfoy();
					Portfoy B = new Portfoy();
					for (Section sec : se) {

						ObjectNode section = mapper.createObjectNode();

						section.put("title", sec.getTitle());
						section.put("id", sec.getId());
						section.put("pid", sec.getPortfoy().getId());

						if (sec.getPortfoy() != null) {

							if (sec.getPortfoy().isSaled()) {

								section.put("status", "tdclass");
								saleSize++;
							}
							if(!sec.getDocument().isEmpty()) {
								section.put("mode", "text-line");
								tapuSize++;
							}else {
								section.put("mode", "");
							}
							if (sec.getPortfoy().isReserved()) {
								section.put("status", "reserveclass");
							}

							ArrayNode TAGS = mapper.createArrayNode();
							Set<Tag> tagss = sec.getPortfoy().getTag();
							for (Tag tag : tagss) {
								ObjectNode Tag = mapper.createObjectNode();
								switch (TagType.values()[tag.getType()]) {
								case Tip:
									Tag.put("tip", tag.getTitle());
									break;

								case Durum:

								default:
									break;
								}

								TAGS.add(Tag);

							}
							section.put("tags", TAGS);

							A = sec.getPortfoy();

							if (A != null) {

								if (A.equals(B)) {
//								cc
								} else {
									if (i == 14) {

										ObjectNode type2 = mapper.createObjectNode();

										type2.put("col", sec.getPortfoy().getSections().size());
										Set<Tag> tags = sec.getPortfoy().getTag();
										for (Tag tag : tags) {

											switch (TagType.values()[tag.getType()]) {
											case Tip:
												type2.put("tip", tag.getTitle());
												break;

											case Durum:

											default:
												break;
											}

										}

										types2.add(type2);

									}

									B = A;

								}
							}

						}
						bolumler2.add(section);

					}
					kat2.put("sections", bolumler2);
					kats2.add(kat2);
				}

			}

			json.put("salesize", saleSize);
			json.put("tapusize", tapuSize);
			json.put("tips", types);
			json.put("tips2", types2);

			json.put("floors", kats);
			json.put("floors2", kats2);

			ArrayNode subs = mapper.createArrayNode();
			ObjectNode auth = mapper.createObjectNode();
			auth.put("title", blok.getProje().getTitle());
			auth.put("id", blok.getProje().getId());
			subs.add(auth);
			json.put("projects", auth);

			String jsonreport = mapper.writeValueAsString(json);
			logger.fatal(user.getAccount().getName() + " LOAD  FLOOR PLAN BY TITLE " + blok.getTitle());
			return jsonreport;

		} catch (Exception e) {
			logger.error("ERROR load FLLOR PLAN FAILED FROM LOADING THIS ID: " + id + " WITH ERROR" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/blok/save", method = RequestMethod.POST)
	@ResponseBody
	public String saveBlok(HttpServletResponse response, @Valid @ModelAttribute AdminBlokForm form,
			Authentication authentication) {

		try {

			User user = (User) authentication.getPrincipal();

			if (user.isAdmin()) {

				if (form.getId() != null) {

					Blok blok = portfoyService.loadBlokById(form.getId());
					blok = form.manipulatBlok(blok);
					blok.setProje(portfoyService.loadProjectById(form.getProjects()));

					portfoyService.save(blok);
					logger.fatal(user.getAccount().getName() + " MODIFY BLOK BY NAME " + blok.getTitle());

				} else {

					Blok blok = portfoyService.findByBlokTitle(form.getTitle());
					if (blok == null) {
						Blok BLOK = form.createBlok();
						BLOK.setProje(portfoyService.loadProjectById(form.getProjects()));
						portfoyService.save(BLOK);
						logger.fatal(user.getAccount().getName() + " CREATE BLOK BY NAME " + BLOK.getTitle());
					} else {
						response.setStatus(HttpStatus.BAD_REQUEST.value());
						return "1";
					}

				}

			}

			return null;
		} catch (Exception e) {
			logger.error("SAVE BLOK HAS AN ERROR BY ERROR: " + e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());

			return null;
		}

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/blok/remove", method = RequestMethod.POST)
	@ResponseBody
	public String deleteBlok(HttpServletResponse response, @RequestParam(value = "ids") List<Long> ids,
			Authentication authentication) {
		try {
			User user = (User) authentication.getPrincipal();
			List<Blok> projects = new ArrayList<Blok>();
			for (Long Projectid : ids) {

				projects.add(new Blok(Projectid));
				Blok b = portfoyService.loadBlokById(Projectid);
				b.setProje(null);
				portfoyService.save(b);
			}

			portfoyService.removeBlok(projects);
			logger.fatal(user.getAccount().getName() + " REMOVE BLOK WITH IDS "
					+ ids.toString().replace("[", "").replace("]", ""));
		} catch (Exception e) {
			logger.error("ERROR  REMOVE BLOK WITH IDS= " + ids + " with error: " + e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());
		}
		return null;
	}

	/**/////////// kat

	@RequestMapping(value = { "/adminpanel/floor_manage" }, method = RequestMethod.GET)
	public String floor() {
		return "html/admin/floor_manage";
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "adminpanel/json/sections/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadSections(HttpServletResponse response, @ModelAttribute SectionFilterForm form
			 ) {
		try {

			Page<Section> pageAuthors = portfoyService.loadAllSections(form);
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			ArrayNode jsonRep = mapper.createArrayNode();

			for (Section section : pageAuthors.getContent()) {
				ObjectNode json = mapper.createObjectNode();
				json.put("id", section.getId());
				json.put("title", section.getTitle());
				json.put("kat", section.getKat().getTitle());
				json.put("blok", section.getKat().getBlok().getTitle());

				if (section.getCreate_date() != null) {

					json.put("create_date", Time.ToTurkishDateOnly(section.getCreate_date()));
				}

				if (section.getEdit_date() != null) {

					json.put("last_edit", Time.ToTurkishDateOnly(section.getEdit_date()));

				}

				jsonRep.add(json);
			}

			ObjectNode page = mapper.createObjectNode();
			page.put("total", pageAuthors.getTotalPages());
			page.put("number", pageAuthors.getNumber() + 1);
			jsonRep.add(page);

			String jsonreport = mapper.writeValueAsString(jsonRep);

			return jsonreport;

		} catch (Exception e) {
			logger.error("Error load projects in adminpanel: " + " with error" + e.getMessage());
			e.printStackTrace();
		}

		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/section/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadSectionById(@RequestParam(value = "id") long id) {
		try {
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			// ArrayNode jsonRep = mapper.createArrayNode();
			ObjectNode json = mapper.createObjectNode();
			Section section = portfoyService.loadSectionById(id);

			json.put("id", section.getId());
			json.put("title", section.getTitle());

			ArrayNode subs = mapper.createArrayNode();

			ObjectNode auth = mapper.createObjectNode();
			auth.put("title", section.getKat().getTitle());
			auth.put("id", section.getKat().getId());

			subs.add(auth);

			json.put("floors", auth);

			String jsonreport = mapper.writeValueAsString(json);
			return jsonreport;

		} catch (Exception e) {
			logger.error("ERROR load FLOOR failed from loading this id: " + id + " with error" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/section/save", method = RequestMethod.POST)
	@ResponseBody
	public String saveSection(HttpServletResponse response, @Valid @ModelAttribute AdminSectionForm form,
			Authentication authentication) {

		try {

			User user = (User) authentication.getPrincipal();

			if (user.isAdmin()) {

				if (form.getId() != null) {

					Section section = portfoyService.loadSectionById(form.getId());
					section = form.manipulatSection(section);
					section.setKat(portfoyService.loadFloorById(Long.parseLong(form.getFloors())));
					portfoyService.saveSection(section);
					logger.fatal(user.getAccount().getName() + " MODIFY SECTION WITH NAME " + section.getTitle());

				} else {

					if (form.getTitle2() != null) {
						if (form.getTitle2() != "") {
							List<String> titles = Source.getAutoStringNumber(form.getTitle(), form.getTitle2());

							for (String title : titles) {
								Section section = portfoyService.findBySectionTitleByFloor(title, form.getFloors());
								if (section == null) {

									form.setTitle(title);
									Section Section = form.createSection();
									Section.setKat(portfoyService.loadFloorById(Long.parseLong(form.getFloors())));
									portfoyService.saveSection(Section);
									logger.fatal(user.getAccount().getName() + " CREATE SECTION WITH NAME "
											+ Section.getTitle());
								} else {
									response.setStatus(HttpStatus.BAD_REQUEST.value());
									return "1";
								}

							}
						}
					}

				}

			}

			return null;
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());

			return null;
		}

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/section/remove", method = RequestMethod.POST)
	@ResponseBody
	public String deleteSection(HttpServletResponse response, @RequestParam(value = "ids") List<Long> ids,
			Authentication authentication) {
		try {
			User user = (User) authentication.getPrincipal();
			List<Section> section = new ArrayList<Section>();
			for (Long Projectid : ids) {

				section.add(new Section(Projectid));
				Section b = portfoyService.loadSectionById(Projectid);
				b.setKat(null);
				portfoyService.saveSection(b);

			}

			portfoyService.removeSection(section);
			logger.fatal(user.getAccount().getName() + " REMOVE SECTIONS WITH IDS: " + ids);
		} catch (Exception e) {
			logger.error("ERROR DELETING SECTIONS WITH IDS= " + ids + " WITH ERROR: " + e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());
		}
		return null;
	}

	/**/////////// bolum

	@RequestMapping(value = { "/adminpanel/section_manage" }, method = RequestMethod.GET)
	public String section() {
		return "html/admin/section_manage";
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "adminpanel/json/floors/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadFloors(HttpServletResponse response, @ModelAttribute FloorFilterForm form) {
		try {
			Page<Kat> pageAuthors = portfoyService.loadAllFloors(form);
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			ArrayNode jsonRep = mapper.createArrayNode();

			for (Kat floor : pageAuthors.getContent()) {
				ObjectNode json = mapper.createObjectNode();
				json.put("id", floor.getId());
				json.put("title", floor.getTitle());
				json.put("blok", floor.getBlok().getTitle());

				if (floor.getCreate_date() != null) {

					json.put("create_date", Time.ToTurkishDateOnly(floor.getCreate_date()));
				}

				if (floor.getEdit_date() != null) {

					json.put("last_edit", Time.ToTurkishDateOnly(floor.getEdit_date()));

				}

				jsonRep.add(json);
			}

			ObjectNode page = mapper.createObjectNode();
			page.put("total", pageAuthors.getTotalPages());
			page.put("number", pageAuthors.getNumber() + 1);
			jsonRep.add(page);

			String jsonreport = mapper.writeValueAsString(jsonRep);
			return jsonreport;

		} catch (Exception e) {
			logger.error("Error load projects in adminpanel: " + " with error" + e.getMessage());
			e.printStackTrace();
		}

		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/floor/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadFloorById(@RequestParam(value = "id") long id) {
		try {
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			// ArrayNode jsonRep = mapper.createArrayNode();
			ObjectNode json = mapper.createObjectNode();
			Kat floor = portfoyService.loadFloorById(id);

			json.put("id", floor.getId());
			json.put("title", floor.getTitle());

			ArrayNode subs = mapper.createArrayNode();

			ObjectNode auth = mapper.createObjectNode();
			auth.put("title", floor.getBlok().getTitle());
			auth.put("id", floor.getBlok().getId());

			subs.add(auth);

			json.put("bloks", auth);

			String jsonreport = mapper.writeValueAsString(json);
			return jsonreport;

		} catch (Exception e) {
			logger.error("ERROR load FLOOR failed from loading this id: " + id + " with error" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/floor/save", method = RequestMethod.POST)
	@ResponseBody
	public String saveFloor(HttpServletResponse response, @Valid @ModelAttribute AdminFloorForm form,
			Authentication authentication) {

		try {

			User user = (User) authentication.getPrincipal();

			if (user.isAdmin()) {

				if (form.getId() != null) {

					Kat floor = portfoyService.loadFloorById(form.getId());
					floor = form.manipulatFloor(floor);
					floor.setBlok(portfoyService.loadBlokById(form.getBloks()));
					portfoyService.saveFloor(floor);
					logger.fatal(user.getAccount().getName() + " MODIFY FLOOR WITH TITLE: " + floor.getTitle());

				} else {

					Kat floor = portfoyService.findByFloorTitle(form.getTitle(), form.getBloks());
					if (floor == null) {
						Kat Floor = form.createFloor();
						Floor.setBlok(portfoyService.loadBlokById(form.getBloks()));
						portfoyService.saveFloor(Floor);
						logger.fatal(user.getAccount().getName() + " CREATE FLOOR WITH TITLE: " + Floor.getTitle());
					} else {
						response.setStatus(HttpStatus.BAD_REQUEST.value());
						return "1";
					}

				}

			}

			return null;
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());

			return null;
		}

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/floor/remove", method = RequestMethod.POST)
	@ResponseBody
	public String deleteFloor(HttpServletResponse response, @RequestParam(value = "ids") List<Long> ids,
			Authentication authentication) {
		try {
			User user = (User) authentication.getPrincipal();
			List<Kat> floors = new ArrayList<Kat>();
			for (Long Projectid : ids) {

				floors.add(new Kat(Projectid));
				Kat b = portfoyService.loadFloorById(Projectid);
				b.setBlok(null);
				portfoyService.saveFloor(b);
			}

			portfoyService.removeFloor(floors);
			logger.fatal(user.getAccount().getName() + " REMOVE FLOORS WITH IDS: "
					+ ids.toString().replace("[", "").replace("]", ""));
		} catch (Exception e) {
			logger.error("ERROR DELETING FLOORS WITH IDS= " + ids + " WITH ERROR: " + e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());
		}
		return null;
	}

}
