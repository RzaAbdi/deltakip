package rza.BPFco.controller.admin;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import rza.BPFco.config.initProperties;
import rza.BPFco.form.AdminCustomerForm;
import rza.BPFco.form.filter.CustomerFilterForm;
import rza.BPFco.model.Account;
import rza.BPFco.model.Contract;
import rza.BPFco.model.Portfoy;
import rza.BPFco.model.portfoys.Customer;
import rza.BPFco.model.portfoys.Document;
import rza.BPFco.model.portfoys.Installment;
import rza.BPFco.service.ContractService;
import rza.BPFco.service.DocumentService;
import rza.BPFco.service.PersonService;
import rza.BPFco.service.PortfoyService;
import rza.BPFco.service.UserService;
import rza.BPFco.service.UserService.User;
import rza.BPFco.utilities.Source;
import rza.BPFco.utilities.Time;

@Controller
public class AdminCustomerController {

	@Autowired
	private PersonService personService;

	@Autowired
	private UserService userService;

	@Autowired
	private ContractService contractService;
	
	@Autowired
	private DocumentService documentService;
	
	@Autowired
	private PortfoyService portfoyService;

	protected static final Logger logger = LogManager.getLogger();

	@RequestMapping(value = { "/customer_manage" }, method = RequestMethod.GET)
	public String index() {

		return "html/admin/customer_manage";
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/customers/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadCustomers(HttpServletResponse response, @ModelAttribute CustomerFilterForm form,
			Authentication authentication) {
		try {

			User user = (User) authentication.getPrincipal();

			if (user != null) {

				if (user.isAdmin()||user.isWatcher()) {
					//
					form.setAdmin(true);

				} else {
					//
					form.setAdmin(false);
					
					List <Long> contracts = contractService.findContractsByUserId(user.getUserId());
					
					form.setContracts(contracts);
					
					form.setAccount(user.getUserId());

				}

			}else {
				return null;
			}
			Page<Customer> pageAuthors;

			if (!initProperties.active) {
				pageAuthors = personService.loadAllCustomers(form);
			} else {
				 boolean validated = userService.loadAccountByEmail();
					if (!validated)
						return null;


				pageAuthors = personService.loadAllCustomers(form);
			}

			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			ArrayNode jsonRep = mapper.createArrayNode();
			int count=0;
			for (Customer customer : pageAuthors.getContent()) {
				count++;
				ObjectNode json = mapper.createObjectNode();
				json.put("cid", customer.getId());
				json.put("fname", customer.getFname());
				json.put("nationality", customer.getNationality());
				json.put("gsm1", customer.getGsm1());
				json.put("source", customer.getSource());
				json.put("mail", customer.getMail());
				json.put("address", customer.getAddress());
				json.put("active", customer.isActive());
				
				
//				PORT.addAll(customer.getPortfoy());
				
				if (customer.getPortfoy() != null && count<2 ) {
					 
					 
						
						Pattern pattern = Pattern.compile("\\d+");
						Matcher matcher = pattern.matcher(customer.getPortfoy());
						
						List<Long> list = new ArrayList<Long>();
						
						while (matcher.find()) {
							list.add(Long.parseLong(matcher.group())); // Add the value to the list
						}
						
						//
						
						Set<Portfoy> portfoyslist = portfoyService.findPortfoysByIds(list);
						ArrayNode portfoyCats = mapper.createArrayNode();
						for (Portfoy portfoy : portfoyslist) {
							
							ObjectNode Pnode = mapper.createObjectNode();
							Pnode.put("title", portfoy.getTitle());
							Pnode.put("id", portfoy.getId());
							portfoyCats.add(Pnode);
						}
						json.put("portfoys", portfoyCats);
					 

				}
				
				if(customer.getContarctDate()!=null) {
					json.put("createDate", Time.ToSplitedDate(customer.getContarctDate()));
				}


				jsonRep.add(json);
			}

			ObjectNode page = mapper.createObjectNode();
			page.put("total", pageAuthors.getTotalPages());
			page.put("number", pageAuthors.getNumber() + 1);
			jsonRep.add(page);

			String jsonreport = mapper.writeValueAsString(jsonRep);
			

			
			return jsonreport;

		} catch (Exception e) {
			logger.error("Error load customer manage in adminpanel: " + " with error" + e.getMessage());
			e.printStackTrace();
		}

		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/customer/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadCustomerById(@RequestParam(value = "id") long id,Authentication authentication) {
		try {
			
			User user = (User) authentication.getPrincipal();
			
			if(user!=null) {
				ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
						false);
				ObjectNode json = mapper.createObjectNode();
				Customer customer = personService.loadCustomerById(id);

				json.put("id", customer.getId());
				json.put("fname", customer.getFname());
				json.put("gsm1", customer.getGsm1());
				json.put("gsm2", customer.getGsm2());
				json.put("passport", customer.getPassport());
				json.put("tax", customer.getTax());
				json.put("mail", customer.getMail());
				json.put("taxOffice", customer.getTaxOffice());
				json.put("address", customer.getAddress());
				json.put("nationality", customer.getNationality());
				json.put("tc", customer.getTc());
				ArrayList<String> cur = Source.CustomerSource();

				ObjectNode jsons = mapper.createObjectNode();

				jsons.put("id", customer.getSource());
				jsons.put("title", cur.get(customer.getSource() - 1));

				json.put("source", jsons);
				String jsonreport = mapper.writeValueAsString(json);
				return jsonreport;
			}
			
			

		} catch (Exception e) {
			logger.error("ERROR load CUSTOMER failed from loading this id: " + id + " with error" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/customer/save", method = RequestMethod.POST)
	@ResponseBody
	public String saveCustomer(HttpServletResponse response, @Valid @ModelAttribute AdminCustomerForm form,
			Authentication authentication) {

		try {

			String name = null;

			User user = (User) authentication.getPrincipal();
			
			if(user.isWatcher()) {
				response.setStatus(HttpStatus.BAD_REQUEST.value());
				return "1";
			}

			HashSet<Account> acc = new HashSet<Account>();

			acc.add(user.getAccount());

			if (form.getId() != null) {

				Customer customer = personService.loadCustomerById(form.getId());
				customer = form.manipulatCustomer(customer);
				
				personService.save(customer);
				logger.fatal(user.getAccount().getName()+ " MODIFY CUSTOMER BY NAME: " +customer.getFname());

			} else {

				Customer cust = personService.findByCustomerName(form.getFname());
				if (cust == null) {
					Customer customer = form.createCustomer();
					customer.setAccount(new HashSet<Account>(acc));
					name = customer.getFname();
					customer.setActive(true);

					personService.save(customer);
					logger.fatal(user.getAccount().getName()+ " CREATE CUSTOMER BY NAME: " +customer.getFname());
				} else {
					response.setStatus(HttpStatus.BAD_REQUEST.value());
					return "1";
				}

			}

			return "name : " + name;
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());

			return null;
		}

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/customer/cancel", method = RequestMethod.POST)
	@ResponseBody
	public String deleteCustomer(HttpServletResponse response, @RequestParam(value = "ids") List<Long> ids,Authentication authentication) {
		try {

			User user = (User) authentication.getPrincipal();
			if(user != null) {
				
				for (Long id : ids) {
					
					List<Customer> customers = personService.findCustomersByIds(ids);
					
					
					List<Contract> contracts = contractService.findContractsByCustomerId(id);
					
					if (contracts != null) {
						if (contracts.size() > 0) {
							
							for (Contract contract : contracts) {
								
								contract.setActive(false);
								
								contractService.save(contract);
								
								Set<Installment> ins = contract.getInstallment();
								
								if (ins.size() > 0) {
									List<Installment> installments = new ArrayList<Installment>();
									installments.addAll(ins);
									contractService.removeInstallment(installments);
								}
								
							}
							for (Customer customer : customers) {
								customer.setActive(false);
								personService.save(customer);
								
							}
							
						}else {
							for (Customer customer : customers) {
								
								customer.setAccount(new HashSet<Account>());
								
								
								Customer cust = personService.findCustomersByIdFetchDoc(customer.getId());
								if(cust!=null) {
									Set<Document> docs = cust.getDocuments();
									for (Document document : docs) {
										
										document.setCustomer(new HashSet<Customer>());
									}
									ArrayList<Document> d = new ArrayList<Document>();
									d.addAll(docs);
									documentService.removeDocuments(d);
									
								}
								
								personService.save(customer);
								
							}
							logger.fatal(user.getAccount().getName()+ " DELETE CUSTOMERS BY IDS: " +ids.toString());
							personService.deleteCustomers(customers);
						}
						
					}
					
				}
			}

		} catch (Exception e) {
			logger.error("ERROR deleting customers with ids= " + ids + " with error: " + e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());
		}
		return null;
	}

}
