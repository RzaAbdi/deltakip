package rza.BPFco.controller.admin;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.node.ArrayNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import rza.BPFco.model.portfoys.Blok;
import rza.BPFco.model.portfoys.Installment;
import rza.BPFco.service.ChartService;
import rza.BPFco.service.ContractService;
import rza.BPFco.service.PortfoyService;
import rza.BPFco.utilities.Source;
import rza.BPFco.utilities.Time;

@Controller
public class AdminChartController {

	@Autowired
	private ChartService chartService;
	@Autowired
	private PortfoyService portfoyService;

	@Autowired
	private ContractService contractService;

	protected static final Logger logger = LogManager.getLogger();

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/chart/installmentcount", method = RequestMethod.GET)
	@ResponseBody
	public String loadSiteVisits(HttpServletResponse response,
			@RequestParam(value = "daterange", required = false) String daterange) {
		try {
			ObjectMapper mapper = new ObjectMapper().setSerializationInclusion(Inclusion.NON_NULL);
			
			if (daterange != null) {

				String[] ss = daterange.split("-");

				Date from = Time.ConvertToDatemorning(ss[0]);

				Date to = Time.ConvertToDatenew(ss[1]);

				List<Object[]> installmentIssue = chartService.findTotalMonthlyInstallmentCount(to, from);
				List<Object[]> installmentPaid = chartService.findTotalMonthlyInstallmentPaidCount(to, from);
				List<Object[]> installmentNotPaid = chartService.findTotalMonthlyInstallmentNotPaidCount(to, from,Time.today());
				List<Object[]> installmentPatrialPaid = chartService.findTotalMonthlyInstallmentPatrialPaidCount(to, from);

				ArrayNode m = mapper.createArrayNode();

				ArrayList<String> months = new ArrayList<>();
				ArrayList<String> years = new ArrayList<>();
				ArrayList<String> conts = new ArrayList<>();
				ArrayList<String> paid = new ArrayList<>();
				ArrayList<String> Notpaid = new ArrayList<>();
				ArrayList<String> partial = new ArrayList<>();

				for (Object[] objects : installmentIssue) {
					months.add(objects[0].toString());
					years.add(objects[1].toString());
					conts.add(objects[2].toString());
				}
				
				 
					
					
				int index = 0;
				int index2 = 0;
				int index3 = 0;
				int size = installmentPaid.size();
				int size2 = installmentNotPaid.size();
				int size3 = installmentPatrialPaid.size();
				
				
					 for (int i = 0; i < months.size(); i++) {
						 
						 
						 if(index<size) {
							 
							 if(installmentPaid.get(index)[0].toString().equals(months.get(i))) {
								 paid.add(installmentPaid.get(index)[1].toString());
								 index++;
							 }else {
								 paid.add("0");
							 }
						 }else {
							 paid.add("0");
							 
						 }
						 
						 if(index2<size2) {
							 
							 if(installmentNotPaid.get(index2)[0].toString().equals(months.get(i))) {
								 Notpaid.add(installmentNotPaid.get(index2)[1].toString());
								 index2++;
							 }else {
								 Notpaid.add("0");
							 }
						 }else {
							 Notpaid.add("0");
							 
						 }
						 if(index3<size3) {
							 
							 if(installmentPatrialPaid.get(index3)[0].toString().equals(months.get(i))) {
								 partial.add(installmentPatrialPaid.get(index3)[1].toString());
								 index3++;
							 }else {
								 partial.add("0");
							 }
						 }else {
							 partial.add("0");
							 
						 }
						 
						 
						 
						
					}
					
					
				 

//				for (Object[] objects : installmentPaid) {
//					paid.add(objects[1].toString());
//				}
//				for (Object[] objects : installmentNotPaid) {
//					Notpaid.add(objects[1].toString());
//				}

				m.add(months.toString());
				m.add(conts.toString());
				m.add(paid.toString());
				m.add(Notpaid.toString());
				m.add(partial.toString());
				m.add(years.toString());

				String jsonreport = mapper.writeValueAsString(m);
				return jsonreport;
			}

		} catch (Exception e) {
			logger.error("Error load installmentcount in index: " + " with error" + e.getMessage());
			e.printStackTrace();

		}

		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/chart/installmentamount", method = RequestMethod.GET)
	@ResponseBody
	public String loadAmount(HttpServletResponse response,
			@RequestParam(value = "daterange", required = false) String daterange) {
		try {
			
			
			DecimalFormatSymbols dfs = new DecimalFormatSymbols();
			dfs.setDecimalSeparator(',');
			dfs.setGroupingSeparator('.');
			DecimalFormat formatter = new DecimalFormat("###,###,###.##");
			formatter.setDecimalFormatSymbols(dfs);
			
			ObjectMapper mapper = new ObjectMapper().setSerializationInclusion(Inclusion.NON_NULL);
			if (daterange != null) {

				ArrayNode m = mapper.createArrayNode();
				ArrayList<String> tr = new ArrayList<>();
				ArrayList<String> euro = new ArrayList<>();
				ArrayList<String> dolar = new ArrayList<>();
				
				float TR = 0l,DOLAR=0l,EURO=0l;
				
				String[] ss = daterange.split("-");

				Date from = Time.ConvertToDatenew(ss[0]);

				Date to = Time.ConvertToDatenew(ss[1]);

				List<Installment> installmentIssue = contractService.loadLastInstallmentChart(from, to);

				ArrayList<String> dates = Source.getMounthBetwwenDates(ss[0], ss[1]);
				ArrayList<String> mounts = new ArrayList<String>();
				for (String date : dates) {
					
					mounts.add(date.split("-")[0]);
					

					for (Installment installment : installmentIssue) {

						String D = Source.getMounth(installment.getIssueDate());
						String Y = Source.getYear(installment.getIssueDate());
						String[] DY = date.split("-");
//						if(!DY[0].contentEquals(D)||!DY[1].contentEquals(Y)) {
//							break;
//						}
						if (DY[0].contentEquals(D)&&DY[1].contentEquals(Y)) {
							// add
							switch (installment.getCurrency()) {
							case 1:// TR
								TR+=Float.parseFloat(installment.getAmount().replaceAll("[^\\d.]", ""));

								break;
							case 2:// DOLAR
								DOLAR+=Float.parseFloat(installment.getAmount().replaceAll("[^\\d.]", ""));

								break;
							case 3:// EURO
								EURO+=Float.parseFloat(installment.getAmount().replaceAll("[^\\d.]", ""));

								break;

							}

							
						} 
						
						
					} 
					
					tr.add(String.valueOf(TR));
					euro.add(String.valueOf(EURO));
					dolar.add(String.valueOf(DOLAR));
					
					TR = 0;
					EURO = 0;
					DOLAR = 0;

				}
				m.add(mounts.toString());
				m.add(tr.toString());
				m.add(dolar.toString());
				m.add(euro.toString());


				String jsonreport = mapper.writeValueAsString(m);
				return jsonreport;
			}

		} catch (Exception e) {
			logger.error("Error load installmentamount in index: " + " with error" + e.getMessage());
			

			e.printStackTrace();

		}

		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}
	
	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/chart/salepie", method = RequestMethod.GET)
	@ResponseBody
	public String loadSale(HttpServletResponse response,
			@RequestParam(value = "id", required = false) long project) {
		try {
			ObjectMapper mapper = new ObjectMapper().setSerializationInclusion(Inclusion.NON_NULL);
			
			List<Blok> bloks = portfoyService.loadBloksByProjectId(project);
			
			if(bloks!=null) {
				
				 
				
				if(bloks.size()>0) {
					ArrayNode node = mapper.createArrayNode();
					
					ArrayList<String> blokname = new ArrayList<>();
					ArrayList<String> counts = new ArrayList<>();
					ArrayList<String> ids = new ArrayList<>();
					for (Blok blok : bloks) {
						long count = chartService.findTotalSale(blok.getId());
						
						blokname.add(blok.getTitle());
						counts.add(String.valueOf(count));
						ids.add(blok.getId().toString());
						
						 
						
					}
					node.add(blokname.toString());
					node.add(counts.toString());
					node.add(ids.toString());
					String jsonreport = mapper.writeValueAsString(node);
					return jsonreport;
				}
				
			}
			

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error load salepie in index: " + " with error" + e.getMessage());
			
		}

		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}
	
	
	
	
	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/chart/contractcount", method = RequestMethod.GET)
	@ResponseBody
	public String loadContractCont(HttpServletResponse response,
			@RequestParam(value = "daterange", required = false) String daterange) {
		try {
			ObjectMapper mapper = new ObjectMapper().setSerializationInclusion(Inclusion.NON_NULL);
				
				if (daterange != null) {

					String[] ss = daterange.split("-");

					Date from = Time.ConvertToDatenew(ss[0]);

					Date to = Time.ConvertToDatenew(ss[1]);

					List<Object[]> Contract = chartService.findTotalMonthlyContractCount(to, from);
					
					

					ArrayNode m = mapper.createArrayNode();

					ArrayList<String> months = new ArrayList<>();
					ArrayList<String> conts = new ArrayList<>();
					ArrayList<String> years = new ArrayList<>();

					for (Object[] objects : Contract) {
						months.add(objects[0].toString());
						conts.add(objects[2].toString());
						years.add(objects[1].toString());
					}

					 

					m.add(months.toString());
					m.add(conts.toString());
					m.add(years.toString());

					String jsonreport = mapper.writeValueAsString(m);
					return jsonreport;
				}
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error load contractcount in index: " + " with error" + e.getMessage());
		}
		
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}
	
	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/chart/accountpie", method = RequestMethod.GET)
	@ResponseBody
	public String loadAccountPie(HttpServletResponse response,
			@RequestParam(value = "date", required = false) String date) {
		try {
			ObjectMapper mapper = new ObjectMapper().setSerializationInclusion(Inclusion.NON_NULL);
			
			if (date != null) {

				String[] ss = date.split("-");
				
				String year = ss[0];
				String mount = ss[1];
				
				

				Date from = Time.ConvertToDatemorning(ss[0]);

				Date to = Time.ConvertToDatenew(ss[1]);

				List<Object[]> installmentIssue = chartService.findTotalMonthlyInstallmentCount(to, from);
				List<Object[]> installmentPaid = chartService.findTotalMonthlyInstallmentPaidCount(to, from);
				List<Object[]> installmentNotPaid = chartService.findTotalMonthlyInstallmentNotPaidCount(to, from,Time.today());
				List<Object[]> installmentPatrialPaid = chartService.findTotalMonthlyInstallmentPatrialPaidCount(to, from);

				ArrayNode m = mapper.createArrayNode();

				ArrayList<String> months = new ArrayList<>();
				ArrayList<String> years = new ArrayList<>();
				ArrayList<String> conts = new ArrayList<>();
				ArrayList<String> paid = new ArrayList<>();
				ArrayList<String> Notpaid = new ArrayList<>();
				ArrayList<String> partial = new ArrayList<>();

				for (Object[] objects : installmentIssue) {
					months.add(objects[0].toString());
					years.add(objects[1].toString());
					conts.add(objects[2].toString());
				}
				
				 
					
					
				int index = 0;
				int index2 = 0;
				int index3 = 0;
				int size = installmentPaid.size();
				int size2 = installmentNotPaid.size();
				int size3 = installmentPatrialPaid.size();
				
				
					 for (int i = 0; i < months.size(); i++) {
						 
						 
						 if(index<size) {
							 
							 if(installmentPaid.get(index)[0].toString().equals(months.get(i))) {
								 paid.add(installmentPaid.get(index)[1].toString());
								 index++;
							 }else {
								 paid.add("0");
							 }
						 }else {
							 paid.add("0");
							 
						 }
						 
						 if(index2<size2) {
							 
							 if(installmentNotPaid.get(index2)[0].toString().equals(months.get(i))) {
								 Notpaid.add(installmentNotPaid.get(index2)[1].toString());
								 index2++;
							 }else {
								 Notpaid.add("0");
							 }
						 }else {
							 Notpaid.add("0");
							 
						 }
						 if(index3<size3) {
							 
							 if(installmentPatrialPaid.get(index3)[0].toString().equals(months.get(i))) {
								 partial.add(installmentPatrialPaid.get(index3)[1].toString());
								 index3++;
							 }else {
								 partial.add("0");
							 }
						 }else {
							 partial.add("0");
							 
						 }
						 
						 
						 
						
					}
					
					
				 

//				for (Object[] objects : installmentPaid) {
//					paid.add(objects[1].toString());
//				}
//				for (Object[] objects : installmentNotPaid) {
//					Notpaid.add(objects[1].toString());
//				}

				m.add(months.toString());
				m.add(conts.toString());
				m.add(paid.toString());
				m.add(Notpaid.toString());
				m.add(partial.toString());
				m.add(years.toString());

				String jsonreport = mapper.writeValueAsString(m);
				return jsonreport;
			}

		} catch (Exception e) {
			logger.error("Error load installmentcount in index: " + " with error" + e.getMessage());
			e.printStackTrace();

		}
		return null;
	}

}
