package rza.BPFco.controller.admin;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import rza.BPFco.form.AdminDocumentTypeForm;
import rza.BPFco.form.filter.DocumentFilterForm;
import rza.BPFco.model.portfoys.DocType;
import rza.BPFco.service.DocumentService;
import rza.BPFco.service.UserService.User;

@Controller
public class AdminDocTypeController {

	@Autowired
	private DocumentService documentService;

	protected static final Logger logger = LogManager.getLogger();

	@RequestMapping(value = { "/adminpanel/doc_type" }, method = RequestMethod.GET)
	public String index() {

		return "html/admin/doctype_manage";
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/doctypes/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadAgents(HttpServletResponse response, @ModelAttribute DocumentFilterForm form,
			Authentication authentication) {
		try {

			User user = (User) authentication.getPrincipal();

			if (user != null) {

				Page<DocType> pageAuthors = documentService.loadAllDocsTypes(form);
				ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
						false);
				ArrayNode jsonRep = mapper.createArrayNode();
				for (DocType document : pageAuthors.getContent()) {
					ObjectNode json = mapper.createObjectNode();
					json.put("id", document.getId());
					json.put("title", document.getTitle());

					jsonRep.add(json);
				}

				ObjectNode page = mapper.createObjectNode();
				page.put("total", pageAuthors.getTotalPages());
				page.put("number", pageAuthors.getNumber() + 1);
				jsonRep.add(page);

				String jsonreport = mapper.writeValueAsString(jsonRep);
				logger.fatal(user.getAccount().getName() + " LOAD DOCTYPES IN ADMINPANEL: ");
				return jsonreport;
			}

		} catch (Exception e) {
			logger.error("Error load DOCTYPES in adminpanel: " + " with error" + e.getMessage());
			e.printStackTrace();
		}

		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/doctypes/loadChecked", method = RequestMethod.POST)
	@ResponseBody
	public String loadCheckedDocTypes(HttpServletResponse response, @ModelAttribute DocumentFilterForm form) {
		try {

			Page<DocType> pageAuthors = documentService.loadCheckedDocsTypes(form);
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			ObjectNode json = mapper.createObjectNode();

			ArrayNode userCats = mapper.createArrayNode();
			for (DocType document : pageAuthors.getContent()) {
				ObjectNode node = mapper.createObjectNode();
				node.put("id", document.getId());
				node.put("title", document.getTitle());

				userCats.add(node);
			}
			json.put("doctypes", userCats);

			String jsonreport = mapper.writeValueAsString(json);
			return jsonreport;

		} catch (Exception e) {
			logger.error("Error loadCheckedDocTypes in adminpanel: " + " with error" + e.getMessage());
			e.printStackTrace();
		}

		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/doctype/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadAgentById(@RequestParam(value = "id") long id) {
		try {
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			ObjectNode json = mapper.createObjectNode();
			DocType docType = documentService.loadDocTypeById(id);

			json.put("id", docType.getId());
			json.put("title", docType.getTitle());

			String jsonreport = mapper.writeValueAsString(json);
			return jsonreport;

		} catch (Exception e) {
			logger.error("ERROR load CUSTOMER failed from loading this id: " + id + " with error" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/doctype/save", method = RequestMethod.POST)
	@ResponseBody
	public String saveCustomer(HttpServletResponse response, @Valid @ModelAttribute AdminDocumentTypeForm form,
			Authentication authentication) {

		try {

			User user = (User) authentication.getPrincipal();

			if (user.isAdmin()) {

				if (form.getId() != null) {

					DocType doc = documentService.loadDocTypeById(form.getId());
					doc = form.manipulatDocType(doc);
					documentService.saveDocType(doc);
					logger.fatal(
							user.getAccount().getName() + " MODIFY DOCTYPES IN ADMINPANEL BY NAME: " + doc.getTitle());

				} else {

					DocType document = documentService.findBDocTypeTitle(form.getTitle());
					if (document == null) {
						DocType doc = form.createDocType();
						documentService.saveDocType(doc);
						logger.fatal(user.getAccount().getName() + " CREATE DOCTYPES IN ADMINPANEL BY NAME: "
								+ doc.getTitle());
					} else {
						response.setStatus(HttpStatus.BAD_REQUEST.value());
						return "1";
					}

				}

			}

			return null;
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());

			return null;
		}

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/doctype/saveChecked", method = RequestMethod.POST)
	@ResponseBody
	public String deleteVariantvalue(HttpServletResponse response,
			@RequestParam(value = "doctypes") List<Long> doctypes, Authentication authentication) {
		try {
			User user = (User) authentication.getPrincipal();
			List<DocType> DocTypes = documentService.findDocmentTypesByIds(doctypes);
			documentService.setCheckDocTypeFalse();

			for (DocType docType : DocTypes) {
				docType.setControlled(true);
				documentService.saveDocType(docType);
				logger.fatal(user.getAccount().getName() + " MODIFY CHECKED DOC LIST IN ADMINPANEL BY DOCTYPE IDS: " + doctypes);
			}

		} catch (Exception e) {
			response.setStatus(HttpStatus.BAD_REQUEST.value());
		}
		return null;
	}

}
