package rza.BPFco.controller.admin;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mysema.query.types.expr.BooleanExpression;

import rza.BPFco.config.initProperties;
import rza.BPFco.form.AdminInstallmentAutoForm;
import rza.BPFco.form.AdminInstallmentForm;
import rza.BPFco.form.filter.InstallmentFilterForm;
import rza.BPFco.model.Contract;
import rza.BPFco.model.Portfoy;
import rza.BPFco.model.portfoys.Customer;
import rza.BPFco.model.portfoys.Installment;
import rza.BPFco.model.portfoys.Mes;
import rza.BPFco.service.ContractService;
import rza.BPFco.service.UserService;
import rza.BPFco.service.UserService.User;
import rza.BPFco.utilities.CreateExcel;
import rza.BPFco.utilities.Source;
import rza.BPFco.utilities.Time;

@Controller
public class AdminInstallmentController {

	@Autowired
	ContractService contractService;
	
	@Autowired
	UserService userService;

	protected static final Logger logger = LogManager.getLogger();

	@RequestMapping(value = { "/installment_manage" }, method = RequestMethod.GET)
	public String index() {

		return "html/admin/installment_manage";
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/installments/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadInstallments(HttpServletResponse response, @ModelAttribute InstallmentFilterForm form) {
		try {
			
//			User user = (User) authentication.getPrincipal();
			form.setChackNotify(false);
			   
			Page<Installment> pageAuthors;
			if (!initProperties.active) {
				pageAuthors = contractService.loadAllInstallments(form);
			}else {
				 boolean validated = userService.loadAccountByEmail();
					if (!validated)
						return null;

				 
				
				 pageAuthors = contractService.loadAllInstallments(form);
			}

			
			
			
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			ArrayNode jsonRep = mapper.createArrayNode();

			BooleanExpression predicates = form.getPredicate();
			
			String sort = form.getSort().toString();
			
			

			for (Installment installment : pageAuthors.getContent()) {
				ObjectNode json = mapper.createObjectNode();
				json.put("id", installment.getId());
				switch (installment.getCurrency()) {
				case 1:// TR
					json.put("cur", "TL");

					break;
				case 2:// DOLAR
					json.put("cur", "USD");

					break;
				case 3:// EURO
					json.put("cur", "EURO");

					break;

				}
				json.put("cid", installment.getContract().getId());
				json.put("amount", installment.getAmount());
				json.put("issue_date", Time.ToTurkishDateMonthDateOnly(installment.getIssueDate()));
				if (installment.getPaymantDate() != null) {
					json.put("paymant_date", Time.ToTurkishDateMonthDateOnly(installment.getPaymantDate()));
				}
				json.put("contract_no", installment.getContract().getContract_no());
				json.put("contractDate", Time.ToTurkishDateMonthDateOnly(installment.getContract().getCreateDate()));
				json.put("paid", installment.isPaid());

				int status = Time.compare(installment.getIssueDate());

				if (!installment.isPaid()) {
					switch (status) {
					case 0:
						json.put("status", "danger");
						break;
					case 1:
						json.put("status", "danger");
						break;
					case 2:
						json.put("status", "info");
						break;
					case 3:
						json.put("status", "warning");
						break;
					default:
						break;
					}
				} else {
					json.put("status", "success");
				}

				json.put("fifteenDays", Time.fifteenDays(installment.getIssueDate()));
				Contract contract = installment.getContract();

				ArrayNode userCats = mapper.createArrayNode();
				for (Customer account : contract.getCustomer()) {
					ObjectNode node = mapper.createObjectNode();
					node.put("name", account.getFname());
					node.put("id", account.getId());
					userCats.add(node);
				}
				json.put("customers", userCats);

				jsonRep.add(json);
			}

			ObjectNode page = mapper.createObjectNode();
			page.put("total", pageAuthors.getTotalPages());
			page.put("number", pageAuthors.getNumber() + 1);
			if (predicates != null) {

				page.put("predicate", predicates.toString());
			}
			if (sort != null) {

				page.put("sort", sort);
			}
			jsonRep.add(page);

			String jsonreport = mapper.writeValueAsString(jsonRep);

			return jsonreport;

		} catch (Exception e) {
			logger.error("Error load INSTALLMENTS in adminpanel: " + " with error" + e.getMessage());
			e.printStackTrace();
		}

		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/installments/excel", method = RequestMethod.POST)
	@ResponseBody
	public String exportInstallments(HttpServletResponse response, @ModelAttribute InstallmentFilterForm form,Authentication authentication) {
		try {
			
			User user = (User) authentication.getPrincipal();
			
			if(form.getSorting().contains("issueDate")) {
				
				form.setSorting(form.getSorting().replace("issueDate: ASC", "issueDate-0"));
				form.setSorting(form.getSorting().replace("issueDate: DESC", "issueDate-1"));
				 
				 
			}
			
			
			form.setSort(form.getSorting());
			form.setPsize(1000);
			Page<Installment> pageAuthors = contractService.loadAllInstallments(form);

			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			ObjectNode json = mapper.createObjectNode();

			CreateExcel createExcel = new CreateExcel();
			String path = createExcel.CrateExcelFromInstallmens(pageAuthors);

			json.put("path", path);
			String jsonreport = mapper.writeValueAsString(json);
			logger.fatal(user.getAccount().getName() + " EXPORT EXCEL FILE FOR INSTALLMENTS ");

			return jsonreport;

		} catch (Exception e) {
			logger.error("Error EXPORT EXCEL FILE in adminpanel: " + " with error" + e.getMessage());
			e.printStackTrace();
		}

		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/installment/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadInstallmentById(@RequestParam(value = "id") long id) {
		try {
			
//			User user = (User) authentication.getPrincipal();
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			// ArrayNode jsonRep = mapper.createArrayNode();
			ObjectNode json = mapper.createObjectNode();

			Installment installment = contractService.loadInstallmentById(id);

			json.put("id", installment.getId());
			json.put("amount", installment.getAmount());
			json.put("description", installment.getDescription());
			json.put("issue_date", Time.ToSplitedDate(installment.getIssueDate()));
			if (installment.getPaymantDate() != null) {
				json.put("paymant_date", Time.ToSplitedDate(installment.getPaymantDate()));
			}
			json.put("paid", installment.isPaid());
			json.put("partial", installment.isPartial());
			if (installment.getPartial_amount() != null) {
				json.put("partial_amount", installment.getPartial_amount());
			}
			ObjectNode node = mapper.createObjectNode();
			node.put("title", installment.getContract().getContract_no());
			node.put("id", installment.getContract().getId());
			node.put("descraption", installment.getDescription());
			json.put("contract_no", node);
			json.put("customer", installment.getContract().getCustomer().iterator().next().getFname());
			json.put("customerid", installment.getContract().getCustomer().iterator().next().getId());
			
			String jsonreport = mapper.writeValueAsString(json);
			return jsonreport;

//			 

		} catch (Exception e) {
			logger.error("ERROR load TAG failed from loading this id: " + id + " with error" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@SuppressWarnings("null")
	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/installment/loadForCalender", method = RequestMethod.POST)
	@ResponseBody
	public String loadInstallmentByIdForCalender(@RequestParam(value = "id") long id) {
		try {
			
//			User user = (User) authentication.getPrincipal();
			DecimalFormatSymbols dfs = new DecimalFormatSymbols();
			dfs.setDecimalSeparator(',');
			dfs.setGroupingSeparator('.');
			DecimalFormat formatter = new DecimalFormat("###,###,###.##");
			formatter.setDecimalFormatSymbols(dfs);
			
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			// ArrayNode jsonRep = mapper.createArrayNode();
			ObjectNode json = mapper.createObjectNode();

			Installment installment = contractService.loadInstallmentByIdJoinMessages(id);

			json.put("id", installment.getId());
			 

			json.put("amount", formatter.format(Float.parseFloat(installment.getAmount().replaceAll("[^\\d.]", ""))));
			json.put("issue_date", Time.ToSplitedDateCalneder(installment.getIssueDate()));
			if (installment.getPaymantDate() != null) {
				json.put("paymant_date", Time.ToSplitedDateCalneder(installment.getPaymantDate()));
			}
			json.put("paid", installment.isPaid());
			json.put("partial", installment.isPartial());
			if (installment.getPartial_amount() != null && !installment.getPartial_amount().isEmpty()) {
				json.put("partial_amount", formatter.format(Float.parseFloat(installment.getPartial_amount().replaceAll("[^\\d.]", ""))));
			}

			if (installment.isPartial()) {
				json.put("paymant_status", "Kısmi Ödendi");

			}
			if (installment.isPaid()) {
				json.put("paymant_status", "Ödendi");
			}
			int d = Time.compare(installment.getIssueDate());
			if (!installment.isPaid()) {

				if (!installment.isPartial()) {
					if (d == 1) {
						json.put("paymant_status", "Ödenmedi");
					}
				}

			}

			if (installment.isPaid()) {
				if (!installment.isPartial()) {
					json.put("partial_amount", formatter.format(Float.parseFloat(installment.getAmount().replaceAll("[^\\d.]", ""))));

				}
			}

			// GET MESSAGES
			Set<Mes> messages = installment.getMes();

			if (messages.size() > 0) {
				//
				for (Mes message : messages) {

					//
					if (installment.isPaid() || installment.isPartial()) {
						int s = Source.MessageType2().indexOf("Teşekkür") + 1;

						if (message.getType() == s) {
							//

						} else {
							json.put("status", "2");// tsk
						}

					} else {

						switch (Time.compare(installment.getIssueDate())) {
						case 0:// same

							if (message.getType() != Source.MessageType2().indexOf("Hatırlatma") + 1) {
								json.put("status", "1");// no reminding
							}

							break;
						case 1:// before

							if (message.getType() != Source.MessageType2().indexOf("Uyarı") + 1) {
								json.put("status", "3");// no warning
							}

							break;
						case 2:// after

							break;
						case 3:// 10 days

							if (message.getType() != Source.MessageType2().indexOf("Hatırlatma") + 1) {
								json.put("status", "1");// no reminding
							}

							break;

						}
					}

				}

			} else {

				if (installment.isPaid() || installment.isPartial()) {

					json.put("status", "2");// tsk

				}

				if (!installment.isPaid()&& !installment.isPartial()) {

					switch (Time.compare(installment.getIssueDate())) {
					case 0:// same

						json.put("status", "1");// no reminding

						break;

					case 1:// before

						json.put("status", "3");// no warning

						break;
					case 3:// 10 days

						json.put("status", "1");// no reminding

						break;

					}
				}

				//
			}

			ObjectNode node = mapper.createObjectNode();
			node.put("title", installment.getContract().getContract_no());
			node.put("id", installment.getContract().getId());
			node.put("descraption", installment.getDescription());
			json.put("customer", installment.getContract().getCustomer().iterator().next().getFname());
			json.put("customerid", installment.getContract().getCustomer().iterator().next().getId());
			json.put("contract", node);

			ArrayNode portfoys = mapper.createArrayNode();
			for (Portfoy portfoy : installment.getContract().getPortfoys()) {
				ObjectNode obj = mapper.createObjectNode();
				obj.put("title", portfoy.getTitle());
				obj.put("id", portfoy.getId());

				portfoys.add(obj);
			}
			json.put("portfoys", portfoys);

			Set<Installment> dd = contractService.loadInstallmentsByContractId(installment.getContract().getId());
			ArrayList<Float> paid = new ArrayList<Float>();
			ArrayNode installments = mapper.createArrayNode();
			int index = 1;
			String source = Source.CurrencySource().get(installment.getContract().getCurrency() - 1);
			json.put("cur", source);
			for (Installment ins : dd) {
				ObjectNode obj = mapper.createObjectNode();
				obj.put("amount", ins.getAmount() + " " + source);
				if (ins.getPartial_amount() != null) {
					if (ins.getPartial_amount() != "") {
						obj.put("partial_amount", ins.getPartial_amount() + " " + source);

					}
				}

				if (ins.isPaid()) {
					obj.put("partial_amount", ins.getAmount() + " " + source);
				}

				obj.put("order", index++);
				obj.put("id", ins.getId());
				obj.put("issue_date", Time.ToTurkishDateMonthDateOnly(ins.getIssueDate()));
				if (ins.getPaymantDate() != null) {
					obj.put("paymant_date", Time.ToTurkishDateMonthDateOnly(ins.getPaymantDate()));
				}
				obj.put("paid", ins.isPaid());

				if (ins.isPartial()) {
					obj.put("paymant_status", "Kısmi Ödendi");
				}
				if (ins.isPaid()) {
					obj.put("paymant_status", "Ödendi");
				}
				int dr = Time.compare(ins.getIssueDate());
				if (!ins.isPaid()) {

					if (!ins.isPartial()) {
						if (dr == 1) {
							obj.put("paymant_status", "Ödenmedi");
						}
					}

				}

				float amount = 0;
				if (ins.isPaid()) {

					if (ins.isPartial()) {
						// partial
						amount = Float.parseFloat(ins.getPartial_amount().replaceAll("[^\\d.]", ""));
						

					} else {
						// amount
						amount = Float.parseFloat(ins.getAmount().replaceAll("[^\\d.]", ""));
					}

				} else if (ins.isPartial()) {
					// partial
					amount = Float.parseFloat(ins.getPartial_amount().replaceAll("[^\\d.]", ""));


				}

				paid.add(amount);
				installments.add(obj);
			}

			float sum = 0;

			for (int i = 0; i < paid.size(); i++) {

				sum += paid.get(i);

			}
			String chash = "0";
			if(!installment.getContract().getCash().isEmpty()) {
				chash = installment.getContract().getCash();
			}
			
			float pesin = Float.parseFloat(chash.replaceAll("[^\\d.]", ""));
			
			json.put("remaining",
					formatter.format(Float.parseFloat(installment.getContract().getPrice().replaceAll("[^\\d.]", ""))-pesin - sum));

			 

			String jsonreport = mapper.writeValueAsString(json);
			return jsonreport;

//			 

		} catch (Exception e) {
			logger.error("ERROR load Instlamment failed from loading this id: " + id + " with error" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/installment/save", method = RequestMethod.POST)
	@ResponseBody
	public String saveCustomer(HttpServletResponse response, @Valid @ModelAttribute AdminInstallmentForm form,
			Authentication authentication) {

		try {

			User user = (User) authentication.getPrincipal();

			if (user.isAdmin()) {

				if (form.getId() != null) {

					Installment installment = contractService.loadInstallmentById(form.getId());
					installment = form.manipulatInstallment(installment);
					Contract contract = contractService.loadContractById(form.getContract_no());
					installment.setContract(contract);
					installment.setCurrency(contract.getCurrency());

					contractService.save(installment);
					logger.fatal(user.getAccount().getName() + " MODIFY  INSTALLMENT BY ID: " + form.getId());

				} else {

					Installment installment = form.createInstallment();
					Contract contract = contractService.loadContractById(form.getContract_no());
					installment.setContract(contract);
					installment.setCurrency(contract.getCurrency());
					contractService.save(installment);
					logger.fatal(user.getAccount().getName() + " CREATE  INSTALLMENT BY ID: " + form.getId());

				}

			}

			return null;
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());

			return null;
		}

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/autoinstallment/save", method = RequestMethod.POST)
	@ResponseBody
	public String saveAutoInstallmets(HttpServletResponse response,
			@Valid @ModelAttribute AdminInstallmentAutoForm form, Authentication authentication) {

		try {

			User user = (User) authentication.getPrincipal();

			if (user.isAdmin()) {

				if (form.getId() != null) {

					Installment installment = contractService.loadInstallmentById(form.getId());

					List<Date> ins = Source.autoInstallmentDate(form.getLimit(), form.getRange(),
							Time.ConvertToDate(form.getIssue_date()));

					for (Date date : ins) {

						installment = form.manipulatInstallment(installment);
						installment.setIssueDate(date);
						installment.setContract(contractService.loadContractById(form.getContract_no()));

						contractService.save(installment);
						logger.fatal(user.getAccount().getName() + " ADD AUTO INSTALLMENT BY CONTRACT ID: " + form.getId());

					}

				} else {

					List<Date> ins = Source.autoInstallmentDate(form.getRange(), form.getLimit(),
							Time.ConvertToDate(form.getIssue_date()));

					for (Date date : ins) {

						Installment installment = form.createInstallment();
						installment.setIssueDate(date);
						installment.setContract(contractService.loadContractById(form.getContract_no()));

						contractService.save(installment);
						logger.fatal(user.getAccount().getName() + " CREATE AUTO INSTALLMENT BY CONTRACT ID: " + form.getId());

					}

				}

			}

			return null;
		} catch (Exception e) {
			logger.error("SAVE AUTO INSTALLMENT ERROR : "+e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());

			return null;
		}

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/installment/remove", method = RequestMethod.POST)
	@ResponseBody
	public String deleteVariantvalue(HttpServletResponse response, @RequestParam(value = "ids") List<Long> ids,Authentication authentication) {
		try {
			
			User user = (User) authentication.getPrincipal();

			List<Installment> installment = new ArrayList<Installment>();
			for (Long id : ids) {

				installment.add(new Installment(id));
			}

			contractService.removeInstallment(installment);
			logger.fatal(user.getAccount().getName() + " REMOVE  INSTALLMENTS BY  ID: " + ids.toString().replace("[", "").replace("]", ""));

		} catch (Exception e) {
			logger.error("ERROR deleting installment with ids= " + ids + " with error: " + e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());
		}
		return null;
	}
	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/installment/changeDate", method = RequestMethod.POST)
	@ResponseBody
	public String changeVariantvalue(HttpServletResponse response) {
		try {
			
			List<Installment> installment = contractService.loadAllInstallments();
			for (Installment ins : installment) {
				
				Date issue=null;
				Date paymant=null;
				
				 if(Time.chackHourse(ins.getIssueDate())) {
					 issue = Time.AddDay(ins.getIssueDate(), 1);
					 
					 
					 
					 if(ins.getPaymantDate()!=null) {
						 if(Time.chackHourse(ins.getPaymantDate())) {
							 Time.AddDay(ins.getPaymantDate(), 1);
						 paymant = Time.AddDay(ins.getPaymantDate(), 1);
						 }
						 
					 }
					 ins.setIssueDate(issue);
					 if(paymant!= null) {
						 ins.setPaymantDate(paymant);
						 
					 }
					 contractService.save(ins);
					 
					 
				 }
				
			}
			
			
			 
			
			 
			
		} catch (Exception e) {
			logger.error("ERROR CHANGE INS DATE with ids= "  + " with error: " + e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());
		}
		return null;
	}

}
