
package rza.BPFco.controller.admin;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import rza.BPFco.config.initProperties;
import rza.BPFco.form.AdminCoverForm;
import rza.BPFco.form.AdminInstallmentAutoForm;
import rza.BPFco.form.filter.CoverFilterForm;
import rza.BPFco.model.Account;
import rza.BPFco.model.Portfoy;
import rza.BPFco.model.Tag;
import rza.BPFco.model.portfoys.Blok;
import rza.BPFco.model.portfoys.Cover;
import rza.BPFco.model.portfoys.Customer;
import rza.BPFco.model.portfoys.InsCover;
import rza.BPFco.model.portfoys.Kat;
import rza.BPFco.model.portfoys.Section;
import rza.BPFco.service.ContractService;
import rza.BPFco.service.CoverService;
import rza.BPFco.service.DocumentService;
import rza.BPFco.service.PersonService;
import rza.BPFco.service.PortfoyService;
import rza.BPFco.service.TagService.TagType;
import rza.BPFco.service.UserService;
import rza.BPFco.service.UserService.User;
import rza.BPFco.utilities.NumToWords;
import rza.BPFco.utilities.Source;
import rza.BPFco.utilities.Time;

@Controller
public class CoverController {

	@Autowired
	ContractService contractService;

	@Autowired
	CoverService coverService;

	@Autowired
	PortfoyService portfoyService;

	@Autowired
	UserService userService;

	@Autowired
	PersonService personService;

	@Autowired
	DocumentService documentService;

	protected static final Logger logger = LogManager.getLogger();

	@RequestMapping(value = { "/cover_manage" }, method = RequestMethod.GET)
	public String index() {

		return "html/admin/cover_manage";
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/covers/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadInstallments(HttpServletResponse response, @ModelAttribute CoverFilterForm form,
			Authentication authentication) {
		try {

			User user = (User) authentication.getPrincipal();
			if (user != null) {

				if (user.isAdmin()) {
					//
					form.setAdmin(true);

				} else {
					//
					form.setAdmin(false);
					form.setTemsilci(user.getUserId());

				}

			} else {
				return null;
			}
			
			Page<Cover> pageAuthors;
			if (!initProperties.active) {
				pageAuthors = coverService.loadAllCovers(form);
			}else {
				 boolean validated = userService.loadAccountByEmail();
					if (!validated)
						return null;

				 
				
				 pageAuthors = coverService.loadAllCovers(form);
			}

			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			ArrayNode jsonRep = mapper.createArrayNode();

			for (Cover cover : pageAuthors.getContent()) {
				ObjectNode json = mapper.createObjectNode();
				json.put("id", cover.getId());
				switch (cover.getCurrency()) {
				case 1:// TR
					json.put("cur", "TL");

					break;
				case 2:// DOLAR
					json.put("cur", "USD");

					break;
				case 3:// EURO
					json.put("cur", "EURO");

					break;

				}
				if (cover.getContractDate() != null) {

					json.put("contractDate", Time.ToTurkishDateOnly(cover.getContractDate()));
				}
				json.put("des", cover.getDes());

				json.put("price", cover.getPrice());
				json.put("cash", cover.getCash());

				String price = cover.getPrice();
				String cash = cover.getCash();
				float toplam = 0;
				if (price != null) {
					if (cash != null) {
						float p = Float.parseFloat(price.replaceAll("[^\\d.]", ""));
						float c = Float.parseFloat(cash.replaceAll("[^\\d.]", ""));
						toplam = p - c;

					}
				}
				Set<InsCover> ins = cover.getInsCover();

				float amount = 0f;
				if (ins != null) {

					for (InsCover insCover : ins) {

						float am = Float.parseFloat(insCover.getAmount().replaceAll("[^\\d.]", ""));
						amount += am;

					}
				}
				DecimalFormatSymbols dfs = new DecimalFormatSymbols();
				dfs.setDecimalSeparator('.');
				dfs.setGroupingSeparator(',');
				DecimalFormat formatter = new DecimalFormat("###,###,###.##");
				formatter.setDecimalFormatSymbols(dfs);

//				NumberFormat formatter = new DecimalFormat("###,###,###.##");
				json.put("remaining", formatter.format(toplam - amount));

				ArrayNode userCats = mapper.createArrayNode();
				for (Customer account : cover.getCustomer()) {
					ObjectNode node = mapper.createObjectNode();
					node.put("name", account.getFname());
					node.put("id", account.getId());
					userCats.add(node);
				}
				json.put("customers", userCats);

				jsonRep.add(json);
			}

			ObjectNode page = mapper.createObjectNode();
			page.put("total", pageAuthors.getTotalPages());
			page.put("number", pageAuthors.getNumber() + 1);
			jsonRep.add(page);

			String jsonreport = mapper.writeValueAsString(jsonRep);
			return jsonreport;

		} catch (Exception e) {
			logger.error("Error load cover in adminpanel: " + " with error" + e.getMessage());
			e.printStackTrace();
		}

		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/cover/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadAgentById(@RequestParam(value = "id") long id) {
		try {
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			ObjectNode json = mapper.createObjectNode();
			Cover cover = coverService.loadCoverById(id);

			json.put("id", cover.getId());
			 
			json.put("dsc", cover.getDsc());
			if (cover.getContractDate() != null) {

				json.put("contractDate", Time.ToSplitedDate(cover.getContractDate()));
			}
			if (cover.getCashDate() != null) {

				json.put("cashDate", Time.ToSplitedDate(cover.getCashDate()));
			}
			json.put("price", cover.getPrice());
			json.put("cash", cover.getCash());
			ArrayList<String> cur = Source.CurrencySource();

			ObjectNode jsons = mapper.createObjectNode();

			jsons.put("id", cover.getCurrency());
			jsons.put("title", cur.get(cover.getCurrency() - 1));

			json.put("currency", jsons);
			json.put("fee", cover.getFee());
			
			if(cover.getFee()==2) {
				json.put("harc", cover.getHarc());
			}

			String price = cover.getPrice();
			String cash = cover.getCash();
			float toplam = 0;
			if (price != null) {
				if (cash != null) {
					float p = Float.parseFloat(price.replaceAll("[^\\d.]", ""));
					float c = Float.parseFloat(cash.replaceAll("[^\\d.]", ""));
					toplam = p - c;

				}
			}
			Set<InsCover> ins = cover.getInsCover();

			float amount = 0f;
			if (ins != null) {

				for (InsCover insCover : ins) {

					float am = Float.parseFloat(insCover.getAmount().replaceAll("[^\\d.]", ""));
					amount += am;

				}
			}
			DecimalFormatSymbols dfs = new DecimalFormatSymbols();
			dfs.setDecimalSeparator('.');
			dfs.setGroupingSeparator(',');
			DecimalFormat formatter = new DecimalFormat("###,###,###.##");
			formatter.setDecimalFormatSymbols(dfs);

			json.put("remaining", formatter.format(toplam - amount));

			ArrayNode jsonRep = mapper.createArrayNode();
			Set<Account> accounts = cover.getAccount();
			for (Account account : accounts) {
				//
				ObjectNode node1 = mapper.createObjectNode();
				node1.put("title", account.getName());
				node1.put("id", account.getId());
				jsonRep.add(node1);

			}
			json.put("temsilci", jsonRep);

			ArrayNode jsonPor = mapper.createArrayNode();
			Set<Portfoy> portfoys = cover.getPortfoy();
			for (Portfoy portfoy : portfoys) {
				//
				ObjectNode node2 = mapper.createObjectNode();
				node2.put("title", portfoy.getTitle());
				node2.put("id", portfoy.getId());
				jsonPor.add(node2);

			}
			json.put("portfoys", jsonPor);

			ArrayNode jsonCus = mapper.createArrayNode();
			Set<Customer> customers = cover.getCustomer();
			for (Customer customer : customers) {
				//
				ObjectNode node3 = mapper.createObjectNode();
				node3.put("title", customer.getFname());
				node3.put("id", customer.getId());
				jsonCus.add(node3);

			}
			json.put("customers", jsonCus);

			String jsonreport = mapper.writeValueAsString(json);
			return jsonreport;

		} catch (Exception e) {
			logger.error("ERROR load SM failed from loading this id: " + id + " with error" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/cover/loadforIns", method = RequestMethod.POST)
	@ResponseBody
	public String loadinsForCover(@RequestParam(value = "id") long id) {
		try {
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			ObjectNode json = mapper.createObjectNode();
			Cover cover = coverService.loadCoverById(id);

			json.put("id", cover.getId());
			json.put("price", cover.getPrice());
			json.put("cash", cover.getCash());
			json.put("cur", Source.CurrencySource().get(cover.getCurrency() - 1));

			ArrayList<String> cur = Source.CurrencySource();

			ObjectNode jsons = mapper.createObjectNode();

			jsons.put("id", cover.getCurrency());
			jsons.put("title", cur.get(cover.getCurrency() - 1));

			json.put("currency", jsons);

			String price = cover.getPrice();
			String cash = cover.getCash();
			float toplam = 0;
			if (price != null) {
				if (cash != null) {
					float p = Float.parseFloat(price.replaceAll("[^\\d.]", ""));
					float c = Float.parseFloat(cash.replaceAll("[^\\d.]", ""));
					toplam = p - c;

				}
			}
			Set<InsCover> ins = cover.getInsCover();

			float amount = 0f;
			if (ins != null) {

				for (InsCover insCover : ins) {

					float am = Float.parseFloat(insCover.getAmount().replaceAll("[^\\d.]", ""));
					amount += am;

				}
			}
			DecimalFormatSymbols dfs = new DecimalFormatSymbols();
			dfs.setDecimalSeparator('.');
			dfs.setGroupingSeparator(',');
			DecimalFormat formatter = new DecimalFormat("###,###,###.##");
			formatter.setDecimalFormatSymbols(dfs);
			json.put("remaining", formatter.format(toplam - amount));

			String jsonreport = mapper.writeValueAsString(json);
			return jsonreport;

		} catch (Exception e) {
			logger.error("ERROR load SM failed from loading this id: " + id + " with error" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/covermodal/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadCoverForModal(@RequestParam(value = "id") long id) {
		try {
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			ObjectNode json = mapper.createObjectNode();
			Cover cover = coverService.loadCoverById(id);

			json.put("id", cover.getId());
			json.put("contractDate", Time.ToTurkishDateMonthDateOnly(cover.getContractDate()));
			if (cover.getCashDate() != null) {

				json.put("cashDate", Time.ToTurkishDateMonthDateOnly(cover.getCashDate()));
			}
			json.put("downDatetr", Time.ToTurkishDateMonthDateOnly(cover.getContractDate()));

			DecimalFormatSymbols dfs = new DecimalFormatSymbols();
			dfs.setDecimalSeparator(',');
			dfs.setGroupingSeparator('.');
			DecimalFormat formatter = new DecimalFormat("###,###,###.##");
			formatter.setDecimalFormatSymbols(dfs);
			json.put("price", formatter.format(Float.parseFloat(cover.getPrice().replaceAll("[^\\d.]", ""))));
			json.put("cash", formatter.format(Float.parseFloat(cover.getCash().replaceAll("[^\\d.]", ""))));
			 
			
			switch (cover.getFee()) {
			case 0:
				json.put("fee", "TAPU- İSKAN ve EKSPERTİZ \n" +
						"ALICIYA AİTTİR");
				break;
			case 1:
				json.put("fee", "TAPU- İSKAN ve EKSPERTİZ \n" +
						"SATICIYA AİTTİR");
				break;
			case 2:
				 
				json.put("fee", cover.getHarc());
				break;

			default:
				break;
			}
			
			
			 
			
			if(cover.getDsc()!=null) {
				if(cover.getDsc().isEmpty()) {
					
					json.put("not", "");
				}else {
					json.put("not", "NOT:");
					json.put("dsc", cover.getDsc());
					
				}
			} 
			
			

			switch (cover.getCurrency()) {
			case 1:// TR
				json.put("cur", "TL");

				break;
			case 2:// DOLAR
				json.put("cur", "USD");

				break;
			case 3:// EURO
				json.put("cur", "EURO");

				break;

			}

			List<String> no = new ArrayList<String>();
			List<String> kat = new ArrayList<String>();
			List<String> blok = new ArrayList<String>();
			String project = "";
			if (cover.getPortfoy() != null) {

				Set<Portfoy> portfoys = cover.getPortfoy();

				List<String> sTags = new ArrayList<String>();
				for (Portfoy portfoy : portfoys) {

					Set<Tag> tags = portfoy.getTag();
					for (Tag tag : tags) {
						if (tag.getType() == TagType.Tip.ordinal()) {
							if (!sTags.contains(tag.getTitle())) {
								sTags.add(tag.getTitle());

							}
						}

					}

					json.put("types", sTags.toString());

					Set<Section> sections = portfoy.getSections();
					for (Section sec : sections) {

						no.add(sec.getTitle());

						Kat floor = sec.getKat();
						if (!kat.contains(floor.getTitle())) {
							kat.add(floor.getTitle());

						}

						Blok block = floor.getBlok();
						if (!blok.contains(block.getTitle())) {

							blok.add(block.getTitle());
						}

						project = block.getProje().getTitle();

						json.put("project", project);
					}

				}

			}
			String n = no.toString().substring(1, no.toString().length() - 1);
			String f = kat.toString().substring(1, kat.toString().length() - 1);
			json.put("sections", n);
			json.put("floors", f);
			String b = blok.toString().replace("Blok", "");
			b = b.substring(1, b.length() - 1);
			json.put("bloks", b);

			ArrayNode jsonRep = mapper.createArrayNode();
			Set<Account> accounts = cover.getAccount();
			for (Account account : accounts) {
				//
				ObjectNode node1 = mapper.createObjectNode();
				node1.put("title", account.getName());
				node1.put("id", account.getId());
				jsonRep.add(node1);

			}
			json.put("temsilci", jsonRep);

			ArrayNode jsonPor = mapper.createArrayNode();
			Set<Portfoy> portfoys = cover.getPortfoy();
			for (Portfoy portfoy : portfoys) {
				//
				ObjectNode node2 = mapper.createObjectNode();
				node2.put("title", portfoy.getTitle());
				node2.put("id", portfoy.getId());
				jsonPor.add(node2);

			}
			json.put("portfoys", jsonPor);

			ArrayNode jsonCus = mapper.createArrayNode();
			Set<Customer> customers = cover.getCustomer();
			for (Customer customer : customers) {
				//
				ObjectNode node3 = mapper.createObjectNode();
				node3.put("title", customer.getFname());
				node3.put("address", customer.getAddress());
				node3.put("gsm", customer.getGsm1());
				node3.put("mail", customer.getMail());
				node3.put("id", customer.getId());
				jsonCus.add(node3);

			}
			json.put("customers", jsonCus);

			ArrayNode jsonIns = mapper.createArrayNode();
			Set<InsCover> installments = cover.getInsCover();
			float amount = 0f;
			float toplam = 0;
			if (installments != null) {
				List<InsCover> insLst = new ArrayList<InsCover>();
				insLst.addAll(installments);
				rza.BPFco.utilities.Sorting sort = new rza.BPFco.utilities.Sorting();
				List<InsCover> newList = sort.SortInsCovers(insLst);
				int index = 1;
				for (InsCover insCover : newList) {
					//
					ObjectNode node4 = mapper.createObjectNode();
//					node4.put("amount", insCover.getAmount());

					node4.put("amount",
							formatter.format(Float.parseFloat(insCover.getAmount().replaceAll("[^\\d.]", ""))));

					node4.put("issueDate", Time.ToTurkishDateMonthDateOnly(insCover.getIssueDate()));
					node4.put("id", insCover.getId());
					jsonIns.add(node4);
					node4.put("order", index++);

					switch (insCover.getCurrency()) {
					case 1:// TR
						node4.put("cur", "TL");

						break;
					case 2:// DOLAR
						node4.put("cur", "USD");

						break;
					case 3:// EURO
						node4.put("cur", "EURO");

						break;

					}

				}
				json.put("installment", jsonIns);

				String price = cover.getPrice();
				String cash = cover.getCash();

				if (price != null) {
					if (cash != null) {
						float p = Float.parseFloat(price.replaceAll("[^\\d.]", ""));
						float c = Float.parseFloat(cash.replaceAll("[^\\d.]", ""));
						toplam = p - c;

					}
				}

				if (installments != null) {

					for (InsCover insCover : newList) {

						float am = Float.parseFloat(insCover.getAmount().replaceAll("[^\\d.]", ""));
						amount += am;

					}
				}
			}
//			DecimalFormatSymbols dfs = new DecimalFormatSymbols();
//			dfs.setDecimalSeparator(',');
//			dfs.setGroupingSeparator('.');
//			DecimalFormat formatter = new DecimalFormat("###,###,###.##");
//			formatter.setDecimalFormatSymbols(dfs);
//			json.put("remaining", formatter.format(toplam - amount));

			String jsonreport = mapper.writeValueAsString(json);
			return jsonreport;

		} catch (Exception e) {
			logger.error("ERROR load cover failed from loading this id: " + id + " with error" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/customer/cover/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadInstallmentById(@RequestParam(value = "id") long id, Authentication authentication) {
		try {
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			// ArrayNode jsonRep = mapper.createArrayNode();

			User user = (User) authentication.getPrincipal();

			ObjectNode json = mapper.createObjectNode();

			Customer customer = personService.loadCustomerById(id);

			json.put("customerid", customer.getId());

			ArrayNode customerCats = mapper.createArrayNode();
			ObjectNode node = mapper.createObjectNode();
			node.put("title", customer.getFname());
			node.put("id", customer.getId());
			customerCats.add(node);

			json.put("customers", customerCats);

			ArrayNode accountCats = mapper.createArrayNode();
			ObjectNode cusNode = mapper.createObjectNode();
			cusNode.put("title", user.getAccount().getName());
			cusNode.put("id", user.getAccount().getId());
			accountCats.add(cusNode);
			json.put("temsilci", accountCats);

			if (customer.getContarctDate() != null) {
				json.put("createDate", Time.ToSplitedDate(customer.getContarctDate()));
			}

			if (customer.getPortfoy() != null) {

				Pattern pattern = Pattern.compile("\\d+");
				Matcher matcher = pattern.matcher(customer.getPortfoy());

				List<Long> list = new ArrayList<Long>();

				while (matcher.find()) {
					list.add(Long.parseLong(matcher.group())); // Add the value to the list
				}

				//

				Set<Portfoy> portfoyslist = portfoyService.findPortfoysByIds(list);
				ArrayNode portfoyCats = mapper.createArrayNode();
				for (Portfoy portfoy : portfoyslist) {

					ObjectNode Pnode = mapper.createObjectNode();
					Pnode.put("title", portfoy.getTitle());
					Pnode.put("id", portfoy.getId());
					portfoyCats.add(Pnode);
				}
				json.put("portfoys", portfoyCats);
			}

			String jsonreport = mapper.writeValueAsString(json);
			return jsonreport;

//			 

		} catch (Exception e) {
			logger.error("ERROR load TAG failed from loading this id: " + id + " with error" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/coverInsLst/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadInstallmentForSenet(@RequestParam(value = "id") long id) {
		try {
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			// ArrayNode jsonRep = mapper.createArrayNode();


			Cover cover = coverService.loadCoverById(id);

			ArrayNode jsonRep = mapper.createArrayNode();

			if (cover.getInsCover() != null) {

				int index = 1;

				rza.BPFco.utilities.Sorting sorting = new rza.BPFco.utilities.Sorting();
				ArrayList<InsCover> insset = new ArrayList<InsCover>();
				insset.addAll(cover.getInsCover());
				List<InsCover> lst = sorting.SortInsCovers(insset);

				for (InsCover insCover : lst) {
					ObjectNode json = mapper.createObjectNode();

					// cover
					json.put("id", cover.getId());
					json.put("guarantor", cover.getGuarantor());
					json.put("place", cover.getPlace());
					json.put("court", cover.getCourt());
					json.put("senettype", Source.SenetType().get(cover.getSenettype()));
					json.put("order", index++);
					json.put("office", cover.getCustomer().iterator().next().getTaxOffice());
					json.put("name", cover.getCustomer().iterator().next().getFname());
					json.put("address", cover.getCustomer().iterator().next().getAddress());
					json.put("tax", cover.getCustomer().iterator().next().getTax());
					if (cover.getCustomer().iterator().next().getTc() != null) {

						json.put("tc", cover.getCustomer().iterator().next().getTc());
					}
					json.put("createDate", Time.ToSplitedDateCalneder(Time.todayDateOnly()));

					// ins

					double money = Double.parseDouble(insCover.getAmount().replaceAll("[^\\d.]", ""));

					int dollars = (int) Math.floor(money);
					double cents = money - dollars;
					int centsAsInt = (int) (100 * cents);

					String res = Source.convertToCurrency(dollars);
					json.put("dollars", "#" + res + "#");

//				    TODO//MoneyWords

					long d = Long.valueOf(dollars);
					long c = Long.valueOf(centsAsInt);

					String dollar = NumToWords.convert(d).replaceAll("\\s+", "");
					String cent = NumToWords.convert(c).replaceAll("\\s+", "");

					json.put("dollarsTxt", dollar);

					json.put("issueDate", Time.ToSplitedDateCalneder(insCover.getIssueDate()));
					json.put("issueDateTr", Time.ToTurkishDateOnly(insCover.getIssueDate()));

					switch (insCover.getCurrency()) {
					case 1:// TR
						json.put("currency", "Türk Lirası");
						json.put("cur", "₺");
						if (centsAsInt != 0) {
							json.put("cent", "Kuruş");
							json.put("centsTxt", cent);
							json.put("cents", "#" + centsAsInt + "#");

						}

						break;
					case 2:// DOLAR
						json.put("currency", "Amerikan Doları");
						json.put("cur", "$");
//						json.put("cent", "Cents");

						break;
					case 3:// EURO
						json.put("currency", "Avro");
						json.put("cur", "€");
//						json.put("cent", "Cents");

						break;

					}

					jsonRep.add(json);

				}

			}

			String jsonreport = mapper.writeValueAsString(jsonRep);
			return jsonreport;

//			 

		} catch (Exception e) {
			logger.error("ERROR load TAG failed from loading this id: " + id + " with error" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/cover/load/senet", method = RequestMethod.POST)
	@ResponseBody
	public String loadCoverForSenet(@RequestParam(value = "id") long id) {
		try {
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			// ArrayNode jsonRep = mapper.createArrayNode();

			ObjectNode json = mapper.createObjectNode();

			Cover cover = coverService.loadCoverById(id);
			json.put("id", cover.getId());
			json.put("guarantor", cover.getGuarantor());
			json.put("place", cover.getPlace());
			json.put("court", cover.getCourt());
			json.put("senettype", cover.getSenettype());
			ObjectNode jsons = mapper.createObjectNode();
			ArrayList<String> cur = Source.SenetType();
			jsons.put("id", cover.getSenettype());
			jsons.put("title", cur.get(cover.getSenettype()));
			json.put("senettype", jsons);
			

			String jsonreport = mapper.writeValueAsString(json);
			return jsonreport;

//			 

		} catch (Exception e) {
			logger.error("ERROR load senet failed from loading this id: " + id + " with error" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/cover/save", method = RequestMethod.POST)
	@ResponseBody
	public String saveCustomer(HttpServletResponse response, @Valid @ModelAttribute AdminCoverForm form,
			Authentication authentication) {

		try {
			User user = (User) authentication.getPrincipal();

			if (form.getId() != null) {

				Cover cover = coverService.loadCoverById(form.getId());
				cover = form.manipulatCover(cover);
				cover.setAccount(new HashSet<Account>(userService.findAccontsByIds(form.getTemsilci())));
				cover.setCustomer(new HashSet<Customer>(personService.findCustomersByIds(form.getCustomers())));
				cover.setPortfoy(new HashSet<Portfoy>(portfoyService.findPortfoysByIds(form.getPortfoys())));
				coverService.save(cover);
				logger.fatal(user.getAccount().getName() + " MODIFY CONTRACT COVER WITH ID:" + form.getId());

				return null;

			} else {

				Cover cover = form.createCover();
				cover.setAccount(new HashSet<Account>(userService.findAccontsByIds(form.getTemsilci())));
				cover.setCustomer(new HashSet<Customer>(personService.findCustomersByIds(form.getCustomers())));
				cover.setPortfoy(new HashSet<Portfoy>(portfoyService.findPortfoysByIds(form.getPortfoys())));
				coverService.save(cover);
				logger.fatal(user.getAccount().getName() + " CREATE NEW 'CONTRACT COVER' FOR CUSTOMER: "
						+ cover.getCustomer().iterator().next().getFname());
				return cover.getId().toString();
			}

		} catch (Exception e) {
			logger.error("ERROR SAVE SCONTRACT COVER WITH ERROR: " + e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());

			return null;
		}

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/cover/manipulate", method = RequestMethod.POST)
	@ResponseBody
	public String manipulateCover(HttpServletResponse response, @Valid @ModelAttribute AdminCoverForm form,
			Authentication authentication) {

		try {
			User user = (User) authentication.getPrincipal();
			if (form.getId() != null) {

				Cover cover = coverService.loadCoverById(form.getId());

				cover.setGuarantor(form.getGuarantor());
				cover.setPlace(form.getPlace());
				cover.setCourt(form.getCourt());
				cover.setSenetDate(Time.todayDateOnly());
				cover.setSenettype((form.getSenettype()));

				coverService.save(cover);
				logger.fatal(user.getAccount().getName() + " MANIPULATE INFORMATION COVER WITH  COVER ID:"
						+ form.getId());

				return null;

			} else {

				response.setStatus(HttpStatus.BAD_REQUEST.value());

				return null;
			}

		} catch (Exception e) {
			logger.error(" ADD 'SENET' INFORMATION TO COVER " + " WITH ERROR: " + e.getMessage());

			response.setStatus(HttpStatus.BAD_REQUEST.value());

			return null;
		}

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/autoinscover/save", method = RequestMethod.POST)
	@ResponseBody
	public String saveAutoInstallmets(HttpServletResponse response,
			@Valid @ModelAttribute AdminInstallmentAutoForm form, Authentication authentication) {

		try {

			User user = (User) authentication.getPrincipal();

			List<Date> ins = Source.autoInstallmentDate(form.getRange(), form.getLimit(),
					Time.ConvertToDate(form.getIssue_date()));

			for (Date date : ins) {

				InsCover installment = form.createInstallmentForCover();
				installment.setIssueDate(date);
				installment.setCover(coverService.loadCoverById(form.getId()));

				coverService.save(installment);

				logger.fatal(
						user.getAccount().getName() + " CREATE AUTO INSTALLMENTS FOR COVER WITH ID" + form.getId());

			}

			return null;
		} catch (Exception e) {
			logger.error(
					"CREATE AUTO INSTALLMENTS FOR COVER WITH ID= " + form.getId() + " WITH ERROR: " + e.getMessage());

			response.setStatus(HttpStatus.BAD_REQUEST.value());

			return null;
		}
	}

	@SuppressWarnings("null")
	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/inscover/save", method = RequestMethod.POST)
	@ResponseBody
	public String saveInstallmets(HttpServletResponse response, Authentication authentication,
			@RequestParam(value = "insid") Long id, @RequestParam(value = "issueDate") Date issueDate,
			@RequestParam(value = "amount") String amount) {

		try {

			User user = (User) authentication.getPrincipal();

			if (user != null) {

				if (id != null) {
					InsCover insCover = coverService.findInsCoverById(id);

					if (insCover != null) {
						insCover.setAmount(amount);
						insCover.setIssueDate(issueDate);
						coverService.save(insCover);
						logger.fatal(user.getAccount().getName() + " MODIFY INSCOVER FOR COVER WITH ID" + id);
					}

				} else {
					logger.error("InsCover Not Found By Id:" + id.toString());
					response.setStatus(HttpStatus.BAD_REQUEST.value());
				}

			}

			return null;
		} catch (Exception e) {
			logger.error("ERROR MODIFY INSCOVER FOR COVER WITH ID= " + id + " WITH ERROR: " + e.getMessage());

			response.setStatus(HttpStatus.BAD_REQUEST.value());

			return null;
		}
	}

	@SuppressWarnings("null")
	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/inscover/load", method = RequestMethod.POST)
	@ResponseBody
	public String LoadInsCover(HttpServletResponse response, Authentication authentication,
			@RequestParam(value = "id") Long id) {

		try {

			User user = (User) authentication.getPrincipal();
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);

			if (user != null) {

				if (id != null) {
					InsCover insCover = coverService.findInsCoverById(id);

					if (insCover != null) {
						ObjectNode json = mapper.createObjectNode();

						json.put("insid", insCover.getId());
						json.put("amount", insCover.getAmount());
						json.put("issueDate", Time.ToSplitedDate(insCover.getIssueDate()));

						String jsonreport = mapper.writeValueAsString(json);
						return jsonreport;
					}

				} else {
					logger.error("InsCover Not Found By Id:" + id.toString());
					response.setStatus(HttpStatus.BAD_REQUEST.value());
				}

			}

			return null;
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());

			return null;
		}
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/cover/remove", method = RequestMethod.POST)
	@ResponseBody
	public String deleteCover(HttpServletResponse response, @RequestParam(value = "ids") List<Long> ids,
			Authentication authentication) {
		try {
			User user = (User) authentication.getPrincipal();
			List<Cover> covers = coverService.findCoversByIds(ids);
			for (Cover cover : covers) {

				cover.setInsCover(new HashSet<InsCover>());
				cover.setAccount(new HashSet<Account>());
				cover.setPortfoy(new HashSet<Portfoy>());
				cover.setCustomer(new HashSet<Customer>());

				Set<InsCover> inss = coverService.findInsCoversByCover(cover.getId());

				if (inss != null) {
					if (inss.size() > 0) {

						coverService.removeinsCover(inss);

					}
				}
				coverService.save(cover);
			}
			coverService.removeCovers(covers);
			logger.fatal(user.getAccount().getName() + " REMOVE COVERS WITH IDS" + ids.toString().replace("[", "").replace("]", ""));

		} catch (Exception e) {
			logger.error("ERROR deleting COVERS with ids= " + ids + " with error: " + e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());
		}
		return null;
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/inscover/remove", method = RequestMethod.POST)
	@ResponseBody
	public String deleteInsCover(HttpServletResponse response, @RequestParam(value = "id") Long id,Authentication authentication) {
		try {
			
			User user = (User) authentication.getPrincipal();

			InsCover insCover = coverService.findInsCoverById(id);
			Set<InsCover> ins = new HashSet<InsCover>();
			ins.add(insCover);

			coverService.removeinsCover(ins);
			logger.fatal(user.getAccount().getName() + " REMOVE INSCOVERS WITH ID" + id);

		} catch (Exception e) {
			logger.error("ERROR deleting insCover with ids= " + id + " with error: " + e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());
		}
		return null;
	}

}
