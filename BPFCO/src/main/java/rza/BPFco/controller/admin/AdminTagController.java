package rza.BPFco.controller.admin;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import rza.BPFco.form.AdminTagForm;
import rza.BPFco.form.filter.TagFilterForm;
import rza.BPFco.model.Tag;
import rza.BPFco.service.TagService;
import rza.BPFco.service.TagService.TagType;
import rza.BPFco.service.UserService.User;

@Controller
public class AdminTagController {

	@Autowired
	private TagService tagService;

	protected static final Logger logger = LogManager.getLogger();

	@RequestMapping(value = { "/adminpanel/tag_manage" }, method = RequestMethod.GET)
	public String index() {
		return "html/admin/tags_manage";
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "adminpanel/json/tags/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadAuthors(HttpServletResponse response, @ModelAttribute TagFilterForm form) {
		try {
			Page<Tag> pageAuthors = tagService.loadAllTags(form);
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			ArrayNode jsonRep = mapper.createArrayNode();
			for (Tag tags : pageAuthors.getContent()) {
				ObjectNode json = mapper.createObjectNode();
				json.put("id", tags.getId());
				json.put("title", tags.getTitle());

				json.put("tip", TagType.values()[tags.getType()].toString());
				
				 
				
				
				jsonRep.add(json);
			}

			ObjectNode page = mapper.createObjectNode();
			page.put("total", pageAuthors.getTotalPages());
			page.put("number", pageAuthors.getNumber() + 1);
			jsonRep.add(page);

			String jsonreport = mapper.writeValueAsString(jsonRep);
			return jsonreport;

		} catch (Exception e) {
			logger.error("Error load accounts in adminpanel: " + " with error" + e.getMessage());
			e.printStackTrace();
		}

		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/tag/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadAuthorById(@RequestParam(value = "id") long id) {
		try {
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			// ArrayNode jsonRep = mapper.createArrayNode();
			ObjectNode json = mapper.createObjectNode();
			Tag tag = tagService.loadTagById(id);

			json.put("id", tag.getId());
			json.put("title", tag.getTitle());
			json.put("type", tag.getType());

			

			String jsonreport = mapper.writeValueAsString(json);
			return jsonreport;

		} catch (Exception e) {
			logger.error("ERROR load TAG failed from loading this id: " + id + " with error" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/tag/save", method = RequestMethod.POST)
	@ResponseBody
	public String saveTag(HttpServletResponse response, @Valid @ModelAttribute AdminTagForm form,
			Authentication authentication) {

		try {

			User user = (User) authentication.getPrincipal();

			if (user.isAdmin()) {

				if (form.getId() != null) {

					Tag tag = tagService.loadTagById(form.getId());
					tag = form.manipulatTag(tag);
					tagService.save(tag);
					logger.fatal(user.getAccount().getName() + " MODIFY TAG BY NAME:" + tag.getTitle());

				} else {

					Tag tags = tagService.findByTagTitle(form.getTitle());
					if (tags == null) {
						Tag tag = form.createTags();
						tagService.save(tag);
						logger.fatal(user.getAccount().getName() + " CREATE TAG BY NAME:" + tag.getTitle());
					} else {
						response.setStatus(HttpStatus.BAD_REQUEST.value());
						return "1";
					}

				}

			}

			return null;
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());

			return null;
		}

	}

}
