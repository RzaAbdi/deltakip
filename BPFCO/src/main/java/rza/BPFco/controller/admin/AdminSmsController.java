package rza.BPFco.controller.admin;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import rza.BPFco.form.AdminMessageForm;
import rza.BPFco.form.filter.MessageFilterForm;
import rza.BPFco.model.portfoys.Installment;
import rza.BPFco.model.portfoys.Mes;
import rza.BPFco.model.portfoys.Reply;
import rza.BPFco.service.ContractService;
import rza.BPFco.service.SmsService;
import rza.BPFco.service.SosyalMedyaService;
import rza.BPFco.service.UserService.User;
import rza.BPFco.utilities.Sorting;
import rza.BPFco.utilities.Source;
import rza.BPFco.utilities.Time;

@Controller
public class AdminSmsController {

	
	
	@Autowired
	private SmsService smsService;
	 

	@Autowired
	private SosyalMedyaService sosyalMedyaService;

	@Autowired
	private ContractService contractService;

	protected static final Logger logger = LogManager.getLogger();

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/message/save", method = RequestMethod.POST)
	@ResponseBody
	public String saveMessage(HttpServletResponse response, @Valid @ModelAttribute AdminMessageForm form,
			Authentication authentication) {

		try {

			User user = (User) authentication.getPrincipal();

			if (user.isAdmin()) {

				if (form.getId() != 0) {
//cc
				} else {

					Mes message = form.createMessage();

					smsService.saveMessage(message);
					Installment ins = contractService.loadInstallmentByIdJoinMessages(form.getInsid());
					Set<Mes> messages = ins.getMes();
					messages.add(message);
					ins.setMes(messages);

					switch (form.getType()) {
					case 1:// reminding

						ins.setReminding(true);

						break;
					case 2:// thanks

						ins.setThanks(true);
						ins.setWarning(true);
						ins.setReminding(true);
						break;
					case 3:// warning
						ins.setReminding(true);
						ins.setWarning(true);
						break;

					default:
						break;
					}
					contractService.save(ins);
				}

			}

			return null;
		} catch (Exception e) {
			logger.error("ERROR SAVE MESSAGE " + e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());

			return null;
		}

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/messages/loadbyId", method = RequestMethod.POST)
	@ResponseBody
	public String loadMessageByInsId(HttpServletResponse response, @ModelAttribute MessageFilterForm form,Authentication authentication) {
		try {
			User user = (User) authentication.getPrincipal();
			
			if(user.isAdmin() || user.isWatcher()) {
				
		 

			Page<Mes> pageAuthors = smsService.loadMessages(form);
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			ArrayNode jsonRep = mapper.createArrayNode();
			for (Mes mes : pageAuthors) {
				ObjectNode json = mapper.createObjectNode();
				json.put("mid", mes.getId());
				json.put("text", mes.getText());

				json.put("sendDate", Time.ToSplitedDateCalneder(mes.getSendDate()));
				json.put("type", Source.MessageType2().get(mes.getType() - 1));

//				 Installment installment = contractService.findInstallmentByMesId(mes.getId());
//					if (installment != null) {
//
//					 
//							json.put("insid", installment.getId());
//					}

				jsonRep.add(json);
			}
			ObjectNode page = mapper.createObjectNode();
			page.put("total", pageAuthors.getTotalPages());
			page.put("number", pageAuthors.getNumber() + 1);
			jsonRep.add(page);

			String jsonreport = mapper.writeValueAsString(jsonRep);
			return jsonreport;
		}
			

		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(HttpStatus.BAD_REQUEST.value());
		}

		return null;
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/message/getinsid", method = RequestMethod.POST)
	@ResponseBody
	public String GetInsIdForMes(HttpServletResponse response, @RequestParam(value = "mid") Long mid) {
		String id = null;
		try {

			Installment installment = contractService.findInstallmentByMesId(mid);
			if (installment != null) {

				id = String.valueOf(installment.getId());
			}

		} catch (Exception e) {
			logger.error("ERROR find installment for message with id= " + mid + " with error: " + e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());
		}

		return id;
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/message/remove", method = RequestMethod.POST)
	@ResponseBody
	public String deleteVariantvalue(HttpServletResponse response, @RequestParam(value = "ids") List<Long> ids,
			Authentication authentication) {
		try {
			User user = (User) authentication.getPrincipal();
			List<Installment> ins = contractService.loadInstallmentsByMesIds(ids);
			Set<Mes> messages = smsService.loadMessagesByIds(ids);

			for (Installment installment : ins) {

				installment.getMes().removeAll(messages);

				Set<Mes> ff = installment.getMes();
				for (Mes mes : ff) {
					for (Mes mes2 : messages) {

						if (mes.getId() == mes2.getId()) {
							messages.remove(mes2);
							switch (mes.getType()) {
							case 0:
								installment.setReminding(false);

								break;
							case 1:
								installment.setThanks(false);

								break;
							case 2:
								installment.setWarning(false);

								break;

							default:
								break;
							}
						}

					}

					if (mes.getReply() != null) {
						for (Reply rep : mes.getReply()) {
							sosyalMedyaService.removeReply(rep);
						}
					}

				}

				installment.setMes(new HashSet<Mes>(messages));
				contractService.save(installment);
			}

			for (Mes mes : messages) {
				Set<Reply> replies = mes.getReply();

				sosyalMedyaService.removeReplies(replies);
			}

			smsService.removeMessage(messages);
			logger.fatal(user.getAccount().getName() + " REMOVE MESSAGES WITH IDS: " + ids);

		} catch (Exception e) {
			logger.error("ERROR DELETING MESSAGES WITH IDS= " + ids + " WITH ERROR: " + e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());
		}
		return null;
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/message/printable/load", method = RequestMethod.POST)
	@ResponseBody
	public String LoadMessage(HttpServletResponse response, @RequestParam(value = "id") Long id,Authentication authentication) {
		try {
			User user = (User) authentication.getPrincipal();
			
			if(user.isAdmin()||user.isWatcher()) {
				ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
						false);
				ObjectNode json = mapper.createObjectNode();
				Mes mes = smsService.loadMessagesById(id);

				json.put("mid", mes.getId());
				json.put("text", mes.getText());
				json.put("sendDate", Time.ToTurkishDateOnly(mes.getSendDate()));

				Installment installment = contractService.findInstallmentByMesId(mes.getId());
				if (installment != null) {

					json.put("insid", installment.getId());
					json.put("institle", installment.getId());
				}

				Set<Reply> replies = mes.getReply();
				List<Reply> reps = new ArrayList<Reply>();
				reps.addAll(replies);

				Sorting sort = new Sorting();
				reps = sort.SortReply(reps);

				if (reps != null) {
					ArrayNode replyCats = mapper.createArrayNode();

					for (Reply reply : reps) {

						ObjectNode node = mapper.createObjectNode();
						node.put("id", reply.getId());
						node.put("note", reply.getNote());
						if (reply.isHasDoc()) {
							node.put("doc", "");

						} else {

							node.put("doc", "hidden");
						}

						node.put("date", Time.ToTurkishDateMonthDateOnly(reply.getCreateDate()));
						replyCats.add(node);

					}
					json.put("replies", replyCats);
				}
				String jsonreport = mapper.writeValueAsString(json);
				return jsonreport;
			}
			

		} catch (Exception e) {
			logger.error("ERROR load message with id= " + id + " with error: " + e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());
		}
		return null;
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/message/reply/save", method = RequestMethod.POST)
	@ResponseBody
	public String saveReply(HttpServletResponse response, Authentication authentication,@RequestParam(value = "id") Long id,
			@RequestParam(value = "mid") Long mid, @RequestParam(value = "note") String note,
			@RequestParam(value = "createDate") Date createDate) {
		try {
			Reply reply = null;
			
			User user = (User) authentication.getPrincipal();
			
			
			if (id != null) {

				reply = sosyalMedyaService.loadReplyById(id);
				reply.setCreateDate(createDate);
				reply.setNote(note);

			} else {

				Mes mes = smsService.loadMessagesById(mid);
				reply = new Reply();

				reply.setCreateDate(createDate);
				reply.setNote(note);
				reply.setMes(mes);
			}
			sosyalMedyaService.save(reply);
			
			if (id != null) {
				logger.fatal(user.getAccount().getName() + " MODIFY REPLY WITH ID: " + id  );
				
			}else {
				logger.fatal(user.getAccount().getName() + " SAVE REPLY FOR MESSAGE WITH ID " + mid);
				
			}

		} catch (Exception e) {
			logger.error("ERROR SAVE MESSAGE WITH ID= " + id + " WITH ERROR: " + e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());
		}
		return null;
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/reply/remove", method = RequestMethod.POST)
	@ResponseBody
	public String deleteReply(HttpServletResponse response, @RequestParam(value = "id") Long id,Authentication authentication) {
		try {
			//
			User user = (User) authentication.getPrincipal();
			Reply reply = sosyalMedyaService.loadReplyById(id);
			if (reply != null) {

				smsService.removeReply(reply);
				logger.fatal(user.getAccount().getName() + " REMOVE REPLY WITH ID " + id);
			}

		} catch (Exception e) {
			logger.error("ERROR remove reply with id= " + id + " with error: " + e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());
		}
		return null;
	}

}
