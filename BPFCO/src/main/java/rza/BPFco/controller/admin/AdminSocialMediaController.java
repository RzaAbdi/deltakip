package rza.BPFco.controller.admin;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.mysema.query.types.expr.BooleanExpression;

import rza.BPFco.form.AdminReplyForm;
import rza.BPFco.form.AdminSosyalMedyaForm;
import rza.BPFco.form.filter.SosyalMedyaFilterForm;
import rza.BPFco.model.Account;
import rza.BPFco.model.portfoys.Chase;
import rza.BPFco.model.portfoys.Reply;
import rza.BPFco.model.portfoys.SosyalMedya;
import rza.BPFco.repository.SosyalMedyaRepository;
import rza.BPFco.service.SosyalMedyaService;
import rza.BPFco.service.SosyalMedyaService.Priority;
import rza.BPFco.service.UserService;
import rza.BPFco.service.UserService.User;
import rza.BPFco.utilities.CreateExcel;
import rza.BPFco.utilities.ReadExcel;
import rza.BPFco.utilities.Sorting;
import rza.BPFco.utilities.Time;

@Controller
public class AdminSocialMediaController {

	@Autowired
	SosyalMedyaService sosyalMedyaService;

	@Autowired
	UserService userService;
	
	@Autowired
	SosyalMedyaRepository sosyalMedyaRepository;
	
	@Autowired
	public HttpSession session;

	protected static final Logger logger = LogManager.getLogger();

	@RequestMapping(value = { "/sosyalmedya_fa" }, method = RequestMethod.GET)
	public String smfa(Model model,@RequestParam(value = "type") long type) {
		  
		
		model.addAttribute("type", type);
		 
		model.addAttribute("language", "0");

		return "html/admin/sosyalmedya";
	}

	@RequestMapping(value = { "/sosyalmedya_ar" }, method = RequestMethod.GET)
	public String smar(Model model,@RequestParam(value = "type") long type) {
		model.addAttribute("type", type);
		model.addAttribute("language", "1");

		return "html/admin/sosyalmedya";
	}
	@RequestMapping(value = { "/crm" }, method = RequestMethod.GET)
	public String crm(Model model,@RequestParam(value = "type") long type) {
		model.addAttribute("type", type);
		model.addAttribute("language", "2");
		
		return "html/admin/sosyalmedya";
	}
	@RequestMapping(value = { "/ajanda" }, method = RequestMethod.GET)
	public String ajanda(Model model) {
		model.addAttribute("yester", Time.ToSplitedDate(Time.addDaysAfterDates(-1, Time.todayDateOnly())));
		model.addAttribute("tommorow", Time.ToSplitedDate(Time.addDaysAfterDates(1, Time.todayDateOnly())));
		model.addAttribute("today", Time.ToSplitedDate(Time.todayDateOnly()));
		model.addAttribute("date", " [ "+Time.ToTurkishDateOnly(Time.today())+ " ] ");
		return "html/admin/ajanda";
	}
	
	@RequestMapping(value="/event-count", method=RequestMethod.GET)
	public String getEventCount(ModelMap map,Authentication authentication) {
		
		if(authentication!=null) {
			User user = (User) authentication.getPrincipal();
			
			 
				
				int followFa = sosyalMedyaRepository.ConuntPinned(user.getUserId(), 0);   		
				int followAr = sosyalMedyaRepository.ConuntPinned(user.getUserId(), 1); 
				if(followFa>0) {
					map.addAttribute("followFa", followFa);
					
				}
				if(followAr>0) {
					
					map.addAttribute("followAr", followAr);
				} 
				return "fragments/header :: #Replace";
			 
			
		}
		return null;
	    
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/sosyalmedya/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadAuthors(HttpServletResponse response, @ModelAttribute SosyalMedyaFilterForm form,
			Authentication authentication) {
		try {

			if (form.getSort().toString().contains("id")) {
				form.setSort("createDate-1");

			}

			if (form.getSort() == null) {

				form.setSort("createDate-1");

			}
			User user = (User) authentication.getPrincipal();

			if (user != null) {

				if (user.isAdmin()) {
					//
					form.setAdmin(true);
					if(form.isFollow()) {
						form.setAccount(user.getUserId());
						
					}

				} else if(user.getAccount().isCallFa() && form.getLanguage()==0){
					
					form.setAdmin(true);
					if(form.isFollow()) {
						form.setAccount(user.getUserId());
						
					}

				}else if(user.getAccount().isCallAr() && form.getLanguage()==1) {
					
					//
					form.setAdmin(true);
					if(form.isFollow()) {
						form.setAccount(user.getUserId());
						
					}else if(user.getAccount().isCallAr() || user.getAccount().isCallFa() && form.getLanguage()==2) {
						
						//
//						form.setAdmin(true);
						if(form.isFollow()) {
							form.setAccount(user.getUserId());
							
							
						}
					}
				}else {
					
					form.setAdmin(false);
					form.setAccount(user.getUserId());
				}

			} else {
				return null;
			}

			List<Object[]> repeats = sosyalMedyaService.sosyalMedyaRepeats();

			form.setLanguage(form.getLanguage());

			Page<SosyalMedya> pageAuthors = sosyalMedyaService.loadAllSosyalMedya(form);
			String sort = form.getSort().toString();
			BooleanExpression predicates = form.getPredicate();
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			ArrayNode jsonRep = mapper.createArrayNode();
			for (SosyalMedya sosyalMedya : pageAuthors.getContent()) {
				ObjectNode json = mapper.createObjectNode();
				json.put("id", sosyalMedya.getId());
				json.put("mail", sosyalMedya.getEmail());
				json.put("name", sosyalMedya.getName());
				json.put("nation", sosyalMedya.getNation());
				json.put("gsm", sosyalMedya.getGsm());
				json.put("color", "#000000");
				if (sosyalMedya.getChase() != null) {
					String title = sosyalMedya.getChase().getTitle();
					json.put("chase", title.substring(0, title.indexOf("(")));
					json.put("color", sosyalMedya.getChase().getColor());
					json.put("chaseId", sosyalMedya.getChase().getId());
				}

				json.put("createDate", Time.ToTurkishDateMonthDateOnly(sosyalMedya.getCreateDate()));
				if (sosyalMedya.getLastDate() != null) {
					json.put("lastDate", Time.ToTurkishDateMonthDateOnly(sosyalMedya.getLastDate()));

				}
				json.put("first_info", sosyalMedya.getFirstInfo());

				if (sosyalMedya.getLastUpdate() != null) {

					json.put("last_info", sosyalMedya.getLastUpdate());
				}

				json.put("type", sosyalMedya.getType());
				json.put("priority", sosyalMedya.getPriority());

				ArrayNode userCats = mapper.createArrayNode();
				for (Account account : sosyalMedya.getAccount()) {
					ObjectNode node = mapper.createObjectNode();
					node.put("title", account.getName());
					node.put("id", account.getId());
					userCats.add(node);
				}
				json.put("accounts", userCats);

				jsonRep.add(json);
			}

			ObjectNode page = mapper.createObjectNode();
			page.put("total", pageAuthors.getTotalPages());
			page.put("number", pageAuthors.getNumber() + 1);

			ArrayNode repeatlist = mapper.createArrayNode();
			for (Object[] object : repeats) {
				ObjectNode repeat = mapper.createObjectNode();
				repeat.put("gsm", object[0].toString());
				repeat.put("repeat", object[1].toString());
				repeatlist.add(repeat);
			}
			page.put("repeats", repeatlist);

			if (predicates != null) {

				page.put("predicate", predicates.toString());
			}
			if (sort != null) {

				page.put("sort", sort);
			}
			jsonRep.add(page);

			String jsonreport = mapper.writeValueAsString(jsonRep);

			 

			return jsonreport;

		} catch (Exception e) {
			logger.error("Error load accounts in adminpanel: " + " with error" + e.getMessage());
			e.printStackTrace();
		}

		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/sosyalmedya/excel", method = RequestMethod.POST)
	@ResponseBody
	public String exportInstallments(HttpServletResponse response, @ModelAttribute SosyalMedyaFilterForm form,
			Authentication authentication) {
		try {

			User user = (User) authentication.getPrincipal();

			if (user != null) {

				if (user.isAdmin()) {
					//
					form.setAdmin(true);

				} else {
					//
					form.setAdmin(false);
					form.setAccount(user.getUserId());

				}

			}

			if (form.getSorting().contains("creatDate")) {

				form.setSorting(form.getSorting().replace("creatDate: ASC", "creatDate-0"));
				form.setSorting(form.getSorting().replace("creatDate: DESC", "creatDate-1"));

			}

			form.setSort(form.getSorting());
			form.setPsize(1000);
			Page<SosyalMedya> pageAuthors = sosyalMedyaService.loadAllSosyalMedya(form);

			if (pageAuthors.getContent().size() > 0) {

				ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
						false);
				ObjectNode json = mapper.createObjectNode();

				CreateExcel createExcel = new CreateExcel();
				String path = createExcel.CrateExcelFromSosyalMedya(pageAuthors);

				json.put("path", path);
				String jsonreport = mapper.writeValueAsString(json);
				logger.fatal(user.getAccount().getName() + " EXPORT SOSYLA MEDYA LIST ");
				return jsonreport;
			} else {
				response.setStatus(HttpStatus.BAD_REQUEST.value());
			}

		} catch (Exception e) {
			logger.error("Error load accounts in adminpanel: " + " with error" + e.getMessage());
			e.printStackTrace();
		}

		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/soysalmedya/printable/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadPrintable(HttpServletResponse response, @RequestParam(value = "smid") long id
			 ) {
		try {

			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);

			SosyalMedya sosyalMedya = sosyalMedyaService.loadSosyalMedyaById(id);
			ObjectNode json = mapper.createObjectNode();

			json.put("id", sosyalMedya.getId());
			json.put("mail", sosyalMedya.getEmail());
			json.put("name", sosyalMedya.getName());
			json.put("nation", sosyalMedya.getNation());
			json.put("gsm", sosyalMedya.getGsm());
			json.put("createDate", Time.ToTurkishDateMonthDateOnly(sosyalMedya.getCreateDate()));
			if (sosyalMedya.getLastDate() != null) {
				json.put("lastDate", Time.ToTurkishDateMonthDateOnly(sosyalMedya.getLastDate()));

			}
			json.put("first_info", sosyalMedya.getFirstInfo());

			if (sosyalMedya.getLastUpdate() != null) {

				json.put("last_info", sosyalMedya.getLastUpdate());
			}

			json.put("type", sosyalMedya.getType());
			json.put("priority", sosyalMedya.getPriority());

			ArrayNode userCats = mapper.createArrayNode();
			for (Account account : sosyalMedya.getAccount()) {
				ObjectNode acc = mapper.createObjectNode();
				acc.put("title", account.getName());
				acc.put("id", account.getId());
				userCats.add(acc);
			}
			json.put("accounts", userCats);

			if (sosyalMedya.getReply() != null) {
				List<Reply> mainList = new ArrayList<Reply>();
				mainList.addAll(sosyalMedya.getReply());
				Sorting sorting = new Sorting();
				List<Reply> replies = sorting.SortReply(mainList);

				ArrayNode replyCats = mapper.createArrayNode();
				for (Reply reply : replies) {

					ObjectNode node = mapper.createObjectNode();
					node.put("id", reply.getId());
					node.put("note", reply.getNote());
					if (reply.getDsc() != null) {

						node.put("dsc", reply.getDsc());
					}
					node.put("date", Time.ToTurkishDateMonthDateOnly(reply.getCreateDate()));

					ArrayNode userReplyCats = mapper.createArrayNode();
					for (Account account : reply.getAccount()) {
						ObjectNode acc = mapper.createObjectNode();
						acc.put("title", account.getName());
						acc.put("id", account.getId());
						userReplyCats.add(acc);
					}
					node.put("accounts", userReplyCats);

					replyCats.add(node);

				}
				json.put("replies", replyCats);
			}

			String jsonreport = mapper.writeValueAsString(json);
			return jsonreport;

		} catch (Exception e) {
			logger.error("Error load accounts in adminpanel: " + " with error" + e.getMessage());
			e.printStackTrace();
		}

		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/sosyalmedya/loadbyId", method = RequestMethod.POST)
	@ResponseBody
	public String loadAgentById(@RequestParam(value = "id") long id) {
		try {
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			ObjectNode json = mapper.createObjectNode();
			SosyalMedya sosyalMedya = sosyalMedyaService.loadSosyalMedyaById(id);

			json.put("id", sosyalMedya.getId());
			json.put("email", sosyalMedya.getEmail());
			json.put("name", sosyalMedya.getName());
			json.put("nation", sosyalMedya.getNation());
			json.put("gsm", sosyalMedya.getGsm());
			json.put("firstPlatform", sosyalMedya.getFirstPlatform());
			json.put("lan", sosyalMedya.getLanguage());
			json.put("recentplatform", sosyalMedya.getCurrentPlatform());
			json.put("createDate", Time.ToSplitedDate(sosyalMedya.getCreateDate()));
			if (sosyalMedya.getLastDate() != null) {
				json.put("lastDate", Time.ToSplitedDate(sosyalMedya.getLastDate()));

			}
			json.put("firstInfo", sosyalMedya.getFirstInfo());
			json.put("follow", sosyalMedya.isFollow());

			if (sosyalMedya.getLastUpdate() != null) {

				json.put("last_info", sosyalMedya.getLastUpdate());
			}

			json.put("type", sosyalMedya.getType());
			json.put("dsc", sosyalMedya.getDsc());

			//
			ObjectNode node = mapper.createObjectNode();
			node.put("title", Priority.values()[sosyalMedya.getPriority() - 1].toString());
			node.put("id", sosyalMedya.getPriority());
			json.put("priority", node);
			//
			ObjectNode chase = mapper.createObjectNode();
			if (sosyalMedya.getChase() != null) {

				chase.put("title", sosyalMedya.getChase().getTitle());
				chase.put("id", sosyalMedya.getChase().getId());
				json.put("chaseId", chase);
			}

			ArrayNode jsonRep = mapper.createArrayNode();
			Set<Account> accounts = sosyalMedya.getAccount();
			for (Account account : accounts) {
				//
				ObjectNode node1 = mapper.createObjectNode();
				node1.put("title", account.getName());
				node1.put("id", account.getId());
				jsonRep.add(node1);

			}
			json.put("account", jsonRep);

			String jsonreport = mapper.writeValueAsString(json);
			return jsonreport;

		} catch (Exception e) {
			logger.error("ERROR load SM failed from loading this id: " + id + " with error" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/reply/loadbyId", method = RequestMethod.POST)
	@ResponseBody
	public String loadReplyById(@RequestParam(value = "id") long id) {
		try {
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			ObjectNode json = mapper.createObjectNode();
			Reply reply = sosyalMedyaService.loadReplyById(id);

			json.put("id", reply.getId());

			if (reply.getSosyalmedya() != null)
				json.put("smid", reply.getSosyalmedya().getId());

			if (reply.getMes() != null)
				json.put("mid", reply.getMes().getId());

			json.put("note", reply.getNote());
			json.put("createDate", Time.ToSplitedDate(reply.getCreateDate()));

			ArrayNode jsonRep = mapper.createArrayNode();
			Set<Account> accounts = reply.getAccount();
			for (Account account : accounts) {
				//
				ObjectNode node1 = mapper.createObjectNode();
				node1.put("title", account.getName());
				node1.put("id", account.getId());
				jsonRep.add(node1);

			}
			json.put("accounts", jsonRep);

			String jsonreport = mapper.writeValueAsString(json);
			return jsonreport;

		} catch (Exception e) {
			logger.error("ERROR load Reply failed from loading this id: " + id + " with error" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/sosyalmedya/save", method = RequestMethod.POST)
	@ResponseBody
	public String saveTag(HttpServletResponse response, @Valid @ModelAttribute AdminSosyalMedyaForm form,
			Authentication authentication) {

		try {

			User user = (User) authentication.getPrincipal();

			if (user.isAdmin()||user.getAccount().isCallFa()||user.getAccount().isCallAr()) {

				if (form.getId() != null) {

					SosyalMedya sosyalMedya = sosyalMedyaService.loadSosyalMedyaById(form.getId());
					sosyalMedya = form.manipulatSM(sosyalMedya);

					if (form.getChaseId() != null) {

						sosyalMedya.setChase(sosyalMedyaService.findChaseById(form.getChaseId()));
					}

					if (form.getAccount() != null) {

						sosyalMedya.setAccount(new HashSet<Account>(userService.findAccontsByIds(form.getAccount())));
					}
					
				 
						int followFa = sosyalMedyaRepository.ConuntPinned(user.getUserId(), 0);   		
						int followAr = sosyalMedyaRepository.ConuntPinned(user.getUserId(), 1); 
						if(followFa>0) {
							session.setAttribute("followFa", "["+followFa+"]");	
							
						}
						if(followAr>0) {
							
							session.setAttribute("followAr", "["+followAr+"]");	
						} 
					 
					
					sosyalMedyaService.save(sosyalMedya);
					logger.fatal(user.getAccount().getName() + " MODIFY SOCIAL MEDIA BY CUSTOMER ID:  "
							+ sosyalMedya.getId());

				} else {

					SosyalMedya sosyalMedya = form.createSM();
					sosyalMedya.setAccount(new HashSet<Account>(userService.findAccontsByIds(form.getAccount())));

					sosyalMedyaService.save(sosyalMedya);
					logger.fatal(user.getAccount().getName() + " CREATE SOCIAL MEDIA BY CUSTOMER NAME:  "
							+ sosyalMedya.getName());
				}

			} else {
				if (form.getId() != null) {

					SosyalMedya sosyalMedya = sosyalMedyaService.loadSosyalMedyaById(form.getId());
					sosyalMedya = form.manipulatSM(sosyalMedya);
					if (form.getChaseId() != null) {

						sosyalMedya.setChase(sosyalMedyaService.findChaseById(form.getChaseId()));
					}
					List<Long> accs = form.getAccount();
					if (!accs.contains(user.getAccount().getId())) {
						accs.add(user.getAccount().getId());
					}
					sosyalMedya.setAccount(new HashSet<Account>(userService.findAccontsByIds(accs)));
					sosyalMedyaService.save(sosyalMedya);
					logger.fatal(user.getAccount().getName() + " MODIFY SOCIAL MEDIA BY CUSTOMER ID:  "
							+ sosyalMedya.getId());

				}
			}

			return null;
		} catch (Exception e) {
			logger.error("SAVE SOCIAL MEDIA HAS ERROR : " + e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());

			return null;
		}

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/chaselist/save", method = RequestMethod.POST)
	@ResponseBody
	public String saveChaseList(HttpServletResponse response, @Valid @ModelAttribute AdminSosyalMedyaForm form,
			Authentication authentication) {

		try {

			User user = (User) authentication.getPrincipal();

			if (form.getAccount() != null) {

				for (Long id : form.getAccount()) {
					Chase chase;
					if (form.getChaseId() != null) {
						chase = sosyalMedyaService.findChaseById(form.getChaseId());

					} else {
						chase = new Chase();

					}

					Account account = userService.loadAccountByIdFetchRole(id);
					chase.setAccount(account);
					chase.setLanguage(form.getLanguage());
					chase.setColor(form.getColor());
					String lan = "";
					if (form.getLanguage() == 0) {
						lan = "FA";
					} else if (form.getLanguage() == 1) {
						lan = "AR";
					}

					chase.setTitle(form.getTitle() + " (" + account.getName() + "-" + lan + " )");

					sosyalMedyaService.saveChase(chase);
					logger.fatal(user.getAccount().getName() + " SAVE CHASE LIST WITH NAME: " + chase.getTitle());
				}

			} else {
				Account account = user.getAccount();
				Chase chase;
				if (form.getChaseId() != null) {
					chase = sosyalMedyaService.findChaseById(form.getChaseId());

				} else {
					chase = new Chase();

				}

				chase.setAccount(account);
				chase.setColor(form.getColor());
				String lan = "";
				if (form.getLanguage() == 0) {
					lan = "FA";
				} else if (form.getLanguage() == 1) {
					lan = "AR";
				}

				chase.setTitle(form.getTitle() + " (" + account.getName() + "-" + lan + " )");
				chase.setLanguage(form.getLanguage());
				sosyalMedyaService.saveChase(chase);
				logger.fatal(user.getAccount().getName() + " SAVE CHASE LIST WITH NAME: " + chase.getTitle());
			}

			return null;
		} catch (Exception e) {
			logger.error("SAVE CHASE LIST HAS ERROR : " + e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());

			return null;
		}

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/chase/add", method = RequestMethod.POST)
	@ResponseBody
	public String addChaseList(HttpServletResponse response, @RequestParam(value = "items") List<Long> ids,
			@RequestParam(value = "chases") Long chase, Authentication authentication) {

		try {

			User user = (User) authentication.getPrincipal();

			boolean err = false;
			for (Long id : ids) {
				SosyalMedya sm = sosyalMedyaService.loadSosyalMedyaById(id);

				Set<Account> acc = sm.getAccount();

				List<Long> accountIds = new ArrayList<Long>();

				for (Account account : acc) {
					accountIds.add(account.getId());
				}
				Chase ch = sosyalMedyaService.findChaseById(chase);
				if (accountIds.contains(ch.getAccount().getId())) {
					sm.setChase(ch);
					sosyalMedyaService.save(sm);
					logger.fatal(user.getAccount().getName() + " ADD SOCIAL MEDIA TO CHASE LIST WITH NAME: "
							+ ch.getTitle());
				} else {

					err = true;

				}

			}

			if (err == true) {
				response.setStatus(HttpStatus.BAD_REQUEST.value());
				return "0";

			}

			return null;
		} catch (Exception e) {
			logger.error("ADD TO CHASE LIST HAS AN ERROR : " + e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());

			return null;
		}

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/sosyalmedya/removefromlst", method = RequestMethod.POST)
	@ResponseBody
	public String RemoveFromList(HttpServletResponse response, @RequestParam(value = "items") List<Long> ids,
			Authentication authentication) {

		try {
			User user = (User) authentication.getPrincipal();

			for (Long id : ids) {
				SosyalMedya sm = sosyalMedyaService.loadSosyalMedyaById(id);

				sm.setChase(null);

				sosyalMedyaService.save(sm);

			}
			logger.fatal(user.getAccount().getName() + " REMOVE SOCIAL MEDIA FROM CHASE LIST WITH SM IDS: " + ids.toString().replace("[", "").replace("]", ""));

			return null;
		} catch (Exception e) {
			logger.error("REMOVE SOCIAL MEDIA FROM CHASE LIST HAS ERROR: " + e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());

			return null;
		}

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/reply/save", method = RequestMethod.POST)
	@ResponseBody
	public String saveR(HttpServletResponse response, @Valid @ModelAttribute AdminReplyForm form,
			Authentication authentication) {

		try {
			
			boolean follow=false;

			User user = (User) authentication.getPrincipal();

			if (form.getId() != null) {

				Reply reply = sosyalMedyaService.loadReplyById(form.getId());
				reply = form.manipulatReply(reply);
				reply.setAccount(new HashSet<Account>(userService.findAccontsByIds(form.getAccounts())));
				SosyalMedya sosyalMedya = sosyalMedyaService.loadSosyalMedyaById(form.getSmid());
				reply.setSosyalmedya(sosyalMedya);
				if(form.getCreateDate()!=null) {
					sosyalMedya.setLastDate(form.getCreateDate());
				}else {
					
					sosyalMedya.setLastDate(Time.todayDateOnly());
				}
				sosyalMedya.setLastUpdate(form.getNote());

				sosyalMedyaService.save(reply);
				sosyalMedyaService.save(sosyalMedya);
				logger.fatal(user.getAccount().getName() + " MODIFY REPLY FOR SOSYAL MEDYA BY SM-ID: "
						+ sosyalMedya.getId());

			} else {

				Reply reply = form.createReply();
				reply.setAccount(new HashSet<Account>(userService.findAccontsByIds(form.getAccounts())));
				SosyalMedya sosyalMedya = sosyalMedyaService.loadSosyalMedyaById(form.getSmid());
				reply.setSosyalmedya(sosyalMedya);
				if(form.getCreateDate()!=null) {
					sosyalMedya.setLastDate(form.getCreateDate());
				}else {
					
					sosyalMedya.setLastDate(Time.todayDateOnly());
				}
				sosyalMedya.setLastUpdate(form.getNote());
				
				if(sosyalMedya.isFollow()) {
					sosyalMedya.setFollow(false);
					follow=true;
				}

				sosyalMedyaService.save(reply);
				sosyalMedyaService.save(sosyalMedya);
				logger.fatal(user.getAccount().getName() + " CREATE REPLY FOR SOSYAL MEDYA BY SM-ID: "
						+ sosyalMedya.getId());
				
				if(follow) {
					int followFa = sosyalMedyaRepository.ConuntPinned(user.getUserId(), 0);   		
					int followAr = sosyalMedyaRepository.ConuntPinned(user.getUserId(), 1); 
					if(followFa>0) {
						session.setAttribute("followFa", "["+followFa+"]");	
						
					}
					if(followAr>0) {
						
						session.setAttribute("followAr", "["+followAr+"]");	
					} 
				}

			}

			return null;
		} catch (Exception e) {
			logger.error("SAVE REPLY FOR SOCIAL MEDIA HAS ERROR: " + e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());

			return null;
		}

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/excel/save", method = RequestMethod.POST)
	@ResponseBody
	public String saveCustomer(HttpServletResponse response,
			@RequestPart(value = "myNewFileName", required = false) MultipartFile file,
			@RequestParam(value = "language") int language, Authentication authentication) {

		try {

			if (file != null) {

				User user = (User) authentication.getPrincipal();

				if (user.isAdmin()) {

//
				 

					ReadExcel readExcel = new ReadExcel(userService, sosyalMedyaService);
					List<SosyalMedya> list = readExcel.ReadSosyalMedyaExcelFromFile(file);

					for (SosyalMedya sosyalMedya : list) {
						//
						sosyalMedya.setLanguage(language);
						sosyalMedyaService.save(sosyalMedya);

						if (sosyalMedya.getReply() != null) {

							Reply reply = new Reply();
							reply.setAccount(sosyalMedya.getReply().iterator().next().getAccount());
							reply.setCreateDate(sosyalMedya.getReply().iterator().next().getCreateDate());
							reply.setNote(sosyalMedya.getLastUpdate());
							reply.setSosyalmedya(sosyalMedya);
							sosyalMedyaService.save(reply);
						}
					}
					logger.fatal(user.getAccount().getName() + " IMPORT EXCEL FOR SOSYAL MEDYA ");

				}
			} else {
				response.setStatus(HttpStatus.BAD_REQUEST.value());

				return null;
			}

			return null;
		} catch (Exception e) {
			response.setStatus(HttpStatus.BAD_REQUEST.value());
			logger.error("IMPORT EXCEL FOR SOSYAL MEDYA HAS ERROR: " + e.getMessage());

			return null;
		}

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/excelreply/save", method = RequestMethod.POST)
	@ResponseBody
	public String saveExcelReply(HttpServletResponse response,
			@RequestPart(value = "myNewFileName", required = false) MultipartFile file,
			@RequestParam(value = "language") int language, Authentication authentication) {

		try {

			if (file != null) {

				User user = (User) authentication.getPrincipal();

				if (user.isAdmin()) {

//

					ReadExcel readExcel = new ReadExcel(userService, sosyalMedyaService);
					List<Reply> list = readExcel.ReadReplyExcelFromFile(file);

					for (Reply reply : list) {

						SosyalMedya medya = reply.getSosyalmedya();

						medya.setLastDate(reply.getCreateDate());
						medya.setLastUpdate(reply.getNote());

						sosyalMedyaService.save(reply);

						//
						medya.setLanguage(language);
						sosyalMedyaService.save(medya);

					}

					logger.fatal(user.getAccount().getName() + " IMPORT EXCEL FOR REPLY SOSYAL MEDYA ");
				}
			} else {
				response.setStatus(HttpStatus.BAD_REQUEST.value());
				return null;
			}

			return null;
		} catch (Exception e) {
			logger.error("IMPORT EXCEL FOR REPLY SOSYAL MEDYA HAS ERROR: " + e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());

			return null;
		}

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/sosyalmedya/remove", method = RequestMethod.POST)
	@ResponseBody
	public String deleteSection(HttpServletResponse response, @RequestParam(value = "ids") List<Long> ids,
			Authentication authentication) {
		try {
			User user = (User) authentication.getPrincipal();
			List<SosyalMedya> sm = new ArrayList<SosyalMedya>();
			for (Long id : ids) {

				SosyalMedya SMEDYA = new SosyalMedya(id);

				SosyalMedya medya = sosyalMedyaService.loadSosyalMedyaById(id);
				medya.setAccount(new HashSet<Account>());
				sosyalMedyaService.save(medya);

				Set<Reply> replies = medya.getReply();

				if (replies != null) {
					for (Reply reply : replies) {
						reply.setAccount(new HashSet<Account>());

						sosyalMedyaService.save(reply);

						sosyalMedyaService.removeReply(reply);

					}
				}

				sm.add(SMEDYA);

			}

			sosyalMedyaService.removeSosyalMedyas(sm);
			logger.fatal(user.getAccount().getName() + " DELETE SOCIAL MEDIA WITH IDS:" + ids.toString().replace("[", "").replace("]", ""));
		} catch (Exception e) {
			logger.error("ERROR DELETING SOSYALMEDYA WITH IDS= " + ids + " WITH ERROR: " + e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());
		}
		return null;
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/sosyalmedya/share", method = RequestMethod.POST)
	@ResponseBody
	public String shareMedya(HttpServletResponse response, @RequestParam(value = "ids") List<Long> ids,
			@RequestParam(value = "accounts") List<Long> accids, Authentication authentication) {
		try {
			User user = (User) authentication.getPrincipal();
			for (Long id : ids) {

				SosyalMedya medya = sosyalMedyaService.loadSosyalMedyaById(id);

				Set<Account> accounts = medya.getAccount();

				List<Account> addedAccounts = userService.findAccontsByIds(accids);

				accounts.addAll(addedAccounts);

				medya.setAccount(new HashSet<Account>(accounts));
				sosyalMedyaService.save(medya);

			}
			logger.fatal(user.getAccount().getName() + " SHARE SOCIAL MEDIA WITH IDS:" + ids);
		} catch (Exception e) {
			logger.error("ERROR SHARE SOCIAL MEDIA WITH IDS= " + ids + " WITH ERROR: " + e.getMessage());
			
			

			response.setStatus(HttpStatus.BAD_REQUEST.value());
		}
		return null;
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/sosyalmedya/update", method = RequestMethod.POST)
	@ResponseBody
	public String updateMedya(HttpServletResponse response, @RequestParam(value = "ids") List<Long> ids,
			@RequestParam(value = "note") String note, Authentication authentication) {
		try {
			User user = (User) authentication.getPrincipal();
			for (Long id : ids) {

				SosyalMedya medya = sosyalMedyaService.loadSosyalMedyaById(id);

				Reply reply = new Reply();

				Set<Account> accounts = new HashSet<Account>();
				accounts.add(user.getAccount());

				medya.setLastDate(Time.today());
				medya.setLastUpdate(note);
				sosyalMedyaService.save(medya);
				reply.setAccount(accounts);
				reply.setNote(note);
				reply.setSosyalmedya(medya);
				reply.setCreateDate(Time.today());
				sosyalMedyaService.save(reply);

			}
			logger.fatal(user.getAccount().getName() + " UPDATE SOCIAL MEDIAS WITH IDS:" + ids.toString().replace("[", "").replace("]", ""));
		} catch (Exception e) {
			logger.error("ERROR update sosyalmedya with ids= " + ids + " with error: " + e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());
		}
		return null;
	}

}
