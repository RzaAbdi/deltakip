package rza.BPFco.controller.admin;

import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mysema.query.types.expr.BooleanExpression;

import rza.BPFco.config.initProperties;
import rza.BPFco.form.AdminContractForm;
import rza.BPFco.form.filter.ContractFilterForm;
import rza.BPFco.form.filter.DocumentFilterForm;
import rza.BPFco.model.Account;
import rza.BPFco.model.Contract;
import rza.BPFco.model.Portfoy;
import rza.BPFco.model.portfoys.Agent;
import rza.BPFco.model.portfoys.Customer;
import rza.BPFco.model.portfoys.DocType;
import rza.BPFco.model.portfoys.Document;
import rza.BPFco.model.portfoys.Installment;
import rza.BPFco.model.portfoys.Proje;
import rza.BPFco.model.portfoys.Section;
import rza.BPFco.service.ContractService;
import rza.BPFco.service.DocumentService;
import rza.BPFco.service.PersonService;
import rza.BPFco.service.PortfoyService;
import rza.BPFco.service.UserService;
import rza.BPFco.service.UserService.User;
import rza.BPFco.utilities.CreateExcel;
import rza.BPFco.utilities.FileExist;
import rza.BPFco.utilities.Source;
import rza.BPFco.utilities.Time;
import rza.BPFco.utilities.Word;

@Controller
public class ContratController {

	@Autowired
	private ContractService contarctService;

	@Autowired
	private PersonService personService;

	@Autowired
	private PortfoyService portfoyService;

	@Autowired
	private UserService userService;

	@Autowired
	private DocumentService documentService;

	protected static final Logger logger = LogManager.getLogger();

	@RequestMapping(value = { "/contracts" }, method = RequestMethod.GET)
	public String index() {
		return "html/admin/contract_manage";
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/contracts/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadcontracts(HttpServletResponse response, @ModelAttribute ContractFilterForm form,
		  Authentication authentication) {
		try {

			User user = (User) authentication.getPrincipal();

			if (user != null) {

				if (user.isAdmin() || user.isWatcher()) {

					//
					form.setAdmin(true);

				} else {
					//
					form.setAdmin(false);

					form.setAccount(user.getUserId());

				}

			} else {
				return null;
			}
			DecimalFormatSymbols dfs = new DecimalFormatSymbols();
			dfs.setDecimalSeparator(',');
			dfs.setGroupingSeparator('.');
			DecimalFormat formatter = new DecimalFormat("###,###,###.##");
			formatter.setDecimalFormatSymbols(dfs);
			Page<Contract> pageAuthors;

			

			if (!initProperties.active) {
				pageAuthors = contarctService.loadContractFetchAll(form);
			} else {
				boolean validated = userService.loadAccountByEmail();
				if (!validated)
					return null;

				pageAuthors = contarctService.loadContractFetchAll(form);
			}
			BooleanExpression predicates = form.getPredicate();

			String sort = form.getSort().toString();
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			ArrayNode jsonRep = mapper.createArrayNode();
			for (Contract contract : pageAuthors.getContent()) {
				ObjectNode json = mapper.createObjectNode();
				json.put("id", contract.getId());
				json.put("contract_no", contract.getContract_no());
				json.put("price", formatter.format(Float.parseFloat(contract.getPrice().replaceAll("[^\\d.]", ""))));
				json.put("cash", contract.getCash());

				// doc
//				Docform.setListId(null);
//				Docform.setControlled(true);
//				Docform.setCid(contract.getId());
//				Docform.setSort("editDate-1");
//				Docform.setP(1);
//				Docform.setPsize(1);
//
//				Page<Document> lastDocDate = documentService.loadAll(Docform);
//
//				if (lastDocDate.getContent().size() > 0) {
//					json.put("lastDocScan",
//							Time.ToTurkishDateMonthDateOnly(lastDocDate.getContent().iterator().next().getEditDate()));
//				}

				json.put("decs2", contract.getDescription2());
				// ÖDENMİŞ TOPLAM TAKSİT VARSA
				Set<Installment> Installments = contract.getInstallment();

				float sum = 0l;
				for (Installment installment : Installments) {
					float amount = 0l;
					if (installment.isPaid()) {

						if (installment.isPartial()) {
							// partial
							if (installment.getPartial_amount() != null) {

								amount = Float.parseFloat(installment.getPartial_amount().replaceAll("[^\\d.]", ""));
							}

						} else {
							// amount

							amount = Float.parseFloat(installment.getAmount().replaceAll("[^\\d.]", ""));
						}

					} else if (installment.isPartial()) {
						// partial
						if (installment.getPartial_amount() != null) {

							amount = Float.parseFloat(installment.getPartial_amount().replaceAll("[^\\d.]", ""));
						}

					}

					sum += amount;

				}

				if (!contract.getCash().isEmpty()) {

					json.put("reamount",
							formatter.format(Float.parseFloat(contract.getPrice().replaceAll("[^\\d.]", ""))
									- Float.parseFloat(contract.getCash().replaceAll("[^\\d.]", "")) - sum));
				} else {

					json.put("reamount",
							formatter.format(Float.parseFloat(contract.getPrice().replaceAll("[^\\d.]", "")) - sum));
				}

				json.put("createdate", Time.ToTurkishDateMonthDateOnly(contract.getCreateDate()));

				String source = Source.CurrencySource().get(contract.getCurrency() - 1);
				json.put("currency", source);

				json.put("active", contract.isActive());
				json.put("installmented", contract.isInstallmented());

				ArrayNode userCats = mapper.createArrayNode();
				for (Customer account : contract.getCustomer()) {
					ObjectNode node = mapper.createObjectNode();
					node.put("name", account.getFname());
					node.put("id", account.getId());
					userCats.add(node);
				}
				json.put("customers", userCats);

				ArrayNode portfoys = mapper.createArrayNode();
				for (Portfoy portfoy : contract.getPortfoys()) {

					for (Section section : portfoy.getSections()) {

						ObjectNode node = mapper.createObjectNode();
						node.put("name", section.getTitle());
						if(!section.getDocument().isEmpty()) {
							
							node.put("status","labeled_nowrap_red");
						}else {
							node.put("status","labeled_nowrap");
							
						}
						node.put("id", section.getId());
						node.put("pid", section.getPortfoy().getId());
						node.put("blok", section.getKat().getBlok().getTitle());
						portfoys.add(node);
					}

				}
				json.put("portfoys", portfoys);

//				json.put("agancy", contract.getContract_no());
//				json.put("temsilci", contract.getContract_no());
				jsonRep.add(json);
			}

			ObjectNode page = mapper.createObjectNode();
			page.put("total", pageAuthors.getTotalPages());
			page.put("number", pageAuthors.getNumber() + 1);
			if (predicates != null) {

				page.put("predicate", predicates.toString());
			}
			if (sort != null) {

				page.put("sort", sort);
			}
			jsonRep.add(page);

			String jsonreport = mapper.writeValueAsString(jsonRep);
			return jsonreport;

		} catch (Exception e) {
			logger.error("Error load contacts in adminpanel: " + " with error" + e.getMessage());
			e.printStackTrace();
		}

		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}
	
	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/contracts/excel", method = RequestMethod.POST)
	@ResponseBody
	public String exportInstallments(HttpServletResponse response, @ModelAttribute ContractFilterForm form,Authentication authentication
			, @ModelAttribute DocumentFilterForm Dform) {
		try {
			
			User user = (User) authentication.getPrincipal();
			
			 
			
			
			form.setSort(form.getSorting());
			form.setPsize(1000);
			Page<Contract> pageAuthors = contarctService.loadContractFetchAll(form);

			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			ObjectNode json = mapper.createObjectNode();

			CreateExcel createExcel = new CreateExcel();
			String path = createExcel.CrateExcelFromContracts(pageAuthors,Dform,documentService);

			json.put("path", path);
			String jsonreport = mapper.writeValueAsString(json);
			logger.fatal(user.getAccount().getName() + " EXPORT EXCEL FILE FOR CONTRACTS ");

			return jsonreport;

		} catch (Exception e) {
			logger.error("Error EXPORT EXCEL FILE in adminpanel: " + " with error" + e.getMessage());
			e.printStackTrace();
		}

		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}


	@SuppressWarnings("null")
	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/contract/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadContractById(@RequestParam(value = "id") long id,
			@RequestParam(value = "plan", required = false) boolean plan, Authentication authentication) {
		try {
			User user = (User) authentication.getPrincipal();

			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			DecimalFormatSymbols dfs = new DecimalFormatSymbols();
			dfs.setDecimalSeparator('.');
			dfs.setGroupingSeparator(',');
			DecimalFormat formatter = new DecimalFormat("###,###,###.##");
			formatter.setDecimalFormatSymbols(dfs);
			ObjectNode json = mapper.createObjectNode();

			Contract contract = new Contract();
			if (plan) {
				Portfoy portfoy = portfoyService.loadPortfoyByIdFetchContract(id);

				Long cid = portfoy.getContract().getId();
				contract = contarctService.loadContractFetchAllById(cid);

			} else {

				contract = contarctService.loadContractFetchAllById(id);
			}

			json.put("id", contract.getId());
			json.put("deed", contract.isDeed());
			json.put("insEnd", contract.isInsEnd());
			if (!contract.isDeed()) {
				json.put("status", "hidden");
			}
			json.put("contract_no", contract.getContract_no());
			json.put("price", formatter.format(Float.parseFloat(contract.getPrice().replaceAll("[^\\d.]", ""))));
			json.put("installmented", contract.isInstallmented());
			json.put("currency", contract.getCurrency());
			json.put("description", contract.getDescription());
			json.put("createDate", Time.ToSplitedDate(contract.getCreateDate()));
			json.put("downDate", Time.ToSplitedDate(contract.getDownDate()));
			json.put("downDatetr", Time.ToTurkishDateMonthDateOnly(contract.getDownDate()));
			if (!contract.getCash().isEmpty()) {

				json.put("cash", formatter.format(Float.parseFloat(contract.getCash().replaceAll("[^\\d.]", ""))));
			}

			if (contract.getDescription2() != null)
				json.put("decs2", contract.getDescription2().replaceAll("\n", System.getProperty("line.separator")));

			json.put("createdatetr", Time.ToTurkishDateMonthDateOnly(contract.getCreateDate()));
			String source = Source.CurrencySource().get(contract.getCurrency() - 1);
			json.put("kur", source);
			
//			if(contract.getProje()!=null) {
//				ObjectNode pnode = mapper.createObjectNode();
//				pnode.put("title", contract.getProje().getTitle());
//				pnode.put("id", contract.getProje().getId());
//				json.put("project", pnode);
//			}

			ArrayNode userCats = mapper.createArrayNode();
			for (Account account : contract.getAccount()) {
				ObjectNode node = mapper.createObjectNode();
				node.put("title", account.getName());
				node.put("id", account.getId());
				userCats.add(node);
			}
			json.put("temsilci", userCats);

			ArrayNode customers = mapper.createArrayNode();
			for (Customer customer : contract.getCustomer()) {
				ObjectNode node = mapper.createObjectNode();
				node.put("title", customer.getFname());
				node.put("id", customer.getId());
				customers.add(node);
			}
			json.put("customers", customers);

			ArrayNode agents = mapper.createArrayNode();
			for (Agent agent : contract.getAgent()) {
				ObjectNode node = mapper.createObjectNode();
				node.put("title", agent.getName());
				node.put("id", agent.getId());
				agents.add(node);
			}
			json.put("agent", agents);

			ArrayNode portfoys = mapper.createArrayNode();
			for (Portfoy portfoy : contract.getPortfoys()) {
				ObjectNode node = mapper.createObjectNode();
				node.put("title", portfoy.getTitle());
				node.put("id", portfoy.getId());

				portfoys.add(node);
			}

			json.put("portfoys", portfoys);

			ArrayList<String> cur = Source.CurrencySource();

			ObjectNode jsons = mapper.createObjectNode();

			jsons.put("id", contract.getCurrency());
			jsons.put("title", cur.get(contract.getCurrency() - 1));

			json.put("currency", jsons);

			ArrayList<Float> paid = new ArrayList<Float>();
			ArrayNode installments = mapper.createArrayNode();

			Set<Installment> dd = contarctService.loadInstallmentsByContractId(contract.getId());

			int index = 1;

			for (Installment installment : dd) {
				ObjectNode node = mapper.createObjectNode();
				String am = formatter.format(Float.parseFloat(installment.getAmount().replaceAll("[^\\d.]", "")));
				node.put("amount", am + " " + source);
				if (installment.getPartial_amount() != null) {
					if (!installment.getPartial_amount().isEmpty()) {
						node.put("partial_amount",
								formatter.format(
										Float.parseFloat(installment.getPartial_amount().replaceAll("[^\\d.]", "")))
										+ " " + source);

					}
				}

				if (installment.isPaid() && !installment.isPartial()) {
					node.put("partial_amount", am + " " + source);
				}

//				if (installment.isPaid()) {
//					node.put("partial_amount", installment.getAmount() + " " + source);
//				}

				node.put("order", index++);
				node.put("id", installment.getId());
				node.put("issue_date", Time.ToTurkishDateMonthDateOnly(installment.getIssueDate()));
				if (installment.getPaymantDate() != null) {
					node.put("paymant_date", Time.ToTurkishDateMonthDateOnly(installment.getPaymantDate()));
				}
				node.put("paid", installment.isPaid());
//				node.put("desc", contract.getDescription());

				if (installment.isPartial()) {
					node.put("paymant_status", "Kısmi Ödendi");
				}
				if (installment.isPaid()) {
					node.put("paymant_status", "Ödendi");
				}
				int d = Time.compare(installment.getIssueDate());
				if (!installment.isPaid()) {

					if (!installment.isPartial()) {
						if (d == 1) {
							node.put("paymant_status", "Ödenmedi");
						}
					}

				}

				float amount = 0l;
				if (installment.isPaid()) {

					if (installment.isPartial()) {
						// partial
						amount = Float.parseFloat(installment.getPartial_amount().replaceAll("[^\\d.]", ""));

					} else {
						// amount
						amount = Float.parseFloat(installment.getAmount().replaceAll("[^\\d.]", ""));
					}

				} else if (installment.isPartial()) {
					// partial
					amount = Float.parseFloat(installment.getPartial_amount().replaceAll("[^\\d.]", ""));

				}
				if (installment.getDescription() != null) {
					node.put("desc", installment.getDescription());

				}

				paid.add(amount);
				installments.add(node);
			}

			float sum = 0l;

			for (int i = 0; i < paid.size(); i++) {

				sum += paid.get(i);

			}
			if (!contract.getCash().isEmpty()) {

				json.put("remaining", formatter.format(Float.parseFloat(contract.getPrice().replaceAll("[^\\d.]", ""))
						- Float.parseFloat(contract.getCash().replaceAll("[^\\d.]", "")) - sum));

			} else {
				json.put("remaining",
						formatter.format(Float.parseFloat(contract.getPrice().replaceAll("[^\\d.]", "")) - sum));

			}
			json.put("installment", installments);

//KALAN MIKTAR

			String jsonreport = mapper.writeValueAsString(json);
			logger.fatal(user.getAccount().getName() + " LOAD CONTRACT WITH NO:" + contract.getContract_no());
			return jsonreport;

		} catch (Exception e) {
			logger.error("ERROR load CONTRACT failed from loading this id: " + id + " with error" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/contract/save", method = RequestMethod.POST)
	@ResponseBody
	public String saveTag(HttpServletResponse response, @Valid @ModelAttribute AdminContractForm form,
			Authentication authentication) {

		try {

			User user = (User) authentication.getPrincipal();

			if (user.isAdmin()) {

				if (form.getId() != null) {

					Contract contract = contarctService.loadContractById(form.getId());
					if (!form.getPortfoys().isEmpty()) {

						Contract con = contarctService.findContractByPortfoysIds(form.getPortfoys());
						if (con != null) {

							if (!con.getId().equals(contract.getId())) {

								response.setStatus(HttpStatus.BAD_REQUEST.value());
								return "999";
							}
						}
					}

					contract = form.manipulatContract(contract);
					 
//					contract.setProje(portfoyService.loadProjectById(form.getProject()));
					HashSet<Customer> customers = new HashSet<Customer>(
							personService.findCustomersByIds(form.getCustomers()));
					contract.setCustomer(customers);
//					contarctService.save(contract);
					contract.setAccount(new HashSet<Account>(userService.findAccontsByIds(form.getTemsilci())));

					if (form.getAgent() != null && !form.getAgent().isEmpty()) {

						contract.setAgent(new HashSet<Agent>(personService.findAgentsByIds(form.getAgent())));
					}

//						contarctService.save(contract);
					StringBuilder portfoySTR = new StringBuilder();

					if (!form.getPortfoys().isEmpty()) {
						Set<Portfoy> port = contract.getPortfoys();

						for (Portfoy portfoy : port) {

							portfoy.setContract(null);

							portfoy.setSaled(false);

							portfoyService.save(portfoy);
						}
						Set<Portfoy> portfoys = portfoyService.findPortfoysByIds(form.getPortfoys());

						StringBuilder sec = new StringBuilder();

						for (Portfoy portfoy : portfoys) {

							portfoy.setSaled(true);
							portfoy.setReserved(false);

							int section = Source.getSection(portfoy.getTitle());
							if (section != 0) {

								if (sec.length() != 0) {

									sec.append(" - " + section);
								} else {
									sec.append(section);
								}

								portfoySTR.append(portfoy.getTitle());
								portfoy.setContract(contract);

								portfoyService.save(portfoy);
							}
						}

						contract.setSection(sec.toString());
					} else {
						contract.setSection("");
						for (Portfoy portfoy : contract.getPortfoys()) {
							portfoy.setContract(null);

							portfoyService.save(portfoy);
						}
					}
//					contract.setLast_portfoy(portfoySTR.toString());
					contract.setDocuments(null);

					contarctService.save(contract);

					logger.fatal(user.getAccount().getName() + " MDIFY CONTRACT WITH NO:" + contract.getContract_no());

				} else {

					Contract contract = contarctService.findByContract_no(form.getContract_no());
					if (contract == null) {
						Contract contrat = form.createContract();
						HashSet<Customer> customers = new HashSet<Customer>(
								personService.findCustomersByIds(form.getCustomers()));
						contrat.setCustomer(customers);

						contrat.setAccount(new HashSet<Account>(userService.findAccontsByIds(form.getTemsilci())));
						if (form.getAgent() != null && !form.getAgent().isEmpty()) {

							contrat.setAgent(new HashSet<Agent>(personService.findAgentsByIds(form.getAgent())));
						}
						StringBuilder portfoySTR = new StringBuilder();
						contarctService.save(contrat);
						StringBuilder sec = new StringBuilder();
						Set<Portfoy> portfoys = portfoyService.findPortfoysByIds(form.getPortfoys());

						if (portfoys.size() == 0) {
							contrat.setSection("");
						}
						for (Portfoy portfoy : portfoys) {

							int section = Source.getSection(portfoy.getTitle());
							if (sec.length() != 0) {

								sec.append(" - " + section);
							} else {
								sec.append(section);
							}
							
							contrat.setSection(sec.toString());

							portfoy.setSaled(true);
							portfoy.setReserved(false);

							portfoySTR.append(portfoy.getTitle());
							portfoy.setContract(contrat);
							portfoyService.save(portfoy);

						}
						contrat.setLast_portfoy(portfoySTR.toString());
						contarctService.save(contrat);
						logger.fatal(user.getAccount().getName() + " CREATE NEW CONTRACT WITH NO:"
								+ contrat.getContract_no());
					} else {
						response.setStatus(HttpStatus.BAD_REQUEST.value());
						return "1";
					}

				}

			}

			return null;
		} catch (Exception e) {
			logger.error("ERROR SAVE CONTRACT WITH NO: " + form.getContract_no() + " WITH ERROR: " + e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());

			return null;
		}

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/contract/word", method = RequestMethod.POST)
	@ResponseBody
	public String saveWord(HttpServletResponse response, @Valid @ModelAttribute AdminContractForm form,
			Authentication authentication) {

		try {

			User user = (User) authentication.getPrincipal();
			ClassPathResource file = new ClassPathResource("../../resources/docs/dubaiContract.docx");
			InputStream inputStream = file.getInputStream();
			StringBuilder builder = new StringBuilder();
			List<Long> portfoyIds = new ArrayList<Long>();
			if (form.getPortfoys() != null) {

				Set<Portfoy> portfoys = portfoyService.findPortfoysByIds(form.getPortfoys());

				ArrayList<Portfoy> ports = new ArrayList<Portfoy>();
				ports.addAll(portfoys);

				int size = portfoys.size();

				for (int i = 0; i < ports.size(); i++) {
					builder.append(ports.get(i).getTitle());

					if (size > 1) {
						if (size != i + 1) {
							builder.append(" Ve ");
						}
					}

					portfoyIds.add(ports.get(i).getId());
				}

			}
			String date = Time.ToSplitedDateCalneder(form.getCreateDate());
			Word w = new Word();
			String path = w.ManipulateWord(inputStream, form.getCustomer(), date, form.getAddress(), form.getEmail(),
					form.getGsm(), builder.toString(), form.getDescription());

			if (path != null) {
				ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
						false);
				ObjectNode json = mapper.createObjectNode();

				json.put("path", "/contracts/" + path);

				// save to doc
				if (form.isSaveToDocs()) {
					Customer customer = personService.findCustomerByEmail(form.getEmail());
					if (customer != null) {
						customer.setPortfoy(portfoyIds.toString());
						customer.setContarctDate(form.getCreateDate());
						personService.save(customer);
						HashSet<Customer> accs = new HashSet<Customer>();
						accs.add(customer);
						DocType doctype = documentService.findBDocTypeTitle("CONTRACT");
						if (doctype != null) {
							Document doc = new Document();
							HashSet<DocType> doctypes = new HashSet<DocType>();
							doctypes.add(doctype);
							doc.setCustomer(accs);
							doc.setDoctype(doctypes);
							doc.setCreate_date(form.getCreateDate());
							doc.setFile_path("/contracts/" + path);
							doc.setAccount(user.getAccount());
							doc.setTitle(form.getCustomer() + " /CONTRACT");
							documentService.saveDocument(doc);

						} else {
							return "Dont Save Doc !";
						}
					} else {

						return "Customer Not Found !";
					}

				}

				String jsonreport = mapper.writeValueAsString(json);
				logger.fatal(user.getAccount().getName() + " CREATE NEW PDF CONTRACT WITH CUSTOMER NANE:"
						+ form.getCustomer());
				return jsonreport;
			}

		} catch (Exception e) {
			logger.error("CREATE NEW PDF CONTRACT WITH CUSTOMER NANE:= " + form.getCustomer() + " WITH ERROR: "
					+ e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());
		}

		return null;

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/contract/teslim", method = RequestMethod.POST)
	@ResponseBody
	public String saveTeslim(HttpServletResponse response, @Valid @ModelAttribute AdminContractForm form,
			Authentication authentication) {

		try {

			User user = (User) authentication.getPrincipal();

			String proje = "";
			String projeAddress = "";
			String ada = "";
			ClassPathResource file = new ClassPathResource("../../resources/docs/teslim.docx");
			InputStream inputStream = file.getInputStream();
			StringBuilder PO = new StringBuilder();
			List<Long> portfoyIds = new ArrayList<Long>();
			if (form.getPortfoys() != null) {

				Set<Portfoy> portfoys = portfoyService.findPortfoysByIds(form.getPortfoys());

				ArrayList<Portfoy> ports = new ArrayList<Portfoy>();
				ports.addAll(portfoys);

				Proje project = portfoyService.findByProjectTitle(ports.iterator().next().getSections().iterator()
						.next().getKat().getBlok().getProje().getTitle());
				proje = project.getTitle();
				projeAddress = project.getAddress();
				ada = project.getAda();

				for (int i = 0; i < ports.size(); i++) {

					portfoyIds.add(ports.get(i).getId());

					Set<Section> sections = ports.get(i).getSections();

					ArrayList<Section> SC = new ArrayList<Section>();
					SC.addAll(sections);
					if (sections.size() > 1) {
						for (int j = 0; j < SC.size(); j++) {

							String blok = SC.get(j).getKat().getBlok().getTitle();
							String kat = SC.get(j).getKat().getTitle();
							String sec = SC.get(j).getTitle();

							if (j == 0) {
								PO.append(" " + blok + " kat:" + kat + " daire:" + sec);

							} else {
								PO.append(" , " + sec);

							}
						}

					} else {
						//
						String blok = SC.get(0).getKat().getBlok().getTitle();
						String kat = SC.get(0).getKat().getTitle();
						String sec = SC.get(0).getTitle();
						PO.append(" " + blok + " kat:" + kat + " daire:" + sec);
						if (ports.size() > i + 1) {
							PO.append(" VE ");
						}

					}

				}

			}

			String date = Time.ToSplitedDateCalneder(form.getCreateDate());
			Word w = new Word();
			String path = w.ManipulateTeslimWord(inputStream, form.getCustomer(), date, proje, projeAddress,
					PO.toString(), ada);

			if (path != null) {
				ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
						false);
				ObjectNode json = mapper.createObjectNode();

				json.put("path", "/contracts/" + path);

				// save to doc
				if (form.isSaveToDocs()) {
					Customer customer = personService.findByCustomerName(form.getCustomer());
					if (customer != null) {
						customer.setPortfoy(portfoyIds.toString());
						customer.setContarctDate(form.getCreateDate());
						personService.save(customer);
						HashSet<Customer> accs = new HashSet<Customer>();
						accs.add(customer);
						DocType doctype = documentService.findBDocTypeTitle("CONTRACT");
						if (doctype != null) {
							Document doc = new Document();
							HashSet<DocType> doctypes = new HashSet<DocType>();
							doctypes.add(doctype);
							doc.setCustomer(accs);
							doc.setDoctype(doctypes);
							doc.setCreate_date(form.getCreateDate());
							doc.setFile_path("/contracts/" + path);
							doc.setAccount(user.getAccount());
							doc.setTitle(form.getCustomer() + " /TeslimTutanak");
							documentService.saveDocument(doc);

						} else {
							return "Dont Save Doc !";
						}
					} else {

						return "Customer Not Found !";
					}

				}

				String jsonreport = mapper.writeValueAsString(json);
				logger.fatal(user.getAccount().getName() + " CREATE NEW PDF 'TESLIM TUTANAGI' WITH CUSTOMER NANE:"
						+ form.getCustomer());
				return jsonreport;
			}

		} catch (Exception e) {
			logger.error("CREATE NEW PDF 'TESLIM TUTANAGI' WITH CUSTOMER NANE:= " + form.getCustomer() + " WITH ERROR: "
					+ e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());
		}

		return null;

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/contract/teknik", method = RequestMethod.POST)
	@ResponseBody
	public String saveTeknik(HttpServletResponse response, @Valid @ModelAttribute AdminContractForm form,
			Authentication authentication) {

		try {

			User user = (User) authentication.getPrincipal();

			String proje = "";
			ClassPathResource file;
			if (form.isKombi()) {

				file = new ClassPathResource("../../resources/docs/teknik.docx");
			} else {
				file = new ClassPathResource("../../resources/docs/teknikNoKombi.docx");

			}

			InputStream inputStream = file.getInputStream();
			StringBuilder PO = new StringBuilder();
			List<Long> portfoyIds = new ArrayList<Long>();
			if (form.getPortfoys() != null) {

				Set<Portfoy> portfoys = portfoyService.findPortfoysByIds(form.getPortfoys());

				ArrayList<Portfoy> ports = new ArrayList<Portfoy>();
				ports.addAll(portfoys);

				 

				for (int i = 0; i < ports.size(); i++) {

					portfoyIds.add(ports.get(i).getId());

					Set<Section> sections = ports.get(i).getSections();

					ArrayList<Section> SC = new ArrayList<Section>();
					SC.addAll(sections);
					if (sections.size() > 1) {
						for (int j = 0; j < SC.size(); j++) {

							String blok = SC.get(j).getKat().getBlok().getTitle();
							String kat = SC.get(j).getKat().getTitle();
							String sec = SC.get(j).getTitle();

							if (j == 0) {
								PO.append(" " + blok + " kat:" + kat + " daire:" + sec);

							} else {
								PO.append(" , " + sec);

							}
						}

					} else {
						//
						String blok = SC.get(0).getKat().getBlok().getTitle();
						String kat = SC.get(0).getKat().getTitle();
						String sec = SC.get(0).getTitle();
						PO.append(" " + blok + " kat:" + kat + " daire:" + sec);
						if (ports.size() > i + 1) {
							PO.append(" VE ");
						}

					}

				}

			}

			String date = Time.ToSplitedDateCalneder(form.getCreateDate());
			Word w = new Word();
			String path = w.ManipulateTeknikWord(inputStream, form.getCustomer(), date, proje, PO.toString(),
					form.getDescription());

			if (path != null) {
				ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
						false);
				ObjectNode json = mapper.createObjectNode();

				json.put("path", "/contracts/" + path);

				// save to doc
				if (form.isSaveToDocs()) {
					Customer customer = personService.findByCustomerName(form.getCustomer());
					if (customer != null) {
						customer.setPortfoy(portfoyIds.toString());
						customer.setContarctDate(form.getCreateDate());
						personService.save(customer);
						HashSet<Customer> accs = new HashSet<Customer>();
						accs.add(customer);
						DocType doctype = documentService.findBDocTypeTitle("CONTRACT");
						if (doctype != null) {
							Document doc = new Document();
							HashSet<DocType> doctypes = new HashSet<DocType>();
							doctypes.add(doctype);
							doc.setCustomer(accs);
							doc.setDoctype(doctypes);
							doc.setCreate_date(form.getCreateDate());
							doc.setFile_path("/contracts/" + path);
							doc.setAccount(user.getAccount());
							doc.setTitle(form.getCustomer() + " /TeknikSartname");
							documentService.saveDocument(doc);

						} else {
							return "Dont Save Doc !";
						}
					} else {

						return "Customer Not Found !";
					}

				}

				String jsonreport = mapper.writeValueAsString(json);
				logger.fatal(user.getAccount().getName() + " CREATE NEW PDF 'TEKNİK ŞARTNAME' WITH CUSTOMER NANE:"
						+ form.getCustomer());

				return jsonreport;
			}

		} catch (Exception e) {
			logger.error("CREATE NEW PDF 'TEKNİK ŞARTNAME' WITH CUSTOMER NANE:= " + form.getCustomer() + " WITH ERROR: "
					+ e.getMessage());

			response.setStatus(HttpStatus.BAD_REQUEST.value());
		}

		return null;

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/contract/dekor", method = RequestMethod.POST)
	@ResponseBody
	public String saveDekor(HttpServletResponse response, @Valid @ModelAttribute AdminContractForm form,
			Authentication authentication) {

		try {

			User user = (User) authentication.getPrincipal();

			 
			ClassPathResource file;

			file = new ClassPathResource("../../resources/docs/dekor.docx");

			InputStream inputStream = file.getInputStream();

			Word w = new Word();
			String path = w.ManipulateDekorWord(inputStream, form);

			if (path != null) {
				ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
						false);
				ObjectNode json = mapper.createObjectNode();

				json.put("path", "/contracts/" + path);

				// save to doc
				if (form.isSaveToDocs()) {
					Customer customer = personService.findByCustomerName(form.getCustomer());
					if (customer != null) {
						HashSet<Customer> accs = new HashSet<Customer>();
						accs.add(customer);
						DocType doctype = documentService.findBDocTypeTitle("CONTRACT");
						if (doctype != null) {
							Document doc = new Document();
							HashSet<DocType> doctypes = new HashSet<DocType>();
							doctypes.add(doctype);
							doc.setCustomer(accs);
							doc.setDoctype(doctypes);
							doc.setCreate_date(form.getCreateDate());
							doc.setFile_path("/contracts/" + path);
							doc.setAccount(user.getAccount());
							doc.setTitle(form.getCustomer() + " /Dekorasyon");
							documentService.saveDocument(doc);

						} else {
							return "Dont Save Doc !";
						}
					} else {

						return "Customer Not Found !";
					}

				}

				String jsonreport = mapper.writeValueAsString(json);
				logger.fatal(user.getAccount().getName() + " CREATE NEW PDF 'DEKORASYON' WITH CUSTOMER NANE:"
						+ form.getCustomer());

				return jsonreport;
			}

		} catch (Exception e) {
			logger.error("CREATE NEW PDF 'DEKORASYON' WITH CUSTOMER NANE:= " + form.getCustomer() + " WITH ERROR: "
					+ e.getMessage());

			response.setStatus(HttpStatus.BAD_REQUEST.value());
		}

		return null;

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/contract/alim", method = RequestMethod.POST)
	@ResponseBody
	public String saveAlimSatim(HttpServletResponse response, @Valid @ModelAttribute AdminContractForm form,
			Authentication authentication) {

		try {

			User user = (User) authentication.getPrincipal();

			Customer customer;
			customer = personService.loadCustomerById(form.getId());
			ClassPathResource file;

			file = new ClassPathResource("../../resources/docs/alim.docx");

			InputStream inputStream = file.getInputStream();

			Word w = new Word();
			String path = w.ManipulateDekorWord(inputStream, form.getCreateDate(), customer, form.getDescription());

			if (path != null) {
				ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
						false);
				ObjectNode json = mapper.createObjectNode();

				json.put("path", "/contracts/" + path);

				// save to doc
				if (form.isSaveToDocs()) {
					customer = personService.loadCustomerById(form.getId());
					if (customer != null) {
						HashSet<Customer> accs = new HashSet<Customer>();
						accs.add(customer);
						DocType doctype = documentService.findBDocTypeTitle("CONTRACT");
						if (doctype != null) {
							Document doc = new Document();
							HashSet<DocType> doctypes = new HashSet<DocType>();
							doctypes.add(doctype);
							doc.setCustomer(accs);
							doc.setDoctype(doctypes);
							doc.setCreate_date(form.getCreateDate());
							doc.setFile_path("/contracts/" + path);
							doc.setAccount(user.getAccount());
							doc.setTitle(form.getCustomer() + " /AlimSatimSozlesmesi");
							documentService.saveDocument(doc);

						} else {
							return "Dont Save Doc !";
						}
					} else {

						return "Customer Not Found !";
					}

				}

				String jsonreport = mapper.writeValueAsString(json);
				logger.fatal(user.getAccount().getName() + " CREATE NEW PDF 'ALIM SATIM' WITH CUSTOMER NANE:"
						+ form.getCustomer());
				return jsonreport;
			}

		} catch (Exception e) {
			logger.error("CREATE NEW PDF 'ALIM SATIM' WITH CUSTOMER NANE:= " + form.getCustomer() + " WITH ERROR: "
					+ e.getMessage());

			response.setStatus(HttpStatus.BAD_REQUEST.value());
		}

		return null;

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/contract/radisson", method = RequestMethod.POST)
	@ResponseBody
	public String saveRadisson(HttpServletResponse response, @Valid @ModelAttribute AdminContractForm form,
			Authentication authentication) {

		try {

			User user = (User) authentication.getPrincipal();

			ClassPathResource file;

			file = new ClassPathResource("../../resources/docs/Radisson.docx");

			InputStream inputStream = file.getInputStream();

			Word w = new Word();
			String path = w.ManipulateRadissonWord(inputStream, form);

			if (path != null) {
				ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
						false);
				ObjectNode json = mapper.createObjectNode();

				json.put("path", "/contracts/" + path);

				String jsonreport = mapper.writeValueAsString(json);
				logger.fatal(user.getAccount().getName() + " CREATE NEW PDF 'RADISSON' WITH CUSTOMER NANE:"
						+ form.getCustomer());

				return jsonreport;
			}

		} catch (Exception e) {
			logger.error("CREATE NEW PDF 'RADISSON' WITH CUSTOMER NANE:= " + form.getCustomer() + " WITH ERROR: "
					+ e.getMessage());

			response.setStatus(HttpStatus.BAD_REQUEST.value());
		}

		return null;

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/contract/aydinlatma", method = RequestMethod.POST)
	@ResponseBody
	public String saveAydinlatma(HttpServletResponse response, @Valid @ModelAttribute AdminContractForm form,
			Authentication authentication) {

		try {

			User user = (User) authentication.getPrincipal();

			ClassPathResource file;

			file = new ClassPathResource("../../resources/docs/aydinlatma.docx");

			InputStream inputStream = file.getInputStream();

			Word w = new Word();
			String path = w.ManipulateAydinlatmaWord(inputStream, form);

			if (path != null) {
				ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
						false);
				ObjectNode json = mapper.createObjectNode();

				json.put("path", "/contracts/" + path);

				String jsonreport = mapper.writeValueAsString(json);
				logger.fatal(user.getAccount().getName() + " CREATE NEW PDF 'AYDINLATMA' WITH CUSTOMER NANE:"
						+ form.getCustomer());

				return jsonreport;
			}

		} catch (Exception e) {
			logger.error("CREATE NEW PDF 'AYDINLATMA' WITH CUSTOMER NANE:= " + form.getCustomer() + " WITH ERROR: "
					+ e.getMessage());

			response.setStatus(HttpStatus.BAD_REQUEST.value());
		}

		return null;

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/contract/dekont", method = RequestMethod.POST)
	@ResponseBody
	public String saveDekont(HttpServletResponse response, @Valid @ModelAttribute AdminContractForm form,
			Authentication authentication) {

		try {

			User user = (User) authentication.getPrincipal();

			String proje = "";
			ClassPathResource file;

			file = new ClassPathResource("../../resources/docs/dekont.docx");

			InputStream inputStream = file.getInputStream();
			StringBuilder PO = new StringBuilder();
			List<Long> portfoyIds = new ArrayList<Long>();
			if (form.getPortfoys() != null) {

				Set<Portfoy> portfoys = portfoyService.findPortfoysByIds(form.getPortfoys());

				ArrayList<Portfoy> ports = new ArrayList<Portfoy>();
				ports.addAll(portfoys);

				Proje project = portfoyService.findByProjectTitle(ports.iterator().next().getSections().iterator()
						.next().getKat().getBlok().getProje().getTitle());
				proje = project.getTitle();

				for (int i = 0; i < ports.size(); i++) {

					portfoyIds.add(ports.get(i).getId());

					Set<Section> sections = ports.get(i).getSections();

					ArrayList<Section> SC = new ArrayList<Section>();
					SC.addAll(sections);
					if (sections.size() > 1) {
						for (int j = 0; j < SC.size(); j++) {

							String blok = SC.get(j).getKat().getBlok().getTitle();
							String kat = SC.get(j).getKat().getTitle();
							String sec = SC.get(j).getTitle();

							if (j == 0) {
								PO.append(" " + blok + " kat:" + kat + " daire:" + sec);

							} else {
								PO.append(" , " + sec);

							}
						}

					} else {
						//
						String blok = SC.get(0).getKat().getBlok().getTitle();
						String kat = SC.get(0).getKat().getTitle();
						String sec = SC.get(0).getTitle();
						PO.append(" " + blok + " kat:" + kat + " daire:" + sec);
						if (ports.size() > i + 1) {
							PO.append(" VE ");
						}

					}

				}

			}
			StringBuilder accounts = new StringBuilder();
			if (form.getTemsilci() != null) {
				List<Account> accountList = userService.findAccontsByIds(form.getTemsilci());
				for (int i = 0; i < accountList.size(); i++) {
					accounts.append(accountList.get(i).getName());
					if (accountList.size() > i + 1) {
						accounts.append("Ve");
					}

				}
			} else {
				accounts.append(user.getAccount().getName());
			}

			Word w = new Word();
			String path = w.ManipulateDekontWord(inputStream, form, accounts.toString(), proje, PO.toString());

			if (path != null) {
				ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
						false);
				ObjectNode json = mapper.createObjectNode();

				json.put("path", "/contracts/" + path);

				String jsonreport = mapper.writeValueAsString(json);
				logger.fatal(user.getAccount().getName() + " CREATE NEW PDF 'DEKONT' WITH CUSTOMER NANE:"
						+ form.getCustomer());

				return jsonreport;
			}

		} catch (Exception e) {
			logger.error("CREATE NEW PDF 'DEKONT' WITH CUSTOMER NANE:= " + form.getCustomer() + " WITH ERROR: "
					+ e.getMessage());

			response.setStatus(HttpStatus.BAD_REQUEST.value());
		}

		return null;

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/contract/cancel", method = RequestMethod.POST)
	@ResponseBody
	public String deleteVariantvalue(HttpServletResponse response, @RequestParam(value = "ids") List<Long> ids,
			Authentication authentication) {
		try {
			User user = (User) authentication.getPrincipal();

			if (user.isAdmin()) {
				for (Long contractİd : ids) {

					Contract contract = contarctService.loadContractById(contractİd);

					Set<Installment> ins = contract.getInstallment();
					for (Installment installment : ins) {
						installment.setActive(false);
						contarctService.save(installment);

					}

					Set<Portfoy> portfoys = contract.getPortfoys();

					StringBuilder portfoySTR = new StringBuilder();

					for (Portfoy portfoy : portfoys) {

						portfoySTR.append(" [ " + portfoy.getTitle() + " ] ");

					}

					if (contract.isActive()) {
						contract.setActive(false);

						String eski = contract.getDescription2();
						if (eski == null) {
							eski = "";
						}
						String cancel = " BU SÖZLEŞMENİN DURUMU " + Time.ToTurkishDateOnly(Time.todayDateTime())
								+ " TARİHİNDE " + user.getUsername()
								+ " ADLI KULLANICI TARAFINDAN İPTAL OLARAK DEĞİŞTİRİLDİ. İPTAL OLUNAN PORTFÖY(LER): "
								+ portfoySTR;
						StringBuilder builder = new StringBuilder();
						builder.append(cancel);
						builder.append("\n");
						builder.append(eski);
						contract.setDescription2(builder.toString());
						Set<Portfoy> ports = contract.getPortfoys();
						for (Portfoy portfoy : ports) {
							portfoy.setSaled(false);
							portfoy.setContract(null);
							portfoyService.save(portfoy);
						}

						contarctService.save(contract);
					} else {
						response.setStatus(HttpStatus.CONFLICT.value());
					}

				}
			} else {
				response.setStatus(HttpStatus.FORBIDDEN.value());
			}

		} catch (Exception e) {
			logger.error("ERROR deleting programs with ids= " + ids + " with error: " + e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());
		}
		return null;
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/contract/active", method = RequestMethod.POST)
	@ResponseBody
	public String activeVariantvalue(HttpServletResponse response, @RequestParam(value = "ids") List<Long> ids,
			Authentication authentication) {
		try {
			User user = (User) authentication.getPrincipal();

			if (user.isAdmin()) {
				for (Long contractİd : ids) {

					Contract contract = contarctService.loadContractById(contractİd);

					Set<Installment> ins = contract.getInstallment();
					for (Installment installment : ins) {
						installment.setActive(true);

					}

					if (!contract.isActive()) {
						contract.setActive(true);
						String eski = contract.getDescription2();
						String active = " BU SÖZLEŞMENİN DURUMU " + Time.ToTurkishDateOnly(Time.todayDateTime())
								+ " TARİHİNDE " + user.getUsername()
								+ " ADLI KULLANICI TARAFINDAN YENİDEN 'AKTİF' OLARAK DEĞİŞTİRİLDİ.";

						StringBuilder builder = new StringBuilder();
						builder.append(active);
						builder.append("\n");
						builder.append(eski);
						contract.setDescription2(builder.toString());
						contarctService.save(contract);
					} else {
						response.setStatus(HttpStatus.CONFLICT.value());
					}

				}
			} else {
				response.setStatus(HttpStatus.FORBIDDEN.value());
			}

		} catch (Exception e) {
			logger.error("ERROR deleting programs with ids= " + ids + " with error: " + e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());
		}
		return null;
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/document/check", method = RequestMethod.POST)
	@ResponseBody
	public String loadAgentById(@RequestParam(value = "id") long id, @ModelAttribute DocumentFilterForm form,
			Authentication authentication) {
		try {
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);

			User user = (User) authentication.getPrincipal();

			if (user.isAdmin() || user.isWatcher()) {

				Page<DocType> checkDoc = documentService.loadCheckedDocsTypes(form);
				ArrayNode jsonRep = mapper.createArrayNode();
				for (DocType docType : checkDoc.getContent()) {
					//

					form.setCid(id);
					form.setDocType(docType.getId());
					form.setPsize(50);
					Page<Document> DOC = documentService.loadAll(form);
					if (DOC.getContent().size() > 0) {
						ObjectNode json = mapper.createObjectNode();
						if (DOC.getContent().size() > 1) {
							// group
							json.put("count", " [ " + DOC.getContent().size() + " ADET ]");

						}
						Document document = DOC.iterator().next();
						String path = document.getFile_path();
						boolean ex = FileExist.check(initProperties.FILEFOLDERNAME + path);

						json.put("type", docType.getTitle());

						json.put("title", document.getTitle());
						json.put("typeId", docType.getId());
						json.put("cid", id);
						json.put("exist", ex);
						if (!ex)
							json.put("saved", true);

						String[] parts = document.getFile_path().split("\\.");
						if (parts.length > 1) {

							json.put("id", document.getId());
							if (ex) {

								String suffix = parts[1];
								json.put("file", suffix);
								json.put("title", document.getFile_path());
								json.put("path", document.getFile_path());
							} else {
								json.put("file", "unknown");

							}

						}

						jsonRep.add(json);

					} else {
						ObjectNode json = mapper.createObjectNode();

						json.put("type", docType.getTitle());
						json.put("exist", false);
						json.put("file", "unknown");
						json.put("title", "BULUNMADI");
						json.put("typeId", docType.getId());
						jsonRep.add(json);
					}

				}

				String jsonreport = mapper.writeValueAsString(jsonRep);
				return jsonreport;
			}
		} catch (Exception e) {
			logger.error(
					"ERROR load Contract failed from loading Docs by this id: " + id + " with error" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}
}
