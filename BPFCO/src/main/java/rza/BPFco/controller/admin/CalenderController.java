package rza.BPFco.controller.admin;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import rza.BPFco.config.initProperties;
import rza.BPFco.form.filter.InstallmentFilterForm;
import rza.BPFco.model.Account;
import rza.BPFco.model.portfoys.Customer;
import rza.BPFco.model.portfoys.Installment;
import rza.BPFco.service.ContractService;
import rza.BPFco.service.UserService;
import rza.BPFco.service.UserService.User;
import rza.BPFco.utilities.Source;
import rza.BPFco.utilities.Time;

@Controller
public class CalenderController {

	@Autowired
	private ContractService contractService;
	
	 

	@Autowired
	private UserService userService;

	protected static final Logger logger = LogManager.getLogger();

	@RequestMapping(value = { "/calender" }, method = RequestMethod.GET)
	public String index() {
		return "html/admin/calender";
	}
	
	@RequestMapping(value = { "/not_paid" }, method = RequestMethod.GET)
	public String not_paid(Model model,Authentication authentication) {
		
		User user = (User) authentication.getPrincipal();
		
		Account account= user.getAccount();
		
		if(account.isAdmin() || account.isWatcher() ) {
			
			List<Installment> inPage = contractService.loadNotPaidInstallments(Time.today());
			DecimalFormatSymbols dfs = new DecimalFormatSymbols();
			dfs.setDecimalSeparator(',');
			dfs.setGroupingSeparator('.');
			DecimalFormat formatter = new DecimalFormat("###,###,###.##");
			formatter.setDecimalFormatSymbols(dfs);
			
			for (Installment installment : inPage) {
				
				float amount = Float.parseFloat(installment.getAmount().replaceAll("[^\\d.]", ""));
				
				if(installment.isPartial()) {
					float part = Float.parseFloat(installment.getPartial_amount().replaceAll("[^\\d.]", ""));
					installment.setPartial_amount(formatter.format(amount-part));
					
				}
				installment.setAmount(formatter.format(Float.parseFloat(installment.getAmount().replaceAll("[^\\d.]", ""))));
				
			}
			model.addAttribute("ins", inPage);
		}
		
		return "html/admin/not_paid";
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/installment/loadCalender", method = RequestMethod.POST)
	@ResponseBody
	public String loadInstallmentsForCalender(HttpServletResponse response, @ModelAttribute InstallmentFilterForm form,
			Authentication authentication) {
		Long idE = 0l;
		String err = "";

		DecimalFormatSymbols dfs = new DecimalFormatSymbols();
		dfs.setDecimalSeparator(',');
		dfs.setGroupingSeparator('.');
		DecimalFormat formatter = new DecimalFormat("###,###,###.##");
		formatter.setDecimalFormatSymbols(dfs);


		try {

			User user = (User) authentication.getPrincipal();
			if (user != null) {

				if (user.isAdmin()||user.isWatcher()) {
					//
					form.setAdmin(true);

				} else {
					//
					form.setAdmin(false);
					form.setTemsilci(user.getUserId());

				}

			} else {
				return null;
			}
			form.setPsize(1000000);
			form.setActive(true);
			Page<Installment> pageAuthors;

			if (!initProperties.active) {
				pageAuthors = contractService.loadAllInstallments(form);
			} else {
				 boolean validated = userService.loadAccountByEmail();
					if (!validated)
						return null;


				pageAuthors = contractService.loadAllInstallments(form);
			}

//			List<Installment> pageAuthors = contractService.loadAllInstallments();
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			ArrayNode jsonRep = mapper.createArrayNode();
			for (Installment installment : pageAuthors) {

				idE = installment.getId();
				ObjectNode json = mapper.createObjectNode();
				json.put("id", installment.getId());

				if (installment.getId() == 61) {
					err = "f";
				}

				err = "contract";
				json.put("groupId", installment.getContract().getId());
				json.put("contractNo", installment.getContract().getContract_no());
				json.put("contractDate", Time.ToSplitedDateJustCalneder(installment.getContract().getCreateDate()));

				Set<Customer> cus = installment.getContract().getCustomer();

				if (cus.size() > 0) {
					json.put("title", cus.iterator().next().getFname());
					json.put("customerid", cus.iterator().next().getId());

				}

				err = "";
				json.put("issueDate", Time.ToSplitedDateJustCalneder(installment.getIssueDate()));
				json.put("amount", formatter.format(Float.parseFloat(installment.getAmount().replaceAll("[^\\d.]", "")))
						+ " " + Source.CurrencySource().get(installment.getCurrency() - 1));

				json.put("start", Time.ToSplitedDateJustCalneder(installment.getIssueDate()));

				if (!installment.isPaid()) {

					switch (Time.compare(installment.getIssueDate())) {
					case 0:// same
						if (!installment.isReminding()) {
							//
							json.put("eventClassNames", "glyphicon glyphicon-star");

						}

						json.put("className", "today-cal");

						break;
					case 1:// before

						if (!installment.isReminding()) {
							//
//							json.put("eventClassNames", "glyphicon glyphicon-star");

						}

						if (!installment.isWarning()) {
							//
							json.put("eventClassNames", "glyphicon glyphicon-star");

						}

						json.put("className", "not-paid");

						break;
					case 2:// after

						json.put("className", "pending");
//						json.put("eventClassNames", "glyphicon glyphicon-star-empty");

						break;

					case 3:// 15 days

						if (!installment.isReminding()) {
							//
							json.put("eventClassNames", "glyphicon glyphicon-star");

						}

						break;

					default:
						json.put("className", "pending");
						break;
					}

					if (installment.isPartial()) {

						json.put("className", "paid-partial");
						if (!installment.isThanks()) {
							//
							json.put("eventClassNames", "glyphicon glyphicon-star");

						} else {

//							json.put("eventClassNames", "glyphicon glyphicon-star-empty");
						}
					}

				} else {
					json.put("className", "paid");
					if (!installment.isThanks()) {
						//
						json.put("eventClassNames", "glyphicon glyphicon-star");

					} else {

//						json.put("eventClassNames", "glyphicon glyphicon-star-empty");
					}
				}
				err = "";
				jsonRep.add(json);

			}

			String jsonreport = mapper.writeValueAsString(jsonRep);
			return jsonreport;

		} catch (Exception e) {
			logger.error("Error load installments in adminpanel installment id: " + idE + err + " with error"
					+ e.getMessage());
			e.printStackTrace();
		}

		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}

}
