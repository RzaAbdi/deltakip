package rza.BPFco.controller.admin;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import rza.BPFco.form.AdminAjandaForm;
import rza.BPFco.form.filter.AjandaFilterForm;
import rza.BPFco.model.Account;
import rza.BPFco.model.Ajanda;
import rza.BPFco.model.portfoys.Reply;
import rza.BPFco.model.portfoys.SosyalMedya;
import rza.BPFco.service.AjandaService;
import rza.BPFco.service.SosyalMedyaService;
import rza.BPFco.service.UserService;
import rza.BPFco.service.UserService.User;
import rza.BPFco.utilities.Sorting;
import rza.BPFco.utilities.Time;

@Controller
public class AdminAjandaController {

	@Autowired
	private AjandaService ajandaService;

	@Autowired
	private UserService userService;

	@Autowired
	private SosyalMedyaService sosyalMedyaService;

	protected static final Logger logger = LogManager.getLogger();

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/ajandas/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadAjandas(HttpServletResponse response, @ModelAttribute AjandaFilterForm form,
			Authentication authentication) {
		try {

			if (authentication != null) {
				User user = (User) authentication.getPrincipal();

				if (user.isAdmin() || user.getAccount().isCallAr() || user.getAccount().isCallFa()) {
					//
					form.setAdmin(true);

				} else {
					//
					form.setAdmin(false);
					form.setAccount(user.getUserId());

				}

				if (user != null) {
					Page<Ajanda> pageAuthors = ajandaService.loadAjandas(form);
					ObjectMapper mapper = new ObjectMapper()
							.configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
					ArrayNode jsonRep = mapper.createArrayNode();
					for (Ajanda ajanda : pageAuthors.getContent()) {
						ObjectNode json = mapper.createObjectNode();
						json.put("id", ajanda.getId());
						json.put("title", ajanda.getTitle());
						json.put("type", ajanda.getType());
						SosyalMedya sm = ajanda.getSosyalmedya();

						if (sm != null) {
							json.put("smname", ajanda.getSosyalmedya().getName().toUpperCase());

							switch (sm.getType()) {
							case 0:// sm
								if (sm.getLanguage() == 0) {
									json.put("link", "/sosyalmedya_fa?type=0&listId=" + sm.getId());
								} else {
									json.put("link", "/sosyalmedya_ar?type=0&listId=" + sm.getId());

								}
								break;
							case 1:// tv
								if (sm.getLanguage() == 0) {
									json.put("link", "/sosyalmedya_fa?type=1&listId=" + sm.getId());
								} else {
									json.put("link", "/sosyalmedya_ar?type=1&listId=" + sm.getId());

								}

								break;
							case 3:// others
								json.put("link", "/crm?type=3&listId=" + sm.getId());

								break;

							default:
								break;
							}
						}

						switch (ajanda.getStatus()) {
						case 1:
							json.put("status", "info");
							json.put("durum", "AÇIK");

							break;
						case 2:
							json.put("status", "warning");
							json.put("durum", "BEKLEMEDE");

							break;
						case 3:
							json.put("status", "success");
							json.put("durum", "YAPILDI");

							break;
						case 4:
							json.put("status", "default");
							json.put("durum", "KAPANDI");

							break;
						case 0:
							json.put("status", "default");

							break;

						default:
							break;
						}

						if (ajanda.getIssueDate().before(Time.today()) && ajanda.getStatus() == 1) {

							json.put("status", "danger");
						}
						json.put("createDate", Time.ToTurkishDateOnly(ajanda.getCreateDate()));
						json.put("issueDate", Time.ToTurkishDateOnly(ajanda.getIssueDate()));
						if (ajanda.getLastUpdate() != null) {

							json.put("lastUpdate", Time.ToTurkishDateOnly(ajanda.getLastUpdate()));
						}

						jsonRep.add(json);
					}

					ObjectNode page = mapper.createObjectNode();
					page.put("total", pageAuthors.getTotalPages());
					page.put("number", pageAuthors.getNumber() + 1);
					jsonRep.add(page);
					String jsonreport = mapper.writeValueAsString(jsonRep);
					return jsonreport;

				}
			}

		} catch (Exception e) {
			logger.error("Error load ajandas in adminpanel: " + " with error" + e.getMessage());
			e.printStackTrace();
		}

		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/ajanda/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadAjandaById(@RequestParam(value = "id") long id, Authentication authentication) {
		try {

			if (authentication != null) {
				User user = (User) authentication.getPrincipal();

				if (user != null) {
					ObjectMapper mapper = new ObjectMapper()
							.configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
					ObjectNode json = mapper.createObjectNode();
					Ajanda ajanda = ajandaService.loadAjandaById(id);

					json.put("id", ajanda.getId());
					json.put("title", ajanda.getTitle());
					json.put("type", ajanda.getType());
					json.put("issueDate", Time.ToSplitedDate(ajanda.getIssueDate()));
					if (ajanda.getSosyalmedya() != null) {
//						ArrayNode contractArray = mapper.createArrayNode();
						ObjectNode jsonC = mapper.createObjectNode();
						jsonC.put("id", ajanda.getSosyalmedya().getId());
						jsonC.put("title", ajanda.getSosyalmedya().getName());

//						contractArray.add(jsonC);
						json.put("sosyal", jsonC);
					}

					if (ajanda.getAccount() != null) {

						ArrayNode contractArray = mapper.createArrayNode();
						for (Account contract : ajanda.getAccount()) {
							ObjectNode jsonC = mapper.createObjectNode();
							jsonC.put("id", contract.getId());
							jsonC.put("title", contract.getName());

							contractArray.add(jsonC);
						}

						json.put("account", contractArray);
					}

					String jsonreport = mapper.writeValueAsString(json);
					return jsonreport;
				}
			}

		} catch (Exception e) {
			logger.error("ERROR load Ajanda failed from loading this id: " + id + " with error" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/ajanda/save", method = RequestMethod.POST)
	@ResponseBody
	public String saveAjanda(HttpServletResponse response, @Valid @ModelAttribute AdminAjandaForm form,
			Authentication authentication) {

		try {

			User user = (User) authentication.getPrincipal();
			if (form.getId() != null) {

				Ajanda ajanda = ajandaService.loadAjandaById(form.getId());
				ajanda = form.manipulatAgent(ajanda);
				ajanda.setAccount(new HashSet<>(userService.findAccontsByIds(form.getAccount())));
				ajanda.setSosyalmedya(sosyalMedyaService.loadSosyalMedyaById(form.getSosyal()));
				ajandaService.save(ajanda);
				logger.fatal(user.getAccount().getName() + " MODIFY AJANDA ID : " + form.getId());
			} else {

				Ajanda ajanda = form.createAgent();
				ajanda.setAccount(new HashSet<>(userService.findAccontsByIds(form.getAccount())));
				ajanda.setSosyalmedya(sosyalMedyaService.loadSosyalMedyaById(form.getSosyal()));
				ajandaService.save(ajanda);
				logger.fatal(user.getAccount().getName() + " CRETAE NEW AJANDA BY NANE : " + form.getTitle());

			}

			return null;
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());

			return null;
		}

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/ajanda/remove", method = RequestMethod.POST)
	@ResponseBody
	public String deleteCover(HttpServletResponse response, @RequestParam(value = "ids") List<Long> ids,
			Authentication authentication) {
		try {
			User user = (User) authentication.getPrincipal();
			if (user.getAccount().isAdmin() || user.getAccount().isCallAr() || user.getAccount().isCallFa()) {

				List<Ajanda> ajandas = ajandaService.loadAjandasByIds(ids);
				for (Ajanda ajanda : ajandas) {

					ajanda.setAccount(new HashSet<Account>());

					ajandaService.save(ajanda);

				}
				ajandaService.removeAjandas(ajandas);
				logger.fatal(user.getAccount().getName() + " REMOVE AJANDAS WITH IDS"
						+ ids.toString().replace("[", "").replace("]", ""));
			}

		} catch (Exception e) {
			logger.error("ERROR deleting COVERS with ids= " + ids + " with error: " + e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());
		}
		return null;
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/ajanda/done", method = RequestMethod.POST)
	@ResponseBody
	public String doneAjanda(HttpServletResponse response, @RequestParam(value = "ids") List<Long> ids,
			@RequestParam(value = "status") int status, @RequestParam(value = "reply") String reply,
			Authentication authentication) {
		try {
			User user = (User) authentication.getPrincipal();

			List<Ajanda> ajandas = ajandaService.loadAjandasByIds(ids);
			for (Ajanda ajanda : ajandas) {

				Reply rep = new Reply();
				
				switch (status) {
				case 1:
					
					rep.setNote(" AÇIK - "+reply);
					break;
				case 2:
					rep.setNote(" BEKLEMEDE - "+reply);
					
					break;
				case 3:
					rep.setNote(" YAPILDI - "+reply);
					
					break;
				case 4:
					rep.setNote(" KAPANDI - "+reply);
					
					break;

				default:
					break;
				}
				Set<Account> accs = new HashSet<Account>();
				
				accs.add(user.getAccount());
				
				rep.setAjanda(ajanda);
				rep.setSosyalmedya(ajanda.getSosyalmedya());
				rep.setAccount(accs);
				rep.setCreateDate(Time.today());
				sosyalMedyaService.save(rep);
				ajanda.getSosyalmedya().setLastUpdate(reply);
				ajanda.getSosyalmedya().setLastDate(Time.today());
				sosyalMedyaService.save(ajanda.getSosyalmedya());

				ajanda.setStatus(status);
				ajandaService.save(ajanda);

			}
			logger.fatal(user.getAccount().getName() + " CHANGE STATUS OF AJANDAS WITH IDS"
					+ ids.toString().replace("[", "").replace("]", ""));

		} catch (Exception e) {
			logger.error("ERROR deleting COVERS with ids= " + ids + " with error: " + e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());
		}
		return null;
	}
	
	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/crm/printable/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadPrintable(HttpServletResponse response, @RequestParam(value = "smid") long id
			) {
		try {

			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			
			Ajanda ajanda = ajandaService.loadAjandaById(id);

			SosyalMedya sosyalMedya = ajanda.getSosyalmedya();
			ObjectNode json = mapper.createObjectNode();

			json.put("id", sosyalMedya.getId());
			json.put("mail", sosyalMedya.getEmail());
			json.put("name", sosyalMedya.getName());
			json.put("nation", sosyalMedya.getNation());
			json.put("gsm", sosyalMedya.getGsm());
			json.put("createDate", Time.ToTurkishDateMonthDateOnly(sosyalMedya.getCreateDate()));
			if (sosyalMedya.getLastDate() != null) {
				json.put("lastDate", Time.ToTurkishDateMonthDateOnly(sosyalMedya.getLastDate()));

			}
			json.put("first_info", sosyalMedya.getFirstInfo());

			if (sosyalMedya.getLastUpdate() != null) {

				json.put("last_info", sosyalMedya.getLastUpdate());
			}

			json.put("type", sosyalMedya.getType());
			json.put("priority", sosyalMedya.getPriority());

			ArrayNode userCats = mapper.createArrayNode();
			for (Account account : ajanda.getAccount()) {
				ObjectNode acc = mapper.createObjectNode();
				acc.put("title", account.getName());
				acc.put("id", account.getId());
				userCats.add(acc);
			}
			json.put("accounts", userCats);

			if (ajanda.getReply() != null) {
				List<Reply> mainList = new ArrayList<Reply>();
				mainList.addAll(ajanda.getReply());
				Sorting sorting = new Sorting();
				List<Reply> replies = sorting.SortReply(mainList);

				ArrayNode replyCats = mapper.createArrayNode();
				for (Reply reply : replies) {

					ObjectNode node = mapper.createObjectNode();
					node.put("id", reply.getId());
					node.put("note", reply.getNote());
					if (reply.getDsc() != null) {

						node.put("dsc", reply.getDsc());
					}
					node.put("date", Time.ToTurkishDateMonthDateOnly(reply.getCreateDate()));

					ArrayNode userReplyCats = mapper.createArrayNode();
					for (Account account : reply.getAccount()) {
						ObjectNode acc = mapper.createObjectNode();
						acc.put("title", account.getName());
						acc.put("id", account.getId());
						userReplyCats.add(acc);
					}
					node.put("accounts", userReplyCats);

					replyCats.add(node);

				}
				json.put("replies", replyCats);
			}

			String jsonreport = mapper.writeValueAsString(json);
			return jsonreport;

		} catch (Exception e) {
			logger.error("Error load printable ajanda in adminpanel: " + " with error" + e.getMessage());
			e.printStackTrace();
		}

		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}


}
