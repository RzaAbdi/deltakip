package rza.BPFco.controller.admin;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import rza.BPFco.form.AdminAgentForm;
import rza.BPFco.form.filter.AgentFilterForm;
import rza.BPFco.model.portfoys.Agent;
import rza.BPFco.service.PersonService;
import rza.BPFco.service.UserService.User;

@Controller
public class AdminAgentController {

	@Autowired
	private PersonService personService;

	protected static final Logger logger = LogManager.getLogger();

	@RequestMapping(value = { "/adminpanel/agent_manage" }, method = RequestMethod.GET)
	public String index() {
		
		return "html/admin/agent_manage";
	}
	
	

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "adminpanel/json/agents/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadAgents(HttpServletResponse response, @ModelAttribute AgentFilterForm form,Authentication authentication) {
		try {
			
			if(authentication!=null) {
			User user = 	(User) authentication.getPrincipal();
			
			if(user!=null) {
				Page<Agent> pageAuthors = personService.loadAllAgents(form);
				ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
						false);
				ArrayNode jsonRep = mapper.createArrayNode();
				for (Agent agent : pageAuthors.getContent()) {
					ObjectNode json = mapper.createObjectNode();
					json.put("id", agent.getId());
					json.put("name", agent.getName());
					json.put("nationality", agent.getNationality());
					json.put("gsm1", agent.getGsm1());
					json.put("source", agent.getSource());
					json.put("company", agent.getCompany());
					jsonRep.add(json);
				}

				ObjectNode page = mapper.createObjectNode();
				page.put("total", pageAuthors.getTotalPages());
				page.put("number", pageAuthors.getNumber() + 1);
				jsonRep.add(page);
				logger.fatal(user.getAccount().getName() + " ENTER TO AGENT MANAGEMENT PAGE");
				String jsonreport = mapper.writeValueAsString(jsonRep);
				return jsonreport;
				
			}
			}
			
			

		} catch (Exception e) {
			logger.error("Error load agents in adminpanel: " + " with error" + e.getMessage());
			e.printStackTrace();
		}

		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/agent/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadAgentById(@RequestParam(value = "id") long id,Authentication authentication) {
		try {
			
			if(authentication!=null) {
				User user = (User) authentication.getPrincipal();
				
				if(user!=null) {
					ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
							false);
					ObjectNode json = mapper.createObjectNode();
					Agent agent = personService.loadAgentById(id);

					json.put("id", agent.getId());
					json.put("name", agent.getName());
					json.put("gsm1", agent.getGsm1());
					json.put("gsm2", agent.getGsm2());
					json.put("passport", agent.getPassport());
					json.put("tax", agent.getTax());
					json.put("address", agent.getAddress());
					json.put("nationality", agent.getNationality());
					json.put("tc", agent.getTc());
					json.put("company", agent.getCompany());

					logger.fatal(user.getAccount().getName()+ " LOAD AGENT BY NAME: " +agent.getName() );

					String jsonreport = mapper.writeValueAsString(json);
					return jsonreport;
				}
			}
			
			
		

		} catch (Exception e) {
			logger.error("ERROR load AGENT failed from loading this id: " + id + " with error" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/agent/save", method = RequestMethod.POST)
	@ResponseBody
	public String saveCustomer(HttpServletResponse response, @Valid @ModelAttribute AdminAgentForm form,
			Authentication authentication) {

		try {

			User user = (User) authentication.getPrincipal();

			if (user.isAdmin()) {

				if (form.getId() != null) {

					Agent agent = personService.loadAgentById(form.getId());
					agent = form.manipulatAgent(agent);
					personService.save(agent);
					logger.fatal(user.getAccount().getName() + " MODIFY AGENT ID : "+ form.getId());
				} else {

					Agent agenta = personService.findByAgentName(form.getName());
					if (agenta == null) {
						Agent  agent = form.createAgent();
						personService.save(agent);
						logger.fatal(user.getAccount().getName()+ " CRETAE NEW AGENT BY NANE : " + form.getName() );

					} else {
						response.setStatus(HttpStatus.BAD_REQUEST.value());
						logger.error(" CREATE NEW AGENT WITH NAME "+ form.getName() + " BECAUSE NAME SIMILARITY");
						return "1";
					}

				}

			}

			return null;
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());

			return null;
		}

	}

}
