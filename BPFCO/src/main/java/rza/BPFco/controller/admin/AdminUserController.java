package rza.BPFco.controller.admin;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import rza.BPFco.form.AccountForm;
import rza.BPFco.form.filter.UserFilterForm;
import rza.BPFco.model.Account;
import rza.BPFco.model.UserRole;
import rza.BPFco.service.UserService;
import rza.BPFco.service.UserService.User;
import rza.BPFco.utilities.Time;

@Controller
public class AdminUserController {

	protected static final Logger logger = LogManager.getLogger();

	@Autowired
	private UserService userService;

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/accounts/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadUsers(HttpServletResponse response, @ModelAttribute UserFilterForm form) {
		try {
			Page<Account> pageUsers = userService.loadAccounts(form);
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			ArrayNode jsonRep = mapper.createArrayNode();
			for (Account account : pageUsers.getContent()) {
				ObjectNode json = mapper.createObjectNode();
				json.put("id", account.getId());
				json.put("email", account.getEmail());
				json.put("active", account.isActive());
				json.put("admin", account.isAdmin());
				json.put("name", account.getName());

				if (account.getCreateDate() != null)
					json.put("createdate", Time.ToTurkishDateOnly(account.getCreateDate()));

				if (account.getLastsign() != null)
					json.put("lastsign", Time.ToTurkishDateOnly(account.getLastsign()));

				json.put("role", account.getRole());

//		 for (UserRole ur : account.getUserroles()) {
//			 
//		     json.put("role", ur.getName());
//		    }
				jsonRep.add(json);
			}

			ObjectNode page = mapper.createObjectNode();
			page.put("total", pageUsers.getTotalPages());
			page.put("number", pageUsers.getNumber() + 1);
			jsonRep.add(page);

			String jsonreport = mapper.writeValueAsString(jsonRep);
			return jsonreport;

		} catch (Exception e) {
			logger.error("Error load accounts in adminpanel: " + " with error" + e.getMessage());
			e.printStackTrace();
		}

		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/account/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadAuthorById(@RequestParam(value = "id") long id) {
		try {
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			// ArrayNode jsonRep = mapper.createArrayNode();
			ObjectNode json = mapper.createObjectNode();
			Account account = userService.loadAccountByIdFetchRole(id);

			json.put("id", account.getId());
			json.put("mail", account.getEmail());
			json.put("name", account.getName());
			json.put("admin", account.isAdmin());
			json.put("active", account.isActive());
			json.put("watcher", account.isWatcher());
			json.put("callFa", account.isCallFa());
			json.put("callAr", account.isCallAr());
			

			String jsonreport = mapper.writeValueAsString(json);
			return jsonreport;

		} catch (Exception e) {
			logger.error("ERROR load ACCOUNT failed from loading this id: " + id + " with error" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/account/save", method = RequestMethod.POST)
	@ResponseBody
	public String saveTag(HttpServletResponse response, @Valid @ModelAttribute AccountForm form,
			Authentication authentication) {

		try {

			User user = (User) authentication.getPrincipal();

			if (user.isAdmin()) {

				if (form.getId() != null) {

					Account account = userService.loadAccountByIdFetchRole(form.getId());

					if (form.isAdmin()) {

						account.setRole(UserRole.admin);

					} else {
						account.setRole(UserRole.user);

					}

					account = form.manipulatAccount(account);

					userService.save(account);

				} else {
//
				}

			}
			logger.fatal(user.getAccount().getName() + " SAVE ACCOUNT WITH ID:" + form.getId());
			return null;
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());
			logger.error(" SAVE ACCOUNT WITH ID= " + form.getId() + " WITH ERROR: " + e.getMessage());
			return null;
		}

	}

}
