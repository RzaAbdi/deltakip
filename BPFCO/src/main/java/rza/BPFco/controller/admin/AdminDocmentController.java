package rza.BPFco.controller.admin;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import rza.BPFco.config.initProperties;
import rza.BPFco.form.AdminDocumentForm;
import rza.BPFco.form.filter.DocumentFilterForm;
import rza.BPFco.model.Contract;
import rza.BPFco.model.portfoys.Agent;
import rza.BPFco.model.portfoys.Customer;
import rza.BPFco.model.portfoys.DocType;
import rza.BPFco.model.portfoys.Document;
import rza.BPFco.model.portfoys.Installment;
import rza.BPFco.service.ContractService;
import rza.BPFco.service.DocumentService;
import rza.BPFco.service.DocumentService.FileType;
import rza.BPFco.service.PersonService;
import rza.BPFco.service.PortfoyService;
import rza.BPFco.service.UserService.User;
import rza.BPFco.utilities.Randomize;
import rza.BPFco.utilities.Time;

@Controller
public class AdminDocmentController {

	@Autowired
	private DocumentService documentService;
	
	@Autowired
	private PortfoyService portfoyService;

	@Autowired
	private PersonService personService;

	@Autowired
	private ContractService contractService;

	protected static final Logger logger = LogManager.getLogger();

	@RequestMapping(value = { "/docs" }, method = RequestMethod.GET)
	public String index() {

		return "html/admin/docs_manage";
	}

 

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/docs/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadAgents(HttpServletResponse response, @ModelAttribute DocumentFilterForm form,
			Authentication authentication) {
		try {
//			
			User user = (User) authentication.getPrincipal();
			if (user != null) {

				if (user.isAdmin()||user.isWatcher()) {
					//
					form.setAdmin(true);

				} else {
					//
					form.setAdmin(false);
					form.setTemsilci(user.getUserId());

				}

			 
			
//			if(form.getCustomerid()!=null) {
//			Set<Installment> inss = contractService.loadInstallmentsByContractId(form.getCustomerid());
//				List<Long> installments = new ArrayList<Long>();
//				
//				for (Installment in : inss) {
//					installments.add(in.getId());
//				}
//				
//				form.setInstallments(installments);
//				
//			}

			Page<Document> pageAuthors = documentService.loadAll(form);
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			ArrayNode jsonRep = mapper.createArrayNode();
			for (Document document : pageAuthors.getContent()) {
				ObjectNode json = mapper.createObjectNode();
				json.put("id", document.getId());
				json.put("title", document.getTitle());
				json.put("path", document.getFile_path());
				json.put("doctype", document.getDoctype().iterator().next().getTitle());
//				json.put("path", initProperties.FILEFOLDERNAME+document.getFile_path());

				String[] parts = document.getFile_path().split("\\.");
				if (parts.length > 1) {

					String suffix = parts[1];
					json.put("type", suffix);
				}

				if (document.getCreate_date() != null) {

					json.put("create_date", Time.ToTurkishDateMonthDateOnly(document.getCreate_date()));
				}

				if (document.getEditDate() != null) {

					json.put("edit_date", Time.ToTurkishDateMonthDateOnly(document.getEditDate()));
				}

				jsonRep.add(json);
			}

			ObjectNode page = mapper.createObjectNode();
			page.put("total", pageAuthors.getTotalPages());
			page.put("number", pageAuthors.getNumber() + 1);
			jsonRep.add(page);
			String jsonreport = mapper.writeValueAsString(jsonRep);
			return jsonreport;
			} 
		} catch (Exception e) {
			logger.error("Error load docs in adminpanel: " + " with error" + e.getMessage());
			e.printStackTrace();
		}

		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/doc/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadAgentById(@RequestParam(value = "id") long id,Authentication authentication) {
		try {
			
			User user = (User) authentication.getPrincipal();
			
			if(user!=null) {
				
				ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
						false);
				ObjectNode json = mapper.createObjectNode();
				
				Document doc = documentService.loadDocFetchAll(id);
				
				json.put("id", doc.getId());
				json.put("title", doc.getTitle());
				if (doc.getCreate_date() != null) {
					
					json.put("create_date", Time.ToSplitedDate(doc.getCreate_date()));
				}
				
				if (doc.getEditDate() != null) {
					
					json.put("edit_date", Time.ToSplitedDate(doc.getEditDate()));
				}
				json.put("path", initProperties.FILEFOLDERNAME + doc.getFile_path());
				
				if (doc.getContract() != null) {
					
					ArrayNode contractArray = mapper.createArrayNode();
					for (Contract contract : doc.getContract()) {
						ObjectNode jsonC = mapper.createObjectNode();
						jsonC.put("id", contract.getId());
						jsonC.put("title", contract.getContract_no());
						
						contractArray.add(jsonC);
					}
					
					json.put("contracts", contractArray);
				}
				
				if (doc.getAgent() != null) {
					ArrayNode Array = mapper.createArrayNode();
					for (Agent contract : doc.getAgent()) {
						ObjectNode node = mapper.createObjectNode();
						node.put("id", contract.getId());
						node.put("title", contract.getName());
						
						Array.add(node);
					}
					
					json.put("agents", Array);
				}
				
				if (doc.getCustomer() != null) {
					ArrayNode Array = mapper.createArrayNode();
					for (Customer customer : doc.getCustomer()) {
						ObjectNode node = mapper.createObjectNode();
						node.put("id", customer.getId());
						node.put("title", customer.getFname());
						Array.add(node);
					}
					
					json.put("customers", Array);
				}
				
				if (doc.getInstallment() != null) {
					ArrayNode Array = mapper.createArrayNode();
					for (Installment installment : doc.getInstallment()) {
						ObjectNode node = mapper.createObjectNode();
						node.put("id", installment.getId());
						node.put("title", installment.getId());
						Array.add(node);
					}
					
					json.put("installments", Array);
				}
				
				if (doc.getDoctype() != null) {
					ArrayNode Array = mapper.createArrayNode();
					for (DocType type : doc.getDoctype()) {
						ObjectNode node = mapper.createObjectNode();
						node.put("id", type.getId());
						node.put("title", type.getTitle());
						Array.add(node);
					}
					
					json.put("doctype", Array);
				}
				if (doc.getSection() != null) {
				 
						ObjectNode node = mapper.createObjectNode();
						node.put("id", doc.getSection().getId());
						node.put("title", doc.getSection().getTitle());
					 
					 
					
					json.put("sections", node);
				}
				
				String jsonreport = mapper.writeValueAsString(json);
				logger.fatal(user.getAccount().getName()+ " LOAD DOC BY ID: " +id );
				
				return jsonreport;
			}
			

		} catch (Exception e) {
			logger.error("ERROR load DOC failed from loading this id: " + id + " with error" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/doc/save", method = RequestMethod.POST)
	@ResponseBody
	public String saveCustomer(HttpServletResponse response, @Valid @ModelAttribute AdminDocumentForm form,
			@RequestPart(value = "myNewFileName", required = false) MultipartFile file, Authentication authentication) {

		try {
			List<DocType> docTypes = null;
			String random = "_" + Randomize.randomNumber() + ".";

			User user = (User) authentication.getPrincipal();

			if (user.isAdmin()) {

				if (form.getId() != null) {

					Document doc = documentService.loadDocumentById(form.getId());
					if (file != null) {
						String str = file.getContentType();
						String[] parts = str.split("/");
						String suffix = parts[1];
//						String name = file.getOriginalFilename();
						// chackee file name
						Document docu = documentService.findByDocPathName(random + suffix);
						if (docu != null) {
							response.setStatus(HttpStatus.BAD_REQUEST.value());
							return "0";
						}

						String docPath = initProperties.FILEFOLDERNAME + random + suffix;

						File pest = new File(docPath);
						file.transferTo(pest);

						switch (suffix) {
						case "jpg":
							form.setType(FileType.jpg.ordinal());
							break;
						case "jpeg":
							form.setType(FileType.jpg.ordinal());
							break;
						case "pdf":
							form.setType(FileType.Pdf.ordinal());
							break;
						case "vnd.openxmlformats-officedocument.wordprocessingml.document":
							form.setType(FileType.docx.ordinal());
							break;
						case "msword":
							form.setType(FileType.docx.ordinal());
							break;

						default:
							break;
						}
						doc.setFile_path(random + suffix);
					}
					if(form.getSections()!=null) {
						
						doc.setSection(portfoyService.loadSectionById(form.getSections()));
					}else {
						doc.setSection(null);
					}
					doc.setAccount(user.getAccount());
					doc = form.manipulatDocument(doc);

					if (form.getDoctype() != null || form.getDoctype().size() > 1) {

						docTypes = documentService.findDocmentTypesByIds(form.getDoctype());
						doc.setDoctype(new HashSet<DocType>(docTypes));
						
						if(!form.getTitle().isEmpty()) {
							doc.setTitle(form.getTitle()+Randomize.TwoDigit()+Randomize.TwoDigit());
							
						}else {
							
							doc.setTitle(docTypes.iterator().next().getTitle()+Randomize.TwoDigit()+Randomize.TwoDigit());
						}
						
						
					}

					if (form.getAgents().size() > 0) {
						doc.setAgent(new HashSet<Agent>(personService.findAgentsByIds(form.getAgents())));
					}

					if (form.getContracts().size() > 0) {
						doc.setContract(new HashSet<Contract>(contractService.findContractsByIds(form.getContracts())));
					}

					if (form.getCustomers().size() > 0) {
						doc.setCustomer(new HashSet<Customer>(personService.findCustomersByIds(form.getCustomers())));
					}

					if (form.getInstallments().size() > 0) {
						doc.setInstallment(new HashSet<Installment>(
								contractService.findInstallmentsByIds(form.getInstallments())));
					}

					if (form.getCreate_date() != null && !form.getCreate_date().isEmpty()) {
						doc.setCreate_date(Time.ConvertToDate(form.getCreate_date()));
					} else {
						doc.setCreate_date(Time.todayDateOnly());

					}

					

					documentService.saveDocument(doc);
					logger.fatal(user.getAccount().getName()+ " MODIFY DOC BY FILE NAME: " + doc.getFile_path() );

				} else {

					Document document = documentService.findByDocumentTitle(form.getTitle());
					if (document == null) {
						// NEW DOC
						/// CHECK EXIST FILE
						if (file != null) {
							String str = file.getContentType();
							String[] parts = str.split("/");
							String suffix = parts[1];
//							String name = file.getOriginalFilename();

							Document docu = documentService.findByDocPathName(random + suffix);
							if (docu != null) {
								response.setStatus(HttpStatus.BAD_REQUEST.value());
								return "0";
							}
							String docPath = initProperties.FILEFOLDERNAME + random + suffix;

							File pest = new File(docPath);
							file.transferTo(pest);

							switch (suffix) {
							case "jpg":
								form.setType(FileType.jpg.ordinal());
								break;
							case "jpeg":
								form.setType(FileType.jpg.ordinal());
								break;
							case "pdf":
								form.setType(FileType.Pdf.ordinal());
								break;
							case "vnd.openxmlformats-officedocument.wordprocessingml.document":
								form.setType(FileType.docx.ordinal());
								break;
							case "msword":
								form.setType(FileType.docx.ordinal());
								break;

							default:
								break;
							}
							form.setFile_path(random + suffix);
							
							
							
							Document doc = form.createDocument();
							
							if(form.getSections()!=null) {
								
								doc.setSection(portfoyService.loadSectionById(form.getSections()));
							}else {
								doc.setSection(null);
							}
							
							// setAgent
							if (form.getDoctype() != null || form.getDoctype().size() > 1) {
								docTypes = documentService.findDocmentTypesByIds(form.getDoctype());
								doc.setDoctype(new HashSet<DocType>(docTypes));
								if(!form.getTitle().isEmpty()) {
									doc.setTitle(form.getTitle()+Randomize.TwoDigit()+Randomize.TwoDigit());
									
								}else {
									
									doc.setTitle(docTypes.iterator().next().getTitle()+Randomize.TwoDigit()+Randomize.TwoDigit());
								}
							}
							
							if (form.getAgents().size() > 0) {
								doc.setAgent(new HashSet<Agent>(personService.findAgentsByIds(form.getAgents())));
							}
							
							if (form.getContracts().size() > 0) {
								doc.setContract(
										new HashSet<Contract>(contractService.findContractsByIds(form.getContracts())));
							}
							
							if (form.getCustomers().size() > 0) {
								doc.setCustomer(
										new HashSet<Customer>(personService.findCustomersByIds(form.getCustomers())));
							}
							
							if (form.getInstallments().size() > 0) {
								doc.setInstallment(new HashSet<Installment>(
										contractService.findInstallmentsByIds(form.getInstallments())));
							}
							
							if (form.getCreate_date() != null && !form.getCreate_date().isEmpty()) {
								doc.setCreate_date(Time.ConvertToDate(form.getCreate_date()));
							} else {
								doc.setCreate_date(Time.todayDateOnly());
								
							}
							doc.setEditDate(Time.todayDateOnly());
							
							documentService.saveDocument(doc);
							logger.fatal(user.getAccount().getName()+ " CREATE DOC BY FILE NAME: " + doc.getFile_path() );
						}


					} else {
						logger.error(user.getAccount().getName()+ " WANT TO CRATE DOC BY FILE NAME " + form.getFile_path() );
						response.setStatus(HttpStatus.BAD_REQUEST.value());
						return "1";
					}

				}

			}

			return null;
		} catch (Exception e) {
			
			logger.error("SAVE DOC HAS AN ERROR: "+e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());

			return null;
		}

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/doc/remove", method = RequestMethod.POST)
	@ResponseBody
	public String deleteVariantvalue(HttpServletResponse response, @RequestParam(value = "ids") List<Long> ids,Authentication authentication) {
		try {

			
			User user = (User) authentication.getPrincipal();
			
			if(user!=null&&user.isAdmin()) {
				
				List<Document> documents = new ArrayList<Document>();
				
				
				for (Long docId : ids) {
					
//					Document doc = documentService.loadDocumentById(docId);
//					String path = doc.getFile_path();
//					String docPath = initProperties.FILEFOLDERNAME + path;
//					
//					File pest = new File(docPath);
					documents.add(new Document(docId));
					
					
				}
				
				documentService.remove(documents);
				logger.fatal(user.getAccount().getName() + " DELETE DOCS BY IDS: " +ids.toString().replace("[", "").replace("]", ""));
			}


		} catch (Exception e) {
			logger.error("ERROR deleting docs with ids= " + ids + " with error: " + e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());
		}
		return null;
	}

}
