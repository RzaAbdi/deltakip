package rza.BPFco.controller.admin;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import rza.BPFco.config.initProperties;
import rza.BPFco.form.filter.InstallmentFilterForm;
import rza.BPFco.model.portfoys.Installment;
import rza.BPFco.service.ContractService;
import rza.BPFco.service.UserService;
import rza.BPFco.service.UserService.User;
import rza.BPFco.utilities.Time;

@Controller
public class AdminController {

	@Autowired
	UserService userService;

	@Autowired
	ContractService contractService;

	@Autowired
	public HttpSession session;

	protected static final Logger logger = LogManager.getLogger();

	@RequestMapping(value = "/adminpanel/**", method = RequestMethod.GET)
	public String index(InstallmentFilterForm form, Authentication authentication) {

		if (authentication != null) {

			User user = (User) authentication.getPrincipal();

			if (user != null && user.getAccount().isAdmin()) {
				String before = Time.ToSplitedDate((Time.fifteenDaysBefore()));
				String after = Time.ToSplitedDate((Time.fifteenNextDays()));
				form.setP(1);
				form.setPsize(30);
//				form.setChackNotify(true);
//				form.setReminding(false);
//				form.setWarning(false);
//				form.setThanks(false);

				form.setDaterange(before + "-" + after);
				Page<Installment> pageIns = contractService.loadAllInstallments(form);
				ArrayList<Installment> list = new ArrayList<Installment>();

				if (pageIns != null)
					for (Installment installment : pageIns) {

						if (installment.isPaid()) {
							//
							if (!installment.isThanks()) {
								//
								installment.setType(1);// thanks
								list.add(installment);
							}

						}
						if (!installment.isPaid()) {
							if (!installment.isPartial()) {

								if (Time.compare(installment.getIssueDate()) == 1) {
									//
									if (!installment.isWarning()) {
										installment.setType(2);// warning
										list.add(installment);
									}
								}
								if (Time.compare(installment.getIssueDate()) == 3) {
									//
									if (!installment.isReminding()) {
										installment.setType(0);// reminding
										list.add(installment);
									}
								}

							}
						}

					}

				session.setAttribute("notifies", list);

				return "html/admin/home";
			}

		}
		return null;

	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/logs/load", method = RequestMethod.POST)
	@ResponseBody
	public String loadAgents(HttpServletResponse response) {
		try {
//			User user = (User) authentication.getPrincipal();
			FileInputStream fstream = new FileInputStream(initProperties.LOGFOLDERNAME + "deltakip.log");
			InputStreamReader isr = new InputStreamReader(fstream, StandardCharsets.UTF_8);
			BufferedReader br = new BufferedReader((isr));
			String strLine;

			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
					false);
			ArrayNode jsonRep = mapper.createArrayNode();

			List<String> tmp = new ArrayList<String>();

			while ((strLine = br.readLine()) != null) {

				tmp.add(strLine);

			}

			for (int i = tmp.size() - 1; i >= 0; i--) {
				ObjectNode json = mapper.createObjectNode();

				String line = tmp.get(i);

				String before = line.substring(0, line.lastIndexOf("["));
				String[] splited = before.split("\\s+");
				json.put("type", splited[2]);

				if (splited[2].contentEquals("FATAL")) {
					json.put("message", line.substring(line.lastIndexOf("]") + 1));
					json.put("date", splited[0]);
					json.put("time", splited[1].substring(0, splited[1].lastIndexOf(",")));
					json.put("today", Time.ToTurkishDateMonthDateOnly(Time.today()));
					jsonRep.add(json);
				}

			}

			fstream.close();
			String jsonreport = mapper.writeValueAsString(jsonRep);
			return jsonreport;

		} catch (Exception e) {
			logger.error("Error load logs in adminpanel: " + " with error" + e.getMessage());
			e.printStackTrace();
		}

		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/adminpanel/json/log/loadBydate", method = RequestMethod.POST)
	@ResponseBody
	public String loadBydate(HttpServletResponse response, @RequestParam(value = "date") String date,
			Authentication authentication) {
		try {

			User user = (User) authentication.getPrincipal();
			if (user != null && date != null) {

				ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
						false);
				ArrayNode jsonRep = mapper.createArrayNode();
				FileInputStream fstream;
				String day, month, year;
				String[] parts = date.split("/");
				day = parts[1];
				month = parts[0];
				year = parts[2];

				if (Time.today().equals(Time.ConvertToDate(date))) {
					File file = new File(initProperties.LOGFOLDERNAME + "deltakip.log");
					if (!file.exists()) {
						return  mapper.writeValueAsString(jsonRep);
					}
					
					fstream = new FileInputStream(initProperties.LOGFOLDERNAME + "deltakip.log");

				} else {
					
					String path = initProperties.LOGFOLDERNAME + "Archive/" + year + "-" + month + "/"
							+ month + "-" + day + "-" + year + ".log";
					
					File file = new File(path);
					if (!file.exists()) {
						return  mapper.writeValueAsString(jsonRep);
					}
					
					fstream = new FileInputStream(path);

				}

				InputStreamReader isr = new InputStreamReader(fstream, StandardCharsets.UTF_8);
				BufferedReader br = new BufferedReader((isr));
				String strLine;

				

				List<String> tmp = new ArrayList<String>();

				while ((strLine = br.readLine()) != null) {

					tmp.add(strLine);

				}

				for (int i = tmp.size() - 1; i >= 0; i--) {
					ObjectNode json = mapper.createObjectNode();

					String line = tmp.get(i);

					String before = line.substring(0, line.lastIndexOf("["));
					if(!before.isEmpty()) {
						
						String[] splited = before.split("\\s+");
						json.put("type", splited[2]);
						
						if (splited[2].contentEquals("FATAL")) {
							json.put("message", line.substring(line.lastIndexOf("]") + 1));
							json.put("date", splited[0]);
							json.put("today", Time.ToTurkishDateMonthDateOnly(Time.ConvertToDate(date)));
							json.put("time", splited[1].substring(0, splited[1].lastIndexOf(",")));
							
							jsonRep.add(json);
						}
					}

				}

				fstream.close();
				String jsonreport = mapper.writeValueAsString(jsonRep);
				
				return jsonreport;

			}

		} catch (Exception e) {
			logger.error("ERROR LOAD LOG BY DATE: " +date+ " WITH ERROR: " + e.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST.value());
		}
		return null;
	}

	@RequestMapping(value = "/adminpanel/log", method = RequestMethod.GET)
	public String log(Model model) {
		model.addAttribute("yester", Time.ToSplitedDate(Time.addDaysAfterDates(-1, Time.todayDateOnly())));
		model.addAttribute("tommorow", Time.ToSplitedDate(Time.addDaysAfterDates(1, Time.todayDateOnly())));
		model.addAttribute("today", Time.ToSplitedDate(Time.todayDateOnly()));
		model.addAttribute("date", " [ "+Time.ToTurkishDateOnly(Time.today())+ " ] ");
		return "html/admin/log";

	}

	@RequestMapping(value = "/changedate", method = RequestMethod.GET)
	public String changedate(ModelMap map, @RequestParam(value = "date") Date date,
			@RequestParam(value = "day") boolean day) {

		if (day) {

			map.addAttribute("yester", Time.ToSplitedDate(Time.addDaysAfterDates(-1, date)));
			map.addAttribute("tommorow", Time.ToSplitedDate(Time.addDaysAfterDates(1, date)));
			map.addAttribute("date", " [ "+Time.ToTurkishDateOnly(date)+ " ] ");

		} else {

			map.addAttribute("yester", Time.ToSplitedDate(Time.addDaysAfterDates(-1, date)));
			map.addAttribute("tommorow", Time.ToSplitedDate(Time.addDaysAfterDates(1, date)));
			map.addAttribute("date", " [ "+Time.ToTurkishDateOnly(date)+ " ] ");
		}

		return "html/admin/log :: #Buttons";
	}

	@RequestMapping(value = "/changeBydate", method = RequestMethod.GET)
	public String changeBydate(ModelMap map, @RequestParam(value = "date") Date date) {

		map.addAttribute("date", " [ "+Time.ToTurkishDateOnly(date)+ " ] ");
		map.addAttribute("yester", Time.ToSplitedDate(Time.addDaysAfterDates(-1, date)));
		map.addAttribute("tommorow", Time.ToSplitedDate(Time.addDaysAfterDates(0, date)));

		return "html/admin/log :: #Buttons";
	}

	@RequestMapping(value = "/changedateToday", method = RequestMethod.GET)
	public String changedateToday(ModelMap map, @RequestParam(value = "date") Date date) {

		map.addAttribute("date", " [ "+Time.ToTurkishDateOnly(date)+ " ] ");
		map.addAttribute("yester", Time.ToSplitedDate(Time.addDaysAfterDates(-1, date)));
		map.addAttribute("tommorow", Time.ToSplitedDate(Time.addDaysAfterDates(1, date)));

		return "html/admin/log :: #Buttons";
	}

	@RequestMapping(value = "/outbox", method = RequestMethod.GET)
	public String pnles() {
		return "html/admin/outbox";

	}

	@RequestMapping(value = "/adminpanel/user_manage", method = RequestMethod.GET)
	public String user() {
		return "html/admin/user_manage";

	}

}
