
package rza.BPFco.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import rza.BPFco.config.initProperties;
import rza.BPFco.form.filter.ChaseFilterForm;
import rza.BPFco.form.filter.SosyalMedyaFilterForm;
import rza.BPFco.model.Account;
import rza.BPFco.model.Contract;
import rza.BPFco.model.Portfoy;
import rza.BPFco.model.Tag;
import rza.BPFco.model.UserRole;
import rza.BPFco.model.portfoys.Agent;
import rza.BPFco.model.portfoys.Blok;
import rza.BPFco.model.portfoys.Chase;
import rza.BPFco.model.portfoys.Customer;
import rza.BPFco.model.portfoys.DocType;
import rza.BPFco.model.portfoys.Installment;
import rza.BPFco.model.portfoys.Kat;
import rza.BPFco.model.portfoys.Proje;
import rza.BPFco.model.portfoys.Section;
import rza.BPFco.model.portfoys.SosyalMedya;
import rza.BPFco.service.ContractService;
import rza.BPFco.service.DocumentService;
import rza.BPFco.service.PersonService;
import rza.BPFco.service.PortfoyService;
import rza.BPFco.service.SmsService;
import rza.BPFco.service.SosyalMedyaService;
import rza.BPFco.service.SosyalMedyaService.Priority;
import rza.BPFco.service.UserService.User;
import rza.BPFco.service.TagService;
import rza.BPFco.service.UserService;
import rza.BPFco.utilities.Source;
import rza.BPFco.utilities.Time;

@Controller
public class SearchController {

	@Autowired
	private UserService userService;

	@Autowired
	private PortfoyService portfoyService;
	
	@Autowired
	private PersonService personService;
	
	@Autowired
	private TagService tagService;

	@Autowired
	private ContractService contractService;
	
	@Autowired
	private SmsService smsService;
	@Autowired
	private DocumentService documentService;
	
	@Autowired
	private SosyalMedyaService sosyalMedyaService;
	
	static final Logger logger = LogManager.getLogger();
//
//	@RequestMapping(produces = "application/json; charset=UTF-8", value = "/json/load/accounts", method = RequestMethod.GET)
//	@ResponseBody
//	public String loadAcounts(HttpServletResponse response,
//			@RequestParam(value = "p", defaultValue = "1") int pagenumber) {
//
//		try {
//
//			// Translate translate = wordService.searchForTraslte(id);
//			Page<Account> accounts = userService.loadAccounts(pagenumber, 20);
//
//			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
//					false);
//
//			ArrayNode jsonRep = mapper.createArrayNode();
//			ObjectNode json;
//			for (Account acc : accounts) {
//				json = mapper.createObjectNode();
////				json.put("mail", acc.getEmail());
//				json.put("id", acc.getId());
////				json.put("active", acc.isActive());
////				json.put("admin", acc.isAdmin());
//				json.put("name", acc.getName());
//				jsonRep.add(json);
//			}
//
//			String jsonreport = mapper.writeValueAsString(jsonRep);
//			return jsonreport;
//		} catch (Exception e) {
//			e.printStackTrace();
//			logger.error("search controller for this keyword throw error :" + "" + " _ " + e.getMessage());
//		}
//
//		response.setStatus(HttpStatus.BAD_REQUEST.value());
//		return null;
//	}
//
//	 

	 
	@RequestMapping(produces="application/json; charset=UTF-8",value = "/json/search/users", method = RequestMethod.GET)
	@ResponseBody
	public String loadUsers(HttpServletResponse response, @RequestParam(value = "s") String searchTerm,@RequestParam(value = "p", defaultValue = "1",required = false) int pageNumber
			) {
		
		try {
			
			
			Page<Account> page = userService.searchAllUsers(searchTerm,pageNumber);
			
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
			
			ObjectNode content = mapper.createObjectNode();
			
			ArrayNode jsonRep = mapper.createArrayNode();
			
			for(Account account : page) {
				ObjectNode json = mapper.createObjectNode();
				json.put("id", account.getId());
				json.put("title", account.getName());
				 
				
				jsonRep.add(json);
			}
			
			content.put("content", jsonRep);
			
			ObjectNode jsonPage = mapper.createObjectNode();
			jsonPage.put("total", page.getTotalPages());
			jsonPage.put("number", page.getNumber() + 1);
			jsonPage.put("items", page.getTotalElements());
			content.put("page", jsonPage);
			
			
			String jsonreport = mapper.writeValueAsString(content);
			return jsonreport;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("search controller for this keyword throw error :" + searchTerm + " _ " + e.getMessage());
		}
		
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}	
	
	
//	@RequestMapping(produces="application/json; charset=UTF-8",value = "/json/search/products", method = RequestMethod.GET)
//	@ResponseBody
//	public String SearchSubSubCatsBySubId(HttpServletResponse response,Authentication authentication,@RequestParam(value = "s") String  search,@RequestParam(value = "p", defaultValue = "1") int pageNumber) {
//		
//		try {
//			
//			Page<Product> page = productService.searchProducts(search,pageNumber);
//			
//			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
//			
//			ObjectNode content = mapper.createObjectNode();
//			
//			ArrayNode jsonRep = mapper.createArrayNode();
//			
//			for(Product product : page) {
//				ObjectNode json = mapper.createObjectNode();
//				json.put("title_tr", product.getName());
//				json.put("id", product.getId());
//				jsonRep.add(json);
//			}
//			
//			content.put("content", jsonRep);
//			
//			ObjectNode jsonPage = mapper.createObjectNode();
//			jsonPage.put("total", page.getTotalPages());
//			jsonPage.put("number", page.getNumber() + 1);
//			jsonPage.put("items", page.getTotalElements());
//			content.put("page", jsonPage);
//			
//			
//			String jsonreport = mapper.writeValueAsString(content);
//			return jsonreport;
//		} catch (Exception e) {
//			e.printStackTrace();
//			logger.error("search controller for this keyword throw error :" + search + " _ " + e.getMessage());
//		}
//		
//		response.setStatus(HttpStatus.BAD_REQUEST.value());
//		return null;
//	}	
//	
	
	@RequestMapping(produces="application/json; charset=UTF-8",value = "/json/search/tags", method = RequestMethod.GET)
	@ResponseBody
	public String loadAuthors(HttpServletResponse response, @RequestParam(value = "s") String searchTerm,@RequestParam(value = "p", defaultValue = "1",required = false) int pageNumber
			) {
		
		try {
			
			
			Page<Tag> page = tagService.searchAllTags(searchTerm,pageNumber);
			
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
			
			ObjectNode content = mapper.createObjectNode();
			
			ArrayNode jsonRep = mapper.createArrayNode();
			
			for(Tag tags : page) {
				ObjectNode json = mapper.createObjectNode();
				json.put("id", tags.getId());
				json.put("title", tags.getTitle());
				 
				
				jsonRep.add(json);
			}
			
			content.put("content", jsonRep);
			
			ObjectNode jsonPage = mapper.createObjectNode();
			jsonPage.put("total", page.getTotalPages());
			jsonPage.put("number", page.getNumber() + 1);
			jsonPage.put("items", page.getTotalElements());
			content.put("page", jsonPage);
			
			
			String jsonreport = mapper.writeValueAsString(content);
			return jsonreport;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("search controller for this keyword throw error :" + searchTerm + " _ " + e.getMessage());
		}
		
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}	
	
	@RequestMapping(produces="application/json; charset=UTF-8",value = "/json/search/projects", method = RequestMethod.GET)
	@ResponseBody
	public String loadProjects(HttpServletResponse response, @RequestParam(value = "s") String searchTerm,@RequestParam(value = "p", defaultValue = "1",required = false) int pageNumber
			) {
		
		try {
			
			
			Page<Proje> page = portfoyService.searchAllProjects(searchTerm,pageNumber);
			
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
			
			ObjectNode content = mapper.createObjectNode();
			
			ArrayNode jsonRep = mapper.createArrayNode();
			
			for(Proje tags : page) {
				ObjectNode json = mapper.createObjectNode();
				json.put("id", tags.getId());
				json.put("title", tags.getTitle());
				 
				
				jsonRep.add(json);
			}
			
			content.put("content", jsonRep);
			
			ObjectNode jsonPage = mapper.createObjectNode();
			jsonPage.put("total", page.getTotalPages());
			jsonPage.put("number", page.getNumber() + 1);
			jsonPage.put("items", page.getTotalElements());
			content.put("page", jsonPage);
			
			
			String jsonreport = mapper.writeValueAsString(content);
			return jsonreport;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("search controller for this keyword throw error :" + searchTerm + " _ " + e.getMessage());
		}
		
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}	
	@RequestMapping(produces="application/json; charset=UTF-8",value = "/json/search/sosyalmedya", method = RequestMethod.GET)
	@ResponseBody
	public String loadsosyalmedya(HttpServletResponse response, @RequestParam(value = "s") String searchTerm,@RequestParam(value = "p", defaultValue = "1",required = false) int pageNumber
			) {
		
		try {
			
			
			Page<SosyalMedya> page = sosyalMedyaService.searchAllCustomers(searchTerm,pageNumber);
			
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
			
			ObjectNode content = mapper.createObjectNode();
			
			ArrayNode jsonRep = mapper.createArrayNode();
			
			for(SosyalMedya tags : page) {
				ObjectNode json = mapper.createObjectNode();
				json.put("id", tags.getId());
				json.put("title", tags.getName());
				
				
				jsonRep.add(json);
			}
			
			content.put("content", jsonRep);
			
			ObjectNode jsonPage = mapper.createObjectNode();
			jsonPage.put("total", page.getTotalPages());
			jsonPage.put("number", page.getNumber() + 1);
			jsonPage.put("items", page.getTotalElements());
			content.put("page", jsonPage);
			
			
			String jsonreport = mapper.writeValueAsString(content);
			return jsonreport;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("search controller for this keyword throw error :" + searchTerm + " _ " + e.getMessage());
		}
		
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}	
	
	@RequestMapping(produces="application/json; charset=UTF-8",value = "/json/search/bloks", method = RequestMethod.GET)
	@ResponseBody
	public String loadBloks(HttpServletResponse response, @RequestParam(value = "s") String searchTerm,@RequestParam(value = "p", defaultValue = "1",required = false) int pageNumber
			) {
		
		try {
			
			
			Page<Blok> page = portfoyService.searchAllBloks(searchTerm,pageNumber);
			
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
			
			ObjectNode content = mapper.createObjectNode();
			
			ArrayNode jsonRep = mapper.createArrayNode();
			
			for(Blok blok : page) {
				ObjectNode json = mapper.createObjectNode();
				json.put("id", blok.getId());
				json.put("title", blok.getTitle());
				 
				
				jsonRep.add(json);
			}
			
			content.put("content", jsonRep);
			
			ObjectNode jsonPage = mapper.createObjectNode();
			jsonPage.put("total", page.getTotalPages());
			jsonPage.put("number", page.getNumber() + 1);
			jsonPage.put("items", page.getTotalElements());
			content.put("page", jsonPage);
			
			
			String jsonreport = mapper.writeValueAsString(content);
			return jsonreport;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("search controller for this keyword throw error :" + searchTerm + " _ " + e.getMessage());
		}
		
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}	
	
	@RequestMapping(produces="application/json; charset=UTF-8",value = "/json/search/floors", method = RequestMethod.GET)
	@ResponseBody
	public String loadFloors(HttpServletResponse response, @RequestParam(value = "s") String searchTerm,@RequestParam(value = "p", defaultValue = "1",required = false) int pageNumber
			) {
		
		try {
			
			
			Page<Kat> page = portfoyService.searchAllFloors(searchTerm,pageNumber);
			
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
			
			ObjectNode content = mapper.createObjectNode();
			
			ArrayNode jsonRep = mapper.createArrayNode();
			
			for(Kat floor : page) {
				ObjectNode json = mapper.createObjectNode();
				json.put("id", floor.getId());
				json.put("title", floor.getTitle() + " / " + floor.getBlok().getTitle());
				 
				
				jsonRep.add(json);
			}
			
			content.put("content", jsonRep);
			
			ObjectNode jsonPage = mapper.createObjectNode();
			jsonPage.put("total", page.getTotalPages());
			jsonPage.put("number", page.getNumber() + 1);
			jsonPage.put("items", page.getTotalElements());
			content.put("page", jsonPage);
			
			
			String jsonreport = mapper.writeValueAsString(content);
			return jsonreport;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("search controller for this keyword throw error :" + searchTerm + " _ " + e.getMessage());
		}
		
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}	
	
	@RequestMapping(produces="application/json; charset=UTF-8",value = "/json/search/sections", method = RequestMethod.GET)
	@ResponseBody
	public String loadSecions(HttpServletResponse response, @RequestParam(value = "s") String searchTerm,@RequestParam(value = "p", defaultValue = "1",required = false) int pageNumber
			) {
		
		try {
			
			
			Page<Section> page = portfoyService.searchAllSections(searchTerm,pageNumber);
			
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
			
			ObjectNode content = mapper.createObjectNode();
			
			ArrayNode jsonRep = mapper.createArrayNode();
			
			for(Section section : page) {
				ObjectNode json = mapper.createObjectNode();
				json.put("id", section.getId());
				json.put("title","NO:" + section.getTitle() + "  KAT: " + section.getKat().getTitle() + " BLOK: " + section.getKat().getBlok().getTitle() );
				 
				
				jsonRep.add(json);
			}
			
			content.put("content", jsonRep);
			
			ObjectNode jsonPage = mapper.createObjectNode();
			jsonPage.put("total", page.getTotalPages());
			jsonPage.put("number", page.getNumber() + 1);
			jsonPage.put("items", page.getTotalElements());
			content.put("page", jsonPage);
			
			
			String jsonreport = mapper.writeValueAsString(content);
			return jsonreport;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("search controller for this keyword throw error :" + searchTerm + " _ " + e.getMessage());
		}
		
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}	
	
	@RequestMapping(produces="application/json; charset=UTF-8",value = "/json/search/open_portfoys", method = RequestMethod.GET)
	@ResponseBody
	public String loadPortfoys(HttpServletResponse response, @RequestParam(value = "s") String searchTerm,@RequestParam(value = "p", defaultValue = "1",required = false) int pageNumber
			) {
		
		try {
			
			
			Page<Portfoy> page = portfoyService.searchAllOpenPortfoys(searchTerm,pageNumber);
			
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
			
			ObjectNode content = mapper.createObjectNode();
			
			ArrayNode jsonRep = mapper.createArrayNode();
			
			for(Portfoy portfoy : page) {
				ObjectNode json = mapper.createObjectNode();
				json.put("id", portfoy.getId());
				json.put("title",portfoy.getTitle()  );
				 
				
				jsonRep.add(json);
			}
			
			content.put("content", jsonRep);
			
			ObjectNode jsonPage = mapper.createObjectNode();
			jsonPage.put("total", page.getTotalPages());
			jsonPage.put("number", page.getNumber() + 1);
			jsonPage.put("items", page.getTotalElements());
			content.put("page", jsonPage);
			
			
			String jsonreport = mapper.writeValueAsString(content);
			return jsonreport;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("search controller for this keyword throw error :" + searchTerm + " _ " + e.getMessage());
		}
		
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}	
	@RequestMapping(produces="application/json; charset=UTF-8",value = "/json/search/portfoys", method = RequestMethod.GET)
	@ResponseBody
	public String loadAllPortfoys(HttpServletResponse response, @RequestParam(value = "s") String searchTerm,@RequestParam(value = "p", defaultValue = "1",required = false) int pageNumber
			) {
		
		try {
			
			
			Page<Portfoy> page = portfoyService.searchAllPortfoys(searchTerm,pageNumber);
			
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
			
			ObjectNode content = mapper.createObjectNode();
			
			ArrayNode jsonRep = mapper.createArrayNode();
			
			for(Portfoy portfoy : page) {
				ObjectNode json = mapper.createObjectNode();
				json.put("id", portfoy.getId());
				json.put("title",portfoy.getTitle()  );
				
				
				jsonRep.add(json);
			}
			
			content.put("content", jsonRep);
			
			ObjectNode jsonPage = mapper.createObjectNode();
			jsonPage.put("total", page.getTotalPages());
			jsonPage.put("number", page.getNumber() + 1);
			jsonPage.put("items", page.getTotalElements());
			content.put("page", jsonPage);
			
			
			String jsonreport = mapper.writeValueAsString(content);
			return jsonreport;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("search controller for this keyword throw error :" + searchTerm + " _ " + e.getMessage());
		}
		
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}	
	
	@RequestMapping(produces="application/json; charset=UTF-8",value = "/json/search/customers", method = RequestMethod.GET)
	@ResponseBody
	public String loadCustomers(HttpServletResponse response, @RequestParam(value = "s") String searchTerm,@RequestParam(value = "p", defaultValue = "1",required = false) int pageNumber
			) {
		
		try {
			Page<Customer> page;
			if (!initProperties.active) {
				page = personService.searchAllCustomers(searchTerm,pageNumber);
			}else {
				 boolean validated = userService.loadAccountByEmail();
					if (!validated)
						return null;

				 
				
				 page = personService.searchAllCustomers(searchTerm,pageNumber);
			}
			
			
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
			
			ObjectNode content = mapper.createObjectNode();
			
			ArrayNode jsonRep = mapper.createArrayNode();
			
			for(Customer customer : page) {
				ObjectNode json = mapper.createObjectNode();
				json.put("id", customer.getId());
				json.put("title",customer.getFname());
				 
				 
				
				jsonRep.add(json);
			}
			
			content.put("content", jsonRep);
			
			ObjectNode jsonPage = mapper.createObjectNode();
			jsonPage.put("total", page.getTotalPages());
			jsonPage.put("number", page.getNumber() + 1);
			jsonPage.put("items", page.getTotalElements());
			content.put("page", jsonPage);
			
			
			String jsonreport = mapper.writeValueAsString(content);
			return jsonreport;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("search controller for this keyword throw error :" + searchTerm + " _ " + e.getMessage());
		}
		
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}	
	@RequestMapping(produces="application/json; charset=UTF-8",value = "/json/search/customersName", method = RequestMethod.GET)
	@ResponseBody
	public String loadCustomersName(HttpServletResponse response, @RequestParam(value = "s") String searchTerm,@RequestParam(value = "p", defaultValue = "1",required = false) int pageNumber
			) {
		
		try {
			
			
			Page<Customer> page = personService.searchAllCustomers(searchTerm,pageNumber);
			
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
			
			ObjectNode content = mapper.createObjectNode();
			
			ArrayNode jsonRep = mapper.createArrayNode();
			
			for(Customer customer : page) {
				ObjectNode json = mapper.createObjectNode();
				json.put("id", customer.getFname());
				json.put("title",customer.getFname());
				
				
				
				jsonRep.add(json);
			}
			
			content.put("content", jsonRep);
			
			ObjectNode jsonPage = mapper.createObjectNode();
			jsonPage.put("total", page.getTotalPages());
			jsonPage.put("number", page.getNumber() + 1);
			jsonPage.put("items", page.getTotalElements());
			content.put("page", jsonPage);
			
			
			String jsonreport = mapper.writeValueAsString(content);
			return jsonreport;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("search controller for this keyword throw error :" + searchTerm + " _ " + e.getMessage());
		}
		
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}	
	@RequestMapping(produces="application/json; charset=UTF-8",value = "/json/search/chases", method = RequestMethod.GET)
	@ResponseBody
	public String loadChases(HttpServletResponse response, @RequestParam(value = "s") String searchTerm,@RequestParam(value = "p", defaultValue = "1",required = false) int pageNumber
			, @ModelAttribute ChaseFilterForm form,
			Authentication authentication) {
		
		try {
			
			User user = (User) authentication.getPrincipal();

			if (user != null) {

				if (user.isAdmin()) {
					//
					form.setAdmin(true);

				} else {
					//
					form.setAdmin(false);
					form.setAccount(user.getUserId());

				}

			}
			form.setSearch(searchTerm);
			
			
			Page<Chase> page = sosyalMedyaService.findChasesForAccount(form);
			
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
			
			ObjectNode content = mapper.createObjectNode();
			
			ArrayNode jsonRep = mapper.createArrayNode();
			
			for(Chase customer : page) {
				ObjectNode json = mapper.createObjectNode();
				json.put("id", customer.getId());
				json.put("title",customer.getTitle());
				
				
				
				jsonRep.add(json);
			}
			
			content.put("content", jsonRep);
			
			ObjectNode jsonPage = mapper.createObjectNode();
			jsonPage.put("total", page.getTotalPages());
			jsonPage.put("number", page.getNumber() + 1);
			jsonPage.put("items", page.getTotalElements());
			content.put("page", jsonPage);
			
			
			String jsonreport = mapper.writeValueAsString(content);
			return jsonreport;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("search controller for this keyword throw error :" + searchTerm + " _ " + e.getMessage());
		}
		
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}	
	
	@RequestMapping(produces="application/json; charset=UTF-8",value = "/json/search/agancy", method = RequestMethod.GET)
	@ResponseBody
	public String loadAgents(HttpServletResponse response, @RequestParam(value = "s") String searchTerm,@RequestParam(value = "p") int pageNumber
			) {
		
		try {
			
			
			Page<Agent> page = personService.searchAllAgents(searchTerm,pageNumber);
			
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
			
			ObjectNode content = mapper.createObjectNode();
			
			ArrayNode jsonRep = mapper.createArrayNode();
			
			for(Agent customer : page) {
				ObjectNode json = mapper.createObjectNode();
				json.put("id", customer.getId());
				json.put("title",customer.getName()  );
				 
				
				jsonRep.add(json);
			}
			
			content.put("content", jsonRep);
			
			ObjectNode jsonPage = mapper.createObjectNode();
			jsonPage.put("total", page.getTotalPages());
			jsonPage.put("number", page.getNumber() + 1);
			jsonPage.put("items", page.getTotalElements());
			content.put("page", jsonPage);
			
			
			String jsonreport = mapper.writeValueAsString(content);
			return jsonreport;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("search controller for this keyword throw error :" + searchTerm + " _ " + e.getMessage());
		}
		
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}	
	
	@RequestMapping(produces="application/json; charset=UTF-8",value = "/json/search/contracts", method = RequestMethod.GET)
	@ResponseBody
	public String loadContracts(HttpServletResponse response, @RequestParam(value = "s") String searchTerm,@RequestParam(value = "p", defaultValue = "1",required = false) int pageNumber
			) {
		
		try {
			
			
			Page<Contract> page = contractService.searchAllContracts(searchTerm,pageNumber);
			
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
			
			ObjectNode content = mapper.createObjectNode();
			
			ArrayNode jsonRep = mapper.createArrayNode();
			
			for(Contract contract : page) {
				ObjectNode json = mapper.createObjectNode();
				json.put("id", contract.getId());
				json.put("title",contract.getContract_no()  );
				 
				
				jsonRep.add(json);
			}
			
			content.put("content", jsonRep);
			
			ObjectNode jsonPage = mapper.createObjectNode();
			jsonPage.put("total", page.getTotalPages());
			jsonPage.put("number", page.getNumber() + 1);
			jsonPage.put("items", page.getTotalElements());
			content.put("page", jsonPage);
			
			
			String jsonreport = mapper.writeValueAsString(content);
			return jsonreport;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("search controller for this keyword throw error :" + searchTerm + " _ " + e.getMessage());
		}
		
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}	
	@RequestMapping(produces="application/json; charset=UTF-8",value = "/json/search/installments", method = RequestMethod.GET)
	@ResponseBody
	public String loadInstallments(HttpServletResponse response, @RequestParam(value = "s") String searchTerm,@RequestParam(value = "p", defaultValue = "1",required = false) int pageNumber
			) {
		
		try {
			
			
			Page<Installment> page = contractService.searchAllInstallments(searchTerm,pageNumber);
			
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
			
			ObjectNode content = mapper.createObjectNode();
			
			ArrayNode jsonRep = mapper.createArrayNode();
			
			for(Installment installment : page) {
				ObjectNode json = mapper.createObjectNode();
				json.put("id", installment.getId());
				json.put("title",installment.getId());
				
				
				jsonRep.add(json);
			}
			
			content.put("content", jsonRep);
			
			ObjectNode jsonPage = mapper.createObjectNode();
			jsonPage.put("total", page.getTotalPages());
			jsonPage.put("number", page.getNumber() + 1);
			jsonPage.put("items", page.getTotalElements());
			content.put("page", jsonPage);
			
			
			String jsonreport = mapper.writeValueAsString(content);
			return jsonreport;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("search controller for this keyword throw error :" + searchTerm + " _ " + e.getMessage());
		}
		
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}	
	@RequestMapping(produces="application/json; charset=UTF-8",value = "/json/search/doctypes", method = RequestMethod.GET)
	@ResponseBody
	public String loadDocTypes(HttpServletResponse response, @RequestParam(value = "s") String searchTerm,@RequestParam(value = "p", defaultValue = "1",required = false) int pageNumber
			) {
		
		try {
			
			
			Page<DocType> page = documentService.searchAllDocTypes(searchTerm,pageNumber);
			
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
			
			ObjectNode content = mapper.createObjectNode();
			
			ArrayNode jsonRep = mapper.createArrayNode();
			
			for(DocType docType : page) {
				ObjectNode json = mapper.createObjectNode();
				json.put("id", docType.getId());
				json.put("title",docType.getTitle());
				
				
				jsonRep.add(json);
			}
			
			content.put("content", jsonRep);
			
			ObjectNode jsonPage = mapper.createObjectNode();
			jsonPage.put("total", page.getTotalPages());
			jsonPage.put("number", page.getNumber() + 1);
			jsonPage.put("items", page.getTotalElements());
			content.put("page", jsonPage);
			
			
			String jsonreport = mapper.writeValueAsString(content);
			return jsonreport;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("search controller for this keyword throw error :" + searchTerm + " _ " + e.getMessage());
		}
		
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}	
	 
	
	 
	
	 
	
	
	@RequestMapping(produces="application/json; charset=UTF-8",value = "/json/search/currency", method = RequestMethod.GET)
	@ResponseBody
	public String loadCurrency(HttpServletResponse response, @RequestParam(value = "s") String searchTerm,@RequestParam(value = "p", defaultValue = "1",required = false) int pageNumber
			) {
		
		try {
			
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
			ObjectNode content = mapper.createObjectNode();
			ArrayNode jsonRep = mapper.createArrayNode();
			
			 
			ArrayList<String> cur = Source.CurrencySource();
			 for (int i = 0; i < cur.size(); i++) {
				 ObjectNode json = mapper.createObjectNode();
				 json.put("id", i+1);
				 json.put("title", cur.get(i));
				 jsonRep.add(json);
				
			}
			content.put("content", jsonRep);
			ObjectNode jsonPage = mapper.createObjectNode();
			jsonPage.put("total", "1");
			jsonPage.put("number", "1");
			jsonPage.put("items", "2");
			content.put("page", jsonPage);
			
			
			String jsonreport = mapper.writeValueAsString(content);
			return jsonreport;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("search controller for this keyword throw error :" + searchTerm + " _ " + e.getMessage());
		}
		
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}	
	@RequestMapping(produces="application/json; charset=UTF-8",value = "/json/search/senettype", method = RequestMethod.GET)
	@ResponseBody
	public String loadSenetTypes(HttpServletResponse response, @RequestParam(value = "s") String searchTerm,@RequestParam(value = "p", defaultValue = "1",required = false) int pageNumber
			) {
		
		try {
			
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
			ObjectNode content = mapper.createObjectNode();
			ArrayNode jsonRep = mapper.createArrayNode();
			
			
			ArrayList<String> cur = Source.SenetType();
			for (int i = 0; i < cur.size(); i++) {
				ObjectNode json = mapper.createObjectNode();
				json.put("id", i+1);
				json.put("title", cur.get(i));
				jsonRep.add(json);
				
			}
			content.put("content", jsonRep);
			ObjectNode jsonPage = mapper.createObjectNode();
			jsonPage.put("total", "1");
			jsonPage.put("number", "1");
			jsonPage.put("items", "2");
			content.put("page", jsonPage);
			
			
			String jsonreport = mapper.writeValueAsString(content);
			return jsonreport;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("search controller for this keyword throw error :" + searchTerm + " _ " + e.getMessage());
		}
		
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}	
	@RequestMapping(produces="application/json; charset=UTF-8",value = "/json/search/priority", method = RequestMethod.GET)
	@ResponseBody
	public String loadpriority(HttpServletResponse response, @RequestParam(value = "s") String searchTerm
			) {
		
		try {
			
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
			ObjectNode content = mapper.createObjectNode();
			ArrayNode jsonRep = mapper.createArrayNode();
			
			
			Priority[] cur = Priority.values();
			for (int i = 0; i < cur.length; i++) {
				ObjectNode json = mapper.createObjectNode();
				json.put("id", i+1);
				json.put("title", cur[i].name());
				jsonRep.add(json);
				
			}
			content.put("content", jsonRep);
			ObjectNode jsonPage = mapper.createObjectNode();
			jsonPage.put("total", "1");
			jsonPage.put("number", "1");
			jsonPage.put("items", "2");
			content.put("page", jsonPage);
			
			
			String jsonreport = mapper.writeValueAsString(content);
			return jsonreport;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("search controller for this keyword throw error :" + searchTerm + " _ " + e.getMessage());
		}
		
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}	
	@RequestMapping(produces="application/json; charset=UTF-8",value = "/json/search/nation", method = RequestMethod.GET)
	@ResponseBody
	public String loadnations(HttpServletResponse response, @RequestParam(value = "s") String searchTerm
			) {
		
		try {
			
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
			ObjectNode content = mapper.createObjectNode();
			ArrayNode jsonRep = mapper.createArrayNode();
			
			
			List<String> nations = sosyalMedyaService.loadNationallities();
			
			
			 
			for (int i = 0; i < nations.size(); i++) {
				ObjectNode json = mapper.createObjectNode();
				json.put("id", nations.get(i));
				json.put("title", nations.get(i));
				jsonRep.add(json);
				
			}
			content.put("content", jsonRep);
			
			
			ObjectNode jsonPage = mapper.createObjectNode();
			jsonPage.put("total", "1");
			jsonPage.put("number", "1");
			jsonPage.put("items", "2");
			content.put("page", jsonPage);
			
			
			String jsonreport = mapper.writeValueAsString(content);
			return jsonreport;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("search controller for this keyword throw error :" + searchTerm + " _ " + e.getMessage());
		}
		
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}	
	@RequestMapping(produces="application/json; charset=UTF-8",value = "/json/search/message_type", method = RequestMethod.GET)
	@ResponseBody
	public String loadMessageTypes(HttpServletResponse response, @RequestParam(value = "s") String searchTerm,@RequestParam(value = "p", defaultValue = "1",required = false) int pageNumber
			) {
		
		try {
			
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
			ObjectNode content = mapper.createObjectNode();
			ArrayNode jsonRep = mapper.createArrayNode();
			
			
			ArrayList<String> cur = Source.MessageType2();
			for (int i = 0; i < cur.size(); i++) {
				ObjectNode json = mapper.createObjectNode();
				json.put("id", i+1);
				json.put("title", cur.get(i));
				jsonRep.add(json);
				
			}
			content.put("content", jsonRep);
			ObjectNode jsonPage = mapper.createObjectNode();
			jsonPage.put("total", "1");
			jsonPage.put("number", "1");
			jsonPage.put("items", "2");
			content.put("page", jsonPage);
			
			
			String jsonreport = mapper.writeValueAsString(content);
			return jsonreport;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("search controller for this keyword throw error :" + searchTerm + " _ " + e.getMessage());
		}
		
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}	
	
	@RequestMapping(produces="application/json; charset=UTF-8",value = "/json/search/range", method = RequestMethod.GET)
	@ResponseBody
	public String loadRanges(HttpServletResponse response, @RequestParam(value = "s") String searchTerm,@RequestParam(value = "p", defaultValue = "1",required = false) int pageNumber
			) {
		
		try {
			
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
			ObjectNode content = mapper.createObjectNode();
			ArrayNode jsonRep = mapper.createArrayNode();
			
			 
			ArrayList<String> cur = Source.RangeSource();
			 for (int i = 0; i < cur.size(); i++) {
				 ObjectNode json = mapper.createObjectNode();
				 json.put("id", i+1);
				 json.put("title", cur.get(i));
				 jsonRep.add(json);
				
			}
			content.put("content", jsonRep);
			ObjectNode jsonPage = mapper.createObjectNode();
			jsonPage.put("total", "1");
			jsonPage.put("number", "1");
			jsonPage.put("items", "2");
			content.put("page", jsonPage);
			
			
			String jsonreport = mapper.writeValueAsString(content);
			return jsonreport;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("search controller for this keyword throw error :" + searchTerm + " _ " + e.getMessage());
		}
		
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}	
	
	@RequestMapping(produces="application/json; charset=UTF-8",value = "/json/search/source", method = RequestMethod.GET)
	@ResponseBody
	public String loadSources(HttpServletResponse response, @RequestParam(value = "s") String searchTerm,@RequestParam(value = "p", defaultValue = "1",required = false) int pageNumber
			) {
		
		try {
			
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
			ObjectNode content = mapper.createObjectNode();
			ArrayNode jsonRep = mapper.createArrayNode();
			
			 
			ArrayList<String> cur = Source.CustomerSource();

			 for (int i = 0; i < cur.size(); i++) {
				 ObjectNode json = mapper.createObjectNode();
				 json.put("id", i+1);
				 json.put("title", cur.get(i));
				 jsonRep.add(json);
				
			}
			content.put("content", jsonRep);
			ObjectNode jsonPage = mapper.createObjectNode();
			jsonPage.put("total", "1");
			jsonPage.put("number", "1");
			jsonPage.put("items", "5");
			content.put("page", jsonPage);
			
			
			String jsonreport = mapper.writeValueAsString(content);
			return jsonreport;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("search controller for this keyword throw error :" + searchTerm + " _ " + e.getMessage());
		}
		
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}	
	
	
	@RequestMapping(produces="application/json; charset=UTF-8",value = "/json/search/roles", method = RequestMethod.GET)
	@ResponseBody
	public String loadRoles(HttpServletResponse response, @RequestParam(value = "s") String searchTerm,@RequestParam(value = "p", defaultValue = "1",required = false) int pageNumber
			) {
		
		try {
			
			
			Page<UserRole> page = userService.searchRoles(searchTerm,pageNumber);
			
			ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
			
			ObjectNode content = mapper.createObjectNode();
			
			ArrayNode jsonRep = mapper.createArrayNode();
			
			for(UserRole role : page) {
				ObjectNode json = mapper.createObjectNode();
				json.put("id", role.getId());
				json.put("title", role.getName());
				 
				
				jsonRep.add(json);
			}
			
			content.put("content", jsonRep);
			
			ObjectNode jsonPage = mapper.createObjectNode();
			jsonPage.put("total", page.getTotalPages());
			jsonPage.put("number", page.getNumber() + 1);
			jsonPage.put("items", page.getTotalElements());
			content.put("page", jsonPage);
			
			
			String jsonreport = mapper.writeValueAsString(content);
			return jsonreport;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("search controller for this keyword throw error :" + searchTerm + " _ " + e.getMessage());
		}
		
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}	

}
