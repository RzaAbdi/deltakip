package rza.BPFco.controller;

import java.security.Principal;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import rza.BPFco.model.Account;
import rza.BPFco.service.UserService;
import rza.BPFco.utilities.Time;

@Controller
public class SigninController extends HttpServlet{

	static final Logger logger = LogManager.getLogger();

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	private UserService userService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@RequestMapping(value = {"/signin/**","/giris/**"})
	public String signin(Principal principal) {
		
		if(principal != null){
			return  "redirect:/";
		}
		
//	    String referrer = request.getHeader("Referer");
//	    request.getSession().setAttribute("url_prior_login", referrer);

        return "signin";
	}
	
	@RequestMapping(value = "signin",method = RequestMethod.POST)
	@ResponseBody
	public String login(Principal principal,HttpServletRequest request,HttpServletResponse response,
						@RequestParam(value = "u") String username,
						@RequestParam(value = "p") String password) {

		try {
			if(principal != null){
				//response.setStatus(HttpStatus.ACCEPTED.value());
				//return null;
			}
			username = username.trim();
			String emailregex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
			if(username.matches(emailregex) || username.equalsIgnoreCase("admin")){
				Account account = userService.loadAccountByEmail(username);
				if(account!=null){

					if (passwordEncoder.matches(password, account.getPassword())) {
						userService.signin(account);
						account.setLastsign(Time.todayDateTime());
						userService.save(account);
						logger.fatal(account.getName() + " LOGIN TO DELTAKIP " );
						
						return null;
					}
					response.setStatus(HttpStatus.BAD_REQUEST.value());
					return "1"; // meaning password is wrong

				}
			}
			response.setStatus(HttpStatus.BAD_REQUEST.value());
			return "0"; // meaning email is wrong

		} catch (Exception e) {
			e.printStackTrace();
		}
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return null;
	}
	
	
	 
	
	
	
}
