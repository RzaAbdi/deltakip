package rza.BPFco.form;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.validator.constraints.NotBlank;

import rza.BPFco.model.portfoys.Reply;
import rza.BPFco.model.portfoys.SosyalMedya;
import rza.BPFco.utilities.Time;

public class AdminReplyForm {

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getSmid() {
		return this.smid;
	}

	public void setSmid(Long smid) {
		this.smid = smid;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public List<Long> getAccounts() {
		return this.accounts;
	}

	public void setAccounts(List<Long> accounts) {
		this.accounts = accounts;
	}

	public static Logger getLogger() {
		return logger;
	}

	static final Logger logger = LogManager.getLogger();

	private Long id;

	private Long smid;

	@NotBlank(message = "note can't be empty")
	private String note;

	private Date createDate;

	private List<Long> accounts;

	public Reply createReply() {
		Reply rp = new Reply();
		rp.setNote(note);
		if (createDate != null) {

			rp.setCreateDate(createDate);
		} else {
			rp.setCreateDate(Time.today());
		}
		return rp;
	}

	public Reply manipulatReply(Reply rp) throws Exception {
		rp.setNote(note);
		if (createDate != null) {

			rp.setCreateDate(createDate);
		} else {
			rp.setCreateDate(Time.today());
		}
		return rp;
	}

}
