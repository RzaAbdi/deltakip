package rza.BPFco.form;


import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import rza.BPFco.model.Account;
import rza.BPFco.model.Tag;
import rza.BPFco.model.UserRole;
import rza.BPFco.utilities.Time;

public class SignupForm {

	static final Logger logger = LogManager.getLogger();

	@NotBlank(message = "Email can't be empty")
	@Email(message = "Email is not correct")
	private String email;

	@NotBlank(message = "password can't be empty")
	@Length(min = 4, message = "password can't be less than 4 characters")
	private String password;
	
	private String newpass;
	
	public String getNewpass() {
		return this.newpass;
	}





	public void setNewpass(String newpass) {
		this.newpass = newpass;
	}





	public String getRepass() {
		return this.repass;
	}





	public void setRepass(String repass) {
		this.repass = repass;
	}




	private String repass;
	
	private String name;
	
	public String getName() {
		return this.name;
	}





	public void setName(String name) {
		this.name = name;
	}




	private Long id;
	
	private Long userrole;
	
	public Long getUserrole() {
	    return userrole;
	}





	public void setUserrole(Long userrole) {
	    this.userrole = userrole;
	}





	public Long getId() {
	    return id;
	}





	public void setId(Long id) {
	    this.id = id;
	}



	public Account createAccount(String encoded) {
		
		Account account = new Account(getEmail(), encoded , UserRole.user, Time.today(),getName());

		 
		 
		return account;

	}

 

	 

	public String getEmail() {
		return email;
	}
 
	 

	public String getPassword() {
		return password;
	}

	 

 



	public Account manipulatBasicInfo(Account account) throws Exception {
		 
		 
		account.setEmail(email);
		account.setName(name);
		return account;
	}

   
 

	public Account saveProfPath(Account account) throws Exception {

		 
		 
		 
		return account;
	}

	 

	 

	public void setEmail(String email) {
		this.email = email;
	}

	 
	 
	
	public void setPassword(String password) {
		this.password = password;
	}
 
}
