package rza.BPFco.form;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import rza.BPFco.model.portfoys.Agent;
import rza.BPFco.model.portfoys.Document;
import rza.BPFco.utilities.Time;

public class AdminDocumentForm {

	static final Logger logger = LogManager.getLogger();

	public static Logger getLogger() {
		return logger;
	}

	private Long id;
	
	private Long sections;
	
	public Long getSections() {
		return this.sections;
	}

	public void setSections(Long sections) {
		this.sections = sections;
	}

	private int type;
	
	public List<Long> getAgents() {
		return this.agents;
	}

	public void setAgents(List<Long> agents) {
		this.agents = agents;
	}

	public List<Long> getContracts() {
		return this.contracts;
	}

	public void setContracts(List<Long> contracts) {
		this.contracts = contracts;
	}

	public List<Long> getInstallments() {
		return this.installments;
	}

	public void setInstallments(List<Long> installments) {
		this.installments = installments;
	}

	public List<Long> getCustomers() {
		return this.customers;
	}

	public void setCustomers(List<Long> customers) {
		this.customers = customers;
	}

	private List<Long> doctype=null;
	private List<Long> agents=null;
	private List<Long> contracts=null;
	private List<Long> installments=null;
	private List<Long> customers=null;
	
	

	 

	public int getType() {
		return this.type;
	}

	public List<Long> getDoctype() {
		return this.doctype;
	}

	public void setDoctype(List<Long> doctype) {
		this.doctype = doctype;
	}

	public void setType(int type) {
		this.type = type;
	}

	private String edit_date;
	
	private String create_date;

	private String title;
	
	private String file_path;

	public String getFile_path() {
		return this.file_path;
	}

	public void setFile_path(String file_path) {
		this.file_path = file_path;
	}

	 


	
	 

	public Document createDocument() {
		Document document = new Document();
		document.setFile_path(file_path);
		document.setType(type);
		

		return document;
	}

	 

	 

 

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}


	public Long getId() {
		return this.id;
	}


	public Document manipulatDocument(Document document) throws Exception {
		document.setType(type);
		 
		
		return document;
	}


	public String getEdit_date() {
		return this.edit_date;
	}

	public void setEdit_date(String edit_date) {
		this.edit_date = edit_date;
	}

	public String getCreate_date() {
		return this.create_date;
	}

	public void setCreate_date(String create_date) {
		this.create_date = create_date;
	}

	public void setId(Long id) {
		this.id = id;
	}




}
