package rza.BPFco.form;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.validator.constraints.NotBlank;

import rza.BPFco.model.Tag;

public class AdminTagForm {
	
	
	public static Logger getLogger() {
		return logger;
	}

	static final Logger logger = LogManager.getLogger();

	private Long id;
	
	@NotBlank(message = "Title can't be empty")
	private String title;
	
	private String title_en;

	private String title_ru;

	
	private String title_ar;

	private boolean active;
	
	
	private int type;
	
	public int getType() {
		return this.type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getTitle_en() {
		return title_en;
	}

	public void setTitle_en(String title_en) {
		this.title_en = title_en;
	}

	public String getTitle_ru() {
		return title_ru;
	}

	public void setTitle_ru(String title_ru) {
		this.title_ru = title_ru;
	}

	public String getTitle_ar() {
		return title_ar;
	}

	public void setTitle_ar(String title_ar) {
		this.title_ar = title_ar;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public Tag createTags() {
		Tag tag = new Tag();
		tag.setTitle(title);
		tag.setType(type);
		 
		tag.setActive(active);
		return tag;
	}
	
	public Tag manipulatTag(Tag tag) throws Exception {
		tag.setTitle(title);
		tag.setType(type);
		 
		tag.setActive(active);
		return tag;
	}

	 
}
