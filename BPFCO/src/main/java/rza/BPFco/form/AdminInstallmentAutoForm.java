

package rza.BPFco.form;

import java.text.ParseException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import rza.BPFco.model.portfoys.InsCover;
import rza.BPFco.model.portfoys.Installment;

public class AdminInstallmentAutoForm {

	public static Logger getLogger() {
		return logger;
	}

	static final Logger logger = LogManager.getLogger();

	private long contract_no;// id

	private long installment;

	public long getContract_no() {
		return this.contract_no;
	}

	public void setContract_no(long contract_no) {
		this.contract_no = contract_no;
	}

	private Long id;
	private int range;
	private int currency;
	private int limit;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	private String amount;
	private String partial_amount;
	private String issue_date;
	
	

	 

	public String getPartial_amount() {
		return this.partial_amount;
	}

	public void setPartial_amount(String partial_amount) {
		this.partial_amount = partial_amount;
	}

	 

	public Installment createInstallment() throws ParseException {
		Installment installment = new Installment();
		installment.setAmount(amount);
		installment.setPaid(false);
		installment.setCurrency(currency);
		return installment;
	}
	public InsCover createInstallmentForCover() throws ParseException {
		InsCover installment = new InsCover();
		installment.setAmount(amount);
		installment.setCurrency(currency);
		return installment;
	}

	public Installment manipulatInstallment(Installment installment) throws Exception {
		installment.setAmount(amount);
		installment.setCurrency(currency);
		return installment;
	}

	public long getInstallment() {
		return this.installment;
	}

	public void setInstallment(long installment) {
		this.installment = installment;
	}

	public String getAmount() {
		return this.amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getIssue_date() {
		return this.issue_date;
	}

	public void setIssue_date(String issue_date) {
		this.issue_date = issue_date;
	}

	public int getRange() {
		return this.range;
	}

	public void setRange(int range) {
		this.range = range;
	}

	public int getCurrency() {
		return this.currency;
	}

	public void setCurrency(int currency) {
		this.currency = currency;
	}

	public int getLimit() {
		return this.limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	 

}




