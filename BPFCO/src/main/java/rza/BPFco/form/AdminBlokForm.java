package rza.BPFco.form;


import java.text.ParseException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.validator.constraints.NotBlank;

import rza.BPFco.model.Portfoy;
import rza.BPFco.model.Tag;
import rza.BPFco.model.portfoys.Blok;
import rza.BPFco.model.portfoys.Proje;
import rza.BPFco.utilities.Time;

public class AdminBlokForm {
	
	
	public static Logger getLogger() {
		return logger;
	}

	static final Logger logger = LogManager.getLogger();

	private Long id;
	
	@NotBlank(message = "Title can't be empty")
	private String title;
	
	 
	private long projects;

	public long getProjects() {
		return this.projects;
	}

	public void setProjects(long projects) {
		this.projects = projects;
	}
	 
 
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	

 
	public Blok createBlok() throws ParseException {
		Blok blok = new Blok();
		blok.setTitle(title);
		blok.setCreate_date(Time.todayDateTime());
		return blok;
	}
	
	public Blok manipulatBlok(Blok blok) throws Exception {
		blok.setTitle(title);
		blok.setEdit_date(Time.todayDateTime());
		return blok;
	}
	 
}
