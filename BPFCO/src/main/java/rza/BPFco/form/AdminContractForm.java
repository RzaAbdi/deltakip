package rza.BPFco.form;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import rza.BPFco.model.Contract;
import rza.BPFco.utilities.Time;

public class AdminContractForm {

	static final Logger logger = LogManager.getLogger();

	public static Logger getLogger() {
		return logger;
	}
	
	private boolean saveToDocs;
	
	public boolean isKombi() {
		return this.kombi;
	}

	public void setKombi(boolean kombi) {
		this.kombi = kombi;
	}

	public String getAmount() {
		return this.amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public int getLimit() {
		return this.limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public String getDayOfMount() {
		return this.dayOfMount;
	}

	public void setDayOfMount(String dayOfMount) {
		this.dayOfMount = dayOfMount;
	}

	public int getRange() {
		return this.range;
	}

	public void setRange(int range) {
		this.range = range;
	}

	private boolean kombi;

	public boolean isSaveToDocs() {
		return this.saveToDocs;
	}

	public void setSaveToDocs(boolean saveToDocs) {
		this.saveToDocs = saveToDocs;
	}

	private Long id;

	private String address;
	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGsm() {
		return this.gsm;
	}

	public void setGsm(String gsm) {
		this.gsm = gsm;
	}

	public String getCustomer() {
		return this.customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}
	
	 
	private String email;
	
	private String amount;
	private int limit;
	private String dayOfMount;
	private int range;
	private String gsm;
	private String customer;
	private Date createDate;

	private String downDate;

	public String getDownDate() {
		return this.downDate;
	}

	public void setDownDate(String downDate) {
		this.downDate = downDate;
	}

	private String price;

	private String description;

	private int currency;

	private boolean installmented;

	private boolean deed;
	
	private boolean insEnd;

	public boolean isInsEnd() {
		return this.insEnd;
	}

	public void setInsEnd(boolean insEnd) {
		this.insEnd = insEnd;
	}

	public boolean isDeed() {
		return this.deed;
	}

	public void setDeed(boolean deed) {
		this.deed = deed;
	}

	public boolean isInstallmented() {
		return this.installmented;
	}

	public void setInstallmented(boolean installmented) {
		this.installmented = installmented;
	}

	private boolean active;
	
 

	public boolean isActive() {
		return this.active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	private String contract_no;

	private String cash;
	
	
	 

	private List<Long> temsilci;

	private List<Long> agent;

	private List<Long> portfoys;

	private List<Long> customers;

	public List<Long> getCustomers() {
		return this.customers;
	}

	public void setCustomers(List<Long> customers) {
		this.customers = customers;
	}

	public List<Long> getTemsilci() {
		return this.temsilci;
	}

	public void setTemsilci(List<Long> temsilci) {
		this.temsilci = temsilci;
	}

	public List<Long> getAgent() {
		return this.agent;
	}

	public void setAgent(List<Long> agent) {
		this.agent = agent;
	}

	public List<Long> getPortfoys() {
		return this.portfoys;
	}

	public void setPortfoys(List<Long> portfoys) {
		this.portfoys = portfoys;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	 

	public String getPrice() {
		return this.price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getCurrency() {
		return this.currency;
	}

	public void setCurrency(int currency) {
		this.currency = currency;
	}

	public String getContract_no() {
		return this.contract_no;
	}

	public void setContract_no(String contract_no) {
		this.contract_no = contract_no;
	}

	public String getCash() {
		return this.cash;
	}

	public void setCash(String cash) {
		this.cash = cash;
	}

	public Contract createContract() throws ParseException {
		Contract contract = new Contract();
		contract.setCash(cash);
		contract.setContract_no(contract_no);
		contract.setCreateDate(createDate);/// must be manual
		contract.setDownDate(createDate);/// must be manual
		contract.setCurrency(currency);
		contract.setDescription(description);
		contract.setPrice(price);
		contract.setActive(true);
		contract.setDeed(deed);
		contract.setInstallmented(installmented);
		contract.setInsEnd(insEnd);
		return contract;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Contract manipulatContract(Contract contract) throws Exception {
		contract.setCash(cash);
		contract.setContract_no(contract_no);
		contract.setCreateDate(createDate);/// must be manual
		contract.setDownDate(createDate);/// must be manual
		contract.setCurrency(currency);
		contract.setDescription(description);
		contract.setPrice(price);
		contract.setDeed(deed);
		contract.setInstallmented(installmented);
		contract.setInsEnd(insEnd);

		return contract;
	}

}
