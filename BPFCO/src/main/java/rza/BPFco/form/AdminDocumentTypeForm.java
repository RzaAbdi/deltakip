package rza.BPFco.form;


import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.validator.constraints.NotBlank;

import rza.BPFco.model.portfoys.DocType;

public class AdminDocumentTypeForm {
	
	
	public static Logger getLogger() {
		return logger;
	}

	static final Logger logger = LogManager.getLogger();

	private Long id;
	
	@NotBlank(message = "Title can't be empty")
	private String title;
	
	 
	
	 
	private List<Long> sections;

	 
 
	public List<Long> getSections() {
		return this.sections;
	}

	public void setSections(List<Long> sections) {
		this.sections = sections;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public DocType createDocType() {
		DocType docType = new DocType();
		docType.setTitle(title);
		return docType;
	}
	
	public DocType manipulatDocType(DocType docType) throws Exception {
		docType.setTitle(title);
		return docType;
	}

	 
}
