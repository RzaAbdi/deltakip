package rza.BPFco.form;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import rza.BPFco.model.portfoys.Cover;

public class AdminCoverForm {

	static final Logger logger = LogManager.getLogger();

	public static Logger getLogger() {
		return logger;
	}

	private String guarantor;
	
	private String harc;

	public String getHarc() {
		return this.harc;
	}

	public void setHarc(String harc) {
		this.harc = harc;
	}

	private String place;

	 
	
	private int fee;

	public int getFee() {
		return this.fee;
	}

	public void setFee(int fee) {
		this.fee = fee;
	}

	private String court;
	
	private String dsc;

	public String getDsc() {
		return this.dsc;
	}

	public void setDsc(String dsc) {
		this.dsc = dsc;
	}

	private int senettype;

	private Date senetDate;

	public String getGuarantor() {
		return this.guarantor;
	}

	public void setGuarantor(String guarantor) {
		this.guarantor = guarantor;
	}

	public String getPlace() {
		return this.place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getCourt() {
		return this.court;
	}

	public void setCourt(String court) {
		this.court = court;
	}

	public int getSenettype() {
		return this.senettype;
	}

	public void setSenettype(int senettype) {
		this.senettype = senettype;
	}

	public Date getSenetDate() {
		return this.senetDate;
	}

	public void setSenetDate(Date senetDate) {
		this.senetDate = senetDate;
	}

	private Date contractDate;
	
	private Date cashDate;

	public Date getCashDate() {
		return this.cashDate;
	}

	public void setCashDate(Date cashDate) {
		this.cashDate = cashDate;
	}

	private List<Long> coverins;

	private List<Long> customers;

	private String des;

	private int currency;

	public int getCurrency() {
		return this.currency;
	}

	public void setCurrency(int currency) {
		this.currency = currency;
	}

	private boolean saveToDocs;

	public boolean isSaveToDocs() {
		return this.saveToDocs;
	}

	public void setSaveToDocs(boolean saveToDocs) {
		this.saveToDocs = saveToDocs;
	}

	private Long id;

	private List<Long> portfoys;

	private String price;

	private String cash;

	public String getCash() {
		return this.cash;
	}

	public void setCash(String cash) {
		this.cash = cash;
	}

	private boolean reminde;

	private String reminding;

	private List<Long> temsilci;

	public Cover createCover() {
		Cover cover = new Cover();
		cover.setContractDate(contractDate);
		cover.setDes(des);
		cover.setReminde(reminde);
		cover.setPrice(price);
		cover.setCash(cash);
		cover.setReminding(price);
		cover.setCurrency(currency);
		cover.setCashDate(cashDate);
		cover.setDsc(dsc);
		cover.setFee(fee);
		if(fee==2) {
			cover.setHarc(harc);
		}

		return cover;
	}

	public Date getContractDate() {
		return this.contractDate;
	}

	public List<Long> getCoverins() {
		return this.coverins;
	}

	public List<Long> getCustomers() {
		return this.customers;
	}

	public String getDes() {
		return this.des;
	}

	public Long getId() {
		return this.id;
	}

	public List<Long> getPortfoys() {
		return this.portfoys;
	}

	public List<Long> getTemsilci() {
		return this.temsilci;
	}

	public boolean isReminde() {
		return this.reminde;
	}

	public Cover manipulatCover(Cover cover) {

		cover.setContractDate(contractDate);
		cover.setDes(des);
		cover.setReminde(reminde);
		cover.setCash(cash);
		cover.setPrice((price));
		cover.setReminding((price));
		cover.setCurrency(currency);
		cover.setCashDate(cashDate);
		cover.setDsc(dsc);
		cover.setFee(fee);
		if(fee==2) {
			cover.setHarc(harc);
		}
		return cover;
	}

	public void setContractDate(Date contractDate) {
		this.contractDate = contractDate;
	}

	public void setCoverins(List<Long> coverins) {
		this.coverins = coverins;
	}

	public void setCustomers(List<Long> customers) {
		this.customers = customers;
	}

	public void setDes(String des) {
		this.des = des;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setPortfoys(List<Long> portfoys) {
		this.portfoys = portfoys;
	}

	public void setReminde(boolean reminde) {
		this.reminde = reminde;
	}

	public String getPrice() {

		return this.price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getReminding() {
		return this.reminding;
	}

	public void setReminding(String reminding) {
		this.reminding = reminding;
	}

	public void setTemsilci(List<Long> temsilci) {
		this.temsilci = temsilci;
	}

}
