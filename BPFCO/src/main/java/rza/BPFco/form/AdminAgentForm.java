package rza.BPFco.form;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import rza.BPFco.model.portfoys.Agent;
import rza.BPFco.utilities.Time;

public class AdminAgentForm {

	static final Logger logger = LogManager.getLogger();

	public static Logger getLogger() {
		return logger;
	}

	private Long id;

	private Date edit_date;
	
	private Date create_date;

	private String name;

	public Date getCreate_date() {
		return this.create_date;
	}

	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}

	private String gsm1;

	private String gsm2;
	
	private String company;



	private String nationality;

	private Long tax;

	private Long tc;

	private String passport;

	private String address;

	public Agent createAgent() {
		Agent agent = new Agent();
		agent.setAddress(address);
		agent.setCreate_date(Time.today());
		agent.setGsm1(gsm1);
		agent.setGsm2(gsm2);
		agent.setNationality(nationality);
		agent.setPassport(passport);
		agent.setTax(tax);
		agent.setTc(tc);
		agent.setName(name);
		agent.setCompany(company);


		return agent;
	}

	public String getAddress() {
		return this.address;
	}

	public Date getEdit_date() {
		return this.edit_date;
	}

 

	public String getGsm1() {
		return this.gsm1;
	}

	public String getGsm2() {
		return this.gsm2;
	}

	public Long getId() {
		return this.id;
	}

 

	public String getNationality() {
		return this.nationality;
	}

	public String getPassport() {
		return this.passport;
	}

	public Long getTax() {
		return this.tax;
	}

	public Long getTc() {
		return this.tc;
	}

	public Agent manipulatAgent(Agent agent) throws Exception {
		agent.setAddress(address);
		agent.setEdit_date(Time.today());
		agent.setGsm1(gsm1);
		agent.setGsm2(gsm2);
		agent.setNationality(nationality);
		agent.setPassport(passport);
		agent.setTax(tax);
		agent.setTc(tc);
		agent.setName(name);
		agent.setCompany(company);
		 
		
		return agent;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setEdit_date(Date edit_date) {
		this.edit_date = edit_date;
	}

	 
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCompany() {
		return this.company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public void setGsm1(String gsm1) {
		this.gsm1 = gsm1;
	}

	public void setGsm2(String gsm2) {
		this.gsm2 = gsm2;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public void setPassport(String passport) {
		this.passport = passport;
	}

	public void setTax(Long tax) {
		this.tax = tax;
	}

	public void setTc(Long tc) {
		this.tc = tc;
	}

}
