package rza.BPFco.form.filter;

import java.util.List;

import com.mysema.query.types.Predicate;

import rza.BPFco.model.portfoys.QCustomer;

public class CustomerFilterForm2 extends FilterForm {

	private List<Long> listId;

	public List<Long> getListId() {
		return listId;
	}

	@Override
	public Predicate predicate() {
		QCustomer customer = QCustomer.customer;

		if (listId != null) {
			addExp(customer.id.in(listId));
		}

//		addStringExp(floor.title, title, nametype);

		return predicate;
	}

	public void setListId(List<Long> listId) {
		this.listId = listId;
	}

}
