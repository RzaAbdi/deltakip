package rza.BPFco.form.filter;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;

import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.path.DateTimePath;
import com.mysema.query.types.path.StringPath;

import rza.BPFco.utilities.Time;

public abstract class FilterForm {

    protected static final Logger logger = LogManager.getLogger();

    protected int pageNumber = 1; // page

    protected int pageSize = 20; // pagesize

    protected Pageable page;

    public void setPage(Pageable page) {
		this.page = page;
	}

	public BooleanExpression getPredicate() {
		return this.predicate;
	}

	public void setPredicate(BooleanExpression predicate) {
		this.predicate = predicate;
	}

	protected BooleanExpression predicate;

    protected Sort sort = new Sort(new Order(Direction.DESC, "id"));

    public abstract Predicate predicate();

    /**
     * Add a BooleanExpression to the chain of Filter
     * 
     * @param booleanExpression
     */
    protected void addExp(BooleanExpression booleanExpression) {

	if (predicate == null) {
	    predicate = booleanExpression;
	} else {
	    predicate = predicate.and(booleanExpression);
	}

    }

    /**
     * Add a BooleanExpression to the chain of Filter But OR
     * 
     * @param booleanExpression
     */
    protected void addExpOr(BooleanExpression booleanExpression) {

	if (predicate == null) {
	    predicate = booleanExpression;
	} else {
	    predicate = predicate.or(booleanExpression);
	}

    }

    protected void addExpDate(DateTimePath<Date> createdate, DateTimePath<Date> lastedit, String value) {

	if (value != null) {

	    Date from = null, to = null;
	    try {
		String[] splilt = Time.SplitedDate(value);
		from = Time.ConvertToDate(splilt[0]);
		to = Time.ConvertToDate(splilt[1]);
	    } catch (ParseException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }

	    addExp(createdate.between(from, to).or(lastedit.between(from, to)));

	}

    }

    /**
     * Adding string expression of 4 types %string% and ...
     * 
     * @param value
     * @param booleanExpression
     */
    protected void addStringExp(StringPath spath, String value, int type) {
	if (value != null) {
	    switch (type) {
	    case 1:
		addExp(spath.like("%" + value + "%"));
		break;
	    case 2:
		addExp(spath.like(value + "%"));
		break;
	    case 3:
		addExp(spath.like("%" + value));
		break;
	    case 4:
		addExp(spath.eq(value));
		break;
	    }
	}

    }

    /**
     * Adding string expression of 4 types %string% and ...
     * 
     * @param value
     * @param booleanExpression
     */
    protected void addStringExpOr(StringPath spath, String value, int type) {
	if (value != null) {
	    switch (type) {
	    case 1:
		addExpOr(spath.like("%" + value + "%"));
		break;
	    case 2:
		addExpOr(spath.like(value + "%"));
		break;
	    case 3:
		addExpOr(spath.like("%" + value));
		break;
	    case 4:
		addExpOr(spath.eq(value));
		break;
	    }
	}

    }

    /**
     * Adding string expression of 4 types %string% and ... but first one is and but
     * other are or
     * 
     * @param value
     * @param booleanExpression
     */
    protected void addListStringExp(StringPath spath, List<String> strings, int type) {

	boolean first = true;
	for (String string : strings) {
	    if (first) {
		addStringExp(spath, string, type);
		first = false;
	    } else {
		addStringExpOr(spath, string, type);
	    }
	}
    }

    /**
     * will convert a string from ajax like: "name-0,id-1" into a List of Orders and
     * will create a sort for the Filter Form
     * 
     * <pre>
     *   <code>form.setSort("name-1,id-0");</code>
     * </pre>
     * 
     * @param string (e.g. "name-0,id-1") ::: 0 means ASC_and 1 means DESC
     */
    public void setSort(String string) {
	try {
	    List<Order> orders = new ArrayList<Sort.Order>();
	    for (String sortString : string.split(",")) {
		Direction dir;
		if (sortString.split("-")[1].equals("0")) {
		    dir = Direction.ASC;
		} else {
		    dir = Direction.DESC;
		}
		orders.add(new Order(dir, sortString.split("-")[0]));
	    }
	    ;

	    sort = new Sort(orders);

	} catch (Exception e) {
	    logger.error("resolving sort failed in FilterForm with the string:: " + string);
	}
    }

    protected List<Long> splitByComma(String string) {
	List<Long> lstLong = new ArrayList<Long>();
	for (String spl : string.split("[^\\d]")) {
	    lstLong.add(Long.valueOf(spl));
	}
	return lstLong;
    }

    public void addExpression(BooleanExpression expression) {
	addExp(expression);
    }

    public boolean hasSort() {
	return sort != null;
    }

    public Sort getSort() {
	return sort;
    }

    public void setP(int p) {
	this.pageNumber = p;
    }

    public void setPsize(int size) {
	this.pageSize = size;
    }

    public int getPageSize() {
	return pageSize;
    }

    public int getPageNumber() {
	return pageNumber;
    }

    public Pageable getPage() {

	return new PageRequest(getPageNumber() - 1, getPageSize(), sort);
    }

//
}
