package rza.BPFco.form.filter;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import com.mysema.query.types.Predicate;

import rza.BPFco.model.portfoys.QSosyalMedya;
import rza.BPFco.utilities.Time;

public class SosyalMedyaFilterForm extends FilterForm {

	private String sorting;

	private Long chases;

	public Long getChases() {
		return this.chases;
	}

	public void setChases(Long chases) {
		this.chases = chases;
	}

	public String getSorting() {
		return this.sorting;
	}

	public void setSorting(String sorting) {
		this.sorting = sorting;
	}

	private List<String> nation;

	public List<String> getNation() {
		return this.nation;
	}

	public void setNation(List<String> nation) {
		this.nation = nation;
	}

	private List<Long> listId;

	private List<Long> temsilci;

	private String gsm;

	private int type;

	public int getType() {
		return this.type;
	}

	public void setType(int type) {
		this.type = type;
	}

	private String customer;
	private String daterange;
	private boolean follow;

	public boolean isFollow() {
		return this.follow;
	}

	public void setFollow(boolean follow) {
		this.follow = follow;
	}

	private String priority;

//	 public int getPriority() {
//		return this.priority;
//	}
//
//	public void setPriority(int priority) {
//		this.priority = priority;
//	}

	public String getPriority() {
		return this.priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getGsm() {
		return this.gsm;
	}

	public void setGsm(String gsm) {
		this.gsm = gsm;
	}

	public String getCustomer() {
		return this.customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getDaterange() {
		return this.daterange;
	}

	public void setDaterange(String daterange) {
		this.daterange = daterange;
	}

	public List<Long> getTemsilci() {
		return this.temsilci;
	}

	public void setTemsilci(List<Long> temsilci) {
		this.temsilci = temsilci;
	}

	private int language;

	private Long account;

	private boolean admin;

	private boolean callFa;
	private boolean callAr;

	public boolean isCallAr() {
		return this.callAr;
	}

	public void setCallAr(boolean callAr) {
		this.callAr = callAr;
	}

	public boolean isAdmin() {
		return this.admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public int getLanguage() {
		return this.language;
	}

	public void setLanguage(int language) {
		this.language = language;
	}

	public Long getAccount() {
		return this.account;
	}

	public void setAccount(Long account) {
		this.account = account;
	}

	private int nametype;

	private String title;

	public List<Long> getListId() {
		return listId;
	}

	public int getNametype() {
		return nametype;
	}

	public String getTitle() {
		return title;
	}

	@Override
	public Predicate predicate() {
		QSosyalMedya sosyalMedya = QSosyalMedya.sosyalMedya;
		 

		if (listId != null) {
			addExp(sosyalMedya.id.in(listId));
		}
		if (temsilci != null) {
			if (temsilci.size() > 0) {

				addExp(sosyalMedya.account.any().id.in(temsilci));
			}
		}

		if (customer != null) {
			if (customer != "") {

				addExp(sosyalMedya.name.contains(customer));
			}
		}
		if (chases != null) {

			addExp(sosyalMedya.chase.id.eq(chases));

		}

		if (gsm != null) {
			if (gsm != "") {

				addExp(sosyalMedya.gsm.contains(gsm));
			}
		}

		if (priority != "") {

			if (priority != null) {

				addExp(sosyalMedya.priority.eq(Integer.parseInt(priority)));
			}
		}

		if (nation != null) {
			if (nation.size() > 0) {
				addExp(sosyalMedya.nation.in(nation));

			}

		}
		if (follow == true) {

			addExp(sosyalMedya.follow.isTrue());
			if (account != 1) {

				addExp(sosyalMedya.account.any().id.eq(account));
			}

		}

		addExp(sosyalMedya.language.eq(language));

		addExp(sosyalMedya.type.eq(type));
		 

		if (!admin) {

			if (account != null) {
				addExp(sosyalMedya.account.any().id.eq(account));
			}
		}

		if (daterange != null & daterange != "") {
			String[] ss = daterange.split("-");
			try {
				Date from = Time.ConvertToDate(ss[0]);

				Date to = Time.ConvertToDate(ss[1]);

				addExp(sosyalMedya.createDate.between(from, to));

			} catch (ParseException exception) {
				// TODO Auto-generated catch-block stub.
				exception.printStackTrace();
			}
		}

		return predicate;
	}

	public void setListId(List<Long> listId) {
		this.listId = listId;
	}

	public void setNametype(int nametype) {
		this.nametype = nametype;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
