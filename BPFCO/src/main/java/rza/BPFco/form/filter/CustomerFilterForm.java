package rza.BPFco.form.filter;

import java.util.List;

import com.mysema.query.types.Predicate;

import rza.BPFco.model.portfoys.QCustomer;

public class CustomerFilterForm extends FilterForm {

	private List<Long> listId;
	private List<Long> contracts;

	public List<Long> getContracts() {
		return this.contracts;
	}

	public void setContracts(List<Long> contracts) {
		this.contracts = contracts;
	}

	private Long account;

	private boolean admin;

	public Long getAccount() {
		return this.account;
	}

	public void setAccount(Long account) {
		this.account = account;
	}

	public boolean isAdmin() {
		return this.admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public List<Long> getTemsilci() {
		return this.temsilci;
	}

	public void setTemsilci(List<Long> temsilci) {
		this.temsilci = temsilci;
	}

	private int nametype;

	private String name;

	private List<Long> temsilci;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Long> getListId() {
		return listId;
	}

	public int getNametype() {
		return nametype;
	}

	public void setListId(List<Long> listId) {
		this.listId = listId;
	}

	public void setNametype(int nametype) {
		this.nametype = nametype;
	}

	@Override
	public Predicate predicate() {

		QCustomer customer = QCustomer.customer;

		if (listId != null) {
			addExp(customer.id.in(listId));
		}

		if (name != null) {
			addExp(customer.fname.eq(name));
		}

		 
			

		if (temsilci != null) {
			if (temsilci.size() > 0) {

				addExp(customer.account.any().id.in(temsilci));
			}
		}

		if (!admin) {

			if (account != null) {
				if (contracts != null) {
					addExp(customer.account.any().id.eq(account).or(customer.contract.any().id.in(contracts)));
				}else {
					
					addExp(customer.contract.any().id.in(contracts));
				}
				

			}
		}

		return predicate;
	}

}
