package rza.BPFco.form.filter;

import java.util.Date;
import java.util.List;

import com.mysema.query.types.Predicate;

import rza.BPFco.model.QAjanda;
import rza.BPFco.model.portfoys.QAgent;

public class AjandaFilterForm extends FilterForm{
	
	
	private List<Long> listId;

	private int type;
	
	private Date issueDate;  
	
	private Long sosyalMedya;
	
	private Long account;
	
	private boolean admin;
	
	private int status;
 
	public int getStatus() {
		return this.status;
	}



	public void setStatus(int status) {
		this.status = status;
	}



	public boolean isAdmin() {
		return this.admin;
	}



	public void setAdmin(boolean admin) {
		this.admin = admin;
	}



	public long getSosyalMedya() {
		return this.sosyalMedya;
	}



	public void setSosyalMedya(long sosyalMedya) {
		this.sosyalMedya = sosyalMedya;
	}




	public Long getAccount() {
		return this.account;
	}



	public void setAccount(Long account) {
		this.account = account;
	}



	public void setSosyalMedya(Long sosyalMedya) {
		this.sosyalMedya = sosyalMedya;
	}



	public Date getIssueDate() {
		return this.issueDate;
	}



	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}



	private String title;
	
	public List<Long> getListId() {
		return listId;
	}

	 

	public String getTitle() {
		return title;
	}

	@Override
	public Predicate predicate() {
		QAjanda ajanda = QAjanda.ajanda;

		if (listId != null) {
			addExp(ajanda.id.in(listId));
		}
		if (sosyalMedya != null) {
			addExp(ajanda.sosyalmedya.id.eq(sosyalMedya));
		}

		if(type!=0) {
			addExp(ajanda.type.eq(type));
		}
		
		 if(issueDate!=null && listId==null) {
			 addExp(ajanda.issueDate.eq(issueDate));
			 
		 }
		 
		 if(sosyalMedya!=null) {
			 addExp(ajanda.sosyalmedya.id.eq(sosyalMedya));
			 
		 }
		 

			if(!admin) {
				if(account!=null) {
					 addExp(ajanda.account.any().id.eq(account));
					 
				 }
			}
		 

		return predicate;
	}

	public void setListId(List<Long> listId) {
		this.listId = listId;
	}

	 

	public int getType() {
		return this.type;
	}



	public void setType(int type) {
		this.type = type;
	}



	public void setTitle(String title) {
		this.title = title;
	}

}
