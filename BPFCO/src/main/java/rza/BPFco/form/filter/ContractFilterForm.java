package rza.BPFco.form.filter;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import com.mysema.query.types.Predicate;

import rza.BPFco.model.QContract;
import rza.BPFco.model.QPortfoy;
import rza.BPFco.model.portfoys.QCustomer;
import rza.BPFco.model.portfoys.QSection;
import rza.BPFco.utilities.Time;

public class ContractFilterForm extends FilterForm {

	private List<Long> listId;

	private int nametype;
	
	private String sorting;

	public String getSorting() {
		return this.sorting;
	}

	public void setSorting(String sorting) {
		this.sorting = sorting;
	}

	private List<Long> customers;
	
	private List<Long> accounts;
	
	
	private Long account;

	private boolean admin;

	public Long getAccount() {
		return this.account;
	}

	public void setAccount(Long account) {
		this.account = account;
	}

	public boolean isAdmin() {
		return this.admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public List<Long> getAccounts() {
		return this.accounts;
	}

	public void setAccounts(List<Long> accounts) {
		this.accounts = accounts;
	}

	private long project;

	public long getProject() {
		return this.project;
	}

	public void setProject(long project) {
		this.project = project;
	}

	private List<Long> bloks;

	private int active;
	
	private int deed;
	
	private int insEnd;

	public int getInsEnd() {
		return this.insEnd;
	}

	public void setInsEnd(int insEnd) {
		this.insEnd = insEnd;
	}

	public int getDeed() {
		return this.deed;
	}

	public void setDeed(int deed) {
		this.deed = deed;
	}

	private String daterange;

	public int getActive() {
		return this.active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public String getDaterange() {
		return this.daterange;
	}

	public void setDaterange(String daterange) {
		this.daterange = daterange;
	}

	public List<Long> getCustomers() {
		return this.customers;
	}

	public void setCustomers(List<Long> customers) {
		this.customers = customers;
	}

	 

	public long getProjects() {
		return this.project;
	}

	public void setProjects(long project) {
		this.project = project;
	}

	public List<Long> getBloks() {
		return this.bloks;
	}

	public void setBloks(List<Long> bloks) {
		this.bloks = bloks;
	}

	private String title;

	public List<Long> getListId() {
		return listId;
	}

	public int getNametype() {
		return nametype;
	}

	public String getTitle() {
		return title;
	}

	@Override
	public Predicate predicate() {
		QContract contract = QContract.contract;
		
		QPortfoy portfoy = contract.portfoys.any();
		
		QSection section = portfoy.sections.any();

		if (listId != null) {
			addExp(contract.id.in(listId));
		}

		if (customers != null) {
			addExp(contract.customer.any().id.in(customers));
		}
		
		if (accounts != null) {
			addExp(contract.account.any().id.in(accounts));
		}

		if (title != null) {
			addExp(contract.portfoys.any().title.contains(title));
		}

		 
//		if (project != 0l) {
//			addExp(contract.proje.id.eq(project));
//		}
		
		

		addStringExp(contract.contract_no, title, nametype);

		if (daterange != null) {

			String[] ss = daterange.split("-");
			try {
				Date from = Time.ConvertToDate(ss[0]);

				Date to = Time.ConvertToDate(ss[1]);

				addExp(contract.createDate.between(from, to));

			} catch (ParseException exception) {
				// TODO Auto-generated catch-block stub.
				exception.printStackTrace();
			}

		}
		if (active != 0) {
			switch (active) {
			case 1:
				addExp(contract.active.isTrue());
				break;
			case 2:
				addExp(contract.active.isFalse());
				break;

			default:
				break;
			}
		}else {
			addExp(contract.active.isFalse().or(contract.active.isTrue()));
		}
		if (deed != 0) {
			switch (deed) {
			case 1:
				addExp(contract.deed.isTrue());
				break;
			case 2:
				addExp(contract.deed.isFalse());
				break;
				
			default:
				break;
			}
		}else {
			addExp(contract.deed.isFalse().or(contract.deed.isTrue()));
		}
		if (insEnd != 0) {
			switch (insEnd) {
			case 1:
				addExp(contract.insEnd.isTrue());
				break;
			case 2:
				addExp(contract.insEnd.isFalse());
				break;
				
			default:
				break;
			}
		}else {
			addExp(contract.insEnd.isFalse().or(contract.insEnd.isTrue()));
		}
		
		if (!admin) {

			if (account != null) {
				addExp( contract.account.any().id.eq(account));
				 
				 
			}
		}


		return predicate;
	}

	public void setListId(List<Long> listId) {
		this.listId = listId;
	}

	public void setNametype(int nametype) {
		this.nametype = nametype;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
