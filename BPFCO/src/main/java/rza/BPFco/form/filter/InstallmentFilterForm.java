package rza.BPFco.form.filter;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import com.mysema.query.types.Predicate;

import rza.BPFco.model.portfoys.QInstallment;
import rza.BPFco.utilities.Time;

public class InstallmentFilterForm extends FilterForm{
	
	
	private String sorting;
	
	private Long temsilci;

	private boolean admin;
	
	private boolean active;
	
	
	public boolean isActive() {
		return this.active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Long getTemsilci() {
		return this.temsilci;
	}

	public void setTemsilci(Long temsilci) {
		this.temsilci = temsilci;
	}

	public boolean isAdmin() {
		return this.admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public String getSorting() {
		return this.sorting;
	}

	public void setSorting(String sorting) {
		this.sorting = sorting;
	}

	private boolean reminding;
	private boolean warning;
	private boolean thanks;
	
	
	public boolean isWarning() {
		return this.warning;
	}

	public void setWarning(boolean warning) {
		this.warning = warning;
	}

	public boolean isThanks() {
		return this.thanks;
	}

	public void setThanks(boolean thanks) {
		this.thanks = thanks;
	}

	private boolean chackNotify;
	
	private String predi;
	
	public String getPredi() {
		return this.predi;
	}

	public void setPredi(String predi) {
		this.predi = predi;
	}

	public boolean isChackNotify() {
		return this.chackNotify;
	}

	public void setChackNotify(boolean chackNotify) {
		this.chackNotify = chackNotify;
	}

	public boolean isReminding() {
		return this.reminding;
	}

	public void setReminding(boolean reminding) {
		this.reminding = reminding;
	}

	private List<Long> listId;
	
	private List<Long> customers;
	
	private List<Long> contracts;

	public List<Long> getCustomers() {
		return this.customers;
	}

	public void setCustomers(List<Long> customers) {
		this.customers = customers;
	}

	public List<Long> getContracts() {
		return this.contracts;
	}

	public void setContracts(List<Long> contracts) {
		this.contracts = contracts;
	}

	private int nametype;
	
	private int paid;
	
	public int getPaid() {
		return this.paid;
	}

	public void setPaid(int paid) {
		this.paid = paid;
	}

	private long installment;

	private Long id; // contract id

	public String getDaterange() {
		return this.daterange;
	}

	public void setDaterange(String daterange) {
		this.daterange = daterange;
	}

	public void setId(Long id) {
		this.id = id;
	}

	private String daterange;  
	 
 

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getInstallment() {
		return this.installment;
	}

	public void setInstallment(long installment) {
		this.installment = installment;
	}

	private String title;
	
	public List<Long> getListId() {
		return listId;
	}

	public int getNametype() {
		return nametype;
	}

	public String getTitle() {
		return title;
	}

	@Override
	public Predicate predicate() {
		QInstallment installment = QInstallment.installment;
		

		if (listId != null) {
			
			addExp(installment.id.in(listId));
		}
		if (customers != null) {
			if(customers.size()>0) {
				addExp(installment.contract.customer.any().id.in(customers));
				
			}
			
			
		}
		
		if(contracts != null ) {
			if(contracts.size()>0) {
				
				addExp(installment.contract.id.in(contracts));
			}
		}
		if(id != null) {
			addExp(installment.contract.id.in(id));
		}
		
		 if(chackNotify) {
			 addExp(installment.reminding.eq(reminding));
			 addExp(installment.warning.eq(warning));
			 addExp(installment.thanks.eq(thanks));
			 
		 }
		 
		 if (!admin) {

				if (temsilci != null) {
					addExp(installment.contract.account.any().id.eq(temsilci));
				}
			}
		 if (active) {
			 
				 addExp(installment.active.isTrue());
		 }
		 
		 
		 

		if(daterange != null & daterange != "") {
			
			
			String[] ss = daterange.split("-");
			try {
				Date from = Time.ConvertToDate(ss[0]);
				
				Date to =Time.ConvertToDatenew(ss[1]);
				
				addExp(installment.issueDate.between(from, to));
				
			} catch (ParseException exception) {
				// TODO Auto-generated catch-block stub.
				exception.printStackTrace();
			}
			
			
		}
		if(paid != 0) {
			switch (paid) {
			case 1:
				addExp(installment.paid.isTrue());
				break;
			case 2:
				addExp(installment.issueDate.before(Time.todayDateOnly()));
				addExp(installment.paid.isFalse());
				break;
				
			default:
				break;
			}
		}
//		addStringExp(floor.title, title, nametype);
													 

		return predicate;
	}

	public void setListId(List<Long> listId) {
		this.listId = listId;
	}

	public void setNametype(int nametype) {
		this.nametype = nametype;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
