package rza.BPFco.form.filter;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import com.mysema.query.types.Predicate;

import rza.BPFco.model.QPortfoy;
import rza.BPFco.model.portfoys.QChase;
import rza.BPFco.model.portfoys.QSosyalMedya;
import rza.BPFco.utilities.Time;

public class ChaseFilterForm extends FilterForm{
	
private String sorting;
	
	
	 
	
	 

	private List<Long> listId;
	
	private List<Long> temsilci;
	
	 
	private String search;
	  
	 
 
	public String getSearch() {
		return this.search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public List<Long> getTemsilci() {
		return this.temsilci;
	}

	public void setTemsilci(List<Long> temsilci) {
		this.temsilci = temsilci;
	}
	private int language;
	 
	 private Long account;

	 private boolean admin;

	public boolean isAdmin() {
		return this.admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public int getLanguage() {
		return this.language;
	}

	public void setLanguage(int language) {
		this.language = language;
	}

	public Long getAccount() {
		return this.account;
	}

	public void setAccount(Long account) {
		this.account = account;
	}

	private int nametype;

	private String title;
	
	public List<Long> getListId() {
		return listId;
	}

	public int getNametype() {
		return nametype;
	}

	public String getTitle() {
		return title;
	}

	@Override
	public Predicate predicate() {
		QChase chase = QChase.chase;

		if (listId != null) {
			addExp(chase.id.in(listId));
		}
		if (!search.isEmpty()) {
			addExp(chase.title.contains(search));
		}
		if (temsilci != null) {
			if (temsilci.size()>0) {
				
				addExp(chase.account.id.in(temsilci));
			}
		}
		
		 
		addExp(chase.language.eq(language));
		
 		if(!admin) {
			
			if (account != null) {
				addExp(chase.account.id.eq(account));
			}
		}
		
			
		 

		return predicate;
	}

	public void setListId(List<Long> listId) {
		this.listId = listId;
	}

	public void setNametype(int nametype) {
		this.nametype = nametype;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
