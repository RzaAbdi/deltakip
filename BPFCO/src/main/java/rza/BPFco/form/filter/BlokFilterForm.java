package rza.BPFco.form.filter;

import java.util.List;

import com.mysema.query.types.Predicate;

import rza.BPFco.model.QPortfoy;

public class BlokFilterForm extends FilterForm{
	
	
	private List<Long> listId;

	private int nametype;
	
	private long projects;

	public long getProjects() {
		return this.projects;
	}

	public void setProjects(long projects) {
		this.projects = projects;
	}

	private String title;
	
	public List<Long> getListId() {
		return listId;
	}

	public int getNametype() {
		return nametype;
	}

	public String getTitle() {
		return title;
	}

	@Override
	public Predicate predicate() {
		QPortfoy portfoy = QPortfoy.portfoy;

		if (listId != null) {
			addExp(portfoy.id.in(listId));
		}

		addStringExp(portfoy.title, title, nametype);
													 

		return predicate;
	}

	public void setListId(List<Long> listId) {
		this.listId = listId;
	}

	public void setNametype(int nametype) {
		this.nametype = nametype;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
