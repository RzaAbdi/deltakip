package rza.BPFco.form.filter;

import java.util.List;

import com.mysema.query.types.Predicate;

import rza.BPFco.model.QPortfoy;
import rza.BPFco.model.portfoys.QKat;

public class FloorFilterForm extends FilterForm{
	
	
	private List<Long> listId;

	private int nametype;
	
	private long bloks;

	 

	public long getBloks() {
		return this.bloks;
	}

	public void setBloks(long bloks) {
		this.bloks = bloks;
	}

	private String title;
	
	public List<Long> getListId() {
		return listId;
	}

	public int getNametype() {
		return nametype;
	}

	public String getTitle() {
		return title;
	}

	@Override
	public Predicate predicate() {
		QKat floor = QKat.kat;

		if (listId != null) {
			addExp(floor.id.in(listId));
		}

//		addStringExp(floor.title, title, nametype);
													 

		return predicate;
	}

	public void setListId(List<Long> listId) {
		this.listId = listId;
	}

	public void setNametype(int nametype) {
		this.nametype = nametype;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
