package rza.BPFco.form.filter;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import com.mysema.query.types.Predicate;

import rza.BPFco.model.portfoys.QDocument;
import rza.BPFco.utilities.Time;

public class DocumentFilterForm extends FilterForm {

	private List<Long> listId;
	
	private List<Long> installments;

	public List<Long> getInstallments() {
		return this.installments;
	}

	public void setInstallments(List<Long> installments) {
		this.installments = installments;
	}

	private int nametype;

	private long types;
	
	private Long temsilci;
	
	private boolean controlled;
	
	public boolean isControlled() {
		return this.controlled;
	}

	public void setControlled(boolean controlled) {
		this.controlled = controlled;
	}

	public Long getTemsilci() {
		return this.temsilci;
	}

	public void setTemsilci(Long temsilci) {
		this.temsilci = temsilci;
	}

	public boolean isAdmin() {
		return this.admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	private boolean admin;

	private Long docType;

	 

	public Long getDocType() {
		return this.docType;
	}

	public void setDocType(Long docType) {
		this.docType = docType;
	}

	private Long insid;
	private Long cid;
	private Long customerid;
	private String customer;
	 

	public String getCustomer() {
		return this.customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	private Long agentid;
	
	private String daterange;

	public String getDaterange() {
		return this.daterange;
	}

	public void setDaterange(String daterange) {
		this.daterange = daterange;
	}

	public Long getAgentid() {
		return this.agentid;
	}

	public void setAgentid(Long agentid) {
		this.agentid = agentid;
	}

	public Long getCustomerid() {
		return this.customerid;
	}

	public void setCustomerid(Long customerid) {
		this.customerid = customerid;
	}

	public Long getCid() {
		return this.cid;
	}

	public void setCid(Long cid) {
		this.cid = cid;
	}

	private String title;

	public List<Long> getListId() {
		return listId;
	}

	public int getNametype() {
		return nametype;
	}

	public String getTitle() {
		return title;
	}

	@Override
	public Predicate predicate() {
		QDocument document = QDocument.document;
		
		predicate = null;

		if (listId != null) {
			addExp(document.id.in(listId));
		}
		if (insid != null) {
			addExp(document.installment.any().id.eq(insid));
		}
		if (cid != null) {
			addExp(document.contract.any().id.eq(cid));
		}
		if (customerid != null) {
			addExp(document.customer.any().id.eq(customerid));
		}
		if (agentid != null) {
			addExp(document.agent.any().id.eq(agentid));
		}
		if (docType != null) {
			addExp(document.doctype.any().id.eq(docType));
		}
		if (customer != null) {
			addExp(document.customer.any().fname.contains(customer));
		}
		if (controlled ==true) {
			addExp(document.doctype.any().controlled.isTrue());
		}
		 

		addStringExp(document.title, title, nametype);

		if (!admin) {

			if (temsilci != null) {
				addExp(document.account.id.eq(temsilci));
				addExp(document.doctype.any().controlled.eq(false));
			}
		}
		

		if (daterange != null & daterange != "") {
			String[] ss = daterange.split("-");
			try {
				Date from = Time.ConvertToDate(ss[0]);

				Date to = Time.ConvertToDate(ss[1]);

				addExp(document.create_date.between(from, to));

			} catch (ParseException exception) {
				// TODO Auto-generated catch-block stub.
				exception.printStackTrace();
			}
		}

		return predicate;
	}

	public long getTypes() {
		return this.types;
	}

	public void setTypes(long types) {
		this.types = types;
	}

	public Long getInsid() {
		return this.insid;
	}

	public void setInsid(Long insid) {
		this.insid = insid;
	}

	public void setListId(List<Long> listId) {
		this.listId = listId;
	}

	public void setNametype(int nametype) {
		this.nametype = nametype;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
