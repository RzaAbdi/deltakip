package rza.BPFco.form.filter;

import java.util.List;

import com.mysema.query.types.Predicate;

import rza.BPFco.model.portfoys.QAgent;

public class AgentFilterForm extends FilterForm{
	
	
	private List<Long> listId;

	private int nametype;
	
	private long agent;

	 

	 

	 

	public long getAgent() {
		return this.agent;
	}

	public void setAgent(long agent) {
		this.agent = agent;
	}

	private String title;
	
	public List<Long> getListId() {
		return listId;
	}

	public int getNametype() {
		return nametype;
	}

	public String getTitle() {
		return title;
	}

	@Override
	public Predicate predicate() {
		QAgent agent = QAgent.agent;

		if (listId != null) {
			addExp(agent.id.in(listId));
		}

//		addStringExp(floor.title, title, nametype);
													 

		return predicate;
	}

	public void setListId(List<Long> listId) {
		this.listId = listId;
	}

	public void setNametype(int nametype) {
		this.nametype = nametype;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
