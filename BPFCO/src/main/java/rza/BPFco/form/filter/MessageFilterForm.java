package rza.BPFco.form.filter;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import com.mysema.query.types.Predicate;

import rza.BPFco.model.QContract;
import rza.BPFco.model.portfoys.QMes;
import rza.BPFco.utilities.Time;

public class MessageFilterForm extends FilterForm{
	
	
	private Long insid;
	
	private List<Long> listId;
	
	private List<Long> customers;
	
	private String cusomer;
	
	public String getCusomer() {
		return this.cusomer;
	}

	public void setCusomer(String cusomer) {
		this.cusomer = cusomer;
	}

	private List<Long> contracts;

	public List<Long> getCustomers() {
		return this.customers;
	}

	public void setCustomers(List<Long> customers) {
		this.customers = customers;
	}

	public List<Long> getContracts() {
		return this.contracts;
	}

	public void setContracts(List<Long> contracts) {
		this.contracts = contracts;
	}

	private int nametype;
	
	private int paid;
	
	public int getPaid() {
		return this.paid;
	}

	public void setPaid(int paid) {
		this.paid = paid;
	}

	private long installment;

	private Long id; // contract id

	public String getDaterange() {
		return this.daterange;
	}

	public void setDaterange(String daterange) {
		this.daterange = daterange;
	}

	public void setId(Long id) {
		this.id = id;
	}

	private String daterange;  
	 
 

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getInstallment() {
		return this.installment;
	}

	public void setInstallment(long installment) {
		this.installment = installment;
	}

	private String title;
	
	public List<Long> getListId() {
		return listId;
	}

	public int getNametype() {
		return nametype;
	}

	public String getTitle() {
		return title;
	}

	@Override
	public Predicate predicate() {
		QMes mes = QMes.mes;
		

		if (listId != null) {
			addExp(mes.id.in(listId));
		}
		 
		if (insid != null) {
			addExp(mes.installment.any().id.eq(insid));
		}
		
		if (cusomer != null) {
			addExp(mes.text.contains(cusomer));
		}
		

		if(daterange != null & daterange != "") {
			String[] ss = daterange.split("-");
			try {
				Date from = Time.ConvertToDate(ss[0]);
				
				Date to =Time.ConvertToDate(ss[1]);
				
				addExp(mes.sendDate.between(from, to));
				
			} catch (ParseException exception) {
				// TODO Auto-generated catch-block stub.
				exception.printStackTrace();
			}
			
			
		}
		
		
//		addStringExp(floor.title, title, nametype);
													 

		return predicate;
	}

	public Long getInsid() {
		return this.insid;
	}

	public void setInsid(Long insid) {
		this.insid = insid;
	}

	public void setListId(List<Long> listId) {
		this.listId = listId;
	}

	public void setNametype(int nametype) {
		this.nametype = nametype;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
