package rza.BPFco.form.filter;

import java.util.List;

import com.mysema.query.types.Predicate;

import rza.BPFco.model.portfoys.QCover;
import rza.BPFco.model.portfoys.QDocument;

public class CoverFilterForm extends FilterForm {

	private List<Long> listId;

	private int nametype;

	
	public Long getTemsilci() {
		return this.temsilci;
	}

	public void setTemsilci(Long temsilci) {
		this.temsilci = temsilci;
	}

	public boolean isAdmin() {
		return this.admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	private Long temsilci;

	private boolean admin;


	private Long customerid;

	 
	private Long id;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCustomerid() {
		return this.customerid;
	}

	public void setCustomerid(Long customerid) {
		this.customerid = customerid;
	}

	 

	private String title;

	public List<Long> getListId() {
		return listId;
	}

	public int getNametype() {
		return nametype;
	}

	public String getTitle() {
		return title;
	}

	@Override
	public Predicate predicate() {
		QCover cover = QCover.cover;

		if (listId != null) {
			addExp(cover.id.in(listId));
		}
		 
		if (customerid != null) {
			addExp(cover.customer.any().id.eq(customerid));
		}
		if (id != null) {
			addExp(cover.id.eq(id));
		}


		if (!admin) {

			if (temsilci != null) {
				addExp(cover.account.any().id.eq(temsilci));
			}
		}

		return predicate;
	}


	public void setListId(List<Long> listId) {
		this.listId = listId;
	}

	public void setNametype(int nametype) {
		this.nametype = nametype;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
