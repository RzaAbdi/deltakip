package rza.BPFco.form.filter;

import java.util.List;

import com.mysema.query.types.Predicate;

import rza.BPFco.model.QPortfoy;

public class PortfoyFilterForm extends FilterForm{
	
	
	private List<Long> listId;
	
	private List<Long> list_tag;
	private List<Long> list_floors;
	private List<Long> list_bloks;

	public List<Long> getList_floors() {
		return this.list_floors;
	}

	public void setList_floors(List<Long> list_floors) {
		this.list_floors = list_floors;
	}

	public List<Long> getList_bloks() {
		return this.list_bloks;
	}

	public void setList_bloks(List<Long> list_bloks) {
		this.list_bloks = list_bloks;
	}

	public List<Long> getList_tag() {
		return this.list_tag;
	}

	public void setList_tag(List<Long> list_tag) {
		this.list_tag = list_tag;
	}

	private int nametype;

	private String title;
	
	public List<Long> getListId() {
		return listId;
	}

	public int getNametype() {
		return nametype;
	}

	public String getTitle() {
		return title;
	}

	@Override
	public Predicate predicate() {
		QPortfoy portfoy = QPortfoy.portfoy;

		if (listId != null) {
			addExp(portfoy.id.in(listId));
		}
		
		if (list_tag != null) {
			addExp(portfoy.tag.any().id.in(list_tag));
			
		}
		if (list_bloks != null) {
			addExp(portfoy.sections.any().kat.blok.id.in(list_bloks));
			
		}
		if (list_floors != null) {
			addExp(portfoy.sections.any().kat.id.in(list_floors));
			
		}
		if (title != null) {
			addExp(portfoy.sections.any().title.eq(title));
			
		}
		
		

		 
		
		
													 

		return predicate;
	}

	public void setListId(List<Long> listId) {
		this.listId = listId;
	}

	public void setNametype(int nametype) {
		this.nametype = nametype;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
