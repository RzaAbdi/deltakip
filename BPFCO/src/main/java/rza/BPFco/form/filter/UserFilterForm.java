package rza.BPFco.form.filter;

import java.util.List;

import com.mysema.query.types.Predicate;

import rza.BPFco.model.QAccount;

public class UserFilterForm  extends FilterForm{

	private List<Long> listId;



	private int emailtype;


	private String email;


	private String active;
	

	private Boolean admin;

	public void setListId(List<Long> listId) {
		this.listId = listId;
	}


	public void setAdmin(Boolean admin) {
		this.admin = admin;
	}


	public void setActive(String active) {
		this.active = active;
	}


	 


	public void setEmailtype(int emailtype) {
		this.emailtype = emailtype;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public Predicate predicate() {
		QAccount account = QAccount.account;

		if (listId != null) {
			addExp(account.id.in(listId));
		}
		if (active != null) {
			addExp(account.active.eq(Boolean.valueOf(active)));
		}
		 


		addStringExp(account.email, email, emailtype);


		return predicate;
	}



}