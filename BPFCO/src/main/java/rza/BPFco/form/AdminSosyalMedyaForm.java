package rza.BPFco.form;


import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import rza.BPFco.model.portfoys.SosyalMedya;
import rza.BPFco.utilities.Time;

public class AdminSosyalMedyaForm {
	
	
	public String getColor() {
		return this.color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public static Logger getLogger() {
		return logger;
	}

	static final Logger logger = LogManager.getLogger();

	private Long id;
	
	private Long chaseId;
	
	public Long getChaseId() {
		return this.chaseId;
	}

	public void setChaseId(Long chaseId) {
		this.chaseId = chaseId;
	}

	private int type;
	
	private boolean follow;
	
	public boolean isFollow() {
		return this.follow;
	}

	public void setFollow(boolean follow) {
		this.follow = follow;
	}

	private String color;
	
	private String title;
	
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	private String name;
	
	public int getType() {
		return this.type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNation() {
		return this.nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}

	public String getGsm() {
		return this.gsm;
	}

	public void setGsm(String gsm) {
		this.gsm = gsm;
	}

	public List<Long> getAccount() {
		return this.account;
	}

	public void setAccount(List<Long> account) {
		this.account = account;
	}

	public String getDsc() {
		return this.dsc;
	}

	public void setDsc(String dsc) {
		this.dsc = dsc;
	}

	 
	public int getLanguage() {
		return this.language;
	}

	public void setLanguage(int language) {
		this.language = language;
	}

	public String getFirstPlatform() {
		return this.firstPlatform;
	}

	public void setFirstPlatform(String firstPlatform) {
		this.firstPlatform = firstPlatform;
	}

	public String getRecentplatform() {
		return this.recentplatform;
	}

	public void setRecentplatform(String recentplatform) {
		this.recentplatform = recentplatform;
	}

	 

	public String getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getFirstInfo() {
		return this.firstInfo;
	}

	public void setFirstInfo(String firstInfo) {
		this.firstInfo = firstInfo;
	}

	public int getPriority() {
		return this.priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	private String email;
	
	private String nation;
	
	private String gsm;
	
	private List<Long> account;
	
	private String dsc;
	
	private int language;
	
	private String firstPlatform;
	
	private String recentplatform;
	
	private String createDate;
	
	private String firstInfo;
	
	private int priority;
	
	
	 

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	 
	
	public SosyalMedya createSM() throws ParseException {
		SosyalMedya sm = new SosyalMedya();
		sm.setName(name);
		sm.setEmail(email);
		sm.setNation(nation);
		sm.setGsm(gsm);
		sm.setDsc(dsc);
		sm.setFirstPlatform(firstPlatform);
		sm.setCurrentPlatform(recentplatform);
		sm.setFirstInfo(firstInfo);
		sm.setCreateDate(Time.ConvertToDateTime(createDate));
		sm.setPriority(priority);
		sm.setLanguage(language);
		sm.setFollow(follow);
		sm.setType(type);
		
		return sm;
	}
	
	public SosyalMedya manipulatSM(SosyalMedya sm) throws Exception {
		sm.setName(name);
		sm.setEmail(email);
		sm.setNation(nation);
		sm.setGsm(gsm);
		sm.setDsc(dsc);
		sm.setFirstPlatform(firstPlatform);
		sm.setCurrentPlatform(recentplatform);
		sm.setFirstInfo(firstInfo);
		sm.setCreateDate(Time.ConvertToDateTime(createDate));
		sm.setPriority(priority); 
		sm.setLanguage(language);
		sm.setFollow(follow);
		sm.setType(type);

		return sm;
	}

	 
}
