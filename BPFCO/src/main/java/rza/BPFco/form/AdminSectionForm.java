package rza.BPFco.form;


import java.text.ParseException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.validator.constraints.NotBlank;

import rza.BPFco.model.portfoys.Section;
import rza.BPFco.utilities.Time;

public class AdminSectionForm {
	
	
	public static Logger getLogger() {
		return logger;
	}

	static final Logger logger = LogManager.getLogger();

	private Long id;
	
	public String getTitle2() {
		return this.title2;
	}

	public void setTitle2(String title2) {
		this.title2 = title2;
	}

	private String title;
	
	@NotBlank(message = "Title can't be empty")
	private String title2;
	
	 
	private String floors;

	 
 
	 
 

 

	 

	public String getFloors() {
		return this.floors;
	}

	public void setFloors(String floors) {
		this.floors = floors;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	 
	
	

  

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Section createSection() throws ParseException {
		Section kat = new Section();
		kat.setTitle(title);
		kat.setCreate_date(Time.todayDateTime());
		return kat;
	}
	
	public Section manipulatSection(Section floor) throws Exception {
		floor.setTitle(title);
		floor.setEdit_date(Time.todayDateTime());
		return floor;
	}
	 
}
