package rza.BPFco.form;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import rza.BPFco.model.Ajanda;
import rza.BPFco.model.portfoys.Agent;
import rza.BPFco.utilities.Time;

public class AdminAjandaForm {

	static final Logger logger = LogManager.getLogger();

	public static Logger getLogger() {
		return logger;
	}

	private Long id;

	private Date editDate;
	
	private Date createDate;
	
	private Date issueDate;

	private String title;
	
	private int type;
	
	private boolean done;

	public boolean isDone() {
		return this.done;
	}



	public void setDone(boolean done) {
		this.done = done;
	}

	private List<Long> account;
	
	private Long sosyal;
 

 

	public List<Long> getAccount() {
		return this.account;
	}



	public void setAccount(List<Long> account) {
		this.account = account;
	}



 



	public Long getSosyal() {
		return this.sosyal;
	}



	public void setSosyal(Long sosyal) {
		this.sosyal = sosyal;
	}



	public int getType() {
		return this.type;
	}



	public void setType(int type) {
		this.type = type;
	}



	public Ajanda createAgent() {
		Ajanda ajanda = new Ajanda();
		if(createDate!=null) {
			ajanda.setCreateDate(createDate);
		}else {
			ajanda.setCreateDate(Time.today());
		}
		
		ajanda.setIssueDate(issueDate);
		ajanda.setTitle(title);
		ajanda.setType(type);
		ajanda.setDone(false);
		ajanda.setStatus(1);

		return ajanda;
	}

	 

	public Date getEditDate() {
		return this.editDate;
	}



	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}



	public Date getCreateDate() {
		return this.createDate;
	}



	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}



	public Date getIssueDate() {
		return this.issueDate;
	}



	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}



	public String getTitle() {
		return this.title;
	}



	public void setTitle(String title) {
		this.title = title;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public Long getId() {
		return this.id;
	}

 

	 

	public Ajanda manipulatAgent(Ajanda ajanda) throws Exception {
		ajanda.setIssueDate(issueDate);
		Date last = Time.today();
		ajanda.setLastUpdate(last);
		ajanda.setTitle(title);
		ajanda.setType(type);
		ajanda.setDone(false);
		
		return ajanda;
	}

	 

}
