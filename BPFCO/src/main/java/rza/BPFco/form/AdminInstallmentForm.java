package rza.BPFco.form;

import java.text.ParseException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import rza.BPFco.model.portfoys.Installment;
import rza.BPFco.utilities.Time;

public class AdminInstallmentForm {

	public static Logger getLogger() {
		return logger;
	}

	static final Logger logger = LogManager.getLogger();

	private long contract_no;// id

	private long installment;

	public long getContract_no() {
		return this.contract_no;
	}

	public void setContract_no(long contract_no) {
		this.contract_no = contract_no;
	}

	private Long id;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	private String description;
	private String amount;
	private String partial_amount;
	private String issue_date;
	private String paymant_date;
	private boolean paid;
	private boolean partial;

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPartial_amount() {
		return this.partial_amount;
	}

	public void setPartial_amount(String partial_amount) {
		this.partial_amount = partial_amount;
	}

	public boolean isPartial() {
		return this.partial;
	}

	public void setPartial(boolean partial) {
		this.partial = partial;
	}

	public Installment createInstallment() throws ParseException {
		Installment installment = new Installment();
		installment.setAmount(amount);
		installment.setIssueDate(Time.ConvertToDate(issue_date));
		installment.setDescription(description);
		installment.setPartial_amount(partial_amount);
 		installment.setPartial(false);
//		if (paymant_date != "" || paymant_date!= null ) {
//
//			installment.setPaymant_date(Time.ConvertToDate(paymant_date));
//		}
		 
		installment.setPaid(false);

		return installment;
	}

	public Installment manipulatInstallment(Installment installment) throws Exception {
		installment.setAmount(amount);
		installment.setIssueDate(Time.ConvertToDate(issue_date));
		if (paymant_date != "") {

			installment.setPaymantDate(Time.ConvertToDate(paymant_date));
		}else {
			installment.setPaymantDate(null);
		}
		installment.setDescription(description);
		installment.setPaid(paid);
		installment.setPartial(partial);
		if(partial) {
			if(partial_amount.isEmpty()) {
				return null;
			}else {
				
				installment.setPartial_amount(partial_amount);
			}
		}
		return installment;
	}

	public long getInstallment() {
		return this.installment;
	}

	public void setInstallment(long installment) {
		this.installment = installment;
	}

	public String getAmount() {
		return this.amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getIssue_date() {
		return this.issue_date;
	}

	public void setIssue_date(String issue_date) {
		this.issue_date = issue_date;
	}

	public String getPaymant_date() {
		return this.paymant_date;
	}

	public void setPaymant_date(String paymant_date) {
		this.paymant_date = paymant_date;
	}

	public boolean isPaid() {
		return this.paid;
	}

	public void setPaid(boolean paid) {
		this.paid = paid;
	}

}

///////////////////////////dsds