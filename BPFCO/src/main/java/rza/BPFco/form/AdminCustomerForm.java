package rza.BPFco.form;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import rza.BPFco.model.Tag;
import rza.BPFco.model.portfoys.Customer;
import rza.BPFco.utilities.Time;

public class AdminCustomerForm {

	static final Logger logger = LogManager.getLogger();

	public static Logger getLogger() {
		return logger;
	}

	private Long id;
	
private String taxOffice;

	public String getTaxOffice() {
	return this.taxOffice;
}

public void setTaxOffice(String taxOffice) {
	this.taxOffice = taxOffice;
}

	public String getMail() {
	return this.mail;
}

public void setMail(String mail) {
	this.mail = mail;
}

	private Date edit_date;

	private String fname;
	private String mail;

	private String lname;

	private String gsm1;

	private String gsm2;

	private int source;

	public int getSource() {
		return this.source;
	}

	public void setSource(int source) {
		this.source = source;
	}

	private String nationality;

	private Long tax;

	private Long tc;

	private String passport;

	private String address;

	public Customer createCustomer() {
		Customer customer = new Customer();
		customer.setAddress(address);
		customer.setCreate_date(Time.today());
		customer.setFname(fname);
		customer.setGsm1(gsm1);
		customer.setGsm2(gsm2);
		customer.setLname(lname);
		customer.setNationality(nationality);
		customer.setPassport(passport);
		customer.setTax(tax);
		customer.setTc(tc);
		customer.setSource(source);
		customer.setTaxOffice(taxOffice);
		customer.setMail(mail);

		return customer;
	}

	public String getAddress() {
		return this.address;
	}

	public Date getEdit_date() {
		return this.edit_date;
	}

	public String getFname() {
		return this.fname;
	}

	public String getGsm1() {
		return this.gsm1;
	}

	public String getGsm2() {
		return this.gsm2;
	}

	public Long getId() {
		return this.id;
	}

	public String getLname() {
		return this.lname;
	}

	public String getNationality() {
		return this.nationality;
	}

	public String getPassport() {
		return this.passport;
	}

	public Long getTax() {
		return this.tax;
	}

	public Long getTc() {
		return this.tc;
	}

	public Customer manipulatCustomer(Customer customer) throws Exception {
		customer.setAddress(address);
		customer.setEdit_date(Time.today());
		customer.setFname(fname);
		customer.setGsm1(gsm1);
		customer.setGsm2(gsm2);
		customer.setLname(lname);
		customer.setNationality(nationality);
		customer.setPassport(passport);
		customer.setTax(tax);
		customer.setTc(tc);
		customer.setSource(source);
		customer.setTaxOffice(taxOffice);
		customer.setMail(mail);


		return customer;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setEdit_date(Date edit_date) {
		this.edit_date = edit_date;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public void setGsm1(String gsm1) {
		this.gsm1 = gsm1;
	}

	public void setGsm2(String gsm2) {
		this.gsm2 = gsm2;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public void setPassport(String passport) {
		this.passport = passport;
	}

	public void setTax(Long tax) {
		this.tax = tax;
	}

	public void setTc(Long tc) {
		this.tc = tc;
	}

}
