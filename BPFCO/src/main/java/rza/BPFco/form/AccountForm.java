package rza.BPFco.form;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import rza.BPFco.model.Account;

public class AccountForm {

	static final Logger logger = LogManager.getLogger();

	private Long id;

	private Long userrole;

	public Long getUserrole() {
		return userrole;
	}

	public void setUserrole(Long userrole) {
		this.userrole = userrole;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	private String name;
	private String mail;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static Logger getLogger() {
		return logger;
	}

	private boolean active;
	
	public boolean isCallFa() {
		return this.callFa;
	}

	public void setCallFa(boolean callFa) {
		this.callFa = callFa;
	}

	public boolean isCallAr() {
		return this.callAr;
	}

	public void setCallAr(boolean callAr) {
		this.callAr = callAr;
	}

	private boolean watcher;
	
	private boolean callFa;
	
	private boolean callAr;

	private boolean admin;

	public Account saveProfPath(Account account) throws Exception {

		return account;
	}

	public Account manipulatAccount(Account account) throws Exception {
		account.setActive(active);
		account.setName(name);
		account.setEmail(mail);
		account.setAdmin(admin);
		account.setWatcher(watcher);
		account.setCallAr(callAr);
		account.setCallFa(callFa);
		return account;
	}

	public boolean isWatcher() {
		return this.watcher;
	}

	public void setWatcher(boolean watcher) {
		this.watcher = watcher;
	}

	public String getMail() {
		return this.mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public boolean isAdmin() {
		return this.admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
}
