package rza.BPFco.form;


import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.validator.constraints.NotBlank;

import rza.BPFco.model.Portfoy;
import rza.BPFco.model.portfoys.Section;

public class AdminPortfoyForm {
	
	
	public static Logger getLogger() {
		return logger;
	}

	static final Logger logger = LogManager.getLogger();

	private Long id;
	
	@NotBlank(message = "Title can't be empty")
	private String title;
	
	private List<Long> tag;
	
	 
	private List<Long> sections;

	public List<Long> getTag() {
		return tag;
	}

	public void setTag(List<Long> tag) {
		this.tag = tag;
	}

 
	private boolean reserved;
 
	 

	

	 
 
	public boolean isReserved() {
		return this.reserved;
	}

	public void setReserved(boolean reserved) {
		this.reserved = reserved;
	}

	public List<Long> getSections() {
		return this.sections;
	}

	public void setSections(List<Long> sections) {
		this.sections = sections;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public Portfoy createPortfoys() {
		Portfoy portfoy = new Portfoy();
		portfoy.setTitle(title);
		portfoy.setReserved(reserved);

 
		return portfoy;
	}
	
	public Portfoy manipulatPortfoy(Portfoy portfoy) throws Exception {
		portfoy.setTitle(title);
		portfoy.setReserved(reserved);
//		portfoy.setActive(active);
		return portfoy;
	}

	 
}
