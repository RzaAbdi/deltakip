package rza.BPFco.form;


import java.text.ParseException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.validator.constraints.NotBlank;

import rza.BPFco.model.portfoys.Blok;
import rza.BPFco.model.portfoys.Kat;
import rza.BPFco.utilities.Time;

public class AdminFloorForm {
	
	
	public static Logger getLogger() {
		return logger;
	}

	static final Logger logger = LogManager.getLogger();

	private Long id;
	
	@NotBlank(message = "Title can't be empty")
	private String title;
	
	 
	private long bloks;

	 
 
	public long getBloks() {
		return this.bloks;
	}

	public void setBloks(long bloks) {
		this.bloks = bloks;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	

 
	public Kat createFloor() throws ParseException {
		Kat kat = new Kat();
		kat.setTitle(title);
		kat.setCreate_date(Time.todayDateTime());
		return kat;
	}
	
	public Kat manipulatFloor(Kat floor) throws Exception {
		floor.setTitle(title);
		floor.setEdit_date(Time.todayDateTime());
		return floor;
	}
	 
}
