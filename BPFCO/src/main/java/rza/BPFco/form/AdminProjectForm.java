package rza.BPFco.form;


import java.text.ParseException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.validator.constraints.NotBlank;

import rza.BPFco.model.Portfoy;
import rza.BPFco.model.Tag;
import rza.BPFco.model.portfoys.Blok;
import rza.BPFco.model.portfoys.Proje;
import rza.BPFco.utilities.Time;

public class AdminProjectForm {
	
	
	public static Logger getLogger() {
		return logger;
	}

	static final Logger logger = LogManager.getLogger();

	private Long id;
	
	@NotBlank(message = "Title can't be empty")
	private String title;
	
	private String ada;
	
	private String address;
	
	 
	
	 
	 
 
	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAda() {
		return this.ada;
	}

	public void setAda(String ada) {
		this.ada = ada;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public Proje createProject() throws ParseException {
		Proje proje = new Proje();
		proje.setTitle(title);
		proje.setAda(ada);
		proje.setAddress(address);
		proje.setCreate_date(Time.todayDateTime());
		return proje;
	}
	
	public Proje manipulatProject(Proje proje) throws Exception {
		proje.setTitle(title);
		proje.setAda(ada);
		proje.setAddress(address);
		proje.setEdit_date(Time.todayDateTime());
		return proje;
	}

 

	 
}
