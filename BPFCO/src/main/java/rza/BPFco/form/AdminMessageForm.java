package rza.BPFco.form;

import java.text.ParseException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.validator.constraints.NotBlank;

import rza.BPFco.model.portfoys.Mes;
import rza.BPFco.utilities.Time;

public class AdminMessageForm {

	public Long getId() {
		return this.id;
	}

	public static Logger getLogger() {
		return logger;
	}

	static final Logger logger = LogManager.getLogger();

	private long id;
	
	
	private long insid;

	 

	public long getInsid() {
		return this.insid;
	}

	public void setInsid(long insid) {
		this.insid = insid;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	private int type;

	private String sendDate;

	@NotBlank(message = "Title can't be empty")
	private String desc;

	 

	public int getType() {
		return this.type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getSendDate() {
		return this.sendDate;
	}

	public void setSendDate(String sendDate) {
		this.sendDate = sendDate;
	}

	public String getDesc() {
		return this.desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Mes createMessage() throws ParseException {
		Mes message = new Mes();
		message.setText(desc);
		if (message.getSendDate() != null) {
			message.setSendDate(Time.ConvertToDatenew(sendDate));
		}else {
			message.setSendDate(Time.todayDateOnly());
		}
		message.setType(type);

		return message;
	}

	public Mes manipulatMessage(Mes message) throws Exception {
		
		message.setText(desc);
		if (message.getSendDate() != null) {
			message.setSendDate(Time.ConvertToDatenew(sendDate));
		}else {
			message.setSendDate(Time.todayDateOnly());
		}
		message.setType(type);

		return message;
	}

}
