package rza.BPFco.service;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.mysema.query.types.ProjectionRole;

import rza.BPFco.form.filter.BlokFilterForm;
import rza.BPFco.form.filter.FloorFilterForm;
import rza.BPFco.form.filter.PortfoyFilterForm;
import rza.BPFco.form.filter.SectionFilterForm;
import rza.BPFco.form.filter.TagFilterForm;
import rza.BPFco.model.Portfoy;
import rza.BPFco.model.Tag;
import rza.BPFco.model.portfoys.Blok;
import rza.BPFco.model.portfoys.Kat;
import rza.BPFco.model.portfoys.Proje;
import rza.BPFco.model.portfoys.Section;
import rza.BPFco.repository.PortfoyRepository;
import rza.BPFco.repository.portfoy.BlokRepository;
import rza.BPFco.repository.portfoy.FloorRepository;
import rza.BPFco.repository.portfoy.ProjectRepository;
import rza.BPFco.repository.portfoy.SectionRepository;
import rza.BPFco.repository.tag.TagRepository;

@Service("portfoySerivce")
public class PortfoyServiceImpl implements PortfoyService {

	@Autowired
	PortfoyRepository portfoyRepository;

	@Autowired
	ProjectRepository projectRepository;

	@Autowired
	BlokRepository blokRepository;

	@Autowired
	FloorRepository floorRepository;

	@Autowired
	SectionRepository sectionRepository;

	@Override
	public Page<Portfoy> loadAllPortfoys(PortfoyFilterForm form) {
		return portfoyRepository.findAll(form.predicate(),form.getPage() );
	}

	@Override
	public Portfoy loadPortfoyById(Long id) {
		return portfoyRepository.loadPortfoyByIdFetchTags(id);
	}

	@Override
	public void save(Portfoy portfoy) {
		portfoyRepository.save(portfoy);
	}

	@Override
	public Portfoy findByPortfoyTitle(String title) {

		return portfoyRepository.findByTitle(title);
	}

	@Override
	public Page<Portfoy> searchAllOpenPortfoys(String searchTerm, int pageNumber) {
		PageRequest page = new PageRequest(pageNumber - 1, 20, Sort.Direction.DESC, "id");
		return portfoyRepository.searchAllOpenPortfoys(searchTerm, page);
	}
	
	@Override
	public Page<Portfoy> searchAllPortfoys(String searchTerm, int pageNumber) {
		PageRequest page = new PageRequest(pageNumber - 1, 20, Sort.Direction.DESC, "id");
		return portfoyRepository.searchAllPortfoys(searchTerm, page);
	}

	@Override
	public Set<Portfoy> findPortfoysByIds(List<Long> portfoyIds) {
		return portfoyRepository.findPortfoysByIds(portfoyIds);
	}

	@Override
	public Page<Proje> loadAllProjects(PortfoyFilterForm form) {
		return projectRepository.findAll(form.getPage());
	}

	@Override
	public Page<Blok> loadAllBloks(BlokFilterForm form) {
		return blokRepository.findAll(form.getPage());
	}

	@Override
	public Proje loadProjectById(Long id) {
		return projectRepository.findOne(id);
	}

	@Override
	public void save(Proje proje) {
		projectRepository.save(proje);

	}

	@Override
	public Proje findByProjectTitle(String title) {

		return projectRepository.findByTitle(title);
	}

	@Override
	public void remove(List<Proje> projects) {
		projectRepository.delete(projects);

	}

	@Override
	public Page<Proje> searchAllProjects(String searchTerm, int pageNumber) {
		PageRequest page = new PageRequest(pageNumber - 1, 20, Sort.Direction.DESC, "id");
		return projectRepository.searchAllProjects(searchTerm, page);
	}

	@Override
	public Blok loadBlokById(long id) {
		return blokRepository.findOne(id);
	}

	@Override
	public void save(Blok blok) {
		blokRepository.save(blok);
	}

	@Override
	public Blok findByBlokTitle(String title) {
		return blokRepository.findByTitle(title);
	}

	@Override
	public void removeBlok(Blok blok) {
		blokRepository.delete(blok);
	}

	@Override
	public void removeBlok(List<Blok> projects) {
		blokRepository.delete(projects);

	}

	@Override
	public Page<Kat> loadAllFloors(FloorFilterForm form) {
		return floorRepository.findAll(form.getPage());
	}

	@Override
	public Kat loadFloorById(Long id) {
		return floorRepository.findOne(id);
	}

	@Override
	public void saveFloor(Kat floor) {
		floorRepository.save(floor);

	}


	@Override
	public Page<Blok> searchAllBloks(String searchTerm, int pageNumber) {
		PageRequest page = new PageRequest(pageNumber - 1, 20, Sort.Direction.DESC, "id");
		return blokRepository.searchAllBloks(searchTerm, page);
	}

	@Override
	public void removeFloor(List<Kat> projects) {
		floorRepository.delete(projects);

	}

	@Override
	public Page<Section> loadAllSections(SectionFilterForm form) {
		return sectionRepository.findAll(form.getPage());

	}

	@Override
	public Section loadSectionById(long id) {
		return sectionRepository.findOne(id);
	}

	@Override
	public void saveSection(Section section) {
		sectionRepository.save(section);

	}

	@Override
	public void removeSection(List<Section> section) {
		sectionRepository.delete(section);

	}

	@Override
	public Page<Kat> searchAllFloors(String searchTerm, int pageNumber) {
		PageRequest page = new PageRequest(pageNumber - 1, 20, Sort.Direction.DESC, "id");
		return floorRepository.searchAllFloors(searchTerm, page);
	}

	@Override
	public Section findBySectionTitle(String title) {

		return sectionRepository.findByTitle(title);
	}

	@Override
	public Page<Section> searchAllSections(String searchTerm, int pageNumber) {
		PageRequest page = new PageRequest(pageNumber - 1, 20, Sort.Direction.DESC, "id");
		return sectionRepository.searchAllSections(searchTerm, page);
	}

	@Override
	public List<Section> findSectionByIds(List<Long> ids) {
		return sectionRepository.findSectionByIds(ids);
	}

	@Override
	public Blok loadBlokPlanById(long id) {
		return blokRepository.loadBlokPlanById(id);
	}

	@Override
	public List<Section> loadSectionsByPortfoyId(Long id) {
	 
		return sectionRepository.loadSectionsByPortfoyId(id);
	}

	@Override
	public Section findBySectionTitleByFloor(String title, String floors) {
		return sectionRepository.findBySectionTitleByFloor(title,Long.parseLong(floors));
	}

	@Override
	public Portfoy loadPortfoyByIdFetchContract(long id) {
		return portfoyRepository.loadPortfoyByIdFetchContract(id);
	}

	@Override
	public Kat findByFloorTitle(String title, long blok) {
		 
			return floorRepository.findByTitleFetchBlok(title,blok);
		 
	}

	@Override
	public List<Blok> loadBloksByProjectId(long project) {
		 
		return blokRepository.loadBloksByProjectId(project);
	}

	@Override
	public Portfoy findByPortfoyBySectionid(Long sectionId) {
		return portfoyRepository.findByPortfoyBySectionid(sectionId);
		
	}

	@Override
	public Blok loadBlokPlanByTitle(String name) {
		return blokRepository.loadBlokPlanByTitle(name);
	}

	 

	
}
