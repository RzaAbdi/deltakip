package rza.BPFco.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import rza.BPFco.model.portfoys.Installment;

public interface ChartService {

	List<Object[]> findTotalMonthlyInstallmentCount(Date to, Date from);
	

	List<Object[]> findTotalMonthlyInstallmentPaidCount(Date to, Date from);
	
	
	List<Object[]> findTotalMonthlyInstallmentPaidCountQueryDsl(Date to, Date from);

	 
	 
	List<Object[]> findTotalMonthlyInstallmentPaidAmount(Date from, Date to);


	List<Object[]> findTotalMonthlyContractCount(Date to, Date from);


	 
	long findTotalSale(long l);


	 
	List<Object[]> findTotalMonthlyInstallmentNotPaidCount(Date to, Date from, Date now);


	 
	List<Object[]> findTotalMonthlyInstallmentPatrialPaidCount(Date to, Date from);
	

	
	 
	 


}
