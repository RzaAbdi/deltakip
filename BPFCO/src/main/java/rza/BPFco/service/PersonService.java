package rza.BPFco.service;

import java.util.List;

import org.springframework.data.domain.Page;

import rza.BPFco.form.filter.AgentFilterForm;
import rza.BPFco.form.filter.CustomerFilterForm;
import rza.BPFco.form.filter.CustomerFilterForm2;
import rza.BPFco.form.filter.InstallmentFilterForm;
import rza.BPFco.model.portfoys.Agent;
import rza.BPFco.model.portfoys.Customer;

public interface PersonService {
	
	 

	Page<Customer> loadAllCustomers(CustomerFilterForm form);

	Customer loadCustomerById(Long id);

	void save(Customer customer);

	Customer findByCustomerName(String title);

	Page<Customer> searchAllCustomers(String searchTerm, int pageNumber);

	List<Customer> findCustomersByIds(List<Long> ids);

	Agent loadAgentById(Long id);

	 
	void save(Agent agent);

	 
	Agent findByAgentName(String name);

	 
	Page<Agent> loadAllAgents(AgentFilterForm form);

	 
	Page<Agent> searchAllAgents(String searchTerm, int pageNumber);

	 
	List<Agent> findAgentsByIds(List<Long> ids);

	 
	Customer findCustomerByEmail(String string);

	 
	void deleteCustomers(List<Customer> customers);

	 
	Customer findCustomersByIdFetchDoc(Long id);

}
