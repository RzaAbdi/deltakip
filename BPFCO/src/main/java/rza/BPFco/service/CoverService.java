package rza.BPFco.service;

import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Page;

import rza.BPFco.form.filter.CoverFilterForm;
import rza.BPFco.model.portfoys.Cover;
import rza.BPFco.model.portfoys.InsCover;

public interface CoverService {

	Cover loadCoverById(Long id);

	void save(Cover cover);

	Page<Cover> loadAllCovers(CoverFilterForm form);

	 
	void save(InsCover installment);

	 
	List<Cover> findCoversByIds(List<Long> ids);

	 
	void removeinsCover(Set<InsCover> ins);

	 
	void removeCovers(List<Cover> covers);

	 
	Set<InsCover> findInsCoversByCover(Long id);

	 
	InsCover findInsCoverById(Long id);

	
	 

	 


	 

	 
}
