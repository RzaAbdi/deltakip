package rza.BPFco.service;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Page;

import rza.BPFco.form.filter.BlokFilterForm;
import rza.BPFco.form.filter.FloorFilterForm;
import rza.BPFco.form.filter.PortfoyFilterForm;
import rza.BPFco.form.filter.SectionFilterForm;
import rza.BPFco.model.Portfoy;
import rza.BPFco.model.Tag;
import rza.BPFco.model.portfoys.Blok;
import rza.BPFco.model.portfoys.Kat;
import rza.BPFco.model.portfoys.Proje;
import rza.BPFco.model.portfoys.Section;

public interface PortfoyService {

	Page<Portfoy> loadAllPortfoys(PortfoyFilterForm form);

	Portfoy loadPortfoyById(Long id);

	void save(Portfoy tag);

	Portfoy findByPortfoyTitle(String title);

	Page<Portfoy> searchAllOpenPortfoys(String searchTerm, int pageNumber);

	Set<Portfoy> findPortfoysByIds(List<Long> ids);

	Page<Proje> loadAllProjects(PortfoyFilterForm form);

	Page<Blok> loadAllBloks(BlokFilterForm form);

	Proje loadProjectById(Long id);
 
	void save(Proje proje);

	Proje findByProjectTitle(String title);

	 
	void remove(List<Proje> projects);
	
	Page<Proje> searchAllProjects(String searchTerm, int pageNumber);
	
	Blok loadBlokById(long id);
	
	void save(Blok blok);
 
	Blok findByBlokTitle(String title);

	 
	void removeBlok(Blok b);

	 
	void removeBlok(List<Blok> projects);

	
	Page<Kat> loadAllFloors(FloorFilterForm form);


	Kat loadFloorById(Long id);

	 
	void saveFloor(Kat floor);

	 
	Kat findByFloorTitle(String title, long blok);

	 
	Page<Blok> searchAllBloks(String searchTerm, int pageNumber);

	 
	void removeFloor(List<Kat> projects);

	 
	Page<Section> loadAllSections(SectionFilterForm form);

	 
	Section loadSectionById(long id);

	 

	 
	void saveSection(Section section);

	 
	void removeSection(List<Section> section);

	 
	Page<Kat> searchAllFloors(String searchTerm, int pageNumber);

	 
	Section findBySectionTitle(String string);

	 
	Page<Section> searchAllSections(String searchTerm, int pageNumber);

	List<Section> findSectionByIds(List<Long> section);

	Blok loadBlokPlanById(long id);

	 
	List<Section> loadSectionsByPortfoyId(Long id);

	 
	Section findBySectionTitleByFloor(String title, String floors);

	 
	Portfoy loadPortfoyByIdFetchContract(long id);

	 
	List<Blok> loadBloksByProjectId(long project);

	 
	Portfoy findByPortfoyBySectionid(Long sectionId);

	 
	Blok loadBlokPlanByTitle(String name);

	 
	Page<Portfoy> searchAllPortfoys(String searchTerm, int pageNumber);

	 

	 

	 
	 
	 

	 
	 

	 
	 

	 
	 

	 
	 

}
