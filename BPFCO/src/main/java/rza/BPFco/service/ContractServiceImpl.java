package rza.BPFco.service;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import rza.BPFco.form.filter.ContractFilterForm;
import rza.BPFco.form.filter.InstallmentFilterForm;
import rza.BPFco.model.Contract;
import rza.BPFco.model.portfoys.Installment;
import rza.BPFco.repository.ContractRepository;
import rza.BPFco.repository.person.InstallmentRepository;

@Service("contractSerivce")
public class ContractServiceImpl implements ContractService {
	
	
	@Autowired
	ContractRepository contractRepository;
	
	@Autowired
	InstallmentRepository installmentRepository;

	@Override
	public Page<Contract> loadAllContracts(ContractFilterForm form) {
		return contractRepository.findAll(form.getPage());
	}

	@Override
	public Contract loadContractById(Long id) {
		return contractRepository.findOne(id);
	}
	 

	@Override
	public Contract findByContract_no(String number) {
		return contractRepository.findBycontract_no(number);
	}

	@Override
	public Page<Contract> searchAllContracts(String searchTerm, int pageNumber) {
		PageRequest page = new PageRequest(pageNumber - 1, 20, Sort.Direction.DESC, "id");
		return contractRepository.searchAllContracts(searchTerm, page);
	}

	@Override
	public List<Contract> findContractsByIds(List<Long> ids) {
		return contractRepository.findContractsByIds(ids);
	}

	@Override
	public void save(Contract contract) {
		contractRepository.save(contract);
		
	}

	@Override
	public Contract loadContractFetchAllById(long id) {
		return contractRepository.loadContractFetchAllById(id);
	}

	
	@Override
	public Page<Contract> loadContractFetchAll(ContractFilterForm form) {
		
		return contractRepository.findAll(form.predicate(),form.getPage());
	}

	@Override
	public Contract findContractByPortfoysIds(List<Long> portfoys) {
		return contractRepository.findContractByPortfoysIds(portfoys);
	}

	@Override
	public Page<Installment> loadAllInstallments(InstallmentFilterForm form) {
		return installmentRepository.findAll(form.predicate(), form.getPage());
	}

	@Override
	public Installment loadInstallmentById(Long id) {
		return installmentRepository.findOne(id);
	}

	@Override
	public void save(Installment installment) {
		installmentRepository.saveAndFlush(installment);
		
	}

	@Override
	public void removeInstallment(List<Installment> installment) {
		installmentRepository.delete(installment);
		
	}

	@Override
	public Set<Installment> loadInstallmentsByContractId(Long id) {
		return installmentRepository.loadInstallmentsByContractId(id);
	}

	@Override
	public Set<Installment> findInstallmentForExpiryDateFifteen(Date date) {
		return installmentRepository.findInstallmentForExpiryDateFifteen( date);
	}

	@Override
	public Page<Installment>  loadLastInstallment(Date from,Date to) {
		Pageable page = new PageRequest(0, 1, Sort.Direction.DESC, "id");
		return installmentRepository.loadLastInstallment(  from, to,page);
	}

//	@Override
//	public List<Installment> loadAllInstallments() {
//		return installmentRepository.loadLastInstallment();
//	}

	@Override
	public Page<Installment> searchAllInstallments(String searchTerm, int pageNumber) {
		PageRequest page = new PageRequest(pageNumber - 1, 20, Sort.Direction.DESC, "id");
		return installmentRepository.searchAllInstallments(searchTerm, page);
	}

	@Override
	public List<Installment> findInstallmentsByIds(List<Long> installments) {
		return installmentRepository.findInstallmentsByIds(installments);
	}

	@Override
	public List<Installment> loadLastInstallmentChart(Date from, Date to) {
		 
		return installmentRepository.loadLastInstallmentChart(from,to);
	}

	@Override
	public Installment loadInstallmentByIdJoinMessages(long id) {
		return installmentRepository.loadInstallmentByIdJoinMessages(id);
	}

	@Override
	public List<Installment> loadInstallmentsByMesIds(List<Long> ids) {
		 
		return installmentRepository.loadInstallmentsByMesIds(ids);
	}

	@Override
	public List<Contract> findContractsByCustomerId(Long id) {
		return contractRepository.findContractsByCustomerId(id);
	}

	@Override
	public Installment findInstallmentByMesId(Long id) {
		return installmentRepository.findInstallmentByMesId(id);
	}

	@Override
	public List<Long> findContractsByUserId(Long userId) {
		return contractRepository.findContractsByUserId(userId);
	}

	@Override
	public List<Installment> loadAllInstallments() {
		 
		return installmentRepository.findAll();
	}

	@Override
	public List<Installment> loadNotPaidInstallments(Date today) {
	 
		return installmentRepository.loadNotPaidInstallments(today);
	}

 

	
}
