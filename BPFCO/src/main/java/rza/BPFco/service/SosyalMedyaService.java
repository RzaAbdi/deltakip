package rza.BPFco.service;

import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Page;

import rza.BPFco.form.filter.ChaseFilterForm;
import rza.BPFco.form.filter.SosyalMedyaFilterForm;
import rza.BPFco.model.portfoys.Chase;
import rza.BPFco.model.portfoys.Proje;
import rza.BPFco.model.portfoys.Reply;
import rza.BPFco.model.portfoys.SosyalMedya;

public interface SosyalMedyaService {
	
	public enum SosyalCstomerType {
		BirinciDerece , İkinciDerece 
	}
	public enum Priority {
		Ciddi , TakibeDevam,İkinciDerece,Acente 
	}

	Page<SosyalMedya> loadAllSosyalMedya(SosyalMedyaFilterForm form);

	 
	SosyalMedya loadSosyalMedyaById(Long id);

	void save(SosyalMedya sosyalMedya);

	void save(Reply reply);

	Reply loadReplyById(Long id);

	List<String> loadNationallities();


	void removeSosyalMedyas(List<SosyalMedya> sm);


	 
	void removeReply(Reply reply);


	List<Object[]> sosyalMedyaRepeats();


	SosyalMedya loadSosyalMedyaByGSM(String string);


	Page<Chase> findChasesForAccount(ChaseFilterForm form);


	void saveChase(Chase chase);


	Chase findChaseById(Long chase);


	 
	void removeReplies(Set<Reply> replies);


	 
	Page<SosyalMedya> searchAllCustomers(String searchTerm, int pageNumber);


	 
	Object loadSosyalMedyaByIds(List<Long> sosyal);
	
	 

	 

}
