package rza.BPFco.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import rza.BPFco.form.filter.DocumentFilterForm;
import rza.BPFco.model.portfoys.DocType;
import rza.BPFco.model.portfoys.Document;
import rza.BPFco.repository.DocumentRepository;
import rza.BPFco.repository.DocumentTypeRepository;

@Service("docmuentSerivce")
public class DocumentServiceImpl implements DocumentService {
	
	@Autowired
	DocumentRepository documentRepository;
	
	@Autowired
	DocumentTypeRepository documentTypeRepository;

	@Override
	public Page<Document> loadAllDocsFetchAll(DocumentFilterForm form) {
		return documentRepository.loadAllDocsFetchType(form.getPage());
	}

	 
	@Override
	public Page<Document> searchAllDocument(String searchTerm, int pageNumber) {
		// TODO Auto-generated method stub.
		return null;
	}

	@Override
	public void removeDocuments(List<Document> documents) {
		documentRepository.delete(documents);
		
	}


	@Override
	public Document loadDocumentById(Long id) {
		return documentRepository.findOne(id);
	}


	@Override
	public Document saveDocument(Document document) {
		return documentRepository.save(document);
	}


	@Override
	public Document findByDocumentTitle(String title) {
	 
		return documentRepository.findByTitle(title);
	}


	@Override
	public void saveDocType(DocType doc) {
		documentTypeRepository.save(doc);
	}


	@Override
	public Page<DocType> loadAllDocsTypes(DocumentFilterForm form) {
		return documentTypeRepository.findAll(form.getPage());
	}


	@Override
	public DocType loadDocTypeById(Long id) {
		return documentTypeRepository.findOne(id);
	}


	@Override
	public DocType findBDocTypeTitle(String title) {
		return documentTypeRepository.findByTitle(title);
	}


	@Override
	public Page<DocType> searchAllDocTypes(String searchTerm, int pageNumber) {
		PageRequest page = new PageRequest(pageNumber - 1, 20, Sort.Direction.DESC, "id");
		return documentTypeRepository.searchAllDocTypes(searchTerm, page);
	}


	@Override
	public List<DocType> findDocmentTypesByIds(List<Long> doctype) {
		return documentTypeRepository.findDocmentTypesByIds(doctype);
	}


	@Override
	public Document loadDocFetchAll(long id) {
		return documentRepository.loadDocFetchAll(id);
	}


	@Override
	public void remove(List<Document> documents) {
		documentRepository.delete(documents);
	}


	@Override
	public Document findByDocPathName(String name) {
		return documentRepository.findByDocPathName(name);
		
	}


	@Override
	public Page<Document> loadAll(DocumentFilterForm form) {
		return documentRepository.findAll(form.predicate(),form.getPage());
	}


	@Override
	public Page<DocType> loadCheckedDocsTypes(DocumentFilterForm form) {
		return documentTypeRepository.loadCheckedDocsTypes(form.getPage());
	}


	@Override
	public void setCheckDocTypeFalse() {
		documentTypeRepository.setCheckDocTypeFalse();
		
	}




	 
	 

}
