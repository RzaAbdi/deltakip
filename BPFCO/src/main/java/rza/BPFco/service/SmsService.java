package rza.BPFco.service;

import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Page;

import rza.BPFco.form.filter.MessageFilterForm;
import rza.BPFco.model.portfoys.Mes;
import rza.BPFco.model.portfoys.Reply;

public interface SmsService {
	
	

	 
	 
	void saveMessage(Mes message);

	Page<Mes> loadMessages(MessageFilterForm form);

	void removeMessage(Set<Mes> messages);

	 
	Set<Mes> loadMessagesByIds(List<Long> ids);

	Mes loadMessagesById(Long id);

	 
	void removeReply(Reply reply);

	 
	 
	 


}
