package rza.BPFco.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import rza.BPFco.form.filter.AgentFilterForm;
import rza.BPFco.form.filter.CustomerFilterForm;
import rza.BPFco.form.filter.CustomerFilterForm2;
import rza.BPFco.form.filter.InstallmentFilterForm;
import rza.BPFco.model.portfoys.Agent;
import rza.BPFco.model.portfoys.Customer;
import rza.BPFco.repository.person.AgentRepository;
import rza.BPFco.repository.person.CustomerRepository;

@Service("personSerivce")
public class PersonServiceImpl implements PersonService {
	
	
	@Autowired
	CustomerRepository customerRepository;
	
	@Autowired
	AgentRepository agentRepository;

	@Override
	public Page<Customer> loadAllCustomers(CustomerFilterForm form) {
		return customerRepository.findAll(form.predicate(),form.getPage());
	}

	@Override
	public Customer loadCustomerById(Long id) {
		return customerRepository.findOne(id);
	}

 

	@Override
	public Customer findByCustomerName(String fname) {
		return customerRepository.findByFname(fname);
	}

	@Override
	public Page<Customer> searchAllCustomers(String searchTerm, int pageNumber) {
		 
		PageRequest page = new PageRequest(pageNumber - 1, 20, Sort.Direction.DESC, "id");
		return customerRepository.searchAllCustomers(searchTerm, page);
	}

	@Override
	public List<Customer> findCustomersByIds(List<Long> ids) {
		return customerRepository.findCustomersByIds(ids);
	}

	@Override
	public void save(Customer customer) {
		customerRepository.save(customer);
	}

	@Override
	public Agent loadAgentById(Long id) {
		return agentRepository.findOne(id);
	}

	@Override
	public void save(Agent agent) {
		agentRepository.save(agent);
		
	}

	@Override
	public Agent findByAgentName(String name) {
		return agentRepository.findByName(name);
	}

	@Override
	public Page<Agent> loadAllAgents(AgentFilterForm form) {
		return agentRepository.findAll(form.getPage());
	}

	@Override
	public Page<Agent> searchAllAgents(String searchTerm, int pageNumber) {
		PageRequest page = new PageRequest(pageNumber - 1, 20, Sort.Direction.DESC, "id");
		return agentRepository.searchAllAgents(searchTerm, page);
	}

	@Override
	public List<Agent> findAgentsByIds(List<Long> ids) {
		return agentRepository.findAgentsByIds(ids);
	}

	@Override
	public Customer findCustomerByEmail(String email) {
		return customerRepository.findByMail(email);
	}

	@Override
	public void deleteCustomers(List<Customer> customers) {
		customerRepository.delete(customers);
		
	}

	@Override
	public Customer findCustomersByIdFetchDoc(Long id) {
		 
		return customerRepository.findCustomersByIdFetchDoc(id);
	}

	 
	
}
