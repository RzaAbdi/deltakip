package rza.BPFco.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;

import rza.BPFco.form.filter.DocumentFilterForm;
import rza.BPFco.model.portfoys.DocType;
import rza.BPFco.model.portfoys.Document;

public interface DocumentService {
	
	public enum FileType {
		Pdf , docx , jpg , jpeg
	}

	Page<Document> loadAllDocsFetchAll(DocumentFilterForm form);

	Document loadDocumentById(Long id);

	Document saveDocument(Document document);

	Document findByDocumentTitle(String title);

	Page<Document> searchAllDocument(String searchTerm, int pageNumber);

	void removeDocuments(List<Document> documents);

	void saveDocType(DocType doc);

	Page<DocType> loadAllDocsTypes(DocumentFilterForm form);

	DocType loadDocTypeById(Long id);

	DocType findBDocTypeTitle(String title);

	 
	Page<DocType> searchAllDocTypes(String searchTerm, int pageNumber);

	 
	List<DocType> findDocmentTypesByIds(List<Long> doctype);

	 
	Document loadDocFetchAll(long id);

	void remove(List<Document> documents);

	 
	Document findByDocPathName(String name);

	 
	Page<Document> loadAll(DocumentFilterForm form);

	 
	Page<DocType> loadCheckedDocsTypes(DocumentFilterForm form);

	 
	void setCheckDocTypeFalse();

	 

	 

	 

	 


	 


}
