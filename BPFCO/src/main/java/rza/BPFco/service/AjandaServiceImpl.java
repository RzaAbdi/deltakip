package rza.BPFco.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import rza.BPFco.form.filter.AjandaFilterForm;
import rza.BPFco.model.Ajanda;
import rza.BPFco.model.portfoys.Cover;
import rza.BPFco.repository.AjandaRepository;

@Service("ajandaSerivce")
public class AjandaServiceImpl implements AjandaService {
	
	@Autowired
	AjandaRepository ajandaRepository;

	@Override
	public Page<Ajanda> loadAjandas(AjandaFilterForm form) {
		return ajandaRepository.findAll(form.predicate(),form.getPage());
	}

	@Override
	public Ajanda loadAjandaById(long id) {
		return ajandaRepository.findOneFetchAcoount(id);
	}

	@Override
	public void save(Ajanda ajanda) {
		ajandaRepository.save(ajanda);
		
	}

	@Override
	public List<Ajanda> loadAjandasByIds(List<Long> ids) {
		return ajandaRepository.findAll(ids);
	}

	@Override
	public void removeAjandas(List<Ajanda> ajandas) {
		ajandaRepository.delete(ajandas);
		
	}

	 
}
