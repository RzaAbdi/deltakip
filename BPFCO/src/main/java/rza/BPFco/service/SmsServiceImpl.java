package rza.BPFco.service;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import rza.BPFco.form.filter.MessageFilterForm;
import rza.BPFco.model.portfoys.Mes;
import rza.BPFco.model.portfoys.Reply;
import rza.BPFco.repository.MessageRepository;
import rza.BPFco.repository.ReplyRepository;

@Service("smsSerivce")
public class SmsServiceImpl implements SmsService {
	
	
	@Autowired
	MessageRepository messageRepository;
	
	@Autowired
	ReplyRepository replyRepository;

	 

	 

	 

	 

	 

	@Override
	public void saveMessage(Mes message) {
		messageRepository.save(message);		
	}


	@Override
	public Page<Mes> loadMessages(MessageFilterForm form) {
		return messageRepository.findAllMessages(form.predicate(),form.getPage());
	}

	 

	@Override
	public void removeMessage(Set<Mes> messages) {
		messageRepository.delete(messages);
		
	}

	@Override
	public Set<Mes> loadMessagesByIds(List<Long> ids) {
		 
		return messageRepository.loadMessagesByIds(ids);
	}

	@Override
	public Mes loadMessagesById(Long id) {
	 
		return messageRepository.findOne(id);
	}

	@Override
	public void removeReply(Reply reply) {
		replyRepository.delete(reply);
		
	}

	 
	 

	 

	 

	 

}
