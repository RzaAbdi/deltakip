package rza.BPFco.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import rza.BPFco.form.filter.ContractFilterForm;
import rza.BPFco.form.filter.InstallmentFilterForm;
import rza.BPFco.model.Contract;
import rza.BPFco.model.portfoys.Installment;
import rza.BPFco.repository.ContractRepository;
import rza.BPFco.repository.person.InstallmentRepository;
import rza.BPFco.repository.portfoy.BlokRepository;

@Service("chartSerivce")
public class ChartServiceImpl implements ChartService {

	@Autowired
	ContractRepository contractRepository;

	@Autowired
	InstallmentRepository installmentRepository;

	@Autowired
	BlokRepository blokRepository;

	@Override
	public List<Object[]> findTotalMonthlyInstallmentCount(Date to, Date from) {
		List<Object[]> map = installmentRepository.findTotalMonthlyInstallmentCount(from, to); // crate a array list

		return map;
	}

	@Override
	public List<Object[]> findTotalMonthlyInstallmentPaidCount(Date to, Date from) {
		List<Object[]> map = installmentRepository.findTotalMonthlyInstallmentPaidCount(from, to);
		return map;
	}

//	@Override
//	public  List<Map<List<Installment>, Integer>> findTotalMonthlyInstallmentAmount(Date from, Date to) {
//		 List<Map<List<Installment>, Integer>> map = installmentRepository.findTotalMonthlyInstallmentAmount(from,to);
//		return map;
//	}

	@Override
	public List<Object[]> findTotalMonthlyInstallmentPaidAmount(Date from, Date to) {
		// TODO Auto-generated method stuCONb.
		return null;
	}

	@Override
	public List<Object[]> findTotalMonthlyContractCount(Date to, Date from) {
		List<Object[]> map = contractRepository.findTotalMonthlyContractCount(from, to);
		return map;
	}

	@Override
	public long findTotalSale(long l) {
		long map = blokRepository.findTotalSale(l);
		return map;

	}

	@Override
	public List<Object[]> findTotalMonthlyInstallmentNotPaidCount(Date to, Date from,Date today) {
		List<Object[]> map = installmentRepository.findTotalMonthlyInstallmentNotPaidCount(from, to,today);
		return map;
	}

	@Override
	public List<Object[]> findTotalMonthlyInstallmentPaidCountQueryDsl(Date to, Date from) {
		List<Object[]> map = installmentRepository.findTotalMonthlyInstallmentNotPaidCount(from, to);
		return map;
	}

	@Override
	public List<Object[]> findTotalMonthlyInstallmentPatrialPaidCount(Date to, Date from) {
		List<Object[]> map = installmentRepository.findTotalMonthlyInstallmentPatrialPaidCount(from, to);
		return map;
	}

}
