package rza.BPFco.service;

import java.util.List;

import org.springframework.data.domain.Page;

import rza.BPFco.form.filter.AjandaFilterForm;
import rza.BPFco.model.Ajanda;

public interface AjandaService {
	 
	Page<Ajanda> loadAjandas(AjandaFilterForm form);
	
	Ajanda loadAjandaById(long id);

	void save(Ajanda ajanda);

	List<Ajanda> loadAjandasByIds(List<Long> ids);

	 
	void removeAjandas(List<Ajanda> ajandas);

	 
	
	 
	 


}
