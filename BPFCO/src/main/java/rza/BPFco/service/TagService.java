package rza.BPFco.service;

import java.util.List;

import org.springframework.data.domain.Page;

import rza.BPFco.form.filter.TagFilterForm;
import rza.BPFco.model.Tag;

public interface TagService {
	
	public enum TagType {
		Belirsiz , Durum , Tip
	}

	Page<Tag> loadAllTags(TagFilterForm form);

	Tag loadTagById(Long id);

	Tag save(Tag tag);

	Tag findByTagTitle(String title);

	Page<Tag> searchAllTags(String searchTerm, int pageNumber);

	List<Tag> findTagsByIds(List<Long> ids);

}
