package rza.BPFco.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import rza.BPFco.form.filter.TagFilterForm;
import rza.BPFco.model.Tag;
import rza.BPFco.repository.tag.TagRepository;

@Service("tagSerivce")
public class TagServiceImpl implements TagService {
	
	@Autowired
	TagRepository tagRepository;

	@Override
	public Page<Tag> loadAllTags(TagFilterForm form) {
		return tagRepository.findAll(form.getPage());
	}

	@Override
	public Tag loadTagById(Long id) {
		return tagRepository.findOne(id);
	}

	@Override
	public Tag save(Tag tag) {
		return tagRepository.saveAndFlush(tag);
	}

	@Override
	public Tag findByTagTitle(String title) {
		return tagRepository.findByTitle(title);
	}

	@Override
	public Page<Tag> searchAllTags(String searchTerm, int pageNumber) {
	    PageRequest page = new PageRequest(pageNumber - 1, 20, Sort.Direction.DESC, "id");
		return tagRepository.searchAllTags(searchTerm, page);
	}

	@Override
	public List<Tag> findTagsByIds(List<Long> ids) {
	    return tagRepository.findTagsByIds(ids);
	}
	

}
