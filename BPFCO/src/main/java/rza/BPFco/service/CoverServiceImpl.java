package rza.BPFco.service;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import rza.BPFco.form.filter.CoverFilterForm;
import rza.BPFco.model.portfoys.Cover;
import rza.BPFco.model.portfoys.InsCover;
import rza.BPFco.repository.CoverRepository;
import rza.BPFco.repository.portfoy.InsCoverRepository;

@Service("coverSerivce")
public class CoverServiceImpl implements CoverService {
	
	
	@Autowired
	CoverRepository coverRepository;
	
	@Autowired
	InsCoverRepository inscoverRepository;

	@Override
	public Cover loadCoverById(Long id) {
		return coverRepository.findOne(id);
	}

	@Override
	public void save(Cover cover) {
		coverRepository.save(cover);
	}

	@Override
	public Page<Cover> loadAllCovers(CoverFilterForm form) {
		return coverRepository.findAll(form.predicate(), form.getPage());
	}

	@Override
	public void save(InsCover installment) {
		inscoverRepository.save(installment);
		
	}

	@Override
	public List<Cover> findCoversByIds(List<Long> ids) {
		return coverRepository.findAll(ids);
	}

	@Override
	public void removeinsCover(Set<InsCover> ins) {
		inscoverRepository.delete(ins);
		
	}

	@Override
	public void removeCovers(List<Cover> covers) {
		coverRepository.delete(covers);
		
	}

	@Override
	public Set<InsCover> findInsCoversByCover(Long id) {
		return inscoverRepository.findInsCoversByCover(id);
	}

	@Override
	public InsCover findInsCoverById(Long id) {
		return inscoverRepository.findOne(id);
	}
	

	
}
