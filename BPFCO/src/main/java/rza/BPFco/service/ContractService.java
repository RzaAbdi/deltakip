package rza.BPFco.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Page;

import rza.BPFco.form.filter.ContractFilterForm;
import rza.BPFco.form.filter.InstallmentFilterForm;
import rza.BPFco.form.filter.TagFilterForm;
import rza.BPFco.model.Contract;
import rza.BPFco.model.Tag;
import rza.BPFco.model.portfoys.Installment;

public interface ContractService {
	
	 

	Page<Contract> loadAllContracts(ContractFilterForm form);

	Contract loadContractById(Long id);

	void save(Contract contract);
	
	

	Contract findByContract_no(String number);

	Page<Contract> searchAllContracts(String searchTerm, int pageNumber);

	List<Contract> findContractsByIds(List<Long> ids);
	
	
	 

	Contract loadContractFetchAllById(long id);

	 
	Page<Contract> loadContractFetchAll(ContractFilterForm form);

	 
	Contract findContractByPortfoysIds(List<Long> portfoys);

	 
	Page<Installment> loadAllInstallments(InstallmentFilterForm form);

	Installment loadInstallmentById(Long id);

	void save(Installment installment);

	void removeInstallment(List<Installment> installment);

	 
	Set<Installment> loadInstallmentsByContractId(Long id);

	 
	Set<Installment> findInstallmentForExpiryDateFifteen(Date date);

	 
	Page<Installment> loadLastInstallment(Date from,Date to);

//	List<Installment> loadAllInstallments();

	Page<Installment> searchAllInstallments(String searchTerm, int pageNumber);

	List<Installment> findInstallmentsByIds(List<Long> installments);

	List<Installment> loadLastInstallmentChart(Date from, Date to);

	Installment loadInstallmentByIdJoinMessages(long id);

	List<Installment> loadInstallmentsByMesIds(List<Long> ids);

	 
	List<Contract> findContractsByCustomerId(Long id);

	 
	Installment findInstallmentByMesId(Long id);

	 
	List<Long> findContractsByUserId(Long userId);

	/**
	 * TODO Put here a description of what this method does.
	 *
	 * @return
	 */
	List<Installment> loadAllInstallments();

	 
	List<Installment> loadNotPaidInstallments(Date today);

	 
	 


	 

	 
}
