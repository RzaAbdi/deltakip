package rza.BPFco.service;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import rza.BPFco.form.filter.ChaseFilterForm;
import rza.BPFco.form.filter.SosyalMedyaFilterForm;
import rza.BPFco.model.portfoys.Chase;
import rza.BPFco.model.portfoys.Proje;
import rza.BPFco.model.portfoys.QSosyalMedya;
import rza.BPFco.model.portfoys.Reply;
import rza.BPFco.model.portfoys.SosyalMedya;
import rza.BPFco.repository.ChaseRepository;
import rza.BPFco.repository.ReplyRepository;
import rza.BPFco.repository.SosyalMedyaRepository;
import rza.BPFco.utilities.JoinDescriptor;

@Service("sosyalMedyaSerivce")
public class SosyalMedyaServiceImpl implements SosyalMedyaService {
	
	@Autowired
	SosyalMedyaRepository sosyalMedyaRepository;
	
	@Autowired
	ReplyRepository replyRepository;
	
	@Autowired
	ChaseRepository chaseRepository;

	@Override
	public Page<SosyalMedya> loadAllSosyalMedya(SosyalMedyaFilterForm form) {
		return sosyalMedyaRepository.findAll(form.predicate(),form.getPage(),JoinDescriptor.leftJoin(QSosyalMedya.sosyalMedya.account.any()));
	}

	@Override
	public SosyalMedya loadSosyalMedyaById(Long id) {
		return sosyalMedyaRepository.findOneFetchAccount(id);
	}

	@Override
	public void save(SosyalMedya sosyalMedya) {
		sosyalMedyaRepository.save(sosyalMedya);
		
	}

	@Override
	public void save(Reply reply) {
		replyRepository.save(reply);	
	}

	@Override
	public Reply loadReplyById(Long id) {
		return replyRepository.findOne(id);
	}

	@Override
	public List<String> loadNationallities() {
		return sosyalMedyaRepository.sosyalMedyaRepository();
	}

	@Override
	public void removeSosyalMedyas(List<SosyalMedya> sm) {
		sosyalMedyaRepository.delete(sm);
		
	}

	@Override
	public void removeReply(Reply reply) {
		replyRepository.delete(reply);
		
	}
	
	
	@Override
	public List<Object[]> sosyalMedyaRepeats() {
		List<Object[]> map = sosyalMedyaRepository.sosyalMedyaRepeats();
		return map;
	}

	@Override
	public SosyalMedya loadSosyalMedyaByGSM(String string) {
	 
		return sosyalMedyaRepository.loadSosyalMedyaByGSM(string);
	}

	@Override
	public Page<Chase> findChasesForAccount(ChaseFilterForm form) {
		return chaseRepository.findAll(form.predicate(),form.getPage());
	}

	@Override
	public void saveChase(Chase chase) {
		chaseRepository.save(chase);
		
	}

	@Override
	public Chase findChaseById(Long chase) {
		return chaseRepository.findOne(chase);
	}

	@Override
	public void removeReplies(Set<Reply> replies) {
		 replyRepository.delete(replies);
		
	}

	@Override
	public Page<SosyalMedya> searchAllCustomers(String searchTerm, int pageNumber) {
		PageRequest page = new PageRequest(pageNumber - 1, 20, Sort.Direction.DESC, "id");
		return sosyalMedyaRepository.searchAllCustomers(searchTerm, page);
	}

	@Override
	public Object loadSosyalMedyaByIds(List<Long> sosyal) {
		return sosyalMedyaRepository.findAll(sosyal);
	}
	
	 

	 

	 

}
