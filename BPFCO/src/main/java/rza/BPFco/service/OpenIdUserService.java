package rza.BPFco.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.openid.OpenIDAttribute;
import org.springframework.security.openid.OpenIDAuthenticationToken;

import rza.BPFco.model.Account;
import rza.BPFco.repository.Account.AccountRepository;

public class OpenIdUserService extends UserService {

	
	@Autowired
	private AccountRepository accountRepository;

	 

	
	
	@Override
	public UserDetails loadUserByUsername(String openID) throws UsernameNotFoundException { // this is reserved for only login process, don't use it in other places

		OpenIDAuthenticationToken token = (OpenIDAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
		
		List<OpenIDAttribute> attributes = token.getAttributes();
		System.err.println(attributes);
		
		Account account = accountRepository.findByEmail(openID);
		//used only in login so we also get Basket information
		if (account == null) {
			logger.warn("user tried to login= " + openID +" into system, but no account with that user name was found");
			throw new UsernameNotFoundException("user not found");
		}
		User user = createUser(account);
		
		 
		 
		save(account);
		return user;
	}

}
