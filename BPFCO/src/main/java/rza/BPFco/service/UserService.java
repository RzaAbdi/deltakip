package rza.BPFco.service;

import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import rza.BPFco.form.filter.UserFilterForm;
import rza.BPFco.model.Account;
import rza.BPFco.model.UserRole;
import rza.BPFco.model.portfoys.Chase;
import rza.BPFco.model.portfoys.Customer;
import rza.BPFco.repository.SosyalMedyaRepository;
import rza.BPFco.repository.Account.AccountRepository;
import rza.BPFco.repository.Account.UserRepository;
import rza.BPFco.repository.Account.UserRoleRepository;
import rza.BPFco.utilities.Time;

public class UserService implements UserDetailsService {

	static final Logger logger = LogManager.getLogger();
	
	@Autowired
	private AccountRepository accountRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private UserRoleRepository userRoleRepository;
	
	@Autowired
	private SosyalMedyaRepository sosyalMedyaRepository;
	
	@Autowired
	public HttpSession session;
	
	
	
	public static class User extends org.springframework.security.core.userdetails.User {
		private static final long serialVersionUID = 1L;
		
		private final Account account;
		public User(Account account) {
			super(account.getEmail(), account.getPassword(), account.getAuthorities());
			this.account = account;

		}
		
		
		public User() {
			super(null,null,null);
			this.account = null;
		}
		
		
		
		
		public Account getAccount() {
			return account;
		}
		
		public Long getUserId() {
			return account.getId();
		}

		 

		public boolean isAdmin() {
			return getAccount().isAdmin();
		}
		public boolean isWatcher() {
			return getAccount().isWatcher();
		}

	}
	

	private Authentication authenticate(Account account) {
		return new UsernamePasswordAuthenticationToken(createUser(account), null, account.getAuthorities());
	}
	
 
	
	public void signin(Account account) {
		SecurityContextHolder.getContext().setAuthentication(
				authenticate(account));

		//necessary session info for each sign in
		session.setAttribute("userName",account.getEmail());
		session.setAttribute("active", account.isActive());
		if(account.isActive()){
			session.setAttribute("userId", account.getId());
			session.setAttribute("userAdmin", account.isAdmin());
			session.setAttribute("userWatcher", account.isWatcher());
			session.setAttribute("userCallFa", account.isCallFa());
			session.setAttribute("userCallAr", account.isCallAr());
			
		} 
		session.setAttribute("fname", account.getEmail().split("@")[0]);	
	
		
		
		
		save(account);
	}
	
	
	protected User createUser(Account account) {
		return new User(account);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException { // this is reserved for only login process, don't use it in other places
		Account account = accountRepository.findByEmail(username);
		//used only in login so we also get Basket information
		if (account == null) {
			logger.warn("user tried to login= " + username +" into system, but no account with that user name was found");
			throw new UsernameNotFoundException("user not found");
		}
		User user = createUser(account);
		
		 
		 
		save(account);
		return user;
	}
	
	public Account save(Account account) {
		return userRepository.save(account);
	}
 
	@PostConstruct
	protected void initialize() {
		if(userRoleRepository.findOne(1L)==null){
			UserRole role = new UserRole();
			role.setId(1L);
			role.setDescription("Default Role for ADMINISTRATOR");
			role.setName("admin");
			userRoleRepository.save(role);
			role.setId(2L);
			role.setDescription("Default Role for Guest Users");
			role.setName("guest");
			userRoleRepository.save(role);
			role.setId(3L);
			role.setDescription("Default Role for Normal Users");
			role.setName("user");
			userRoleRepository.save(role);
	}
		if (accountRepository.findOne(1L) == null) {  //create admin and guest if DB empty
			String encoded = passwordEncoder.encode("admin");
			Account account = new Account("admin", encoded,  "ROLE_ADMIN", Time.today(),"ADMIN");
			account.setUserroles(UserRole.ADMIN);
			accountRepository.save(account);
			
			account =  Account.guestUser();
			accountRepository.save(account);
		}
	}
 
	public Account loadAccountByEmail(String email) {
		return userRepository.findByEmail(email);
	}
	public boolean loadAccountByEmail() {
		Account acc = userRepository.findByEmail();
		if (acc == null)
			return false;
		
		if (acc.getLastsign().before(Time.fifteenDaysBefore())) {
			return false;
		}
		
		return true;
	}



	public Page<Account> loadAccounts(int pagenumber, int pagesize) {
		PageRequest pagerequest = new PageRequest(pagenumber - 1, pagesize, Sort.Direction.DESC, "id");
		return userRepository.findAll(pagerequest);
 	}



	public Page<Account> loadAccounts(UserFilterForm form) {
	    return userRepository.findAll(form.getPage());
	}



	public Account loadAccountByIdFetchRole(long id) {
	    return userRepository.findOne(id);
	}



	public Page<UserRole> searchRoles(String searchTerm, int pageNumber) {
	    PageRequest page = new PageRequest(pageNumber - 1, 10, Sort.Direction.DESC, "id");
	    return userRoleRepository.searchRoles(searchTerm, page);
	}



	public UserRole LoadUserRoleById(Long userrole) {
	    return userRoleRepository.findOne(userrole);
	}



	 
	public Page<Account> searchAllUsers(String searchTerm, int pageNumber) {
		 PageRequest page = new PageRequest(pageNumber - 1, 10, Sort.Direction.DESC, "id");
		return userRepository.searchAllUsers(searchTerm,page);
	}



	 
	public List<Account> findAccontsByIds(List<Long> ids) {
		return userRepository.findAccontsByIds(ids);
	}


 


 
}
