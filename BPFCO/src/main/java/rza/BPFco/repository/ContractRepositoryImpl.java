package rza.BPFco.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.QueryDslRepositorySupport;

import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.types.Predicate;

import rza.BPFco.model.Contract;
import rza.BPFco.model.QContract;

public class ContractRepositoryImpl extends QueryDslRepositorySupport implements ContractRepositoryCustom {

    public ContractRepositoryImpl() {
	super(Contract.class);
    }

    @Override
    public Page<Contract> searchAllContracts(String searchTerm, Pageable pageable) {
	QContract contract = QContract.contract;

	JPQLQuery query = from(contract).where(contract.contract_no.contains(searchTerm));

	query = getQuerydsl().applyPagination(pageable, query);

	List<Contract> matchedNames = query.list(contract);

	Long count = from(contract).where(contract.contract_no.contains(searchTerm)).singleResult(contract.contract_no.countDistinct());

	Page<Contract> myObjectPage = new PageImpl<Contract>(matchedNames, pageable, count);

	return myObjectPage;

    }
//	@Override
//	public Page<Contract> loadContractFetchAll(Predicate predicate,Pageable page) {
//		QContract contract = QContract.contract;
//		JPQLQuery query = from(contract).join(contract.customer).join(contract.portfoys).where(predicate);
//		query = getQuerydsl().applyPagination(page, query);
//		List<Contract> matchedNames = query.list(contract);
//		
//		Long count =from(contract).join(contract.customer).leftJoin(contract.portfoys).where(predicate).singleResult(contract.countDistinct());
//		Page<Contract> myObjectPage = new PageImpl<Contract>(matchedNames, page, count);
//		return myObjectPage;
//	}

}
