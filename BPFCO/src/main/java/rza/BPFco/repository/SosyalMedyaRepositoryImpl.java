package rza.BPFco.repository;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.QueryDslRepositorySupport;
import org.springframework.data.querydsl.EntityPathResolver;
import org.springframework.data.querydsl.SimpleEntityPathResolver;

import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.types.EntityPath;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.path.PathBuilder;

import rza.BPFco.model.portfoys.QSosyalMedya;
import rza.BPFco.model.portfoys.SosyalMedya;
import rza.BPFco.utilities.JoinDescriptor;

public class SosyalMedyaRepositoryImpl extends QueryDslRepositorySupport implements SosyalMedyaRepositoryCustom{
	
	private  EntityPath<SosyalMedya> path;
    private  PathBuilder<SosyalMedya> builder;
    private static final EntityPathResolver DEFAULT_ENTITY_PATH_RESOLVER = SimpleEntityPathResolver.INSTANCE;

    public SosyalMedyaRepositoryImpl(JpaEntityInformation<SosyalMedya, Long> entityInformation, EntityManager entityManager, EntityPathResolver resolver) {
	 
	super(SosyalMedya.class);
	this.path = resolver.createPath(entityInformation.getJavaType());
    this.builder = new PathBuilder<>(path.getType(), path.getMetadata());
	
    }
    

    public SosyalMedyaRepositoryImpl(JpaEntityInformation<SosyalMedya, Long> entityInformation, EntityManager entityManager) {
        this(entityInformation, entityManager, DEFAULT_ENTITY_PATH_RESOLVER);
    }
    
    public SosyalMedyaRepositoryImpl() {
    	super(SosyalMedya.class);
    }
    
    
	@Override
	public Page<SosyalMedya> searchAllCustomers(String searchTerm, PageRequest pageable) {
		QSosyalMedya sosyalMedya = QSosyalMedya.sosyalMedya;

		JPQLQuery query = from(sosyalMedya).where(sosyalMedya.name.contains(searchTerm));

		query = getQuerydsl().applyPagination(pageable, query);

		List<SosyalMedya> matchedNames = query.list(sosyalMedya);

		Long count = from(sosyalMedya).where(sosyalMedya.name.contains(searchTerm)).singleResult(sosyalMedya.name.countDistinct());

		Page<SosyalMedya> myObjectPage = new PageImpl<SosyalMedya>(matchedNames, pageable, count);

		return myObjectPage;
	}

	@Override
	public Page<SosyalMedya> findAll(Predicate predicate, Pageable pageable, JoinDescriptor joinDescriptors) {
		QSosyalMedya sosyalMedya = QSosyalMedya.sosyalMedya;
		JPQLQuery query = from(sosyalMedya).join(sosyalMedya.account).fetch().where(predicate);
         query = getQuerydsl().applyPagination(pageable, query);
 		List<SosyalMedya> matchedNames = query.list(sosyalMedya);
 		Long count = from(sosyalMedya).join(sosyalMedya.account).where(predicate).singleResult(sosyalMedya.name.countDistinct());
         

        

		Page<SosyalMedya> myObjectPage = new PageImpl<SosyalMedya>(matchedNames, pageable, count);

		return myObjectPage;
	}
	
	 protected JPQLQuery createFetchQuery(Predicate predicate, JoinDescriptor... joinDescriptors) {
		 
		JPQLQuery query = getQuerydsl().createQuery(path);
	        for(JoinDescriptor joinDescriptor: joinDescriptors)
	            join(joinDescriptor, query);
	        return query.where(predicate);
	        
	    }
	 
	 private JPQLQuery join(JoinDescriptor joinDescriptor, JPQLQuery query) {
	        switch(joinDescriptor.type) {
	            case DEFAULT:
	                throw new IllegalArgumentException("cross join not supported");
	            case INNERJOIN:
	                query.innerJoin(joinDescriptor.path);
	                break;
	            case JOIN:
	                query.join(joinDescriptor.path);
	                break;
	            case LEFTJOIN:
	                query.leftJoin(joinDescriptor.path);
	                break;
	            case RIGHTJOIN:
	                query.rightJoin(joinDescriptor.path);
	                break;
	            case FULLJOIN:
	                query.fullJoin(joinDescriptor.path);
	                break;
	        }
	        return query.fetch();
	    }

    

}
