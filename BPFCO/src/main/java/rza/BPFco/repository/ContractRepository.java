package rza.BPFco.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import rza.BPFco.model.Contract;

@Repository("contractRepository")
public interface ContractRepository extends JpaRepository<Contract, Long>,QueryDslPredicateExecutor<Contract>, ContractRepositoryCustom {

	@Query("SELECT t FROM Contract t WHERE t.contract_no = (?1) ")
	Contract findBycontract_no(String title);

	@Query("SELECT c FROM Contract c LEFT JOIN FETCH c.account a  LEFT JOIN FETCH c.customer cc LEFT JOIN FETCH c.portfoys p LEFT JOIN FETCH c.agent a LEFT JOIN c.installment i WHERE c.id = (?1) ORDER BY i.id ASC")
	Contract loadContractFetchAllById(long id);

//	@Transactional
//	@Query("SELECT DISTINCT c FROM Contract c  JOIN  c.customer cc LEFT JOIN c.portfoys p")
//	Page<Contract> loadContractFetchAll( Predicate predicate,Pageable page);

	@Query("SELECT c FROM Contract c LEFT JOIN c.portfoys p WHERE p.id IN (?1)")
	Contract findContractByPortfoysIds(List<Long> portfoys);

	 
	@Query("SELECT c FROM Contract c WHERE c.id IN (?1)")
	List<Contract> findContractsByIds(List<Long> ids);

	@Query("SELECT MONTH(c.createDate),YEAR(c.createDate), COUNT(s)  FROM Contract c LEFT JOIN c.portfoys p  JOIN  p.sections s WHERE c.active = TRUE  AND c.createDate BETWEEN :startDate AND :endDate  GROUP BY  YEAR(c.createDate) , MONTH(c.createDate)  ORDER BY YEAR(c.createDate) , MONTH(c.createDate) ASC")
	List<Object[]> findTotalMonthlyContractCount(@Param("startDate")Date startDate,@Param("endDate")Date endDate);

	 
	@Query("SELECT c FROM Contract c LEFT JOIN c.customer cu WHERE cu.id = (?1)")
	List<Contract> findContractsByCustomerId(Long id);

	@Query("SELECT c.id FROM Contract c LEFT JOIN c.account a WHERE a.id = (?1)")
	List<Long> findContractsByUserId(Long userId);
	

}
