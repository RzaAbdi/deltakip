package rza.BPFco.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import rza.BPFco.model.Portfoy;
import rza.BPFco.model.portfoys.Section;
import rza.BPFco.repository.portfoy.PortfoyRepositoryCustom;

@Repository("portfoyRepository")
public interface PortfoyRepository extends JpaRepository <Portfoy, Long>,QueryDslPredicateExecutor<Portfoy>, PortfoyRepositoryCustom{

	Portfoy findByTitle(String title);
	
	
	
	@Query("SELECT t FROM Portfoy t WHERE t.id IN (?1) ")
	List<Portfoy> findTagsByIds(List<Long> ids);



	@Query("SELECT t FROM Section t WHERE t.id IN (?1) ")
	Set<Section> findSectionsByIds(List<Long> section);



	@Query("SELECT t FROM Portfoy t WHERE t.id IN (?1) ")
	Set<Portfoy> findPortfoysByIds(List<Long> portfoyIds);



	@Query("SELECT p FROM Portfoy p JOIN FETCH p.tag t WHERE p.id = (?1) ")
	Portfoy loadPortfoyByIdFetchTags(Long id);



	@Query("SELECT p FROM Portfoy p JOIN p.contract c WHERE p.id = (?1) ")
	Portfoy loadPortfoyByIdFetchContract(long id);


	@Query("SELECT p FROM Portfoy p JOIN p.sections s WHERE s.id = (?1) ")
	Portfoy findByPortfoyBySectionid(Long sectionId);
	
	
//	@Query("SELECT t FROM Portfoy t LEFT JOIN t.sections s LEFT JOIN s.kat k LEFT JOIN k.blok b WHERE b.id = (?1) ")
//	Set<Portfoy> findPortfoysForFloorPlan(Long portfoyId);
}
