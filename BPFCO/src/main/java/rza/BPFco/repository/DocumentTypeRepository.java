package rza.BPFco.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import rza.BPFco.model.portfoys.DocType;

@Repository("documentTypeRepository")
public interface DocumentTypeRepository extends JpaRepository<DocType, Long>,QueryDslPredicateExecutor<DocType>, DocumentTypeRepositoryCustom {

	
	@Query("SELECT t FROM DocType t WHERE t.title = (?1) ")
	DocType findByTitle(String title);

	@Query("SELECT t FROM DocType t WHERE t.id IN (?1) ")
	List<DocType> findDocmentTypesByIds(List<Long> doctype);

	@Query("SELECT t FROM DocType t WHERE t.controlled = TRUE ") 
	Page<DocType> loadCheckedDocsTypes(Pageable page);

	@Modifying  
	@Transactional
	@Query("UPDATE DocType d SET d.controlled = FALSE WHERE d.controlled = TRUE ")
	void setCheckDocTypeFalse();

	 

	 
	 
	 

	 
 
}
