package rza.BPFco.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.mysema.query.types.Predicate;

import rza.BPFco.model.portfoys.SosyalMedya;
import rza.BPFco.utilities.JoinDescriptor;

public interface SosyalMedyaRepositoryCustom {

	Page<SosyalMedya> searchAllCustomers(String searchTerm, PageRequest page);
	
	Page<SosyalMedya> findAll(Predicate predicate, Pageable pageable, JoinDescriptor joinDescriptors);
	
	
}
