package rza.BPFco.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.QueryDslRepositorySupport;

import com.mysema.query.jpa.JPQLQuery;

import rza.BPFco.model.portfoys.DocType;
import rza.BPFco.model.portfoys.QDocType;

public class DocumentTypeRepositoryImpl extends QueryDslRepositorySupport implements DocumentTypeRepositoryCustom {

    public DocumentTypeRepositoryImpl() {
	super(DocType.class);
    }

    @Override
    public Page<DocType> searchAllDocTypes(String searchTerm, Pageable pageable) {
	QDocType docType = QDocType.docType;

	JPQLQuery query = from(docType).where(docType.title.contains(searchTerm));

	query = getQuerydsl().applyPagination(pageable, query);

	List<DocType> matchedNames = query.list(docType);

	Long count = from(docType).where(docType.title.contains(searchTerm)).singleResult(docType.title.countDistinct());

	Page<DocType> myObjectPage = new PageImpl<DocType>(matchedNames, pageable, count);

	return myObjectPage;

    }

}
