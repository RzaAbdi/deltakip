package rza.BPFco.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import rza.BPFco.model.portfoys.Mes;
import rza.BPFco.repository.portfoy.MessageRepositoryCustom;

@Repository("messageRepository")
public interface MessageRepository extends JpaRepository<Mes, Long>,QueryDslPredicateExecutor<Mes>, MessageRepositoryCustom {

	 
	
	@Query("SELECT m FROM Mes m LEFT JOIN FETCH m.installment i WHERE m.id IN (?1) ")
	List<Mes> loadMessageFetchInstallment(List<Long> ids);

	@Query("SELECT m FROM Mes m WHERE m.id IN (?1) ")
	Set<Mes> loadMessagesByIds(List<Long> ids);

 
	//
	
	 
 

	
	 
 
}
