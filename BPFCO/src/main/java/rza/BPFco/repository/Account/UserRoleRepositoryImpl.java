package rza.BPFco.repository.Account;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.QueryDslRepositorySupport;

import com.mysema.query.jpa.JPQLQuery;

import rza.BPFco.model.QUserRole;
import rza.BPFco.model.UserRole;

public class UserRoleRepositoryImpl extends QueryDslRepositorySupport implements UserRoleRepositoryCustom {

    public UserRoleRepositoryImpl() {
	super(UserRole.class);
    }

    @Override
    public Page<UserRole> searchRoles(String searchTerm,  Pageable pageable) {
	 
	QUserRole userRole = QUserRole.userRole;

	JPQLQuery query = from(userRole).where(userRole.name.contains(searchTerm));

	query = getQuerydsl().applyPagination(pageable, query);

	List<UserRole> matchedNames = query.list(userRole);

	Long count = from(userRole).where(userRole.name.contains(searchTerm))
			.singleResult(userRole.name.countDistinct());

	Page<UserRole> myObjectPage = new PageImpl<UserRole>(matchedNames, pageable, count);

	return myObjectPage;
    }

}
