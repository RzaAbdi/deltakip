package rza.BPFco.repository.Account;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import rza.BPFco.model.UserRole;

public interface UserRoleRepositoryCustom {
    
    Page<UserRole> searchRoles(String searchTerm, Pageable page);

}
