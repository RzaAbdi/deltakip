package rza.BPFco.repository.Account;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import rza.BPFco.model.Account;

 

@Repository("userRepository")
public interface UserRepository extends JpaRepository <Account, Long>,QueryDslPredicateExecutor<Account>, UserRepositoryCustom {

	@Query("SELECT a FROM Account a WHERE a.email=(?1)")
	Account findByEmail(String email);

	@Query("SELECT t FROM Account t WHERE t.id IN (?1) ")
	List<Account> findAccontsByIds(List<Long> ids);

	@Query("SELECT a FROM Account a WHERE a.email='rza.abdi84@gmail.com'")
	Account findByEmail();

	 

	 
	 

	 

//	@Query("SELECT a FROM Account a WHERE a.email LIKE '%(?1)%' AND a.fname LIKE '%(?1)%' AND a.lname LIKE '%(?1)%'")
//	Page<Account> findByEmailContainingOrFnameContainingOrLnameContaining(String searchTerm, String searchTerm2, String searchTerm3, Pageable page);

 
//	@Query("SELECT a FROM Account a LEFT JOIN FETCH a.userroles WHERE a.id = (?1)")
//	Account findByIdFetchRoles(Long userId);
 

 
	
	
 

}
