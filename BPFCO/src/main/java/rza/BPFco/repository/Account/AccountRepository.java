package rza.BPFco.repository.Account;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import rza.BPFco.model.Account;

@Repository("accountRepository")
public interface AccountRepository  extends JpaRepository <Account, Long>
{

	@Query("SELECT a FROM Account a WHERE a.email = (?1)")
	Account findByEmail(String pid);

	
	 
	
	
}
