package rza.BPFco.repository.Account;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import rza.BPFco.model.Account;

public interface UserRepositoryCustom {
    
    Page<Account> searchAllUsers(String searchTerm, Pageable page);
    


}
