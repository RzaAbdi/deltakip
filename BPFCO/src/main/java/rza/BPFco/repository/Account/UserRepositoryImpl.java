package rza.BPFco.repository.Account;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.support.QueryDslRepositorySupport;

import com.mysema.query.jpa.JPQLQuery;

import rza.BPFco.model.Account;
import rza.BPFco.model.QAccount;
import rza.BPFco.model.QUserRole;
import rza.BPFco.model.UserRole;

public class UserRepositoryImpl extends QueryDslRepositorySupport implements UserRepositoryCustom {

    public UserRepositoryImpl() {
	super(Account.class);
    }

    @Override
    public Page<Account> searchAllUsers(String searchTerm,  Pageable pageable) {
	 
	QAccount qAccount = QAccount.account;

	JPQLQuery query = from(qAccount).where(qAccount.name.contains(searchTerm));

	query = getQuerydsl().applyPagination(pageable, query);

	List<Account> matchedNames = query.list(qAccount);

	Long count = from(qAccount).where(qAccount.name.contains(searchTerm))
			.singleResult(qAccount.name.countDistinct());

	Page<Account> myObjectPage = new PageImpl<Account>(matchedNames, pageable, count);

	return myObjectPage;
    }

	 

 
}
