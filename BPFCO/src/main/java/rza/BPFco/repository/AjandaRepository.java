package rza.BPFco.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import rza.BPFco.model.Ajanda;

@Repository("ajandaRepository")
public interface AjandaRepository extends JpaRepository<Ajanda, Long>, QueryDslPredicateExecutor<Ajanda> {

	@Query("SELECT a FROM Ajanda a LEFT JOIN FETCH a.account c LEFT JOIN FETCH a.reply r WHERE a.id = (?1)")
	Ajanda findOneFetchAcoount(long id);

	 
	 

}
