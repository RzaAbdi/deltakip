package rza.BPFco.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import rza.BPFco.model.portfoys.DocType;

public interface DocumentTypeRepositoryCustom {
    Page<DocType> searchAllDocTypes(String searchTerm, Pageable pageable);


}
