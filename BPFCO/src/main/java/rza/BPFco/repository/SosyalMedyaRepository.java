package rza.BPFco.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import rza.BPFco.model.portfoys.SosyalMedya;

@Repository("sosyalMedyaRepository")
public interface SosyalMedyaRepository
		extends JpaRepository<SosyalMedya, Long>, QueryDslPredicateExecutor<SosyalMedya>, SosyalMedyaRepositoryCustom {

	@Query("SELECT i.nation FROM SosyalMedya i  GROUP BY i.nation ORDER BY i.nation ASC")
	List<String> sosyalMedyaRepository();

	@Query("SELECT gsm, COUNT(*) FROM SosyalMedya  GROUP BY gsm  HAVING COUNT(*) > 1")
	List<Object[]> sosyalMedyaRepeats();

	@Query("SELECT i FROM SosyalMedya i  WHERE i.gsm = (?1) ")
	SosyalMedya loadSosyalMedyaByGSM(String string);
	
	
	@Query("SELECT COUNT(*)  FROM SosyalMedya s JOIN s.account a WHERE a.id = (?1) AND s.language=(?2) AND s.follow = TRUE")
	 int ConuntPinned(Long id,int language);

	@Query("SELECT i FROM SosyalMedya i LEFT JOIN FETCH i.account a LEFT JOIN FETCH i.reply r WHERE i.id = (?1) ")
	SosyalMedya findOneFetchAccount(Long id);

	 


}
