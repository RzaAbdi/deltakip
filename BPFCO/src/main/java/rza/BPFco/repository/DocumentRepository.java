package rza.BPFco.repository;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import rza.BPFco.model.portfoys.Document;
import rza.BPFco.repository.portfoy.DocmentRepositoryCustom;

@Repository("documentRepository")
public interface DocumentRepository extends JpaRepository<Document, Long> ,QueryDslPredicateExecutor<Document>, DocmentRepositoryCustom{

	
	@Query("SELECT t FROM Document t WHERE t.title = (?1) ")
	Document findByTitle(String title);

 
	
	@Query("SELECT d FROM Document d JOIN  d.doctype t  ")
	Page<Document> loadAllDocsFetchType(Pageable page);

	@Query("SELECT d FROM Document d JOIN  d.doctype t LEFT JOIN FETCH d.contract c  LEFT JOIN FETCH d.installment i  LEFT JOIN FETCH d.customer s  LEFT JOIN FETCH d.agent a WHERE d.id = (?1)")
	Document loadDocFetchAll(long id);

	@Query("SELECT t FROM Document t WHERE t.file_path = (?1) ")
	Document findByDocPathName(String name);


	 
 
}
