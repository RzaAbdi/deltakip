package rza.BPFco.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mysema.query.types.Predicate;

import rza.BPFco.model.Contract;

public interface ContractRepositoryCustom {
    Page<Contract> searchAllContracts(String searchTerm, Pageable pageable);

//    Page<Contract> loadContractFetchAll( Predicate predicate,Pageable page);
}
