package rza.BPFco.repository.person;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import rza.BPFco.model.portfoys.Installment;

@Repository("installmentRepository")
public interface InstallmentRepository extends JpaRepository <Installment, Long>,QueryDslPredicateExecutor<Installment>, InstallmentRepositoryCustom{
	
	
	

	
	
	@Query("SELECT t FROM Installment t WHERE t.id IN (?1) ")
	List<Installment> findInstallmentsByIds(List<Long> ids);
 

	@Query("SELECT t FROM Installment t LEFT JOIN FETCH t.contract c WHERE c.id = (?1) ORDER BY t.issueDate ASC")
	Set<Installment> loadInstallmentsByContractId(Long id);

	@Query("SELECT t FROM Installment t LEFT JOIN FETCH t.contract c WHERE t.issueDate = (?1) ")
	Set<Installment> findInstallmentForExpiryDateFifteen(Date date);

	@Query("SELECT t FROM Installment t  WHERE t.issueDate BETWEEN :startDate AND :endDate ")
	 Page <Installment>loadLastInstallment(@Param("startDate")Date startDate,@Param("endDate")Date endDate,Pageable page);


	@Query("SELECT MONTH(i.issueDate),YEAR(i.issueDate), COUNT(i)  FROM Installment i WHERE i.active=TRUE AND i.issueDate BETWEEN :startDate AND :endDate  GROUP BY  YEAR(i.issueDate) , MONTH(i.issueDate)  ORDER BY YEAR(i.issueDate) , MONTH(i.issueDate) ASC")
	List<Object[]> findTotalMonthlyInstallmentCount(@Param("startDate")Date startDate,@Param("endDate")Date endDate);
	 
	@Query("SELECT MONTH(i.issueDate), COUNT(i)  FROM Installment i WHERE  i.active=TRUE AND i.paid = TRUE  AND i.issueDate BETWEEN :startDate AND :endDate   GROUP BY  YEAR(i.issueDate) , MONTH(i.issueDate)  ORDER BY YEAR(i.issueDate) , MONTH(i.issueDate) ASC")
	List<Object[]> findTotalMonthlyInstallmentPaidCount(@Param("startDate")Date startDate,@Param("endDate")Date endDate);
	
	
	 


	 
	@Query("SELECT t FROM Installment t  WHERE t.active=TRUE AND t.issueDate BETWEEN :startDate AND :endDate ORDER BY YEAR(t.issueDate) , MONTH(t.issueDate) ASC")
	List<Installment> loadLastInstallmentChart(@Param("startDate")Date startDate,@Param("endDate")Date endDate);


//	@Query("SELECT i FROM Installment i WHERE i.id = (?1) ")
	@Query("SELECT i FROM Installment i LEFT JOIN FETCH i.mes m WHERE i.id = (?1) ")
	Installment loadInstallmentByIdJoinMessages(long id);


	@Query("SELECT i FROM Installment i LEFT JOIN FETCH i.mes m WHERE m.id IN (?1) ")
	List<Installment> loadInstallmentsByMesIds(List<Long> ids);


	@Query("SELECT i FROM Installment i LEFT JOIN FETCH i.mes m WHERE m.id = (?1) ")
	Installment findInstallmentByMesId(Long id);


	@Query("SELECT MONTH(i.issueDate), COUNT(i)  FROM Installment i WHERE  i.active=TRUE AND i.issueDate BETWEEN :startDate AND :endDate AND i.issueDate < :today AND i.paid = FALSE  GROUP BY  YEAR(i.issueDate) , MONTH(i.issueDate)  ORDER BY YEAR(i.issueDate) , MONTH(i.issueDate) ASC")
	List<Object[]> findTotalMonthlyInstallmentNotPaidCount(@Param("startDate")Date startDate,@Param("endDate")Date endDate,@Param("today")Date today);


	 
	@Query("SELECT MONTH(i.issueDate), COUNT(i)  FROM Installment i WHERE  i.active=TRUE AND i.paid = FALSE AND i.partial = TRUE AND i.issueDate BETWEEN :startDate AND :endDate  GROUP BY  YEAR(i.issueDate) , MONTH(i.issueDate)  ORDER BY YEAR(i.issueDate) , MONTH(i.issueDate) ASC")
	List<Object[]> findTotalMonthlyInstallmentPatrialPaidCount(@Param("startDate")Date startDate,@Param("endDate")Date endDate);


	@Query("SELECT i FROM Installment i LEFT JOIN FETCH i.contract c LEFT JOIN FETCH c.customer s WHERE i.active=TRUE AND i.paid = FALSE AND i.issueDate < :today ORDER BY i.issueDate ASC")
	List<Installment> loadNotPaidInstallments(@Param("today")Date today);

 


//	@Query("DELETE FROM Installment i ORDER BY i.id DESC LIMIT 1")
//	void deleteLastInstallment();


	 


	 
	 

	
	 

//	@Query("SELECT i , MONTH(i.issue_date) FROM Installment i WHERE i.issue_date BETWEEN :startDate AND :endDate GROUP BY MONTH(i.issue_date) ORDER BY MONTH(i.issue_date) ASC")
//	 List<Map<List<Installment>, Integer>> findTotalMonthlyInstallmentAmount(@Param("startDate")Date startDate,@Param("endDate")Date endDate);


	

}
