package rza.BPFco.repository.person;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import rza.BPFco.model.portfoys.Agent;

@Repository("agentRepository")
public interface AgentRepository extends JpaRepository <Agent, Long>,QueryDslPredicateExecutor<Agent>, AgentRepositoryCustom{
	
	
	

	Agent findByName(String fname);
	
	
	@Query("SELECT t FROM Agent t WHERE t.id IN (?1) ")
	List<Agent> findAgentsByIds(List<Long> ids);

	 
	

}
