package rza.BPFco.repository.person;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import rza.BPFco.model.portfoys.Customer;

@Repository("customerRepository")
public interface CustomerRepository extends JpaRepository <Customer, Long>,QueryDslPredicateExecutor<Customer>, CustomerRepositoryCustom{
	
	
	

	Customer findByFname(String fname);
	
	
	@Query("SELECT t FROM Customer t  WHERE t.id IN (?1) ")
	List<Customer> findCustomersByIds(List<Long> ids);

	@Query("SELECT t FROM Customer t JOIN FETCH t.documents d  WHERE t.id = (?1) ")
	Customer findCustomersByIdFetchDoc(Long id);
	
	Customer findByMail(String email);
	

}
