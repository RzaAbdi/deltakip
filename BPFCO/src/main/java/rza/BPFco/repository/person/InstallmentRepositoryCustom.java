package rza.BPFco.repository.person;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import rza.BPFco.model.portfoys.Installment;

public interface InstallmentRepositoryCustom {
    Page<Installment> searchAllInstallments(String searchTerm, Pageable pageable);

	List<Object[]> findTotalMonthlyInstallmentNotPaidCount(Date from, Date to);

}
