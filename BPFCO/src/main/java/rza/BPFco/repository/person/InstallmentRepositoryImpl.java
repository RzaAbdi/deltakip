package rza.BPFco.repository.person;

import java.util.Date;
import java.util.List;import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.QueryDslRepositorySupport;

import com.mysema.commons.lang.CloseableIterator;
import com.mysema.query.FilteredClause;
import com.mysema.query.QueryModifiers;
import com.mysema.query.ResultTransformer;
import com.mysema.query.SearchResults;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.types.CollectionExpression;
import com.mysema.query.types.EntityPath;
import com.mysema.query.types.Expression;
import com.mysema.query.types.MapExpression;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.ParamExpression;
import com.mysema.query.types.Path;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.NumberExpression;

import rza.BPFco.model.portfoys.Customer;
import rza.BPFco.model.portfoys.Installment;
import rza.BPFco.model.portfoys.QCustomer;
import rza.BPFco.model.portfoys.QInstallment;

public class InstallmentRepositoryImpl extends QueryDslRepositorySupport implements InstallmentRepositoryCustom {

    public InstallmentRepositoryImpl() {
	super(Installment.class);
    }

    @Override
    public Page<Installment> searchAllInstallments(String searchTerm, Pageable pageable) {
	QInstallment installment = QInstallment.installment;
	
	JPQLQuery query = null;
	Long count ;
	if(searchTerm!="") {
		
		 query = from(installment).where(installment.id.eq(new Long(searchTerm)));
	}else {
		query = from(installment);
	}


	query = getQuerydsl().applyPagination(pageable, query);

	List<Installment> matchedNames = query.list(installment);
	
	if(searchTerm!="") {
		
		 count = from(installment).where(installment.id.eq(new Long(searchTerm))).singleResult(installment.id.countDistinct());
	}else {
		count = from(installment).singleResult(installment.id.countDistinct());
	}


	Page<Installment> myObjectPage = new PageImpl<Installment>(matchedNames, pageable, count);

	return myObjectPage;

    }

	@Override
	public List<Object[]> findTotalMonthlyInstallmentNotPaidCount(Date from, Date to) {
		QInstallment installment = QInstallment.installment;
//		JPQLQuery query = from(installment.issueDate).where(installment.active.isTrue());

		 
		return null;
	}

	 

}
