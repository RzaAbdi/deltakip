package rza.BPFco.repository.person;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import rza.BPFco.model.portfoys.Customer;

public interface CustomerRepositoryCustom {
    Page<Customer> searchAllCustomers(String searchTerm, Pageable pageable);


}
