package rza.BPFco.repository.person;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.QueryDslRepositorySupport;

import com.mysema.query.jpa.JPQLQuery;

import rza.BPFco.model.portfoys.Customer;
import rza.BPFco.model.portfoys.QCustomer;

public class CustomerRepositoryImpl extends QueryDslRepositorySupport implements CustomerRepositoryCustom {

    public CustomerRepositoryImpl() {
	super(Customer.class);
    }

    @Override
    public Page<Customer> searchAllCustomers(String searchTerm, Pageable pageable) {
	QCustomer customer = QCustomer.customer;

	JPQLQuery query = from(customer).where(customer.fname.contains(searchTerm));

	query = getQuerydsl().applyPagination(pageable, query);

	List<Customer> matchedNames = query.list(customer);

	Long count = from(customer).where(customer.fname.contains(searchTerm)).singleResult(customer.fname.countDistinct());

	Page<Customer> myObjectPage = new PageImpl<Customer>(matchedNames, pageable, count);

	return myObjectPage;

    }

}
