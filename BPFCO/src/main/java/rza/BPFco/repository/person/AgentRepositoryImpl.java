package rza.BPFco.repository.person;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.QueryDslRepositorySupport;

import com.mysema.query.jpa.JPQLQuery;

import rza.BPFco.model.portfoys.Agent;
import rza.BPFco.model.portfoys.QAgent;

public class AgentRepositoryImpl extends QueryDslRepositorySupport implements AgentRepositoryCustom {

    public AgentRepositoryImpl() {
	super(Agent.class);
    }

    @Override
    public Page<Agent> searchAllAgents(String searchTerm, Pageable pageable) {
	QAgent agent = QAgent.agent;

	JPQLQuery query = from(agent).where(agent.name.contains(searchTerm));

	query = getQuerydsl().applyPagination(pageable, query);

	List<Agent> matchedNames = query.list(agent);

	Long count = from(agent).where(agent.name.contains(searchTerm)).singleResult(agent.name.countDistinct());

	Page<Agent> myObjectPage = new PageImpl<Agent>(matchedNames, pageable, count);

	return myObjectPage;

    }

}
