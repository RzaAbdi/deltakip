package rza.BPFco.repository.person;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import rza.BPFco.model.portfoys.Agent;

public interface AgentRepositoryCustom {
    Page<Agent> searchAllAgents(String searchTerm, Pageable pageable);


}
