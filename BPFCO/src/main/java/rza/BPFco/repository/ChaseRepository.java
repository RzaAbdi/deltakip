package rza.BPFco.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import rza.BPFco.model.portfoys.Chase;

@Repository("chaseRepository")
public interface ChaseRepository
		extends JpaRepository<Chase, Long>, QueryDslPredicateExecutor<Chase>{

	 
 

}
