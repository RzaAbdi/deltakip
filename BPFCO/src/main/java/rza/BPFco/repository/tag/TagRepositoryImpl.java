package rza.BPFco.repository.tag;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.QueryDslRepositorySupport;

import com.mysema.query.jpa.JPQLQuery;

import rza.BPFco.model.QTag;
import rza.BPFco.model.Tag;

public class TagRepositoryImpl extends QueryDslRepositorySupport implements TagRepositoryCustom {

    public TagRepositoryImpl() {
	super(Tag.class);
    }

    @Override
    public Page<Tag> searchAllTags(String searchTerm, Pageable pageable) {
	QTag tag = QTag.tag;

	JPQLQuery query = from(tag).where(tag.title.contains(searchTerm));

	query = getQuerydsl().applyPagination(pageable, query);

	List<Tag> matchedNames = query.list(tag);

	Long count = from(tag).where(tag.title.contains(searchTerm)).singleResult(tag.title.countDistinct());

	Page<Tag> myObjectPage = new PageImpl<Tag>(matchedNames, pageable, count);

	return myObjectPage;

    }

}
