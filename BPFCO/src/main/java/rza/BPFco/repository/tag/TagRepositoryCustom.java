package rza.BPFco.repository.tag;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import rza.BPFco.model.Tag;

public interface TagRepositoryCustom {
    Page<Tag> searchAllTags(String searchTerm, Pageable pageable);


}
