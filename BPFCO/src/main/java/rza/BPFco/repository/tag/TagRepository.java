package rza.BPFco.repository.tag;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import rza.BPFco.model.Tag;

@Repository("tagRepository")
public interface TagRepository extends JpaRepository <Tag, Long>,QueryDslPredicateExecutor<Tag>, TagRepositoryCustom{

	Tag findByTitle(String title);
	
	
	
	@Query("SELECT t FROM Tag t WHERE t.id IN (?1) ")
	List<Tag> findTagsByIds(List<Long> ids);
	
	

}
