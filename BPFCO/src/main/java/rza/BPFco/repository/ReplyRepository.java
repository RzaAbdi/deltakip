package rza.BPFco.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import rza.BPFco.model.portfoys.Reply;
import rza.BPFco.model.portfoys.SosyalMedya;

@Repository("replyRepository")
public interface ReplyRepository extends JpaRepository<Reply, Long> {

	
	 
 
}
