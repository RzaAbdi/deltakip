package rza.BPFco.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mysema.query.types.Predicate;

import rza.BPFco.model.Contract;
import rza.BPFco.model.portfoys.Cover;

@Repository("coverRepository")
public interface CoverRepository extends JpaRepository<Cover, Long>,QueryDslPredicateExecutor<Cover> {


}
