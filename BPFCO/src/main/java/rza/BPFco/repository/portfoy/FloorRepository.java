package rza.BPFco.repository.portfoy;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import rza.BPFco.model.portfoys.Kat;

@Repository("floorRepository")
public interface FloorRepository extends JpaRepository <Kat, Long>,QueryDslPredicateExecutor<Kat>, FloorRepositoryCustom{

	
	
	@Query("SELECT t FROM Kat t JOIN t.blok b WHERE t.title = (?1) AND b.id = (?2) ")
	Kat findByTitleFetchBlok(String title, Long blokid);
	
	
	
	@Query("SELECT t FROM Kat t WHERE t.id IN (?1) ")
	List<Kat> findFloorByIds(List<Long> ids);
	

}
