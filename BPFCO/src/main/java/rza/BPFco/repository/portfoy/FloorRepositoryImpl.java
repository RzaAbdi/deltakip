package rza.BPFco.repository.portfoy;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.QueryDslRepositorySupport;

import com.mysema.query.jpa.JPQLQuery;

import rza.BPFco.model.portfoys.Kat;
import rza.BPFco.model.portfoys.QKat;

public class FloorRepositoryImpl extends QueryDslRepositorySupport implements FloorRepositoryCustom {

    public FloorRepositoryImpl() {
	super(Kat.class);
    }

    @Override
    public Page<Kat> searchAllFloors(String searchTerm, Pageable pageable) {
	QKat qKat = QKat.kat;

	JPQLQuery query = from(qKat).where(qKat.title.contains(searchTerm));

	query = getQuerydsl().applyPagination(pageable, query);

	List<Kat> matchedNames = query.list(qKat);

	Long count = from(qKat).where(qKat.title.contains(searchTerm)).singleResult(qKat.title.countDistinct());

	Page<Kat> myObjectPage = new PageImpl<Kat>(matchedNames, pageable, count);

	return myObjectPage;

    }

}
