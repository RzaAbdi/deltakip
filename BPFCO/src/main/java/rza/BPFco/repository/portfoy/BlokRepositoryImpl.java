package rza.BPFco.repository.portfoy;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.QueryDslRepositorySupport;

import com.mysema.query.jpa.JPQLQuery;

import rza.BPFco.model.QTag;
import rza.BPFco.model.Tag;
import rza.BPFco.model.portfoys.Blok;
import rza.BPFco.model.portfoys.Proje;
import rza.BPFco.model.portfoys.QBlok;
import rza.BPFco.model.portfoys.QProje;

public class BlokRepositoryImpl extends QueryDslRepositorySupport implements BlokRepositoryCustom {

    public BlokRepositoryImpl() {
	super(Blok.class);
    }

    @Override
    public Page<Blok> searchAllBloks(String searchTerm, Pageable pageable) {
	QBlok blok = QBlok.blok;

	JPQLQuery query = from(blok).where(blok.title.contains(searchTerm));

	query = getQuerydsl().applyPagination(pageable, query);

	List<Blok> matchedNames = query.list(blok);

	Long count = from(blok).where(blok.title.contains(searchTerm)).singleResult(blok.title.countDistinct());

	Page<Blok> myObjectPage = new PageImpl<Blok>(matchedNames, pageable, count);

	return myObjectPage;

    }

}
