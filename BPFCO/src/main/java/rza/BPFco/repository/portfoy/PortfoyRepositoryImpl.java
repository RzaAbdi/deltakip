package rza.BPFco.repository.portfoy;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.QueryDslRepositorySupport;

import com.mysema.query.jpa.JPQLQuery;

import rza.BPFco.model.Portfoy;
import rza.BPFco.model.QPortfoy;

public class PortfoyRepositoryImpl extends QueryDslRepositorySupport implements PortfoyRepositoryCustom {

 

	public PortfoyRepositoryImpl() {
		super(Portfoy.class);
	}

	@Override
	public Page<Portfoy> searchAllOpenPortfoys(String searchTerm, Pageable pageable) {

		QPortfoy portfoy = QPortfoy.portfoy;

		JPQLQuery query = from(portfoy).where(portfoy.sections.any().title.eq(searchTerm).and(portfoy.saled.eq(false)).and(portfoy.reserved.isFalse()));

		query = getQuerydsl().applyPagination(pageable, query);

		List<Portfoy> matchedNames = query.list(portfoy);

		Long count = from(portfoy).where(portfoy.sections.any().title.eq(searchTerm).and(portfoy.saled.eq(false)).and(portfoy.reserved.isFalse()))
				.singleResult(portfoy.sections.any().title.countDistinct());

		Page<Portfoy> myObjectPage = new PageImpl<Portfoy>(matchedNames, pageable, count);

		return myObjectPage;

	}

	@Override
	public Page<Portfoy> searchAllPortfoys(String searchTerm, Pageable pageable) {
		QPortfoy portfoy = QPortfoy.portfoy;

		JPQLQuery query = from(portfoy).where(portfoy.sections.any().title.eq(searchTerm));

		query = getQuerydsl().applyPagination(pageable, query);

		List<Portfoy> matchedNames = query.list(portfoy);

		Long count = from(portfoy).where(portfoy.sections.any().title.eq(searchTerm))
				.singleResult(portfoy.sections.any().title.countDistinct());

		Page<Portfoy> myObjectPage = new PageImpl<Portfoy>(matchedNames, pageable, count);

		return myObjectPage;
	}

}
