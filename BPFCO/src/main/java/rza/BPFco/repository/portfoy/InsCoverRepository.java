package rza.BPFco.repository.portfoy;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import rza.BPFco.model.portfoys.InsCover;

@Repository("insCoverRepository")
public interface InsCoverRepository extends JpaRepository <InsCover, Long>,QueryDslPredicateExecutor<InsCover>{

	@Query("SELECT i FROM InsCover i  JOIN i.cover c  WHERE c.id = (?1) ")
	Set<InsCover> findInsCoversByCover(Long id);

	
	
	 
	

}
