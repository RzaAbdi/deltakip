package rza.BPFco.repository.portfoy;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import rza.BPFco.model.Portfoy;
import rza.BPFco.model.Tag;
import rza.BPFco.model.portfoys.Blok;
import rza.BPFco.model.portfoys.Kat;
import rza.BPFco.model.portfoys.Proje;
import rza.BPFco.model.portfoys.Section;

@Repository("sectionRepository")
public interface SectionRepository extends JpaRepository <Section, Long>,QueryDslPredicateExecutor<Section>, SectionRepositoryCustom{

	Section findByTitle(String title);
	
	
	
	@Query("SELECT t FROM Section t WHERE t.id IN (?1) ")
	List<Section> findFloorByIds(List<Long> ids);


	@Query("SELECT t FROM Section t WHERE t.id IN (?1) ")
	List<Section> findSectionByIds(List<Long> ids);



	@Query("SELECT t FROM Section t JOIN t.portfoy p WHERE p.id = (?1) ")
	List<Section> loadSectionsByPortfoyId(Long id);


	@Query("SELECT t FROM Section t LEFT JOIN t.kat k WHERE t.title = (?1) AND k.id = (?2) ")
	Section findBySectionTitleByFloor(String title, long l);
	

}
