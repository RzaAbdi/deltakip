package rza.BPFco.repository.portfoy;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import rza.BPFco.model.portfoys.Blok;

@Repository("blokRepository")
public interface BlokRepository extends JpaRepository <Blok, Long>,QueryDslPredicateExecutor<Blok>, BlokRepositoryCustom{

	Blok findByTitle(String title);
	
	
	
	@Query("SELECT t FROM Blok t WHERE t.id IN (?1) ")
	List<Blok> findBlokByIds(List<Long> ids);
	
	
	@Transactional
	@Query("SELECT b FROM Blok b JOIN b.kats k JOIN k.sections s  JOIN  s.portfoy p JOIN p.tag t WHERE b.id IN (?1) ")
	Blok loadBlokPlanById(Long id);
	
	@Transactional
	@Query("SELECT b FROM Blok b JOIN b.kats k JOIN k.sections s  JOIN  s.portfoy p JOIN p.tag t  WHERE b.title = (?1) ")
	Blok loadBlokPlanByTitle(String name);
	
	
	@Query("SELECT  COUNT(s)  FROM Blok b JOIN b.kats k JOIN k.sections s  JOIN  s.portfoy p  WHERE p.saled = TRUE AND b.id = (?1)")
	Long findTotalSale(Long id);


	@Query("SELECT b FROM Blok b LEFT JOIN b.proje p  WHERE p.id = (?1) ")
	List<Blok> loadBloksByProjectId(long project);
	

}
