package rza.BPFco.repository.portfoy;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import rza.BPFco.model.portfoys.Blok;

public interface BlokRepositoryCustom {
    Page<Blok> searchAllBloks(String searchTerm, Pageable pageable);


}
