package rza.BPFco.repository.portfoy;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.QueryDslRepositorySupport;

import com.mysema.query.jpa.JPQLQuery;

import rza.BPFco.model.QTag;
import rza.BPFco.model.Tag;
import rza.BPFco.model.portfoys.Proje;
import rza.BPFco.model.portfoys.QProje;

public class ProjectRepositoryImpl extends QueryDslRepositorySupport implements ProjectRepositoryCustom {

    public ProjectRepositoryImpl() {
	super(Proje.class);
    }

    @Override
    public Page<Proje> searchAllProjects(String searchTerm, Pageable pageable) {
	QProje proje = QProje.proje;

	JPQLQuery query = from(proje).where(proje.title.contains(searchTerm));

	query = getQuerydsl().applyPagination(pageable, query);

	List<Proje> matchedNames = query.list(proje);

	Long count = from(proje).where(proje.title.contains(searchTerm)).singleResult(proje.title.countDistinct());

	Page<Proje> myObjectPage = new PageImpl<Proje>(matchedNames, pageable, count);

	return myObjectPage;

    }

}
