package rza.BPFco.repository.portfoy;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.QueryDslRepositorySupport;

import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.types.Predicate;

import rza.BPFco.model.portfoys.Mes;
import rza.BPFco.model.portfoys.QMes;

public class MessageRepositoryImpl extends QueryDslRepositorySupport implements MessageRepositoryCustom {

 

	public MessageRepositoryImpl() {
		super(Mes.class);
	}

	@Override
	public Page<Mes> findAllMessages(Predicate predicate, Pageable pageable) {
		 

		QMes mes = QMes.mes;

		JPQLQuery query = from(mes).leftJoin(mes.installment).fetch().where(predicate);

		query = getQuerydsl().applyPagination(pageable, query);

		List<Mes> matchedNames = query.list(mes);

		Long count =from(mes).leftJoin(mes.installment).where(predicate)
				.singleResult(mes.id.countDistinct());

		Page<Mes> myObjectPage = new PageImpl<Mes>(matchedNames, pageable, count);

		return myObjectPage;

	}


}
