package rza.BPFco.repository.portfoy;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import rza.BPFco.model.Portfoy;

public interface PortfoyRepositoryCustom {
    Page<Portfoy> searchAllOpenPortfoys(String searchTerm, Pageable pageable);
    Page<Portfoy> searchAllPortfoys(String searchTerm, Pageable pageable);


}
