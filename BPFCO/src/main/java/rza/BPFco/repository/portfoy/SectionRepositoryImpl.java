package rza.BPFco.repository.portfoy;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.QueryDslRepositorySupport;

import com.mysema.query.jpa.JPQLQuery;

import rza.BPFco.model.portfoys.Kat;
import rza.BPFco.model.portfoys.QKat;
import rza.BPFco.model.portfoys.QSection;
import rza.BPFco.model.portfoys.Section;

public class SectionRepositoryImpl extends QueryDslRepositorySupport implements SectionRepositoryCustom {

    public SectionRepositoryImpl() {
	super(Section.class);
    }

    @Override
    public Page<Section> searchAllSections(String searchTerm, Pageable pageable) {
	QSection section = QSection.section;

	 
	JPQLQuery query = from(section).where(section.title.eq(searchTerm));

	query = getQuerydsl().applyPagination(pageable, query);

	List<Section> matchedNames = query.list(section);

	Long count = from(section).where(section.title.eq(searchTerm)).singleResult(section.title.countDistinct());
     
	Page<Section> myObjectPage = new PageImpl<Section>(matchedNames, pageable, count);

	return myObjectPage;

    }

}
