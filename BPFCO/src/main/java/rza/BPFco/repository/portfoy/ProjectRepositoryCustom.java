package rza.BPFco.repository.portfoy;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import rza.BPFco.model.portfoys.Proje;

public interface ProjectRepositoryCustom {
    Page<Proje> searchAllProjects(String searchTerm, Pageable pageable);


}
