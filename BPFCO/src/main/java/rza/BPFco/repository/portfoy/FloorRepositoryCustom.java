package rza.BPFco.repository.portfoy;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import rza.BPFco.model.portfoys.Kat;

public interface FloorRepositoryCustom {
    Page<Kat> searchAllFloors(String searchTerm, Pageable pageable);


}
