package rza.BPFco.repository.portfoy;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import rza.BPFco.model.portfoys.Section;

public interface SectionRepositoryCustom {
    Page<Section> searchAllSections(String searchTerm, Pageable pageable);


}
