package rza.BPFco.repository.portfoy;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mysema.query.types.Predicate;

import rza.BPFco.model.portfoys.Mes;

public interface MessageRepositoryCustom {
	
	@Transactional 
	Page<Mes> findAllMessages(Predicate predicate, Pageable page);
    


}
