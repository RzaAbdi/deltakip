package rza.BPFco.repository.portfoy;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import rza.BPFco.model.Portfoy;
import rza.BPFco.model.Tag;
import rza.BPFco.model.portfoys.Proje;
import rza.BPFco.repository.tag.TagRepositoryCustom;

@Repository("projectRepository")
public interface ProjectRepository extends JpaRepository <Proje, Long>,QueryDslPredicateExecutor<Proje>, ProjectRepositoryCustom{

	Proje findByTitle(String title);
	
	
	
	@Query("SELECT t FROM Proje t WHERE t.id IN (?1) ")
	List<Proje> findProjectsByIds(List<Long> ids);
	
	

}
