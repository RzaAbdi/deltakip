$(document).ready(function() {

	 
	$("#Table").meshop({
		url : "/adminpanel/json/bloks/load",
		page : $('[data-id=Table_page]'),
		modifyJson : modifyJson,
		loop : loop,
		filterForm:$('#filter-form')

	});
	
	
	
$('#printBlokPlan').click(function(e) {
	
	 
		
		$("#printable").printThis({ 
		    debug: false,              
		    importCSS: true,             
		    importStyle: true,         
		    printContainer: true,       
		    loadCSS: "../css/style.css", 
		    pageTitle: "My Modal",             
		    removeInline: false,        
		    printDelay: 333,            
		    header: null,             
		    formValues: true          
		}); 
		});


$('#print').click(function(e) {
	
	$("#modalDiv").printThis({ 
		debug: false,              
		importCSS: true,             
		importStyle: true,         
		printContainer: true,       
		loadCSS: "../css/style.css", 
		pageTitle: "My Modal",             
		removeInline: false,        
		printDelay: 333,            
		header: null,             
		formValues: true          
	}); 
});
	
	
	function modifyJson(json) {

	 
		

		return json;
	}
	
		$("#new_blok").click(function(e) {
		var modal = $('#new_blok_modal');
		modal.modal('show');

		modal.meshop({
			json : {
				active : false,
			},
//			loop : loopModal
		});

	});

	
	
	function loop(obj, json, mainObj) {

		obj.data('id', json.id);

		obj.find('.scheck').click(function(e) {

			e.stopPropagation();
			var checkbox = obj.find(".selected");

			if (checkbox.hasClass('glyphicon-unchecked')) {
				checkbox.addClass(" glyphicon-check").removeClass(" glyphicon-unchecked ");
			} else {
				checkbox.addClass(" glyphicon-unchecked").removeClass(" glyphicon-check ");
				del.removeClass("disabled");
			}
			var count = $('span.glyphicon-check').map(function check_del(index, obj) {

				return $(obj).closest('tr').data('id');
			})
			if (count.length > 0) {

				del.removeClass("disabled");
			} else {
				del.addClass("disabled");
				$('#selectall').removeClass(" glyphicon-check").addClass(" glyphicon-unchecked ");
			}

		});
		
		
		obj.find('#plan').click(function(e) {
			e.stopPropagation();
			var plan = obj.find('#plan');
			plan.addClass('loading')
			
			var modal = $('#floorPanel_modal');
			
			modal.meshop({

				url : "/json/blokplan/load",
				params : {
					id : json.id,
				},
			 loop : loopModal,
			}).on("render", function(obj, json) {
				plan.removeClass('loading')
				modal.modal('show');
				
			})
			
			
		});
			
			
			
		 


		obj.click(function(e) {
			e.stopPropagation();
			var modal = $('#new_blok_modal');
			modal.modal('show');
			modal.meshop({

				url : "/adminpanel/json/blok/load",
				params : {
					id : json.id,
				},
			// loop : loopModal,
			// modifyJson : modifyJsonModal
			})

		});

	}
	;
	
 
	
	function loopModal(obj, json, mainobj) {
		
		 
		
		
		obj.find('td.tdclass').attr("style","background-color:red!important");
		obj.find('td.reserveclass').attr("style","background-color:#ff9900!important");
		
		obj.find('td.section').click(function(e) {
			e.stopPropagation();
			
			var id = $(this).attr('id');
			if ($(this).hasClass('tdclass')){
				var modal = $('#contract_modal');
				modal.modal('show');
				modal.meshop({

					url : "/json/contract/load",
					params : {
						id : id,
					plan: true,
					},
				})
				
			} else {
				
				var win = window.open("/portfoy_manage?listId="+id);
				if (win) {
					win.focus()
				} else {
					alert('Please allow popups for this website');
				}
				 
			}
			
			
		});
		  

	}
 

$('#save').click(function() {
	$('#save,#resetModal').addClass("disabled");
	$(this).addClass('loading');
	var form = $('#blok_form');
	$.ajax({
		url : "/adminpanel/json/blok/save",
		type : "POST",
		data : form.serialize(),
	}).done(function(respond) {
		// window.location =
		// "/";

		$('#new_blok_modal').modal('hide');
		meshop.refresh($("#Table"));
		meshop.log("ok", tdelete);

	}).fail(function(e) {
		meshop.log("error", terror);

	}).always(function() {
		$('#save').removeClass('loading').removeClass('disabled');
		// meshop.refresh($("#Table"));
	});
//	 meshop.refresh($("#Table"));

});

var del = $('#delete-blok')
		var selectall = $('#selectall');
		selectall.parent().click(function(e) {
			if (selectall.hasClass('glyphicon-unchecked')) {
				check(selectall);
				check($(".selected"));
				del.removeClass("disabled");
			} else {
				uncheck(selectall);
				uncheck($(".selected"));
				del.addClass("disabled");
			}

			function check(obj) {
				obj.addClass(" glyphicon-check").removeClass(" glyphicon-unchecked ");
			}
			function uncheck(obj) {
				obj.removeClass(" glyphicon-check ").addClass(" glyphicon-unchecked");
			}

		});

		del.click(function(e) {
			var modal = meshop.alert("ask", "blok(lar) silinecek. Emin misiniz?", "Uyarı");
			modal.size("sm");
			modal.yes = function() {
				e.preventDefault();
				del.addClass('loading');
				var selecteditems = "";
				var count = $('span.glyphicon-check.selected').map(function(index, obj) {
					return $(obj).closest('tr').data('id');
				})
				var selecteditems = count.get().join();

				if (selecteditems) {
					$.post("/adminpanel/json/blok/remove", {
						ids : selecteditems
					}, function(data) {
						meshop.refresh($("#Table"));
						meshop.log("ok", tdelete);
					}).fail(function(e) {
						del.removeClass('loading');
						meshop.log("error", terror);
					}).always(function() {
						del.removeClass('loading');
						del.addClass("disabled");
					});
				}
			};
		});


});