$(document).ready(function() {
	$('.daterange').daterangepicker().val("");
	$("#Table").meshop({
		url : "/adminpanel/json/programs/load",
		page : $('[data-id=Table_page]'),
		modifyJson : modifyJson,
		loop : loop,
		filterForm:$('#filter-form')

	});
	
	
	function modifyJson(json) {

		json.trclass = json.active ? '' : 'danger';
		

		return json;
	}

	
		
	 $("#collapseExample").on("hide.bs.collapse", function(){
		
		 
		 
	 });
	
	

	function loop(obj, json, mainObj) {
		obj.data('id', json.id);
		obj.click(function(e) {
			e.stopPropagation();
			var modal = $('#new_program_modal');

			modal.modal('show');
//			modal.find('input[name="banner_path"]').attr('disabled', 'disabled');

			modal.meshop({

				url : "/adminpanel/json/program/load",
				params : {
					id : json.id,
				},
loop : loopModal,
//				modifyJson : modifyJsonModal
			})

		});

	}

	$("#new_product").click(function(e) {
		var modal = $('#new_program_modal');
		modal.modal('show');

		modal.meshop({
			json : {
				active : false,
			},
			loop : loopModal
		});

	});

	function loopModal(obj, json, mainobj) {

		$('.glyphicon-open-file.banner').click(function(e) {
			$(this).find('input[type="file"]').click();
		});

		$('.glyphicon-open-file.banner input').click(function(e) {
			e.stopPropagation();
		});

		$('#banner_file').change(function() { // select banner
			var ext = $(this).val().split('.').pop().toLowerCase();
			if ($.inArray(ext, [ 'jpg', 'jpeg' ]) == -1) {
				meshop.log("error", "یوکله‌نه‌بیلینر .jpg تکجه");
			} else {
				$("input[name='banner_path']").val(this.value || 'Nothing selected').removeAttr('disabled');
			}
		});

	}

	$(".btn_upload").click(function(e) {

		e.stopPropagation();

		$("#upload").trigger("click");

	});

	$("#save").click(function(e) {

		e.stopPropagation();

		save();

	});

	function save() {
		var formData = new FormData();
		var form = $('#program_form');
		var modal = $('#new_program_modal');
		var isanyfile = false;
		$("#banner_file").each(function(i, input) {
			var oo = input.files[0];
			if (oo) {
				formData.append("myNewFileName", oo);
				isanyfile = true;
			}
		});
		var serilized = form.serializeArray();
		$.each(serilized, function(key, input) {
			formData.append(input.name, input.value);
		});
		if (isanyfile) {

			$("#progress_banner").removeClass("hidden");
			$('#progress_banner .progress-bar').show().fadeIn(500, function() {
			});
			$("#progress_mob_cover").removeClass("hidden");
			$('#progress_mob_cover .progress-bar').show().fadeIn(500, function() {
			});

		}
		$.ajax({

			success : function(response) {

				modal.modal('hide');
				meshop.refresh($("#Table"));

			},
			error : function(err) {
				meshop.log("error", " یوکلنیرکن بیر سورون یاشاندی. لوطفن یئنه دنه‌یین");
				$('#progress_banner').addClass("progress-bar-danger");

			},
			complete : function(e) {

				if (e.responseText == 'a') {
					meshop.log("error", " بو یایین آدی داها اؤنجه قوللانیلمیشدیر. باشقا آد سئچینیز");
					$('#progress_banner .progress-bar').addClass("progress-bar-danger");
					$('#progress_banner').parent().addClass("has-error").removeClass("has-success")
				}

			},

			xhr : function() {
				var myXhr = $.ajaxSettings.xhr();
				if (myXhr.upload) { // Check if upload property exists
					progressBar = $("#progress_banner");
					progressBar = $("#progress_banner");
					myXhr.upload.addEventListener('progress', progressHandlingFunction, false);

				}
				return myXhr;
			},

			data : formData,
			processData : false,
			contentType : false,
			url : "/adminpanel/json/program/save",
			type : "POST",

		});

	}

	function progressHandlingFunction(e) {
		if (e.lengthComputable) {
			console.log("e.loaded" + e.loaded + ":" + e.total);

			var prog = progressBar.find(".progress-bar")

			var percent = Math.round(e.loaded * 100 / e.total) + "%";
			prog.css({
				width : percent
			});// 45% Tamamlandı
			if (percent == "100%") {

				setTimeout(function() {

					prog.fadeOut(500, function() {
						prog.css({
							width : '1%'
						});
						progressBar.addClass("hidden");
						$("#uploadXml").prop("disabled", false);
						prog.removeClass("progress-bar-danger");
					});

				}, 2000);

			}
		}
	}

});