$(document).ready(function() {
	
	Spinner();
	Spinner.show();
	
	var pathname = window.location.href;
	path = pathname.split('?')[1];
	 
	if(path!= null) {
	url =	"/json/sosyalmedya/load?"+path+"&language="+language;
	}else {
		url =	"/json/sosyalmedya/load?language="+language;
	}
	 
	$("#Table").meshop({
		url : url,
		page : $('[data-id=Table_page]'),
		modifyJson : modifyJson,
		loop : loop,
		filterForm:$('#filter-form')

	}).on("empty",function(){
		
		$("#Table").addClass('hidden');
		$('[data-id=Table_page]').addClass('hidden');
		$("#empty").removeClass('hidden');
		
	}).on("render", function(obj, json) {
		$("#Table").removeClass('hidden');
		$('[data-id=Table_page]').removeClass('hidden');
		$("#empty").addClass('hidden');
		
	});
	
	$('#print').click(function(e) {
		
		$("#modalDiv").printThis({ 
		    debug: false,              
		    importCSS: true,             
		    importStyle: true,         
		    printContainer: true,       
		    loadCSS: "../css/style.css", 
		    pageTitle: "My Modal",             
		    removeInline: false,        
		    printDelay: 333,            
		    header: null,             
		    formValues: true          
		}); 
		});
	
	$('input[type="file"]').ezdz();
	
	var click = false;
	
	$("input.daterange").click(function(e) {
		
		if(click==false) {
			
			$('input[name="daterange"]').daterangepicker({
				opens: 'left',
				
				
			}, function(start, end, label) {
				console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
			});
		}
		click=true;
		
	});
	
	
	function modifyJson(json) {

		switch(json.priority) {
		case 4:
			json.trclass= "warning"
			break;
		  case 1:
		    json.trclass= "info"
		    break;
		  case 2:
			  json.trclass= "success"
		    break;
		  case 3:
			  json.trclass= "danger"
			  break;
		  default:
		    // code block
		}
		
		
//		 json.bg= 	isDark($(json.color).css("background-color") ? 'white' : 'black');
		 json.textColor= 	isDark(json.color) ? 'white' : 'black';
	 
		

		return json;
	}
	
	function isDark( color ) {
		var rgb = hexToRgb(color.replace('#',''));
		
	    var match = /rgb\((\d+).*?(\d+).*?(\d+)\)/.exec(rgb);
	    return ( match[1] & 255 )
	         + ( match[2] & 255 )
	         + ( match[3] & 255 )
	           < 3 * 256 / 2;
	}
	
	function hexToRgb(hex) {
		var arrBuff = new ArrayBuffer(4);
		  var vw = new DataView(arrBuff);
		  vw.setUint32(0,parseInt(hex, 16),false);
		  var arrByte = new Uint8Array(arrBuff);

	    return "rgb("+arrByte[1] + "," + arrByte[2] + "," + arrByte[3]+")";
		}
	
		$(".new_sm").click(function(e) {
		var modal = $('#new_smedya_modal');
		modal.modal('show');

		modal.meshop({
			json : {
				active : false,
			},
			loop : loopModal
		});

	});

	function loopModal(obj, json, mainobj) {

	  

	}
	
	function loop(obj, json, mainObj) {

		obj.data('id', json.id);

		obj.find('.scheck').click(function(e) {

			e.stopPropagation();
			var checkbox = obj.find(".selected");

			if (checkbox.hasClass('glyphicon-unchecked')) {
				checkbox.addClass(" glyphicon-check").removeClass(" glyphicon-unchecked ");
			} else {
				checkbox.addClass(" glyphicon-unchecked").removeClass(" glyphicon-check ");
				del.removeClass("disabled");
				addToList.removeClass("disabled");
				removeFromList.removeClass("disabled");
				share.removeClass("disabled");
				update.removeClass("disabled");
			}
			var count = $('span.glyphicon-check').map(function check_del(index, obj) {

				return $(obj).closest('tr').data('id');
			})
			if (count.length > 0) {

				del.removeClass("disabled");
				share.removeClass("disabled");
				addToList.removeClass("disabled");
				removeFromList.removeClass("disabled");
				update.removeClass("disabled");
			} else {
				del.addClass("disabled");
				share.addClass("disabled");
				addToList.addClass("disabled");
				removeFromList.addClass("disabled");
				update.addClass("disabled");
				$('#selectall').removeClass(" glyphicon-check").addClass(" glyphicon-unchecked ");
			}

		});


		obj.click(function(e) {
			Spinner.hide();
			Spinner.show();
			e.stopPropagation();
			var modal = $('#sosyalmedya_print_modal');
			modal.meshop({

				url : "/json/soysalmedya/printable/load",
				params : {
					smid : json.id,
				},
			 loop : loopReply,
			// modifyJson : modifyJsonModal
			}).on("render", function(obj, json) {
				 
				modal.modal('show');
				Spinner.hide();
			})
			
			smid=json.id;

		});
		obj.find('.chase-labeled').click(function(e) {
			e.stopPropagation();
			var modal = $('#createList_modal');
			modal.modal('show');
			modal.meshop({
				json : {
				chaseId : json.chaseId,
				account : json.accounts,
				title : json.chase,
				color: json.color
			},
				loop : loopChase
			});
			
			
			
		});
		obj.find('#reply').click(function(e) {
			e.stopPropagation();
			var modal = $('#new_reply_modal');
			modal.modal('show');
			modal.meshop({
				json : {
				smid : json.id,
					accounts : json.accounts,
				},
				loop : loopModal
			});
				 
		 
			
		});
		obj.find('#ajanda').click(function(e) {
			e.stopPropagation();
			e.preventDefault();  //stop the browser from following
			
			var win = window.open( "/ajanda/?sosyalMedya="+json.id);
			if (win) {
			    //Browser has allowed it to be opened
			    win.focus();
			} else {
			    //Browser has blocked it
			    alert('Please allow popups for this website');
			}
			
			
			
		});
		obj.find('#edit').click(function(e) {
			e.stopPropagation();
			var modal = $('#new_smedya_modal');
		
			modal.meshop({

				url : "/json/sosyalmedya/loadbyId",
				params : {
					id : json.id,
				},
			// loop : loopModal,
			// modifyJson : modifyJsonModal
			}).on("render", function(obj, json) {
				modal.modal('show');
				
			});
			
			
		});
		
		obj.find('#addAjanda').click(function(e) {
			var modal = $('#newAjanda');
			e.stopPropagation();
			modal.modal('show');

			modal.meshop({
				json : {
				sosyal : {id:json.id,title:json.name},
				},
			});
		});
		
		

	}
	;
	
	
	$('#saveAjanda').click(function() {
		$('#saveAjanda,#resetModal').addClass("disabled");
		$(this).addClass('loading');
		var form = $('#Ajandaform');
		$.ajax({
			url : "/json/ajanda/save",
			type : "POST",
			data : form.serialize(),
		}).done(function(respond) {
			$('#newAjanda').modal('hide');
			meshop.log("ok", tok);

		}).fail(function(e) {
			meshop.log("error",terror);

		}).always(function() {
			$('#saveAjanda').removeClass('loading').removeClass('disabled');
			// meshop.refresh($("#Table"));
		});
	});
	
  
    
    function loopChase(obj, json, mainobj) {
    	
    	var colorButton = document.getElementById("primary_color");
        var colorDiv = $('input#color')
        colorButton.onchange = function() {
//            colorDiv.innerHTML = colorButton.value;
            colorDiv.val(colorButton.value);
        
        }
    }
	
	function loopReply(obj, json, mainobj) {

		obj.find('i.edit-reply').click(function(e) {
			e.stopPropagation();
			
			var id = $(this).attr('data-id');
			
				
				var modal = $('#new_reply_modal');
				
				modal.meshop({

					url : "/json/reply/loadbyId",
					params : {
						id : id,
						 
					},
				}).on("render", function(obj, json) {
					modal.modal('show');
					
				});
			
			
		});
		 

	}
	$(".sosyalxcel").click(function(e) {

		var modal = $('#new_file_modal');
		modal.modal('show');
		$("#setAspectRatio").trigger("click");
		setTimeout(function() {
			$('#saveCanvas').click();
		}, 200);
		modal.meshop({
			json : {
				language : language,
			},
		// loop : loopModal
		});

	});
	$(".takipexcel").click(function(e) {
		
		var modal = $('#new_replyfile_modal');
		modal.modal('show');
		$("#setAspectRatio").trigger("click");
		setTimeout(function() {
			$('#saveCanvas').click();
		}, 200);
		modal.meshop({
			json : {
			language : language,
		},
			// loop : loopModal
		});
		
	});
	
	
	$('#saveREPLY').click(function() {
		var formData = new FormData();
		var form = $('form#reply_doc_form');
		$('input[type="file"]').each(function(i, input) {
			var oo = input.files[0];
			if (oo) {
				formData.append("myNewFileName", oo);
				
			}
		});
		
		var other_data = form.serializeArray();
		$.each(other_data,function(key,input){
			formData.append(input.name,input.value);
		});
		
		$.ajax({
			
			success : function(response) {
			
			meshop.log("ok", tok);
			meshop.refresh($("#Table"));
		},
			
			error : function(err) {
			
			if (err.responseText == '2') {
				meshop.log("error", trepeat);
			}else{
				meshop.log("error", "");
				
			}
			
		},
			
			
			data : formData,
			processData : false,
			contentType : false,
			url : "/adminpanel/json/excelreply/save",
			type : "POST",
			
		});
		
	});
	$('#saveDOC').click(function() {
		var formData = new FormData();
		 var form = $('form#doc_form');
//		 var language =$('input[name="language"]').val();
//		  formData.append('language', language);
	 
		 $('input[type="file"]').each(function(i, input) {
				var oo = input.files[0];
				if (oo) {
					formData.append("myNewFileName", oo);
					 
				}
		 });
		 
		 var other_data = form.serializeArray();
		 $.each(other_data,function(key,input){
			 formData.append(input.name,input.value);
		 });
		 
			$.ajax({

				success : function(response) {
					
					meshop.log("ok", tok);
					meshop.refresh($("#Table"));
				},

				error : function(err) {
					
					if (err.responseText == '2') {
						meshop.log("error", trepeat);
					}else{
						meshop.log("error", "");
						
					}

				},
			

				data : formData,
				processData : false,
				contentType : false,
				url : "/adminpanel/json/excel/save",
				type : "POST",

			});

	});
	
	 $(".excel").click(function(e) {
		 e.stopPropagation();
		 var sort= $(this).data().sort;
		 data = new FormData();
//		 data.append( 'predicate', predicate);
//		 data.append( 'sort', sort);
		 formData = $("form#filter-form :input[value!='']").serializeArray()
		 
		 
			$.ajax({
				url : "/json/sosyalmedya/excel?sorting="+sort,
//				url : "/adminpanel/json/installments/excel",
				type : "POST",
				data : formData,
			}).done(function(respond) {
				// window.location =
				// "/";

				 e.preventDefault();  //stop the browser from following
				    window.location.href = "/files/"+respond.path;
				 

			}).fail(function(e) {
				meshop.log("error", terror);

			}).always(function() {
				$('#save').removeClass('loading').removeClass('disabled');
				// meshop.refresh($("#Table"));
			});
		 
	 });

$('#saveSosyal').click(function() {
	$('#saveSosyal,#resetModal').addClass("disabled");
	$(this).addClass('loading');
	var form = $('#sm_form');
	$.ajax({
		url : "/json/sosyalmedya/save",
		type : "POST",
		data : form.serialize(),
	}).done(function(respond) {
		// window.location =
		// "/";

		$('#new_smedya_modal').modal('hide');
		meshop.refresh($("#Table"));
		updateEventCount();
		meshop.log("ok", tok);

	}).fail(function(e) {
		meshop.log("error", terror);

	}).always(function() {
		$('#saveSosyal').removeClass('loading').removeClass('disabled');
		// meshop.refresh($("#Table"));
	});
//	 meshop.refresh($("#Table"));

});

function updateEventCount() {
    $.get("event-count").done(function(fragment) { // get from controller
        $("#SosylaCount").replaceWith(fragment); // update snippet of page
        // update snippet of page
    });
}

$('#saveReply').click(function() {
	$('#saveReply,#resetModal').addClass("disabled");
	$(this).addClass('loading');
	var form = $('#reply_form');
	$.ajax({
		url : "/json/reply/save",
		type : "POST",
		data : form.serialize(),
	}).done(function(respond) {
		// window.location =
		// "/";
		
		$('#new_reply_modal').modal('hide');
		meshop.refresh($("#Table"));
		meshop.refresh($("#sosyalmedya_print_modal"));
		meshop.log("ok", tok);
		updateEventCount();

		
	}).fail(function(e) {
		meshop.log("error",terror);
		
	}).always(function() {
		$('#saveReply').removeClass('loading').removeClass('disabled');
		// meshop.refresh($("#Table"));
	});
//	 meshop.refresh($("#Table"));
	
});
$('#saveList').click(function() {
	$('#saveList,#resetModal').addClass("disabled");
	$(this).addClass('loading');
	var form = $('#list_form');
	$.ajax({
		url : "/json/chaselist/save",
		type : "POST",
		data : form.serialize(),
	}).done(function(respond) {
		// window.location =
		// "/";
		
		$('#createList_modal').modal('hide');
		 
		meshop.log("ok", tok);
		 meshop.refresh($("#Table"));
		
	}).fail(function(e) {
		meshop.log("error", terror);
		
	}).always(function() {
		$('#saveList').removeClass('loading').removeClass('disabled');
	});
//	
	
});


var del = $('.delete')
var share = $('.share')
var addToList = $('.addToList')
var addList = $('#addList')
var removeFromList = $('.removeFormList')
var createList = $('.createList')
var update = $('.update')
		var selectall = $('#selectall');
		selectall.parent().click(function(e) {
			if (selectall.hasClass('glyphicon-unchecked')) {
				check(selectall);
				check($(".selected"));
				del.removeClass("disabled");
				share.removeClass("disabled");
				update.removeClass("disabled");
				removeFromList.removeClass("disabled");
				addToList.removeClass("disabled");
			} else {
				uncheck(selectall);
				uncheck($(".selected"));
				del.addClass("disabled");
				share.addClass("disabled");
				update.addClass("disabled");
				removeFromList.addClass("disabled");
				addToList.addClass("disabled");
			}

			function check(obj) {
				obj.addClass(" glyphicon-check").removeClass(" glyphicon-unchecked ");
			}
			function uncheck(obj) {
				obj.removeClass(" glyphicon-check ").addClass(" glyphicon-unchecked");
			}

		});

	 
		del.click(function(e) {
			var modal = meshop.alert("ask", " Emin misiniz?", "Uyarı");
			modal.size("sm");
			modal.yes = function() {
				e.preventDefault();
				del.addClass('loading');
				var selecteditems = "";
				var count = $('span.glyphicon-check.selected').map(function(index, obj) {
					return $(obj).closest('tr').data('id');
				})
				var selecteditems = count.get().join();

				if (selecteditems) {
					$.post("/adminpanel/json/sosyalmedya/remove", {
						ids : selecteditems
					}, function(data) {
						meshop.refresh($("#Table"));
						meshop.log("ok", tdelete);
					}).fail(function(e) {
						del.removeClass('loading');
						meshop.log("error", terror);
					}).always(function() {
						del.removeClass('loading');
						del.addClass("disabled");
					});
				}
			};
		});
		removeFromList.click(function(e) {
			var modal = meshop.alert("ask", " Emin misiniz?", "Uyarı");
			modal.size("sm");
			modal.yes = function() {
				e.preventDefault();
				removeFromList.addClass('loading');
				var selecteditems = "";
				var count = $('span.glyphicon-check.selected').map(function(index, obj) {
					return $(obj).closest('tr').data('id');
				})
						var selecteditems = count.get().join();
				
				if (selecteditems) {
					$.post("/json/sosyalmedya/removefromlst", {
						items : selecteditems
					}, function(data) {
						meshop.refresh($("#Table"));
						meshop.log("ok", tok);
					}).fail(function(e) {
						removeFromList.removeClass('loading');
						meshop.log("error", terror);
					}).always(function() {
						removeFromList.removeClass('loading');
						removeFromList.addClass("disabled");
					});
				}
			};
		});
		
		
		addList.click(function(e) {
			var modal = meshop.alert("ask", " Listeye Eklencek. Emin misiniz?", "Uyarı");
			modal.size("sm");
			modal.yes = function() {
				e.preventDefault();
//				addList.addClass('loading');
				var selecteditems = "";
				var count = $('span.glyphicon-check.selected').map(function(index, obj) {
					return $(obj).closest('tr').data('id');
				})
						var selecteditems = count.get().join();
				
				var formData = new FormData();
				if (selecteditems) {
					addList.addClass('loading');
					var form = $('#chase_form');
					formData.append("items", selecteditems);
//					
					var other_data = form.serializeArray();
					$.each(other_data,function(key,input){
						formData.append(input.name,input.value);
					});
					
					$.ajax({

						success : function(response) {
						var modal = $('#addToList_modal');
						modal.modal('hide');
							meshop.log("ok", tok);
							meshop.refresh($("#Table"));
							addList.removeClass('loading');
						},

						error : function(err) {
							addList.removeClass('loading');
							if (err.responseText == '0') {
								var modal = $('#addToList_modal');
								modal.modal('hide');
								meshop.log("warning", tlisterror);
								meshop.refresh($("#Table"));
								
								
							}else{
								meshop.log("error",terror);
								
							}

						},
					

						data : formData,
						processData : false,
						contentType : false,
						url : "/json/chase/add",
						type : "POST",

					});
				}
			};
		});
		
		
		
		
		share.click(function(e) {
			
			
			var modal = $('#share_modal');
			modal.modal('show');
		 
			modal.meshop({
				json : {
				accounts:[], 
				},
			});
		 
 
		});
		update.click(function(e) {
			
			
			var modal = $('#update_modal');
			modal.modal('show');
			
			modal.meshop({
				json : {
				accounts:[], 
			},
			});
			
			
		});
		addToList.click(function(e) {
			
			
			var modal = $('#addToList_modal');
			modal.modal('show');
			
			modal.meshop({
				json : {
				accounts:[], 
				lan:language,
			},
			});
			
			
		});
		createList.click(function(e) {
			
			
			var modal = $('#createList_modal');
			modal.modal('show');
			
			modal.meshop({
				json : {
				accounts:[], 
				
			},
			});
			
			
		});
		
//		sharing
		
		$("#sharing").click(function(e) {
			var modal = meshop.alert("ask", " Emin misiniz?", "Uyarı");
			modal.size("sm");
			var accouts = $("#share_modal input[name='accounts']").val();
			modal.yes = function() {
				e.preventDefault();
				$("#sharing").addClass('loading');
				var selecteditems = "";
				var count = $('span.glyphicon-check.selected').map(function(index, obj) {
					return $(obj).closest('tr').data('id');
				})
				var selecteditems = count.get().join();

				if (selecteditems) {
					$.post("/adminpanel/json/sosyalmedya/share", {
						ids : selecteditems,
						accounts :accouts
					}, function(data) {
						meshop.refresh($("#Table"));
						meshop.log("ok", tok);
						modal.modal('hide');
					}).fail(function(e) {
						$("#sharing").removeClass('loading');
						meshop.log("error", terror);
					}).always(function() {
						$("#sharing").removeClass('loading');
						$("#sharing").removeClass("disabled");
					});
				}
			};
		});
		
		//update
		
		$("#saveUpdate").click(function(e) {
			var modal = meshop.alert("ask", " Emin misiniz?", "Uyarı");
			modal.size("sm");
			var note = $("#update_modal textarea[name='note']").val();
			modal.yes = function() {
				e.preventDefault();
				$("#saveUpdate").addClass('loading');
				var selecteditems = "";
				var count = $('span.glyphicon-check.selected').map(function(index, obj) {
					return $(obj).closest('tr').data('id');
				})
						var selecteditems = count.get().join();
				
				if (selecteditems) {
					$.post("/adminpanel/json/sosyalmedya/update", {
						ids : selecteditems,
						note :note
					}, function(data) {
						meshop.refresh($("#Table"));
						meshop.log("ok", tok);
						modal.modal('hide');
					}).fail(function(e) {
						$("#saveUpdate").removeClass('loading');
						meshop.log("error", terror);
					}).always(function() {
						$("#saveUpdate").removeClass('loading');
						$("#saveUpdate").removeClass("disabled");
					});
				}
			};
		});
		


});