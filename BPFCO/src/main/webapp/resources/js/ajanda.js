$(document).ready(function() {

	 
	Spinner();
	Spinner.show();
	 
	var pathname = window.location.href;
	path = pathname.split('?')[1];
	 
	if(path!= null) {
	url =	"/json/ajandas/load?"+path+"&"+moment(new Date()).format("MM/DD/YYYY");
	}else {
		url =	"/json/ajandas/load?issueDate="+moment(new Date()).format("MM/DD/YYYY");
	}
	
	$("#Table").meshop({
		url : url,
		page : $('[data-id=Table_page]'),
		loop:loop
	}).on("render", function(obj, json) {
		$("#DATE").text(' ['+json[0].today+']');
		$("#Table").removeClass('hidden');
		$("#empty").addClass('hidden');
		Spinner.hide();
	}).on("empty",function(){
		
		$('#searchLog').removeClass("disabled");
		$('#searchLog').removeClass("loading");
		$("#Table").addClass('hidden');
		$("#empty").removeClass('hidden');
		Spinner.hide();
	});
	
	
$('#print').click(function(e) {
		
		$("#modalDiv").printThis({ 
		    debug: false,              
		    importCSS: true,             
		    importStyle: true,         
		    printContainer: true,       
		    loadCSS: "../css/style.css", 
		    pageTitle: "My Modal",             
		    removeInline: false,        
		    printDelay: 333,            
		    header: null,             
		    formValues: true          
		}); 
		});
	
	 $('#searchDate').click(function(e) {
		 e.stopPropagation();
		 var modal = $('#serachDateModal');
		 modal.modal('show');
		 
	 }); 
	 
	 $('#prev').click(function(e) {
		 var date = $('#yesterday').text();
			Spinner.show();
			$.ajax({
				url : "/changedate",
				type : "GET",
				async: true,
				 data: {date:date,day:true}
			}).done(function(fragment) {
				 $("#Buttons").replaceWith(fragment);
				 
				 
				 $("#Table").meshop({
						url : "/json/ajandas/load?issueDate="+date,
						page : $('[data-id=Table_page]'),
						loop:loop
					}).on("render", function(obj, json) {
						$("#DATE").text(' ['+json[0].today+']');
						$("#Table").removeClass('hidden');
						$("#empty").addClass('hidden');
						Spinner.hide();
					}).on("empty",function(){
						
						$('#searchLog').removeClass("disabled");
						$('#searchLog').removeClass("loading");
						$("#Table").addClass('hidden');
						$("#empty").removeClass('hidden');
						Spinner.hide();
					});
				 
			})
		 
	 }); 
	 $('#next').click(function(e) {
		 var date = $('#tommorow').text();
			Spinner.show();
		 $.ajax({
			 url : "/changedate",
			 type : "GET",
			 async: true,
			 data: {date:date,day:false}
		 }).done(function(fragment) {
			 
			 $("#Buttons").replaceWith(fragment);
			 $("#Table").meshop({
					url : "/json/ajandas/load?issueDate="+date,
					page : $('[data-id=Table_page]'),
						loop:loop
			 }).on("render", function(obj, json) {
				 $("#Table").removeClass('hidden');
				 $("#empty").addClass('hidden');
					Spinner.hide();
			 }).on("empty",function(){
				 
				 $('#searchLog').removeClass("disabled");
				 $('#searchLog').removeClass("loading");
				 $("#Table").addClass('hidden');
				 $("#empty").removeClass('hidden');
					Spinner.hide();
			 });
			 
		 })
		 
	 }); 
	
	$('#searchLog').click(function() {
		Spinner.show();
		 var modal = $('#serachDateModal');
		$('#searchLog,#resetModal').addClass("disabled");
		$(this).addClass('loading');
		var date = $('input[name="date"]').val();
		
		 $.ajax({
			 url : "/changeBydate",
			 type : "GET",
			 async: true,
			 data: {date:date}
		 }).done(function(fragment) {
			 $("#Buttons").replaceWith(fragment);
		
		$("#Table").meshop({
			url : "/json/ajandas/load?issueDate="+date,
			page : $('[data-id=Table_page]'),
			loop:loop
		}).on("render", function(obj, json) {
			$("#Table").removeClass('hidden');
			$("#empty").addClass('hidden');
			$('#searchLog').removeClass("disabled");
			$('#searchLog').removeClass("loading");
			 modal.modal('hide');
			 
				Spinner.hide();
			 
			 
		}).on("empty",function(){
			
			$('#searchLog').removeClass("disabled");
			$('#searchLog').removeClass("loading");
			$("#Table").addClass('hidden');
			$("#empty").removeClass('hidden');
			modal.modal('hide');
			Spinner.hide();
//		 meshop.refresh($("#Table"));
		});
		

		 })
	});
	$('#today').click(function() {
		$(this).addClass('loading');
		var date = $(this).val();
		Spinner.show();
		$.ajax({
			url : "/changedateToday",
			type : "GET",
			async: true,
			 data: {date:date}
		}).done(function(fragment) {
			 $("#Buttons").replaceWith(fragment);
				Spinner.hide();
		});	
		
		
		$("#Table").meshop({
			url : "/json/ajandas/load?issueDate="+date,
			page : $('[data-id=Table_page]'),
			loop:loop
		}).on("render", function(obj, json) {
			$("#Table").removeClass('hidden');
			$("#empty").addClass('hidden');
			$('#today').removeClass("loading");
			Spinner.hide();
		}).on("empty",function(){
			
			$('#today').removeClass("loading");
			$("#Table").addClass('hidden');
			$("#empty").removeClass('hidden');
			Spinner.hide();
//		 meshop.refresh($("#Table"));
		});
	});
	
	
	
	
	$('#newAjandaBtn').click(function() {
		var modal = $('#newAjanda');
		
		modal.modal('show');

		modal.meshop({
			json : {
				active : false,
			},
		});
	});
	
	function loop(obj, json, mainObj) {
		
	

		obj.data('id', json.id);

		obj.find('.scheck').click(function(e) {

			e.stopPropagation();
			var checkbox = obj.find(".selected");

			if (checkbox.hasClass('glyphicon-unchecked')) {
				checkbox.addClass(" glyphicon-check").removeClass(" glyphicon-unchecked ");
			} else {
				checkbox.addClass(" glyphicon-unchecked").removeClass(" glyphicon-check ");
				del.removeClass("disabled");
				done.removeClass("disabled");
			}
			var count = $('span.glyphicon-check').map(function check_del(index, obj) {

				return $(obj).closest('tr').data('id');
			})
			if (count.length > 0) {

				del.removeClass("disabled");
				done.removeClass("disabled");
			} else {
				del.addClass("disabled");
				done.addClass("disabled");
				$('#selectall').removeClass(" glyphicon-check").addClass(" glyphicon-unchecked ");
			}

		});

		obj.click(function(e) {
			Spinner.hide();
			Spinner.show();
			e.stopPropagation();
			var modal = $('#sosyalmedya_print_modal');
			modal.meshop({

				url : "/json/crm/printable/load",
				params : {
					smid : json.id,
				},
			 loop : loopReply,
			// modifyJson : modifyJsonModal
			}).on("render", function(obj, json) {
				 
				modal.modal('show');
				Spinner.hide();
			})
			
			smid=json.id;

		});

		obj.find('.edit').click(function(e) {
			e.stopPropagation();
			Spinner.hide();
			Spinner.show();
			var modal = $('#newAjanda');
			modal.meshop({

				url : "/json/ajanda/load",
				params : {
					id : json.id,
				},
			 
			}).on("render", function(obj, json) {
				 
				modal.modal('show');
				Spinner.hide();
			})

		});
		
		obj.find('.sm').click(function(e) {
			e.stopPropagation();
		});

	}
	;
	
	function loopReply(obj, json, mainobj) {

		obj.find('i.edit-reply').click(function(e) {
			e.stopPropagation();
			
			var id = $(this).attr('data-id');
			
				
				var modal = $('#new_reply_modal');
				
				modal.meshop({

					url : "/json/reply/loadbyId",
					params : {
						id : id,
						 
					},
				}).on("render", function(obj, json) {
					modal.modal('show');
					
				});
			
			
		});
		 

	}
	
	$('#saveAjanda').click(function() {
		$('#saveAjanda,#resetModal').addClass("disabled");
		$(this).addClass('loading');
		var form = $('#Ajandaform');
		$.ajax({
			url : "/json/ajanda/save",
			type : "POST",
			data : form.serialize(),
		}).done(function(respond) {
			// window.location =
			// "/";

			$('#newAjanda').modal('hide');
			meshop.refresh($("#Table"));
			meshop.log("ok", tok);

		}).fail(function(e) {
			meshop.log("error",terror);

		}).always(function() {
			$('#saveAjanda').removeClass('loading').removeClass('disabled');
			// meshop.refresh($("#Table"));
		});
	});
	
	
	$('#saveReply').click(function() {
		$('#saveReply,#resetModal').addClass("disabled");
		$(this).addClass('loading');
		var form = $('#reply_form');
		$.ajax({
			url : "/json/reply/save",
			type : "POST",
			data : form.serialize(),
		}).done(function(respond) {
			// window.location =
			// "/";
			
			$('#new_reply_modal').modal('hide');
//			meshop.refresh($("#Table"));
			meshop.refresh($("#sosyalmedya_print_modal"));
			meshop.log("ok", tok);
			$('#saveReply').removeClass('loading').removeClass('disabled');

			
		}).fail(function(e) {
			meshop.log("error",terror);
			
		}).always(function() {
			$('#saveReply').removeClass('loading').removeClass('disabled');
			// meshop.refresh($("#Table"));
		});
//		 meshop.refresh($("#Table"));
		
	});
	
	var del = $('#deleteAjanda')
			var done = $('#done')
			var selectall = $('#selectall');
			selectall.parent().click(function(e) {
				if (selectall.hasClass('glyphicon-unchecked')) {
					check(selectall);
					check($(".selected"));
					del.removeClass("disabled");
					done.removeClass("disabled");
				} else {
					uncheck(selectall);
					uncheck($(".selected"));
					del.addClass("disabled");
					done.addClass("disabled");
				}

				function check(obj) {
					obj.addClass(" glyphicon-check").removeClass(" glyphicon-unchecked ");
				}
				function uncheck(obj) {
					obj.removeClass(" glyphicon-check ").addClass(" glyphicon-unchecked");
				}

			});
			
			del.click(function(e) {
				var modal = meshop.alert("ask", " Emin misiniz?", "Uyarı");
				modal.size("sm");
				modal.yes = function() {
					e.preventDefault();
					del.addClass('loading');
					var selecteditems = "";
					var count = $('span.glyphicon-check.selected').map(function(index, obj) {
						return $(obj).closest('tr').data('id');
					})
					var selecteditems = count.get().join();

					if (selecteditems) {
						$.post("/json/ajanda/remove", {
							ids : selecteditems
						}, function(data) {
							meshop.refresh($("#Table"));
							meshop.log("ok", tdelete);
						}).fail(function(e) {
							del.removeClass('loading');
							meshop.log("error", terror);
						}).always(function() {
							del.removeClass('loading');
							del.addClass("disabled");
							done.removeClass('loading');
							done.addClass("disabled");
						});
					}
				};
			});
			
			
	done.click(function(e) {
				
				e.stopPropagation();
				
				var modal = $('#changeStatus');
				modal.modal('show');
				$('#statusBTN').addClass('disabled');
			 

			});
	$('#statusBTN').click(function(e) {
		
		e.stopPropagation();
		e.preventDefault();
		$('#statusBTN').addClass('loading');
		var selecteditems = "";
		var count = $('span.glyphicon-check.selected').map(function(index, obj) {
			return $(obj).closest('tr').data('id');
		})
				var selecteditems = count.get().join();
		
		var status = $('#status option:selected').val()
				
				var reply = $("textarea[name='reply']").val()
		
		if (selecteditems) {
			$.post("/json/ajanda/done", {
				ids : selecteditems,
				status:status,
				reply:reply
				
			}, function(data) {
				meshop.refresh($("#Table"));
				meshop.log("ok", tok);
			}).fail(function(e) {
				$('#statusBTN').removeClass('loading');
				meshop.log("error", terror);
			}).always(function() {
				$('#statusBTN').removeClass('loading');
				$('#statusBTN').addClass("disabled");
				del.removeClass('loading');
				del.addClass("disabled");
				$('#changeStatus').modal('hide');
			});
		}
		 
	 

	});
	
	$('#status').on('change', function(e){
		$('#statusBTN').removeClass('disabled');
		});
	
});