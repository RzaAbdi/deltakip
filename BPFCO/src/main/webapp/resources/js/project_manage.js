$(document).ready(function() {

	 
	$("#Table").meshop({
		url : "/adminpanel/json/projects/load",
		page : $('[data-id=Table_page]'),
		modifyJson : modifyJson,
		loop : loop,
		filterForm:$('#filter-form')

	});
	
	
	function modifyJson(json) {

		 
		

		return json;
	}
	
		$("#new_portfoy").click(function(e) {
		var modal = $('#new_project_modal');
		modal.modal('show');

		modal.meshop({
			json : {
				active : false,
			},
//			loop : loopModal
		});

	});

		function loop(obj, json, mainObj) {

			obj.data('id', json.id);

			obj.find('.scheck').click(function(e) {

				e.stopPropagation();
				var checkbox = obj.find(".selected");

				if (checkbox.hasClass('glyphicon-unchecked')) {
					checkbox.addClass(" glyphicon-check").removeClass(" glyphicon-unchecked ");
				} else {
					checkbox.addClass(" glyphicon-unchecked").removeClass(" glyphicon-check ");
					del.removeClass("disabled");
				}
				var count = $('span.glyphicon-check').map(function check_del(index, obj) {

					return $(obj).closest('tr').data('id');
				})
				if (count.length > 0) {

					del.removeClass("disabled");
				} else {
					del.addClass("disabled");
					$('#selectall').removeClass(" glyphicon-check").addClass(" glyphicon-unchecked ");
				}

			});


			obj.click(function(e) {
				e.stopPropagation();
				var modal = $('#new_project_modal');
				modal.modal('show');
				modal.meshop({

					url : "/adminpanel/json/project/load",
					params : {
						id : json.id,
					},
				// loop : loopModal,
				// modifyJson : modifyJsonModal
				})

			});

		}
		;
	

	$('#save').click(function() {
		$('#save,#resetModal').addClass("disabled");
		$(this).addClass('loading');
		var form = $('#proje_form');
		$.ajax({
			url : "/adminpanel/json/project/save",
			type : "POST",
			data : form.serialize(),
		}).done(function(respond) {
			// window.location =
			// "/";

			$('#new_project_modal').modal('hide');
			meshop.refresh($("#Table"));
			meshop.log("ok", tok);

		}).fail(function(e) {
			meshop.log("error", terror);

		}).always(function() {
			$('#save').removeClass('loading').removeClass('disabled');
			// meshop.refresh($("#Table"));
		});
//		 meshop.refresh($("#Table"));

	});
	
	var del = $('#delete-project')
			var selectall = $('#selectall');
			selectall.parent().click(function(e) {
				if (selectall.hasClass('glyphicon-unchecked')) {
					check(selectall);
					check($(".selected"));
					del.removeClass("disabled");
				} else {
					uncheck(selectall);
					uncheck($(".selected"));
					del.addClass("disabled");
				}

				function check(obj) {
					obj.addClass(" glyphicon-check").removeClass(" glyphicon-unchecked ");
				}
				function uncheck(obj) {
					obj.removeClass(" glyphicon-check ").addClass(" glyphicon-unchecked");
				}

			});

			del.click(function(e) {
				var modal = meshop.alert("ask", "Emin misiniz?", "Uyarı");
				modal.size("sm");
				modal.yes = function() {
					e.preventDefault();
					del.addClass('loading');
					var selecteditems = "";
					var count = $('span.glyphicon-check.selected').map(function(index, obj) {
						return $(obj).closest('tr').data('id');
					})
					var selecteditems = count.get().join();

					if (selecteditems) {
						$.post("/adminpanel/json/project/remove", {
							ids : selecteditems
						}, function(data) {
							meshop.refresh($("#Table"));
							meshop.log("ok", tdelete);
						}).fail(function(e) {
							del.removeClass('loading');
							meshop.log("error", terror);
						}).always(function() {
							del.removeClass('loading');
							del.addClass("disabled");
						});
					}
				};
			});


});