$(document)
		.ready(
				function() {

					$("#submit").click(
							function(e) {
								e.preventDefault();
								if (meshop.check.length($('input[name=fname]'),
										2)
										& meshop.check.length(
												$('input[name=lname]'), 2)
										& meshop.check
												.email($('input[name=email]'))
										& meshop.check.length(
												$('input[name=password]'), 5)
										& meshop.check.match(
												$('[data-id=rpassword]'), $(
														'input[name=password]')
														.val())) {

									var src = getProfilePic();

									$.post("/signup", $("#signup_form").serialize(),

									function(data) {

										setPicture();

										// window.location = "/";
									}).fail(function(message) {
										alert(message.responseText);
									});
								}
							});

					function setPicture() {

						var src = getProfilePic();

						var blobBin = atob(src.split(',')[1]);
						var array = [];
						for (var i = 0; i < blobBin.length; i++) {
							array.push(blobBin.charCodeAt(i));
						}
						var file = new Blob([ new Uint8Array(array) ], {
							type : 'image/png'
						});

						var username = $('input[name=email]').val();

						var password = $('input[name=password]').val();

						var formdata = new FormData();
						formdata.append("myNewFileName", file);
						formdata.append("username", username);
						formdata.append("password", password);
						$.ajax({
							url : "/json/upload/profil",
							type : "POST",

							// data: { 'myNewFileName': file , 'username' :
							// $('input[name=email]'),'password' :
							// $('input[name=password]') } ,
							data : formdata,
							processData : false,
							contentType : false,
						}).done(function(respond) {
							window.location = "/";
						});

					}

					function getProfilePic() {

						var canvas = $("#my_profile")
						var domImg = new Image();
						domImg.src = canvas.get(0).toDataURL();

						return domImg.src;

					}

					$("#resetModal").click(function(e) {

						var canvas = $("#my_profile")
						var domImg = new Image();
						domImg.src = canvas.get(0).toDataURL();
						domImg.height = 100;

						$('body').append(domImg);

					});

					$("#btn_upload").click(function(e) {

						$("#upload").trigger("click");

					});

					$("#setAspectRatio").trigger("click");

					$('.mytextbox')
							.bind(
									'keyup blur',
									function() {
										$(this)
												.val(
														$(this)
																.val()
																.replace(
																		/[^a-zA-Z0-9_-İıĞğÇçŞşƏəÖöÜüÖö'']/g,
																		''))

									});
				});
