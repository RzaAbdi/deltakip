$(document).ready(function() {
	Spinner();
	Spinner.show();
	var last;
	var delteTr;
	
	var cid;
	var insid;
	
	$('#getSenet').click(function(e) {
		
		$(".senetprint").printThis({ 
		    debug: false,              
		    importCSS: true,             
		    importStyle: true,         
		    printContainer: true,       
		    loadCSS: "../css/style.css", 
		    pageTitle: "My Modal",             
		    removeInline: false,        
		    printDelay: 333,            
		    header: null,             
		    formValues: true          
		}); 
		});
	
	
	$('#getCover').click(function(e) {
		
		$("#modalCoverDiv").printThis({ 
		    debug: false,              
		    importCSS: true,             
		    importStyle: true,         
		    printContainer: true,       
		    loadCSS: "../css/style.css", 
		    pageTitle: "My Modal",             
		    removeInline: false,        
		    printDelay: 333,            
		    header: null,             
		    formValues: true          
		}); 
		});
	 
	var pathname = window.location.href;
	path = pathname.split('?')[1];
	if(path!= null) {
	url =	"/json/covers/load"+"?"+path;
	}else {
		url =	"/json/covers/load";
	}
	 

	$("#Table").meshop({
		url : url,
		page : $('[data-id=Table_page]'),
		modifyJson : modifyJson,
		loop : loop,
	// filterForm:$('#filter-form')
	}).on("empty",function(){
		Spinner.hide();
		
		$("#Table").addClass('hidden');
		$('[data-id=Table_page]').addClass('hidden');
		$("#empty").removeClass('hidden');
	}).on("render", function(obj, json) {
		Spinner.hide();
		
	});
	
	
	function modifyJson(json) {

	 
		

		return json;
	}
	
		$("#new_cover").click(function(e) {
		var modal = $('#new_cover_modal');
		modal.modal('show');

		modal.meshop({
			json : {
				active : false,
			},
			 
		});

	});

	 
		$('#new_cover_modal').on("render", function(e, json, robj) {
			
			 
			 if(json.fee ==2) {
					$("[name='harc']").removeAttr('disabled');

			 }else {
				 $("[name='harc']").attr('disabled','disabled');
			 }
			
		});

		
		$(document.body).on("change","#satici",function(){
			  
			$("[name='harc']").attr('disabled','disabled');
			
			});
		$(document.body).on("change","#alici",function(){
			  
			$("[name='harc']").attr('disabled','disabled');
			
		});
		$(document.body).on("change","#ozel",function(){
			$("[name='harc']").removeAttr('disabled');
			
			
		});
	
	function loop(obj, json, mainObj) {
		

		obj.data('id', json.id);

		obj.find('.scheck').click(function(e) {

			e.stopPropagation();
			var checkbox = obj.find(".selected");

			if (checkbox.hasClass('glyphicon-unchecked')) {
				checkbox.addClass(" glyphicon-check").removeClass(" glyphicon-unchecked ");
			} else {
				checkbox.addClass(" glyphicon-unchecked").removeClass(" glyphicon-check ");
				del.removeClass("disabled");
			}
			var count = $('span.glyphicon-check').map(function check_del(index, obj) {

				return $(obj).closest('tr').data('id');
			})
			if (count.length > 0) {

				del.removeClass("disabled");
			} else {
				del.addClass("disabled");
				$('#selectall').removeClass(" glyphicon-check").addClass(" glyphicon-unchecked ");
			}

		});

		
		obj.find("#edit_cover").click(function(e) {
			Spinner.hide();
			Spinner.show();
			e.stopPropagation();
			var modal = $('#new_cover_modal');
			modal.meshop({

				url : "/json/cover/load",
				params : {
					id : json.id,
				},
			 loop : loopModal,
			// modifyJson : modifyJsonModal
			}).on("render", function(obj, json) {
				Spinner.hide();
				modal.modal('show');
			});
			 
		});
		obj.find("#add_ins").click(function(e) {
			Spinner.hide();
			Spinner.show();
			e.stopPropagation();
			var modal = $('#new_inscover_modal');
			modal.meshop({
				
				url : "/json/cover/loadforIns",
				params : {
				id : json.id,
			},
				loop : loopIns,
				// modifyJson : modifyJsonModal
			}).on("render", function(obj, json) {
				Spinner.hide();
				modal.modal('show');
			});
			
		});
		
		obj.find("#printableSenet").click(function(e) {
			Spinner.hide();
			Spinner.show();
			e.stopPropagation();
var modal = $('#senet_modal');
			
			modal.meshop({

				url : "/json/coverInsLst/load",
				params : {
					id : json.id,
				},
			}).on("render", function(obj, json) {
				modal.modal('show');
				
				
				Spinner.hide();
			}).on("error", function(obj, json) {
				Spinner.hide();
				 
			});
			
		});
		
 
		obj.find("#newSenet").click(function(e) {
			Spinner.hide();
			Spinner.show();
			e.stopPropagation();
			
			
			
			var modal = $('#new_senet_modal');
			
			modal.meshop({
				
				url : "/json/cover/load/senet",
				params : {
				id : json.id,
			},
				 
			}).on("render", function(obj, json) {
				Spinner.hide();
				modal.modal('show');
			});
//			
			cid = json.id;
			
		});
		
		obj.click(function(e) {
			Spinner.hide();
			Spinner.show();
			e.stopPropagation();
			var modal = $('#cover_modal');
			modal.meshop({

				url : "/json/covermodal/load",
				params : {
					id : json.id,
				},
				loop : loopCover,
			// modifyJson : modifyJsonModal
			}).on("render", function(obj, json) {
				modal.modal('show');
				Spinner.hide();
			}).on("error", function(obj, json) {
				Spinner.hide();
				 
			});
			 
		});

	}
	;
	
	function loopCover(obj, json, mainObj) {
		
		obj.find(".edit-InsCover").click(function(e) {
			e.stopPropagation();
			var id = $(this).attr('data-id');
			
			
			var modal = $('#editInsForCover');
			
			var tr = $(this).parent().parent();
			tr.addClass('loading')
			
			modal.meshop({
				
				url : "/json/inscover/load",
				params : {
				id : id,
			},
				 
			}).on("render", function(obj, json) {
				modal.modal('show');
				tr.removeClass('loading')
				
				var object =$(this);
				
				var input = object.find("input[name='amount']")
				
						input.blur(function() {
					
					var val =  parseFloat(this.value.replace(/,/g, ""));
					
					var fix =  val.toFixed(2);
					
					var price = fix.toString()
							.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					
					$(this).val(price );
					
//				setRemining();
					
					
				});
			});
			
		});
		obj.find(".del-InsCover").click(function(e) {
			e.stopPropagation();
			var id = $(this).attr('data-id');
			tr = $(this).parent().parent();
			
			 
				var modal = meshop.alert("ask", "Silinecek, Emin misiniz?", "Uyarı");
				modal.size("sm");
				modal.yes = function() {
					
					tr.addClass('loading')
					e.preventDefault();
			 
						$.post("/json/inscover/remove", {
							id : id
						}, function(data) {
							meshop.refresh($("#Table"));
							meshop.refresh($('#cover_modal'));
							meshop.log("ok", tdelete);
						}).fail(function(e) {
							tr.removeClass('loading');
							meshop.log("error", terror);
						}).always(function() {
						});
				};
		 
			
		 
			
		});
		
		
		
		
	}
	 
	
	
	
	function loopModal(obj,json) {
		
		
obj.find("input[name='remaining']").blur(function() {
			
			var val =  parseFloat(this.value.replace(/,/g, ""));
			
			var fix =  val.toFixed(2);
			
			var price = fix.toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			
			$(this).val(price );
			
//			setRemining();
			
    
	});


		obj.find("input[name='cash']").blur(function() {
			
			var val =  parseFloat(this.value.replace(/,/g, ""));
			
			var fix =  val.toFixed(2);
			
			var price = fix.toString()
					.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			
			$(this).val(price.toString() );
			
//			setRemining();
			
			
			
			
			
		});
		obj.find("input[name='amount']").blur(function() {
			
			var val =  parseFloat(this.value.replace(/,/g, ""));
			
			var fix =  val.toFixed(2);
			
			var price = fix.toString()
					.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			
			$(this).val(price.toString() );
			
//			setRemining();
			
			
			
			
			
		});

		
		
		
		
	}
	function loopIns(obj, json, mainObj) {
		
		
obj.find("input[name='remaining']").blur(function() {
			
			var val =  parseFloat(this.value.replace(/,/g, ""));
			
			var fix =  val.toFixed(2);
			
			var price = fix.toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			
			$(this).val(price );
			
			setRemining();
			
    
	});
	 
		obj.find("input[name='amount']").blur(function() {
			
			var val =  parseFloat(this.value.replace(/,/g, ""));
			
			var fix =  val.toFixed(2);
			
			var price = fix.toString()
					.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			
			$(this).val(price.toString() );
			
			setRemining();
			
			
			
			
			
		});
		
		obj.find("input[name='limit']").blur(function() {
			
			setRemining();
			
		});
		
		function setRemining() {
			
		var p =	obj.find("input[name='remaining']").val().replace(/,/g, "") ;
		var a =	obj.find("input[name='amount']").val().replace(/,/g, "") ;
		if(p=="") {
			p=0
		}
		 
		var l =  parseFloat(obj.find("input[name='limit']").val().replace(/,/g, ""));
		
		 
		if(isNaN(last)) {
			last=0;
		}
		
		var last_total= (p);
		var limit;
		if(!isNaN(l)) {
			
			if(a=="") {
				
				limit = last_total/l; 
				last_total = (last_total-(limit*l));
				var val =  parseFloat(limit.toString().replace(/,/g, ""));
				var fix =  val.toFixed(2);
				var remain = fix.toString()
						.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				obj.find("input.am").val(remain);
			}else {
				last_total = (last_total-(a*l));
			}
			
				
				
				
				
		}
		
		
		
			
var val =  parseFloat(last_total.toString().replace(/,/g, ""));

last = val;
			
			var fix =  val.toFixed(0);
			
			 last_total = fix.toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			 
			
			 
			obj.find("span.rem").html(last_total);
		 
			obj.find("span.tak").html(l + " X " + obj.find("input[name='amount']").val().replace(/,/g, ""));
			
			
		}
		
		
	}
	;

	

	$("#new_cover").click(function(e) {
		 
		e.stopPropagation();
		var modal = $('#new_cover_modal');
		modal.modal('show');
			modal.meshop({
				json : {
					active : false,
				},
		// modifyJson : modifyJsonModal
		}) 
	});

$('#save_cover').click(function() {
	$('#save_cover,#resetModal').addClass("disabled");
	$(this).addClass('loading');
	var form = $('#cover_form');
	$.ajax({
		url : "/json/cover/save",
		type : "POST",
		data : form.serialize(),
	}).done(function(respond) {
		// window.location =
		// "/";

		$('#new_cover_modal').modal('hide');
		meshop.refresh($("#Table"));
		meshop.log("ok", tok);

	}).fail(function(e) {
		meshop.log("error",terror);

	}).always(function() {
		$('#save_cover').removeClass('loading').removeClass('disabled');
		// meshop.refresh($("#Table"));
	});
});
$('#saveInsCover').click(function() {
	$('#saveInsCover,#resetModal').addClass("disabled");
	$(this).addClass('loading');
	var form = $('#installmentCover_form');
	$.ajax({
		url : "/json/inscover/save",
		type : "POST",
		data : form.serialize(),
	}).done(function(respond) {
//		$('#new_cover_modal').modal('hide');
		
		meshop.refresh($('#cover_modal'));
		meshop.refresh($("#Table"));
		meshop.log("ok", tok);
		
	}).fail(function(e) {
		meshop.log("error",terror);
		
	}).always(function() {
		$('#saveInsCover').removeClass('loading').removeClass('disabled');
		// meshop.refresh($("#Table"));
	});
});
$('#manipulateCover').click(function() {
	
	
	$('#manipulateCover,#resetModal').addClass("disabled");
	$(this).addClass('loading');
	var form = $('#manipulate_form');
	$.ajax({
		url : "/json/cover/manipulate",
		type : "POST",
		data : form.serialize(),
	}).done(function(respond) {
		var modal = $('#senet_modal');
		
		modal.meshop({

			url : "/json/coverInsLst/load",
			params : {
				id : cid,
			},
		}).on("render", function(obj, json) {
			modal.modal('show');
			
			
			Spinner.hide();
		}).on("error", function(obj, json) {
			Spinner.hide();
			 
		});

		meshop.log("ok", tok);
		
	}).fail(function(e) {
		meshop.log("error", terror);
		
	}).always(function() {
		$('#manipulateCover').removeClass('loading').removeClass('disabled');
		// meshop.refresh($("#Table"));
	});
});
//	 meshop.refresh($("#Table"));

	$('#save_auto_ins').click(function() {
		$('#save_auto_ins,#resetModal').addClass("disabled");
		$(this).addClass('loading');
		var form = $('#auto_ins_form');
		$.ajax({
			url : "/json/autoinscover/save",
			type : "POST",
			data : form.serialize(),
		}).done(function(respond) {
			// window.location =
			// "/";
			
			$('#new_inscover_modal').modal('hide');
			meshop.refresh($("#Table"));
			meshop.log("ok",tok);
			
		}).fail(function(e) {
			meshop.log("error", terror);
			
		}).always(function() {
			$('#save_auto_ins').removeClass('loading').removeClass('disabled');
			// meshop.refresh($("#Table"));
		});
//	 meshop.refresh($("#Table"));
		
});

var del = $('#delete')
		var selectall = $('#selectall');
		selectall.parent().click(function(e) {
			if (selectall.hasClass('glyphicon-unchecked')) {
				check(selectall);
				check($(".selected"));
				del.removeClass("disabled");
			} else {
				uncheck(selectall);
				uncheck($(".selected"));
				del.addClass("disabled");
			}

			function check(obj) {
				obj.addClass(" glyphicon-check").removeClass(" glyphicon-unchecked ");
			}
			function uncheck(obj) {
				obj.removeClass(" glyphicon-check ").addClass(" glyphicon-unchecked");
			}

		});

		del.click(function(e) {
			var modal = meshop.alert("ask", " Emin misiniz?", "Uyarı");
			modal.size("sm");
			modal.yes = function() {
				e.preventDefault();
				del.addClass('loading');
				var selecteditems = "";
				var count = $('span.glyphicon-check.selected').map(function(index, obj) {
					return $(obj).closest('tr').data('id');
				})
				var selecteditems = count.get().join();

				if (selecteditems) {
					$.post("/adminpanel/json/cover/remove", {
						ids : selecteditems
					}, function(data) {
						meshop.refresh($("#Table"));
						meshop.log("ok", tdelete);
					}).fail(function(e) {
						del.removeClass('loading');
						meshop.log("error", terror);
					}).always(function() {
						del.removeClass('loading');
						del.addClass("disabled");
					});
				}
			};
		});
		
		

});