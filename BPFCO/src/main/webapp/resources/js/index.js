$(document).ready(function() {
	
	 
	Spinner();
	Spinner.hide();
	
		 
	 
	$('#Tab').meshop({
		json : {
		project : {id: 1, title:"DELTA DUBAI COMFORT WATERPARK"},
		},
		 
	});
	
	
	
	 
   
		
	
$('#printBlokPlan').click(function(e) {
	
	 
		
		$("#printable").printThis({ 
		    debug: false,              
		    importCSS: true,             
		    importStyle: true,         
		    printContainer: true,       
		    loadCSS: "../css/style.css", 
		    pageTitle: "My Modal",             
		    removeInline: false,        
		    printDelay: 333,            
		    header: null,             
		    formValues: true          
		}); 
		});


$('#print').click(function(e) {
	
	$("#modalDiv").printThis({ 
		debug: false,              
		importCSS: true,             
		importStyle: true,         
		printContainer: true,       
		loadCSS: "../css/style.css", 
		pageTitle: "My Modal",             
		removeInline: false,        
		printDelay: 333,            
		header: null,             
		formValues: true          
	}); 
});
	
	
	
	var thisYear = (new Date()).getFullYear();    
	var start = new Date("6/1/" + (thisYear-1));
	var end = new Date("12/31/" + thisYear);
	var startDate = moment(start.valueOf());
	var endDate = moment(end.valueOf());
	 var date = new Date();
	 var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
	
	$('#table').datepicker({
		endDate: today,
		  format: 'MM yyyy',
		  startView: "months",
		  minViewMode: "months",
		  autoclose: true
		}).on('changeDate', function (ev) {
			 var y = String(ev.date).split(" ")[3];
			 var m = String(ev.date).split(" ")[1];
			var month =  moment().month(m).format("MM");
			var startDate = moment([y, month - 1]);
			
			
			 meshop.log(startDate.toDate())
		});
	
	$('#table').datepicker('setDate', today);
	
	$('input[name="daterange"]').daterangepicker({
		opens: 'left',
		startDate: startDate,
		 endDate: endDate,
	}, function(start, end, label) {
		getInstallmentCountValue();
	});
	
	
	
	$('input[name="daterangeAmount"]').daterangepicker({
		opens: 'left',
		startDate: startDate,
		endDate: endDate,
	}, function(start, end, label) {
		getInstallmentAmountValue();
	});
	
	
	$('input[name="daterangeContract"]').daterangepicker({
		opens: 'left',
		startDate: startDate,
		endDate: endDate,
	}, function(start, end, label) {
		getInstallmentSaleValue();
	});
	
	$(document.body).on("change","#project",function(){
		getSaleValue();
		});
	$('#project').val(1).trigger('change.select2');
	 
	
	getInstallmentCountValue();
	getInstallmentAmountValue();
	getInstallmentSaleValue();
	getSaleValue();
 
	
	function getInstallmentAmountValue() {
		
		setTimeout(function() {
			var form = $('#inst_amount_form');
			var val = form.serializeArray()[1].value;
			getInstallmentAmount(val)
		}, 100);
	}
	
	function getInstallmentCountValue() {
		Spinner.show();
		setTimeout(function() {
			var form = $('#inst_form');
			var val = form.serializeArray()[1].value;
			getInstallmentCont(val)
		}, 10);
	}
	
	function getInstallmentSaleValue() {
		setTimeout(function() {
			var form = $('#inst_form');
			var val = form.serializeArray()[1].value;
			getInstallmentSale(val)
		}, 100);
	}
	
	function getSaleValue() {
		setTimeout(function() {
			var form = $('#sale_form');
			var val = form.serializeArray()[0].value;
			getSale(val)
		}, 100);
	}
	
	 function getSale(id){
		 $.get("/json/chart/salepie", $.param({
			 'id' : id }), function(json) {
			 ids = JSON.parse(json[2]);
			 counts = JSON.parse(json[1]);
			 parsedTest = JSON.stringify(json[0]);
			 pars = JSON.parse(parsedTest);
			 pars = pars.replace('[','');
			 pars = pars.replace(']','');
			bloks =  pars.split(',');
			 Spinner.hide();
			
			 var saleCount = document.getElementById('sale').getContext('2d');
			 if( window.chart4){
				 window.chart4.destroy();
			 }
			 window.chart4 = new Chart(saleCount, {
				 // The type of chart we want to create
				 type: 'bar',
				 data: {
					 labels: bloks,
					 datasets: [{
						 label: tchart7,
						 backgroundColor: '#073c66',
						 borderColor: '#073c66',
						 data: counts
					 },
					            
					 ]
				 },
				 
			 });
			 
			 document.getElementById('sale').onclick = function(evt) {
				   var activePoint = window.chart4.getElementAtEvent(evt)[0];
				   
				   if(activePoint) {
					   
					   var index =  activePoint._index;
					   var id = ids[index];
					   
					   if(id!=null) {
						   Spinner.show();
						   var modal = $('#floorPanel_modal');
						   
						   modal.meshop({
							   
							   url : "/json/blokplan/load",
							   params : {
							   id : id,
						   },
							   loop : loopModal,
						   }).on("render", function(obj, json) {
							   modal.modal('show');
							   Spinner.hide();
							   
						   })
					   }
				   }
				    
				};
			 
		 });
		 }
	 function getPie(date){
		 $.get("/json/chart/accountpie", $.param({
			 'date' : date }), function(json) {
			 ids = JSON.parse(json[2]);
			 counts = JSON.parse(json[1]);
			 parsedTest = JSON.stringify(json[0]);
			 pars = JSON.parse(parsedTest);
			 pars = pars.replace('[','');
			 pars = pars.replace(']','');
			 bloks =  pars.split(',');
			 Spinner.hide();
			 
			 var saleCount = document.getElementById('sale').getContext('2d');
			 if( window.chart4){
				 window.chart4.destroy();
			 }
			 window.chart4 = new Chart(saleCount, {
				 // The type of chart we want to create
				 type: 'pie',
				 data: {
				 labels: bloks,
				 datasets: [{
					 
					 data: counts
				 },
				            
				            ]
			 },
				 
			 });
			 
			 document.getElementById('pie').onclick = function(evt) {
				 var activePoint = window.chart4.getElementAtEvent(evt)[0];
				 
				 if(activePoint) {
					 
					 var index =  activePoint._index;
					 var id = ids[index];
					 
					 if(id!=null) {
						 Spinner.show();
						 var modal = $('#floorPanel_modal');
						 
						 modal.meshop({
							 
							 url : "/json/blokplan/load",
							 params : {
							 id : id,
						 },
							 loop : loopModal,
						 }).on("render", function(obj, json) {
							 modal.modal('show');
							 Spinner.hide();
							 
						 })
					 }
				 }
				 
			 };
			 
		 });
	 }
		 
		function loopModal(obj, json, mainobj) {
			
			 
			
			
			obj.find('td.tdclass').attr("style","background-color:red!important");
			obj.find('td.reserveclass').attr("style","background-color:#ff9900!important");

			obj.find('td.section').click(function(e) {
				e.stopPropagation();
				
				var id = $(this).attr('id');
				
				
				
				
				if ($(this).hasClass('tdclass')){
					
					var td = $(this);
					
					td.addClass("loading");
					td.addClass("disabled");
					
					var modal = $('#contract_modal');
					modal.meshop({

						url : "/json/contract/load",
						params : {
							id : id,
						plan: true,
						},
					}).on("render", function(obj, json) {
						modal.modal('show');
						td.removeClass("loading");
						td.removeClass("disabled");
						
						
					})
					
					 
				} else {
					
					var win = window.open("/portfoy_manage?listId="+id);
					if (win) {
						win.focus()
					} else {
						alert('Please allow popups for this website');
					}
					
					 
				}
				
				
			});
			  

		}
	
	
	 function getInstallmentCont(daterange){
		 
		 $.get("/adminpanel/json/chart/installmentcount", $.param({
			 'daterange' : daterange }), function(json) {
			 parsetIns = JSON.parse(json[0]);
			 data = JSON.parse(json[1]);
			 paid = JSON.parse(json[2]);
			 Notpaid = JSON.parse(json[3]);
			 partial = JSON.parse(json[4]);
			 years = JSON.parse(json[5]);
			 var lastFour = daterange.substr(daterange.length - 4);
			 Spinner.hide();
			 var InstallmentCount = document.getElementById('InstallmentCountChart').getContext('2d');
			 if( window.chart){
				 window.chart.destroy();
			 }
			 window.chart = new Chart(InstallmentCount, {
				 // The type of chart we want to create
				 type: 'line',
				 
				 
				 // The data for our dataset
				 data: {
				 labels: parsetIns,
				 datasets: [{
					 label: tchart1,
					 minHeight: 25,
					 borderColor: '#4290d7',
					 data: data
				 } ,
				            
				            {
					 label: tchart2,
					 borderColor: '#429055',
					 minHeight: 25,
					 data: paid
				            },
				            
				            {
								 label: tchart8,
								 borderColor: '#f97277',
								 minHeight: 25,
								 hidden: true,
								 data: Notpaid
							            },
				            {
				            	label: tchart9,
				            	borderColor: '#997277',
				            	minHeight: 25,
				            	 hidden: true,
				            	data: partial
				            }
				 ]
			 },
				 
				 // Configuration options go here
				 options: {}
			 
			 
			 });
			 
			 
			 document.getElementById('InstallmentCountChart').onclick = function(evt) {
				   var activePoint = window.chart.getElementAtEvent(evt)[0];
				   var index =  activePoint._index;
				   var mount = parsetIns[index];
				   var year = years[index];
				   
				   daterange = getDateRange(mount,year);
				   
				   var win = window.open('/installment_manage?daterange='+daterange, '_blank');
				   if (win) {
				       //Browser has allowed it to be opened
				       win.focus();
				   } else {
				       //Browser has blocked it
				       alert('Please allow popups for this website');
				   }
				    
				};
			 
		 });
	 };
	

	 
	 function getInstallmentAmount(daterange){
		 
		 $.get("/adminpanel/json/chart/installmentamount", $.param({
			 'daterange' : daterange }), function(json) {
			 tr = JSON.parse(json[1]);
			 dolar = JSON.parse(json[2]);
			 euro = JSON.parse(json[3]);
			 
			 date = JSON.parse(json[0]);
			 Spinner.hide();
		 var InstallmentPrice = document.getElementById('InstallmentCountPrice').getContext('2d');
		 
		 if( window.chart2){
			 window.chart2.destroy();
		 }
		 
		 window.chart2 = new Chart(InstallmentPrice, {
			 // The type of chart we want to create
			 type: 'bar',
			 
			 // The data for our dataset
			 data: {
			 labels: date,
			 datasets: [{
				 label: tchart4,
				 backgroundColor: '#073c66',
				 borderColor: '#073c66',
				 data: tr
			 },
			            
			            {
				 label: tchart5,
				 backgroundColor: '#429055',
				 borderColor: '#429055',
				 data: euro
			            },
			            
			            {
							 label: tchart6,
							 backgroundColor: '#53c0d6',
							 borderColor: '#53c0d6',
							 data: dolar
						            }
			 
			 ]
		 },
			 
			 // Configuration options go here
			 options: {}
		 });
		 });
	 };
	 

	 function getInstallmentSale(daterange){
	 $.get("/adminpanel/json/chart/contractcount", $.param({
		 'daterange' : daterange }), function(json) {
		 parsedCont = JSON.parse(json[0]);
		 data = JSON.parse(json[1]);
		 cyears = JSON.parse(json[2]);
		 
		 
		 var InstallmentCount = document.getElementById('ContractCountChart').getContext('2d');
		 if( window.chart3){
			 window.chart3.destroy();
		 }
		 window.chart3 = new Chart(ContractCountChart, {
			 // The type of chart we want to create
			 type: 'line',
			 
			 
			 // The data for our dataset
			 data: {
			 labels: parsedCont,
			 datasets: [{
				 label: tchart3,
//			backgroundColor: '#4290d7',
				 minHeight: 25,
				 
				 borderColor: '#4290d7',
				 data: data
			 }  
			 
			 ]
		 },
			 
			 // Configuration options go here
			 options: {}
		 });
		 
		 
		 document.getElementById('ContractCountChart').onclick = function(evt) {
			   var activePoint = window.chart3.getElementAtEvent(evt)[0];
			   var index =  activePoint._index;
			   var mount = parsedCont[index];
			   var lastFour = cyears[index];
			   
			   daterange = getDateRange(mount,lastFour);
			   
			   var win = window.open('/contracts?daterange='+daterange, '_blank');
			   if (win) {
			       //Browser has allowed it to be opened
			       win.focus();
			   } else {
			       //Browser has blocked it
			       alert('Please allow popups for this website');
			   }
			    
			};
		 
	 });
	 }
	 
	 function getDateRange(mount,lastFour) {
		 var daterange;
		 
		   
		   switch(mount) {
		   case 1:
		     // code block
			   daterange =  "01/01/"+lastFour+"-01/31/"+lastFour;
		     break;
		   case 2:
			   daterange =  "02/01/"+lastFour+"-02/28/"+lastFour;
		     // code block
		     break;
		   case 3:
			   daterange =  "03/01/"+lastFour+"-03/31/"+lastFour;
			   // code block
			   break;
		   case 4:
			   daterange =  "04/01/"+lastFour+"-04/30/"+lastFour;
			   // code block
			   break;
		   case 5:
			   daterange =  "05/01/"+lastFour+"-05/31/"+lastFour;
			   // code block
			   break;
		   case 6:
			   daterange =  "06/01/"+lastFour+"-06/31/"+lastFour;
			   // code block
			   break;
		   case 7:
			   daterange =  "07/01/"+lastFour+"-07/31/"+lastFour;
			   // code block
			   break;
		   case 8:
			   daterange =  "08/01/"+lastFour+"-08/31/"+lastFour;
			   // code block
			   break;
		   case 9:
			   daterange =  "09/01/"+lastFour+"-09/30/"+lastFour;
			   // code block
			   break;
		   case 10:
			   // code block
			   daterange =  "10/01/"+lastFour+"-10/31/"+lastFour;
			   break;
		   case 11:
			   // code block
			   daterange =  "11/01/"+lastFour+"-11/30/"+lastFour;
			   break;
		   case 12:
			   daterange =  "12/01/"+lastFour+"-12/31/"+lastFour;
			   // code block
			   break;
		   default:
		     // code block
		 }
		 
		 return daterange;
	 }
	 

});